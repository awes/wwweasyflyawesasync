﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;

namespace Utilidades.Models
{
    [Serializable()]
    public class mdlCompra
    {
        public mdlCompra()
        {

        }
        #region MODELO PARA CONSULTA DE ITINERARIO
        #region MIEMBROS
        public const int INT_TIPO_VUELO_IDA = 0;
        public const int INT_TIPO_VUELO_IDA_Y_REGRESO = 1;
        int _adulto = 0;
        int _niño = 0;
        int _infante = 0;
        string _strIATAOrigen;
        DateTime? _FechaSalida;
        DateTime? _FechaRegreso;
        DataTable _dtAvail;
        DataTable _dtAvailReturn;
        #endregion

        #region PROPIEDADES
        //************************************************************************************************************************
        // INFORMACION PARA CONSULTA DE ITINERARIO
        //************************************************************************************************************************
        [DisplayName("NombreCache")]
        public string NombreCache
        {
            get
            {
                if (_FechaSalida.HasValue)
                    return IATAOrigen + IATADestino + _FechaSalida.Value.ToShortDateString() + _FechaRegreso.Value.ToShortDateString();
                else
                    return "";
            }
        }

        [DisplayName("Fecha Salida")]
        public DateTime? FechaSalida
        {
            get
            {
                return _FechaSalida;
            }
            set
            {
                _FechaSalida = value;
            }
        }

        [DisplayName("Fecha Regreso")]
        public DateTime? FechaRegreso
        {
            get
            {
                return _FechaRegreso;
            }
            set
            {
                _FechaRegreso = value;
            }
        }
        [DisplayName("Disponibilidad")]
        public DataTable dtAvail
        {
            get
            {
                return _dtAvail;
            }
            set
            {
                _dtAvail = value;
            }
        }
        [DisplayName("Disponibilidad")]
        public DataTable dtAvailReturn
        {
            get
            {
                return _dtAvailReturn;
            }
            set
            {
                _dtAvailReturn = value;
            }
        }
        [DisplayName("Ciudad Origen")]
        public string IATAOrigen
        {
            get
            {
                return _strIATAOrigen;
            }
            set
            {
                _strIATAOrigen = value;
            }
        }

        [DisplayName("Ciudad Destino")]
        public string IATADestino { get; set; }

        [DisplayName("ID Origen")]
        public int IDOrigen { get; set; }

        [DisplayName("ID Destino")]
        public int IDDestino { get; set; }

        [DisplayName("Aeropuerto Origen")]
        public string ATOOrigen { get; set; }

        [DisplayName("Aeropuerto Destino")]
        public string ATODestino { get; set; }

        [DisplayName("Adultos")]
        public int Adultos
        {
            get
            {
                return _adulto;
            }
            set
            {
                _adulto = value;
            }
        }

        [DisplayName("Niños")]
        public int Niños
        {
            get
            {
                return _niño;
            }
            set
            {
                _niño = value;
            }
        }

        [DisplayName("Infantes")]
        public int Infantes
        {
            get
            {
                return _infante;
            }
            set
            {
                _infante = value;
            }
        }

        [DisplayName("Tipo Vuelo")]
        public int TipoVuelo { get; set; }

        //************************************************************************************************************************
        // FIN INFORMACION PARA CONSULTA DE ITINERARIO
        //************************************************************************************************************************
        #endregion
        #endregion

        #region MODELO DE REGISTRO DE PAX
        #region MIEMBROS
        public List<mdlPax> _paxAdultos = new List<mdlPax>();
        public List<mdlPax> _paxNiños = new List<mdlPax>();
        public List<mdlPax> _paxInfantes = new List<mdlPax>();
        #endregion

        #region PROPIEDADES
        public List<mdlPax> paxAdultos
        {
            get
            {
                return _paxAdultos;
            }
            set
            {
                _paxAdultos = value;
            }
        }
        public List<mdlPax> paxNiños
        {
            get
            {
                return _paxNiños;
            }
            set
            {
                _paxNiños = value;
            }
        }
        public List<mdlPax> paxInfantes
        {
            get
            {
                return _paxInfantes;
            }
            set
            {
                _paxInfantes = value;
            }
        }
        #endregion
        #endregion

        #region MODELO DE REGISTRO DE COMPRADOR
        #region MIEMBROS
        public mdlComprador _comprador = new mdlComprador();
        #endregion

        #region PROPIEDADES
        public mdlComprador Comprador
        {
            get
            {
                return _comprador;
            }
            set
            {
                _comprador = value;
            }
        }
        public bool almacenarCookie { get; set; }
        #endregion
        #endregion

        #region MODELO DE SELECCION DE DISPONIBILIDAD
        #region MIEMBROS
        // VERSION DE KIU
        //List<Flight> _VuelosKiuIda;
        //List<Flight> _VuelosKiuRegreso;
        #endregion

        #region PROPIEDADES
        
        public string idVueloSeleccionadoIda { get; set; }
        public string ClaseIda { get; set; }
        public string fechaSalida { get; set; }
        public string fechaLlegada { get; set; }

        public string idVueloSeleccionadoRegreso { get; set; }
        public string ClaseRegreso { get; set; }
        public string fechaRegresoSalida { get; set; }
        public string fechaRegresoLlegada { get; set; }

        #endregion
        #endregion

        #region MODELO DE ESTADO INTERACCION KIU (ERROR Y MENSAJES)
        public bool modeloKiuOK { get; set; }
        public string MensajeError { get; set; }
        public string MensajeErrorToolTip { get; set; }
        #endregion

        #region METODOS VALIDEZ MODELO
        /// <summary>
        /// Verifica la informacion completa para hacer consulta de itinerario
        /// </summary>
        public bool isValidConsultaItinerario
        {
            get
            {
                MensajeError = "";
                MensajeErrorToolTip = "";

                bool blnIsValid = true;

                DateTime dteFechaReferencia = new DateTime(DateTime.Now.AddDays(-1).Year,
                                                           DateTime.Now.AddDays(-1).Month,
                                                           DateTime.Now.AddDays(-1).Day,
                                                           23, 59, 59);

                // AUTOCORRIGE LA FECHA DE REGRESO SI NO ESTA BIEN
                if ((!_FechaSalida.HasValue) || (_FechaSalida < dteFechaReferencia))
                    _FechaSalida = new DateTime(DateTime.Now.Year,
                                                DateTime.Now.Month,
                                                DateTime.Now.Day,
                                                0, 0, 0);

                // AUTOCORRIGE LA FECHA DE REGRESO SI NO ESTA BIEN
                if ((!_FechaRegreso.HasValue) || (_FechaRegreso < _FechaSalida))
                    _FechaRegreso = _FechaSalida;

                if (TipoVuelo == INT_TIPO_VUELO_IDA_Y_REGRESO)
                    blnIsValid &= (_FechaRegreso >= _FechaSalida);

                if ((IATAOrigen == null) || (IATAOrigen.Trim() == ""))
                {
                    MensajeError = " Origen";
                    MensajeErrorToolTip = MensajeError;
                    blnIsValid = false;
                }

                if ((IATADestino == null) || (IATADestino.Trim() == ""))
                {
                    MensajeError += " Destino";
                    MensajeErrorToolTip = MensajeError;
                    blnIsValid = false;
                }


                if (blnIsValid)
                {
                    if (Adultos < 1)
                    {
                        MensajeError = "Debe haber por lo menos un adulto";
                        MensajeErrorToolTip = "Cant ADT = " + Adultos.ToString();
                        return false;
                    }

                    if (Adultos + Niños + Infantes > 6)
                    {
                        MensajeError = "Máximo seis pasajeros";
                        MensajeErrorToolTip = "Cant ADT = " + Adultos.ToString();
                        return false;
                    }

                    if (Infantes > Adultos)
                    {
                        MensajeError = "Máximo un infante por Adulto";
                        MensajeErrorToolTip = "Cant ADT = " + Adultos.ToString();
                        return false;
                    }

                    if (Adultos + Niños + Infantes == 0)
                    {
                        MensajeError = "Mínimo un pasajero";
                        MensajeErrorToolTip = "Cant ADT = " + Adultos.ToString();
                        return false;
                    }
                }
                //// SI ES VALIDO INCIALIZA LAS LISTA DE PAX
                //http://www.dotnetperls.com/initialize-array

                if (MensajeError != "")
                    MensajeError = "Seleccione " + MensajeError;

                MensajeErrorToolTip = MensajeError;
                return blnIsValid;
            }
        }

        
        /// <summary>
        /// Verifica la informacion completa para continuar con proceso despues de seleccion de itinerario
        /// </summary>
        public bool isValidSeleccionItinerario
        {
            get
            {
                StringBuilder strMensaje = new StringBuilder();

                // SOLO ES VALIDO SI LOS PREVIOS SON VALIDOS
                //bool blnIsValid = isValidConsultaItinerario;
                bool blnIsValid = true;

                bool blnIsValidVueloIda = true;
                bool blnIsValidVueloRegreso = true;

                blnIsValidVueloIda = ((idVueloSeleccionadoIda != null) && (idVueloSeleccionadoIda != ""));
                blnIsValid &= blnIsValidVueloIda;

                if (!blnIsValidVueloIda)
                    strMensaje.Append("Seleccione vuelo de ida");

                blnIsValid &= (ClaseIda != null) && (ClaseIda.Trim() != "");

                if (TipoVuelo == INT_TIPO_VUELO_IDA_Y_REGRESO)
                {
                    blnIsValidVueloRegreso = ((idVueloSeleccionadoRegreso != null) && (idVueloSeleccionadoRegreso != ""));
                    blnIsValid &= blnIsValidVueloRegreso;

                    if (!blnIsValidVueloRegreso)
                    {
                        if (strMensaje.Length > 0)
                            strMensaje.Append(" y ");

                        strMensaje.Append("Seleccione vuelo de regreso");
                    }

                    blnIsValid &= (ClaseRegreso != null) && (ClaseRegreso.Trim() != "");

                }


                MensajeError = strMensaje.ToString();
                MensajeErrorToolTip = MensajeError;

                return blnIsValid;
            }
        }
        /// <summary>
        /// Verifica la informacion de pax completa
        /// </summary>
        /// <returns></returns>
        public bool isValidRegistroPax
        {
            get
            {
                // SOLO ES VALIDO SI LOS PREVIOS SON VALIDOS
                //bool blnIsValid = isValidSeleccionItinerario;
                bool blnIsValid = true;
                StringBuilder strMensaje = new StringBuilder();

                // LISTAS CREADAS
                //blnIsValid = ((paxAdultos != null) & (paxNiños != null) & (paxInfantes != null));

                if (paxAdultos != null)
                    foreach (mdlPax oPax in paxAdultos)
                    {
                        if (oPax == null) { blnIsValid = false; strMensaje.Append(" ADT == null"); }
                        if (!oPax.isValid) { blnIsValid = false; strMensaje.Append(" ADT == invalid"); }
                    }

                // SOLO VERIFICA SI LOS ADULTOS ESTAN OK
                if (paxNiños != null)
                    foreach (mdlPax oPax in paxNiños)
                    {
                        if (oPax == null) { blnIsValid = false; strMensaje.Append(" CHD == null"); }
                        if (!oPax.isValid) { blnIsValid = false; strMensaje.Append(" CHD == invalid"); }
                    }

                // SOLO VERIFICA SI LOS NIÑOS Y ADULTOS ESTAN OK
                if (paxInfantes != null)
                    foreach (mdlPax oPax in paxInfantes)
                    {
                        if (oPax == null) { blnIsValid = false; strMensaje.Append(" INF == null"); }
                        if (!oPax.isValid) { blnIsValid = false; strMensaje.Append(" INF == invalid"); }
                    }

                if (!blnIsValid)
                {
                    MensajeError = "Por favor registre todos los pasajeros";
                    MensajeErrorToolTip = strMensaje.ToString();
                }
                return blnIsValid;
            }
        }
        /// <summary>
        /// Verifica la informacion de comprador completa
        /// </summary>
        /// <returns></returns>
        public bool isValidComprador
        {
            get
            {
                // SOLO ES VALIDO SI LOS PREVIOS SON VALIDOS
                //bool blnIsValid = isValidRegistroPax;
                bool blnIsValid = true;

                if (blnIsValid)
                {
                    if (Comprador == null) return false;
                    if (!Comprador.isValid) return false;
                }

                return blnIsValid;
            }
        }
        ///// <summary>
        ///// Verifica si ya se selecciono la forma d pago
        ///// </summary>
        //public bool isValidSeleccionFormaPago
        //{
        //    get
        //    {
        //        // SOLO ES VALIDO SI LOS PREVIOS SON VALIDOS
        //        //bool blnIsValid = isValidComprador;
        //        bool blnIsValid = true;

        //        if (blnIsValid)
        //        {
        //            if (FormaPago == null) return false;
        //            if (FormaPago.Trim() == "") return false;
        //        }

        //        return blnIsValid;
        //    }
        //}
        ///// <summary>
        ///// Indica si se puede enviar a pago
        ///// </summary>
        //public bool isValidPago
        //{
        //    get
        //    {
        //        // SOLO ES VALIDO SI LOS PREVIOS SON VALIDOS
        //        //bool blnIsValid = isValidSeleccionFormaPago;
        //        bool blnIsValid = true;

        //        if (blnIsValid)
        //        {
        //            if (CodigoReserva == null) return false;
        //            if (CodigoReserva.Trim() == "") return false;

        //            if (CodigoReservaKIU == null) return false;
        //            if (CodigoReservaKIU.Trim() == "") return false;
        //        }

        //        return blnIsValid;
        //    }
        //}
        


        #endregion

        #region MODELO DE TARIFA SELECCIONADA
        #region MIEMBROS

        #endregion

        #region PROPIEDADES
        public CTarifaAcumulada TarifaTotal { get; set; }
        public CTarifaAcumulada TarifaKIUTotal { get; set; }
        #endregion
        #endregion

        #region METODOS INTERNOS DEL MODELO
        /// <summary>
        /// Calcula la tarifa para un tipo de pax y acumula en el total
        /// </summary>
        /// <param name="tipoPax"></param>
        /// <param name="intCantidad"></param>
        /// <returns></returns>
        public void calculaTarifaTipoPax(CConstantes.enuTipoPax tipoPax, int intCantidad, string strPrice, string strFuel, string strTax, string strPriceR, string strFuelR, string strTaxR, string strJourney, int discount, double dblFee)
        {
            //double dblTarifa = -1;
            //double dblCombustible = -1;
            //double dblTasaAeroportuaria = -1;

            CTarifa TarifaIda = new CTarifa();
            CTarifa TarifaRegreso = new CTarifa();

            // SUMA CARGO ADMINISTRATIVO
            TarifaTotal.mdblCargoAdministrativo += intCantidad * dblFee;//double.Parse(configurationReader.read("feeAdministrativo", "MVCFuncional"));

            // TABLA DE TARIFAS
            //DataTable dttTarifas = dtAvail[0];
            /**********************************************************************************************
            *  TARIFA DE IDA
            * **********************************************************************************************/
            // OBTIENE LA TARIFA DE LA CLASE
            //DataRow[] oTarifa = dttTarifas.Select("Class = '" + ClaseIda + "' and FlightNumber ='" + idVueloSeleccionadoIda + "'");

            // DISCRIMINACION DE LA TARIFA
            TarifaIda.strClase = ClaseIda;
            TarifaIda.tipoPax = tipoPax;

            // OBTIENE LA TARIFA DEL VUELO
            //if (double.TryParse(oTarifa[0]["Rate"].ToString(), out dblTarifa))
                // ASOCIA LA TARIFA NETA ONE WAY A LA CLASE
                TarifaIda.mdblValor = Convert.ToDouble(strPrice);

            // COMBUSTIBLE
            //if (double.TryParse(oTarifa[0]["Fuel"].ToString(), out dblCombustible))
                TarifaIda.mdblCombustible = Convert.ToDouble(strFuel);

            //if (double.TryParse(CCORE_CPanel.configuracionString("COMBUSTIBLE_OW"), out dblCombustible))
            //    TarifaIda.mdblCombustible = dblCombustible;

            // TASA AEROPORTUARIA
            //if (double.TryParse(oTarifa[0]["Tax"].ToString(), out dblTasaAeroportuaria))
                TarifaIda.mdblTasa = Convert.ToDouble(strTax);
            
            // ACUMULA TARIFA CONSOLIDADA
            TarifaTotal.mdblValor += TarifaIda.valorFactor() * intCantidad;
            TarifaTotal.mdblCombustible += TarifaIda.mdblCombustible * intCantidad;
            TarifaTotal.mdblTasa += TarifaIda.mdblTasa * intCantidad;

            /**********************************************************************************************
            *  TARIFA DE RETORNO
            * **********************************************************************************************/
            if (TipoVuelo == INT_TIPO_VUELO_IDA_Y_REGRESO)
            {
                if (strJourney == "2")
                {
                    TarifaRegreso.strClase = ClaseRegreso;
                    TarifaRegreso.tipoPax = tipoPax;

                    // OBTIENE LA TARIFA DE LA CLASE
                    //oTarifa = dttTarifas.Select("Class = '" + ClaseRegreso + "' and FlightNumber ='" + idVueloSeleccionadoRegreso + "'");

                    // OBTIENE LA TARIFA DEL VUELO
                    //if (double.TryParse(oTarifa[0]["Rate"].ToString(), out dblTarifa))
                    // ASOCIA LA TARIFA NETA ONE WAY A LA CLASE
                    TarifaRegreso.mdblValor = Convert.ToDouble(strPriceR);

                    // COMBUSTIBLE
                    //if (double.TryParse(oTarifa[0]["Fuel"].ToString(), out dblCombustible))
                    TarifaRegreso.mdblCombustible = Convert.ToDouble(strFuelR);

                    //if (double.TryParse(CCORE_CPanel.configuracionString("COMBUSTIBLE_OW"), out dblCombustible))
                    //    TarifaRegreso.mdblCombustible = dblCombustible;

                    // TASA AEROPORTUARIA
                    //if (double.TryParse(oTarifa[0]["Tax"].ToString(), out dblTasaAeroportuaria))
                    TarifaRegreso.mdblTasa = Convert.ToDouble(strTaxR);

                    // ACUMULA TARIFA CONSOLIDADA
                    TarifaTotal.mdblValor += TarifaRegreso.valorFactor() * intCantidad;
                    TarifaTotal.mdblCombustible += TarifaRegreso.mdblCombustible * intCantidad;
                    TarifaTotal.mdblTasa += TarifaRegreso.mdblTasa * intCantidad;
                }       
            }
            if (discount != 0)
            {
                TarifaTotal.mboolAplicaPorcentajeDescuento = true;
                TarifaTotal.mIntPorcentajeDescuento = discount;
            }
        }

        /// <summary>
        /// Actualiza los calculos de la tarifa de ida y, si aplica, de regreso 
        /// </summary>
        public void calculaTarifa(string strPrice, string strFuel, string strTax, string strPriceR, string strFuelR, string strTaxR, string strJourney, string discount, string strFee)
        {
            //if (isValidSeleccionItinerario)
            //{
                // TARIFA CONSOLIDADA
                TarifaTotal = new CTarifaAcumulada();

                TarifaTotal.intTotalPax = Adultos + Niños;

                // TARIFAS DE ADULTOS
                calculaTarifaTipoPax(CConstantes.enuTipoPax.Adulto, Adultos, strPrice, strFuel, strTax, strPriceR, strFuelR, strTaxR, strJourney, Convert.ToInt32(discount), Convert.ToDouble(strFee));

                if (Niños > 0)
                {
                    // TARIFAS DE NIÑOS
                    calculaTarifaTipoPax(CConstantes.enuTipoPax.Niño, Niños, strPrice, strFuel, strTax, strPriceR, strFuelR, strTaxR, strJourney, Convert.ToInt32(discount), Convert.ToDouble(strFee));
                }

                // CALCULA EL DESCUENTO Y LO RPESENTA VIA JQUERY
                //calculaDescuento();
            //}
        }
        ///// <summary>
        ///// Actualiza los calculos de la descuentos sobre la tarifa calculada
        ///// </summary>
        //public void calculaDescuento()
        //{
        //    double mdblValorDescontado = 0;
        //    double mdblTotal = 0;

        //    if (InformacionPromociones.CodigoPromocionalValido)
        //        if (InformacionPromociones.aplicaPorcentaje)
        //        {
        //            mdblValorDescontado = (double)InformacionPromociones.PorcentajeAplicable;
        //            mdblTotal = (double)(TarifaTotal.mdblTotal * (100 - InformacionPromociones.PorcentajeAplicable) / 100);
        //        }
        //        else
        //        {
        //            mdblValorDescontado = (double)InformacionPromociones.ValorAplicableDescuento;
        //            mdblTotal = (double)(TarifaTotal.mdblTotal - InformacionPromociones.ValorAplicableDescuento);
        //        }

        //    // ASIGNA LOS VALORES A LA TARIFA. EL OBJETO CALCULA LOS VALORES DE DESCUENTO  Y LOS TOTALES
        //    TarifaTotal.mdblValorDescuento = InformacionPromociones.ValorAplicableDescuento;
        //    TarifaTotal.mIntPorcentajeDescuento = InformacionPromociones.PorcentajeAplicable;
        //    TarifaTotal.mboolAplicaPorcentajeDescuento = InformacionPromociones.aplicaPorcentaje;
        //}
        /// <summary>
        /// Metodo auxiliar para procesar el form de pax y extraer los datos de las colecciones internas.
        /// El binder de razor no devuelve las colecciones ya inicializadas
        /// </summary>
        /// <param name="Formulario"></param>
        /// <param name="strCadenaID"></param>
        /// <returns></returns>
        public void procesaForm(FormCollection Formulario, string tipo)
        {
            string nombre = "";
            string apellido = "";
            string numDocumento = "";
            char[] caracteres = { ' ' };
            mdlPax[] lstPaxADT = new mdlPax[Adultos];
           

            // INICIALIZA
            for (int intContador = 0; intContador < lstPaxADT.Length; intContador++)
                if (lstPaxADT[intContador] == null) lstPaxADT[intContador] = new mdlPax("A");

            mdlPax[] lstPaxCHD = new mdlPax[Niños];

            // INICIALIZA
            for (int intContador = 0; intContador < lstPaxCHD.Length; intContador++)
                if (lstPaxCHD[intContador] == null) lstPaxCHD[intContador] = new mdlPax("C");

            mdlPax[] lstPaxINF = new mdlPax[Infantes];

            // INICIALIZA
            for (int intContador = 0; intContador < lstPaxINF.Length; intContador++)
                if (lstPaxINF[intContador] == null) lstPaxINF[intContador] = new mdlPax("I");

            //paxAdultos[0].firstname
            Regex oRegAdultos = new Regex(@"^_paxAdultos\[(?<numero>[0-9])*\].(?<propiedad>[a-z0-9A-Z]*)$");
            Regex oRegNiños = new Regex(@"^_paxNiños\[(?<numero>[0-9])*\].(?<propiedad>[a-z0-9A-Z]*)$");
            Regex oRegInfantes = new Regex(@"^_paxInfantes\[(?<numero>[0-9])*\].(?<propiedad>[a-z0-9A-Z]*)$");

            // POR CADA ENTRADA EN EL FORM
            foreach (string strEntrada in Formulario.AllKeys)
            {
                int intCodigo = 0;
                string strPropiedad = "";

                // OBTIENE LAS COINCIDENCIAS PARA CADA TIPO
                MatchCollection oMatchesADT = oRegAdultos.Matches(strEntrada);
                MatchCollection oMatchesCHD = oRegNiños.Matches(strEntrada);
                MatchCollection oMatchesINF = oRegInfantes.Matches(strEntrada);

                if (oMatchesADT.Count > 0)
                {
                    // OBTIENE PARTES DE LA CADENA
                    intCodigo = int.Parse(oMatchesADT[0].Groups["numero"].ToString());
                    strPropiedad = oMatchesADT[0].Groups["propiedad"].ToString();

                    //SUBINDICE FUERA DEL INTERVALO
                    if (intCodigo > Adultos - 1)
                        continue;
                    if (strPropiedad == "Nombre")
                    {
                        nombre = Formulario[strEntrada].Replace(".", "");
                        nombre = nombre.Replace("ñ", "n");
                        nombre = nombre.Replace("Ñ", "N");
                        nombre = nombre.Replace("á", "a");
                        nombre = nombre.Replace("é", "e");
                        nombre = nombre.Replace("í", "i");
                        nombre = nombre.Replace("ó", "o");
                        nombre = nombre.Replace("ú", "u");
                        nombre = nombre.Replace("Á", "A");
                        nombre = nombre.Replace("É", "E");
                        nombre = nombre.Replace("Í", "I");
                        nombre = nombre.Replace("Ó", "O");
                        nombre = nombre.Replace("Ú", "U");
                        nombre = nombre.Replace(",", "");
                        nombre = nombre.TrimEnd(caracteres);
                        if (strPropiedad == "Nombre") lstPaxADT[intCodigo].Nombre = nombre == "Nombre" ? null : nombre;
                    }
                    if (strPropiedad == "Apellido")
                    {
                        apellido = Formulario[strEntrada].Replace(".", "");
                        apellido = apellido.Replace("ñ", "n");
                        apellido = apellido.Replace("Ñ", "N");
                        apellido = apellido.Replace("á", "a");
                        apellido = apellido.Replace("é", "e");
                        apellido = apellido.Replace("í", "i");
                        apellido = apellido.Replace("ó", "o");
                        apellido = apellido.Replace("ú", "u");
                        apellido = apellido.Replace("Á", "A");
                        apellido = apellido.Replace("É", "E");
                        apellido = apellido.Replace("Í", "I");
                        apellido = apellido.Replace("Ó", "O");
                        apellido = apellido.Replace("Ú", "U");
                        apellido = apellido.Replace(",", "");
                        apellido = apellido.TrimEnd(caracteres);
                        if (strPropiedad == "Apellido") lstPaxADT[intCodigo].Apellido = apellido == "Apellido" ? null : apellido;
                    }
                    // CARGA SUS DATOS 
                    if (strPropiedad == "TipoDocumento") lstPaxADT[intCodigo].TipoDocumento = Formulario[strEntrada] == "TipoDocumento" ? null : Formulario[strEntrada];
                    if (strPropiedad == "NumeroDocumento")
                    {
                        numDocumento = Formulario[strEntrada].Replace(".", "");
                        numDocumento = numDocumento.Replace(",", "");
                        numDocumento = numDocumento.TrimEnd(caracteres);
                        if (strPropiedad == "NumeroDocumento") lstPaxADT[intCodigo].NumeroDocumento = numDocumento == "No. de documento" ? null : numDocumento;
                    }

                }
                if (oMatchesCHD.Count > 0)
                {
                    // OBTIENE PARTES DE LA CADENA
                    intCodigo = int.Parse(oMatchesCHD[0].Groups["numero"].ToString());
                    strPropiedad = oMatchesCHD[0].Groups["propiedad"].ToString();

                    //SUBINDICE FUERA DEL INTERVALO
                    if (intCodigo > Niños - 1)
                        continue;

                    if (strPropiedad == "Nombre")
                    {
                        nombre = Formulario[strEntrada].Replace(".", "");
                        nombre = nombre.Replace("ñ", "n");
                        nombre = nombre.Replace("Ñ", "N");
                        nombre = nombre.Replace("á", "a");
                        nombre = nombre.Replace("é", "e");
                        nombre = nombre.Replace("í", "i");
                        nombre = nombre.Replace("ó", "o");
                        nombre = nombre.Replace("ú", "u");
                        nombre = nombre.Replace("Á", "A");
                        nombre = nombre.Replace("É", "E");
                        nombre = nombre.Replace("Í", "I");
                        nombre = nombre.Replace("Ó", "O");
                        nombre = nombre.Replace("Ú", "U");
                        nombre = nombre.Replace(",", "");
                        nombre = nombre.TrimEnd(caracteres);
                        if (strPropiedad == "Nombre") lstPaxCHD[intCodigo].Nombre = Formulario[strEntrada] == "Nombre" ? null : Formulario[strEntrada];
                    }

                    if (strPropiedad == "Apellido")
                    {
                        apellido = Formulario[strEntrada].Replace(".", "");
                        apellido = apellido.Replace("ñ", "n");
                        apellido = apellido.Replace("Ñ", "N");
                        apellido = apellido.Replace("á", "a");
                        apellido = apellido.Replace("é", "e");
                        apellido = apellido.Replace("í", "i");
                        apellido = apellido.Replace("ó", "o");
                        apellido = apellido.Replace("ú", "u");
                        apellido = apellido.Replace("Á", "A");
                        apellido = apellido.Replace("É", "E");
                        apellido = apellido.Replace("Í", "I");
                        apellido = apellido.Replace("Ó", "O");
                        apellido = apellido.Replace("Ú", "U");
                        apellido = apellido.Replace(",", "");
                        apellido = apellido.TrimEnd(caracteres);
                        if (strPropiedad == "Apellido") lstPaxCHD[intCodigo].Apellido = Formulario[strEntrada] == "Apellido" ? null : Formulario[strEntrada];
                    }
                    // CARGA SUS DATOS

                    if (strPropiedad == "TipoDocumento") lstPaxCHD[intCodigo].TipoDocumento = Formulario[strEntrada] == "TipoDocumento" ? null : Formulario[strEntrada];
                    if (strPropiedad == "NumeroDocumento")
                    {
                        numDocumento = Formulario[strEntrada].Replace(".", "");
                        numDocumento = numDocumento.Replace(",", "");
                        numDocumento = numDocumento.TrimEnd(caracteres);
                        if (strPropiedad == "NumeroDocumento") lstPaxCHD[intCodigo].NumeroDocumento = Formulario[strEntrada] == "No. de documento" ? null : Formulario[strEntrada];
                    }

                }
                if (oMatchesINF.Count > 0)
                {
                    // OBTIENE PARTES DE LA CADENA
                    intCodigo = int.Parse(oMatchesINF[0].Groups["numero"].ToString());
                    strPropiedad = oMatchesINF[0].Groups["propiedad"].ToString();

                    //SUBINDICE FUERA DEL INTERVALO
                    if (intCodigo > Infantes - 1)
                        continue;

                    // OBTIENE REFERENCIA PARA CARGAR SUS DATOS

                    if (strPropiedad == "Nombre")
                    {
                        nombre = Formulario[strEntrada].Replace(".", "");
                        nombre = nombre.Replace("ñ", "n");
                        nombre = nombre.Replace("Ñ", "N");
                        nombre = nombre.Replace("á", "a");
                        nombre = nombre.Replace("é", "e");
                        nombre = nombre.Replace("í", "i");
                        nombre = nombre.Replace("ó", "o");
                        nombre = nombre.Replace("ú", "u");
                        nombre = nombre.Replace("Á", "A");
                        nombre = nombre.Replace("É", "E");
                        nombre = nombre.Replace("Í", "I");
                        nombre = nombre.Replace("Ó", "O");
                        nombre = nombre.Replace("Ú", "U");
                        nombre = nombre.Replace(",", "");
                        nombre = nombre.TrimEnd(caracteres);
                        if (strPropiedad == "Nombre") lstPaxINF[intCodigo].Nombre = Formulario[strEntrada] == "Nombre" ? null : Formulario[strEntrada];
                    }
                    if (strPropiedad == "Apellido")
                    {
                        apellido = Formulario[strEntrada].Replace(".", "");
                        apellido = apellido.Replace("ñ", "n");
                        apellido = apellido.Replace("Ñ", "N");
                        apellido = apellido.Replace("á", "a");
                        apellido = apellido.Replace("é", "e");
                        apellido = apellido.Replace("í", "i");
                        apellido = apellido.Replace("ó", "o");
                        apellido = apellido.Replace("ú", "u");
                        apellido = apellido.Replace("Á", "A");
                        apellido = apellido.Replace("É", "E");
                        apellido = apellido.Replace("Í", "I");
                        apellido = apellido.Replace("Ó", "O");
                        apellido = apellido.Replace("Ú", "U");
                        apellido = apellido.Replace(",", "");
                        apellido = apellido.TrimEnd(caracteres);
                        if (strPropiedad == "Apellido") lstPaxINF[intCodigo].Apellido = Formulario[strEntrada] == "Apellido" ? null : Formulario[strEntrada];
                    }
                    // CARGA SUS DATOS

                    if (strPropiedad == "TipoDocumento") lstPaxINF[intCodigo].TipoDocumento = Formulario[strEntrada] == "TipoDocumento" ? null : Formulario[strEntrada];
                    if (strPropiedad == "NumeroDocumento")
                    {
                        numDocumento = Formulario[strEntrada].Replace(".", "");
                        numDocumento = numDocumento.Replace(",", "");
                        numDocumento = numDocumento.TrimEnd(caracteres);
                        if (strPropiedad == "NumeroDocumento") lstPaxINF[intCodigo].NumeroDocumento = Formulario[strEntrada] == "No. de documento" ? null : Formulario[strEntrada];
                    }

                }

                if (tipo == "A")
                {
                    if (strEntrada == "_comprador.TelefonoCelular") Comprador.TelefonoCelular = Formulario[strEntrada] == "TelefonoCelular" ? null : Formulario[strEntrada];
                    if (strEntrada == "_comprador.Email") Comprador.Email = Formulario[strEntrada] == "CorreoElectronico" ? null : Formulario[strEntrada];
                    //if (strEntrada == "FormaPago") FormaPago = Formulario[strEntrada] == "FormaPago" ? null : Formulario[strEntrada];
                }
                if (tipo == "C")
                {
                    if (strEntrada == "_comprador.TelefonoCelular") Comprador.TelefonoCelular = Formulario[strEntrada] == "TelefonoCelular" ? null : Formulario[strEntrada];
                    if (strEntrada == "_comprador.Email") Comprador.Email = Formulario[strEntrada] == "CorreoElectronico" ? null : Formulario[strEntrada];
                }

            }

            paxAdultos = new List<mdlPax>(lstPaxADT);
            paxNiños = new List<mdlPax>(lstPaxCHD);
            paxInfantes = new List<mdlPax>(lstPaxINF);

        }
        #endregion
    }
}
