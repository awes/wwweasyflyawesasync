﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Utilidades.Models
{
    [Serializable]
    public class CTarifa
    {
        private const string STR_FORMATO = "{0:###,###}";

        //LISTAS DE CLASE
        List<char> lstClases75Niños = new List<char>() { 'T', 'W', 'Y', 'B', 'H' };
        List<char> lstClases100Niños = new List<char>() { 'K', 'E', 'G', 'N', 'O' };

        public string strClase { get; set; }
        public CConstantes.enuTipoPax tipoPax { get; set; }

        [DisplayFormat(DataFormatString = STR_FORMATO, ApplyFormatInEditMode = false)]
        public double mdblValor { get; set; }
        [DisplayFormat(DataFormatString = STR_FORMATO, ApplyFormatInEditMode = false)]
        public double mdblIVAValor
        {
            get { return mdblValor * .16; }
        }
        [DisplayFormat(DataFormatString = STR_FORMATO, ApplyFormatInEditMode = false)]
        public double mdblCombustible { get; set; }
        [DisplayFormat(DataFormatString = STR_FORMATO, ApplyFormatInEditMode = false)]
        public double mdblIVACombustible
        {
            get { return mdblCombustible * .16; }
        }
        [DisplayFormat(DataFormatString = STR_FORMATO, ApplyFormatInEditMode = false)]
        public double mdblTasa { get; set; }
        [DisplayFormat(DataFormatString = STR_FORMATO, ApplyFormatInEditMode = false)]
        public double mdblCargoAdministrativo { get; set; }
        [DisplayFormat(DataFormatString = STR_FORMATO, ApplyFormatInEditMode = false)]
        public double mdblIVACargoAdministrativo
        {
            get { return mdblCargoAdministrativo * .16; }
        }
        [DisplayFormat(DataFormatString = STR_FORMATO, ApplyFormatInEditMode = false)]
        public double mdblTotal
        {
            get { return valorFactor() + valorIVAFactor() + mdblCombustible + mdblIVACombustible + mdblTasa; }
        }

        public double valorFactor()
        {
            double dblFactor = 1;
            switch (tipoPax)
            {
                case CConstantes.enuTipoPax.Adulto:
                    break;
                case CConstantes.enuTipoPax.Niño:
                    if (strClase == "")
                        dblFactor = 1;
                    else if (lstClases75Niños.Contains(strClase.ToCharArray()[0]))
                        dblFactor = 0.75;
                    else
                        dblFactor = 1;
                    break;
                case CConstantes.enuTipoPax.Infante:
                    //if (strClase == "K") dblFactor = 0.75;
                    dblFactor = 0;
                    break;
                default:
                    break;
            }

            return mdblValor * dblFactor;
        }
        public double valorIVAFactor()
        {
            double dblFactor = 1;
            switch (tipoPax)
            {
                case CConstantes.enuTipoPax.Adulto:
                    break;
                case CConstantes.enuTipoPax.Niño:
                    if (strClase == "")
                        dblFactor = 1;
                    else if (lstClases75Niños.Contains(strClase.ToCharArray()[0]))
                        dblFactor = 0.75;
                    else
                        dblFactor = 1;
                    break;
                case CConstantes.enuTipoPax.Infante:
                    //if (strClase == "K") dblFactor = 0.75;
                    break;
                default:
                    break;
            }

            return mdblIVAValor * dblFactor;
        }
        public CTarifa()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}
