﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilidades.Models
{
    public class CTarifaAcumulada
    {
        public CTarifaAcumulada()
        {
            intTotalPax = 0;
            mdblValor = 0;
            mdblCombustible = 0;
            mdblTasa = 0;
            mdblCargoAdministrativo = 0;
        }
        public CTarifaAcumulada(CTarifaAcumulada oBase)
        {
            intTotalPax = oBase.intTotalPax;
            mdblValor = oBase.mdblValor;
            mdblCombustible = oBase.mdblCombustible;
            mdblTasa = oBase.mdblTasa;
            mdblCargoAdministrativo = oBase.mdblCargoAdministrativo;
        }

        private const string STR_FORMATO = "{0:###,###}";

        public int intTotalPax { get; set; }
        public double mdblValor { get; set; }
        public double mdblIVAValor
        {
            get { return mdblValor * .16; }
        }
        public double mdblCombustible { get; set; }
        public double mdblIVACombustible
        {
            get { return mdblCombustible * .16; }
        }
        public double mdblTasa { get; set; }
        public double mdblCargoAdministrativo { get; set; }
        public double mdblIVACargoAdministrativo
        {
            get { return mdblCargoAdministrativo * .16; }
        }
        /// <summary>
        /// Valor total acumulado de toda la compra, incluyento tarifa, inmpuestos y fee
        /// </summary>
        public double mdblTotal
        {
            get { return mdblValor + mdblIVAValor + mdblCombustible + mdblIVACombustible + mdblTasa + mdblCargoAdministrativo + mdblIVACargoAdministrativo; }
        }
        /// <summary>
        /// Valor en pesos de decuento
        /// </summary>
        public double mdblValorDescuento { get; set; }
        /// <summary>
        /// Valor en porcentaje de descuento. de 0 a 100.
        /// </summary>
        public int mIntPorcentajeDescuento { get; set; }
        /// <summary>
        /// Indica si se aplica porcentaje (true) o valor (false) de descuento
        /// </summary>
        public bool mboolAplicaPorcentajeDescuento { get; set; }

        /// <summary>
        /// Valor de descuento en pesos, calculado a partir de porcentaje de decuento o de valor de descuento
        /// </summary>
        public double mdblValorDescontado
        {
            get
            {
                if (mboolAplicaPorcentajeDescuento)
                {
                    if (mIntPorcentajeDescuento != 0)
                        return mIntPorcentajeDescuento * (mdblValor + mdblIVAValor + mdblCombustible + mdblIVACombustible) / 100;
                    else
                        return 0;
                }
                else
                    return mdblValorDescuento;
            }
        }
        /// <summary>
        /// Valor total con el descuento aplicado
        /// </summary>
        public double mdblValorConDescuento
        {
            get { return mdblTotal - mdblValorDescontado; }
        }

        /// <summary>
        /// Valor total acumulado de toda la compra, incluyento tarifa, inmpuestos y fee por persona (dividido entre el numero toal de personas)
        /// </summary>
        public double mdblPorPersona
        {
            get { return (mdblValor + mdblIVAValor + mdblCombustible + mdblIVACombustible + mdblTasa + mdblCargoAdministrativo + mdblIVACargoAdministrativo) / intTotalPax; }
        }
    }
}
