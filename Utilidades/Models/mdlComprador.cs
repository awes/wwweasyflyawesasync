﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilidades.Models
{
    public class mdlComprador
    {
        public string _telefonoCelular = "";
        public string _email = "";
        public mdlComprador()
        {
        }

        
        [DisplayName("Tel. Cel")]
        public string TelefonoCelular
        {
            get
            {
                return _telefonoCelular;
            }
            set
            {
                _telefonoCelular = value;
            }
        }
        [DisplayName("Email")]
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
            }
        }
        

        [DisplayName("País")]
        public string País { get; set; }
        [DisplayName("Ciudad")]
        public string Ciudad { get; set; }
        [DisplayName("CodigoPaís")]
        public string CodigoPaís { get; set; }
        [DisplayName("CodigoCiudad")]
        public string CodigoCiudad { get; set; }

        [DisplayName("OK")]
        public bool isValid
        {
            get
            {
                bool blnIsValid = true;
                blnIsValid &= isValidTelefonoCelular;
                blnIsValid &= isValidCorreoElectronico;
               
                return blnIsValid;
            }
        }
        public bool isValidTelefonoCelular { get { return ((TelefonoCelular != null) && (TelefonoCelular != "") && (TelefonoCelular != "Celular")); } }
        public bool isValidCorreoElectronico
        {
            get
            {
                bool blnValido = true;
                blnValido &= (Email != null) && (Email != "") && (Email != "Correo");
                return blnValido;
            }
        }
     
    }
}
