﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilidades.Models
{
    public class mdlPax
    {
        public int _codigo = 0;
        public string _nombre = "";
        public string _apellido = "";
        public string _tipoDocumento = "";
        public string _numeroDocumento = "";
        public string _tipo = "";
        public mdlPax()
        {
        }
        public mdlPax(string strTipoPax)
        {
            Tipo = strTipoPax;
        }

        public int Codigo
        {
            get
            {
                return _codigo;
            }
            set
            {
                _codigo = value;
            }
        }
        public string Nombre
        {
            get
            {
                return _nombre;
            }
            set
            {
                _nombre = value;
            }
        }
        public string Apellido
        {
            get
            {
                return _apellido;
            }
            set
            {
                _apellido = value;
            }
        }
        public string TipoDocumento
        {
            get
            {
                return _tipoDocumento;
            }
            set
            {
                _tipoDocumento = value;
            }
        }
        public string NumeroDocumento
        {
            get
            {
                return _numeroDocumento;
            }
            set
            {
                _numeroDocumento = value;
            }
        }
        public string Tipo
        {
            get
            {
                return _tipo;
            }
            set
            {
                _tipo = value;
            }
        }

        public bool isValid
        {
            get
            {
                bool blnIsValid = true;

                blnIsValid &= ((Nombre != null) && (Nombre != ""));
                blnIsValid &= ((Apellido != null) && (Apellido != ""));
                blnIsValid &= ((NumeroDocumento != null) && (NumeroDocumento != ""));

                return blnIsValid;

            }
        }
    }
}
