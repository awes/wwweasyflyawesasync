﻿// Set timeout variables.
var timoutWarning = 8400;
var timoutNow = 9000;

var warningTimer;
var timeoutTimer;

var nav = $('.resumen');


$(document).ready(function () {
    checkWidth(true);

    // IF USER USE BACK BOTTON IN PAX PAGE
    $('input[name="radiog_darkD"]').each(function () {
        if ($(this).is(":checked")) {
            var data = $(this).parent().attr("name")
            reservationSelect(data);
            return false;
        }
    });

    $('input[name="radiog_darkR"]').each(function () {
        if ($(this).is(":checked")) {
            var data = $(this).parent().attr("name")
            reservationSelect(data);
            return false;
        }
    });

    $(window).resize(function () {
        checkWidth(false);
    });

    $("#refoundTexta").click(function () {
        var refoundText = "";
        refoundText = refoundText + '<span style="font-size:13px; text-align:justify;">';
        refoundText = refoundText + '• <b>Tarifas económicas</b>: No reembolsable. Aplica penalidad de $90.000 por cambio de nombre, ruta, fecha, vuelo y por no presentación al aeropuerto. En todos los casos se revisa a tarifa superior.<br /><br />';
        refoundText = refoundText + '• <b>Tarifas preferenciales</b>: Reembolsable con penalidad de $90.000. No aplica penalidad por cambio de nombre, ruta, fecha o vuelo antes del viaje.  Aplica penalidad de $90.000 por cambios en vuelo después de operado. En todos los casos se revisa a tarifa superior.<br /><br />';
        refoundText = refoundText + '• <b>Derecho a retracto</b>: El derecho de retracto deberá ejercerse en un plazo de 48 horas a partir de la compra y 8 días antes del vuelo.';
        refoundText = refoundText + '</span>';
        invokeModalError(refoundText)
    })

    $(".but").click(function () {
        $(this).toggleClass("but_up");
        $(".display").toggle();
    });
    $(".D").click(function (e) {
        var data = $(this).attr("name")
        reservationSelect(data);
        e.stopPropagation();
        e.preventDefault();
    });
    $(".R").click(function (e) {
        var data = $(this).attr("name")
        reservationSelect(data);
        e.stopPropagation();
        e.preventDefault();
    });

    $(".info.economica h2").mouseover(function () {
        $(".modal_eco2").show();
    });
    $(".info.preferencial h2").mouseover(function () {
        $(".modal_pref2").show();
    });
    $(".info.economica h2").mouseout(function () {
        $(".modal_eco2").hide();
    });
    $(".info.preferencial h2").mouseout(function () {
        $(".modal_pref2").hide();
    });
});


$(window).scroll(function () {
    if ($(this).scrollTop() > 165 && $(window).width() > 900) {
        nav.addClass("f-nav");
    } else {
        nav.removeClass("f-nav");

    }
});

function toPaxPage() {
    $.blockUI();
    var areFligthsSelected = false;
    $('input[name="radiog_darkD"]').each(function () {
        if ($(this).is(":checked")) {
            areFligthsSelected = true;
            return false;
        }
    });
    if ($('#regreso').length > 0) {
        if ($('input[name="radiog_darkR"]').length > 0) {
            if (areFligthsSelected) {
                areFligthsSelected = false;
                $('input[name="radiog_darkR"]').each(function () {
                    if ($(this).is(":checked")) {
                        areFligthsSelected = true;
                        return false;
                    }
                });
            } else {
                areFligthsSelected = false;
            }
        } else {
            areFligthsSelected = false;
        }
    }
    if (areFligthsSelected) {
        if ($('#refoundText').is(":checked")) {
            $("#frmAvail").submit();
        } else {
            invokeModalError("Debe aceptar las condiciones de reembolso");
            $.unblockUI();
            return false;
        }
    } else {
        if ($('#regreso').length == 0) {
            invokeModalError("Debe seleccionar al menos un vuelo de ida.");
        } else {
            invokeModalError("Debe seleccionar al menos un vuelo de ida y un vuelo de regreso.");
        }
        $.unblockUI();
        return false;
    }
}

function checkWidth(init) {
    /*If browser resized, check width again */
    if ($(window).width() < 900) {
        $('.resumen').addClass('g-nav');
        $('.resumen').removeClass('f-nav');
    }
    else {
        if (!init) {
            $('.resumen').removeClass('g-nav');
            $('.resumen').addClass('f-nav');
        }
    }
}

// Start timers.
function StartTimers() {
    warningTimer = setTimeout("IdleWarning()", timoutWarning);
    timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
}

// Reset timers.
function ResetTimers() {
    clearTimeout(warningTimer);
    clearTimeout(timeoutTimer);
    StartTimers();
    $("#modal").dialog('close');
}

// Show idle timeout warning dialog.
function IdleWarning() {
    $('#modal').foundation('reveal', 'open');
}

// Logout the user.
function IdleTimeout() {
    window.location = logoutUrl;
}

function reservationSelect(datos) {
    var str = datos.split("^");

    var positionSecond = $(".H2Book").offset().top;
    var heightSecond = $(".H2Book").height();
    var isReturn = "R";
    if (str[6] == "1") {
        isReturn = "D"
    }
    if (str[0] == 1) {
        var positionFirst = $(".regreso").offset().top;
        var heightFirst = $(".regreso").height();
        if (isReturn == "D") {
            $("html, body").animate({ scrollTop: positionFirst - heightFirst }, 1000);
        }
        if (isReturn == "R") {
            $("html, body").animate({ scrollTop: positionSecond - heightSecond }, 1000);
        }
    } else {
        if (isReturn == "D") {
            $("html, body").animate({ scrollTop: positionSecond - heightSecond }, 1000);
        }
    }
    var remove = $(".bg_naranja" + str[6]).hasClass(isReturn);
    if (str[8] == "E") {
        $('#radio-' + str[7]).prop("checked", true);
        if (remove)
            $(".large-3 div").removeClass('bg_naranja' + str[6]);
        $('#divSelect-' + str[7]).addClass('bg_naranja' + str[6]);
    }
    if (str[8] == "P") {
        $('#radioP-' + str[7]).prop("checked", true);
        if (remove)
            $(".large-3 div").removeClass('bg_naranja' + str[6]);
        $('#divSelectP-' + str[7]).addClass('bg_naranja' + str[6]);
    }
    if (str[8] == "ER") {
        $('#radioR-' + str[7]).prop("checked", true);
        if (remove)
            $(".large-3 div").removeClass('bg_naranja' + str[6]);
        $('#divSelectR-' + str[7]).addClass('bg_naranja' + str[6]);
    }
    if (str[8] == "PR") {
        $('#radioPR-' + str[7]).prop("checked", true);
        if (remove)
            $(".large-3 div").removeClass('bg_naranja' + str[6]);
        $('#divSelectPR-' + str[7]).addClass('bg_naranja' + str[6]);
    }

    var urlSelect = '/reservation/reservationPost?strInfoSeleccion=' + datos;

    //$.blockUI({
    //    message: $('#domMessage'),
    //    css: { backgroundColor: 'transparent', color: '#fff', border: "none" }
    //});

    $.get(urlSelect, null, updateTariff)
        .fail(function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            mixpanel.track("Error", { "Number": "3001", "Page": "Flight", "url": "/reservation/reservationPost", "Details": "no se pudo continuar a pax" });
            errorObject = JSON.parse(jqXHR.responseText);
            log("submit::error:" + errorObject.Message);
        })
        .always(function () {
            //// AQUI HAY QUE PONER LIMPIEZA -- OPCIONAL
            //alert('getJSON request ended!');
        });

}

function updateTariff(data) {
    $.unblockUI();

    try {
        if (data.data.error == 0) {
            $('#mdblValor').html(data.data.mdblValor);
            $('#mdblIVAValor').html(data.data.mdblIVAValor);
            $('#mdblCombustible').html(data.data.mdblCombustible);
            $('#mdblIVACombustible').html(data.data.mdblIVACombustible);
            $('#mdblTasa').html(data.data.mdblTasa);
            $('#mdblCargoAdministrativo').html(data.data.mdblCargoAdministrativo);
            $('#mdblIVACargoAdministrativo').html(data.data.mdblIVACargoAdministrativo);
            $('#mdblTotal').html(data.data.mdblTotal);
            $('#mdblValorDescontado').html(data.data.mdblValorDescontado);
            $('#mdblTotalConDescuento').html(data.data.mdblTotalConDescuento);
            $('#mdblValorDescontadoP2').html(data.data.mdblValorDescontado);
            $('#mdblTotalConDescuentoP2').html(data.data.mdblTotalConDescuento);
            $('#mdblValorPopUp').html(data.data.mdblValor);
            $('#mdblIVAValorPopUp').html(data.data.mdblIVAValor);
            $('#mdblCombustiblePopUp').html(data.data.mdblCombustible);
            $('#mdblIVACombustiblePopUp').html(data.data.mdblIVACombustible);
            $('#mdblTasaPopUp').html(data.data.mdblTasa);
            $('#mdblCargoAdministrativoPopUp').html(data.data.mdblCargoAdministrativo);
            $('#mdblIVACargoAdministrativoPopUp').html(data.data.mdblIVACargoAdministrativo);
            $('#mdblTotalPopUp1').html(data.data.mdblTotal);
            $('#mdblTotalPopUp2').html(data.data.mdblTotal);
            $('#mdblValorDescontadoPopUp').html(data.data.mdblValorDescontado);
            $('#mdblTotalConDescuentoPopUp').html(data.data.mdblTotalConDescuento);
            $('#mdblVueloIda').html(data.data.mdblVueloIda);
            $('#mdblFechaIti').html(data.data.mdblFechaIti);
            $('#mdblHoraIti').html(data.data.mdblHoraIti);
            $('#mdblFechaItiLlegada').html(data.data.mdblFechaItiLlegada);
            $('#mdblHoraItiLlegada').html(data.data.mdblHoraItiLlegada);
            $('#mdblFechaItiPopUp').html(data.data.mdblFechaIti);
            $('#mdblHoraItiPopUp').html(data.data.mdblHoraIti);
            $('#mdblFechaItiLlegadaPopUp').html(data.data.mdblFechaItiLlegada);
            $('#mdblHoraItiLlegadaPopUp').html(data.data.mdblHoraItiLlegada);
            $('#mdblVueloRegreso').html(data.data.mdblVueloRegreso);
            $('#mdblFechaItiRegreso').html(data.data.mdblFechaItiRegreso);
            $('#mdblHoraItiRegreso').html(data.data.mdblHoraItiRegreso);
            $('#mdblFechaItiRegresoLlegada').html(data.data.mdblFechaItiRegresoLlegada);
            $('#mdblHoraItiRegresoLlegada').html(data.data.mdblHoraItiRegresoLlegada);
            $('#mdblFechaItiRegresoPopUp').html(data.data.mdblFechaItiRegreso);
            $('#mdblHoraItiRegresoPopUp').html(data.data.mdblHoraItiRegreso);
            $('#mdblFechaItiRegresoLlegadaPopUp').html(data.data.mdblFechaItiRegresoLlegada);
            $('#mdblHoraItiRegresoLlegadaPopUp').html(data.data.mdblHoraItiRegresoLlegada);
            $('#mdblTasas').html(data.data.mdblTasas);
            $('#mdblTasasPopUp').html(data.data.mdblTasas);
            $('#mdblItinerarioIda').html(data.data.mdblFechaIti + " / " + data.data.mdblHoraIti + " / " + data.data.mdblHoraItiLlegada);
            $('#mdblItinerarioRegreso').html(data.data.mdblFechaItiRegreso + " / " + data.data.mdblHoraItiRegreso + " / " + data.data.mdblHoraItiRegresoLlegada);
            $('#mdblItinerarioIdaPopUp').html(data.data.mdblFechaIti + " / " + data.data.mdblHoraIti);
            $('#mdblItinerarioIdaLlegadaPopUp').html(data.data.mdblFechaItiLegada + " / " + data.data.mdblHoraItiLlegada);
            $('#mdblItinerarioRegresoPopUp').html(data.data.mdblFechaItiRegreso + " / " + data.data.mdblHoraItiRegreso);
            $('#mdblItinerarioRegresoLlegadaPopUp').html(data.data.mdblFechaItiRegresoLlegada + " / " + data.data.mdblHoraItiRegresoLlegada);
            $("#btnContinuar").show();
            $("#errorMessage").hide();
            $(this).bind("click");
        }
    }
    catch (ex) {
        if (data.error == 3) {
            $("#btnContinuar").hide();
            $("#errorMessage").html(data.Mensaje);
            $("#errorMessage").show("fast");
        }
    }
}