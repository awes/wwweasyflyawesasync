﻿var resizeTimer;
var resized = false;
var logHead = "home::ready:";





$(document).ready(function () {

    attachEvents();

    //bannerSearch();

    disappearingInscriptionBox();
    
    lastestSearch();

    // original design inclussion
    $(document).foundation();
    getLonLat(true);
});



function disappearingInscriptionBox() {
    $("#inscriptionDiv").stop(true, true).fadeOut(7000);
}

function attachEvents() {
    $("#lastestSearch").change(function () {
        var value = $("#lastestSearch option:selected").val();
        if (value != "none") {
            toFlight(lastestSearches[parseInt(value)]);
        }
    })

    $("body header .header").click(function () {
        window.location("/");
    });

    $("#destinations").change(function () {
        $("#destinationMessage").hide();
    });

    $("#btnPurchase").click(function () {
        var reservation = $("#reservationCodePurchase").val();
        var lastName = $("#lastNamePurchase").val();

        urlPurchase = "/consultation/purchaseReturn?ReservationCode=" + reservation + "&lastName=" + lastName;
        $.blockUI();
        $.getJSON(urlPurchase, null, processPurchase)
            .fail(function (jqXHR, textStatus, data) {
                $.unblockUI();
                mixpanel.track("Error", { "Number": "2001", "Page": "Home", "url": "/consultation/purchaseReturn", "Details": "Su reserva ha expirado." });
                invokeModalError("Su reserva ha expirado.");
                errorObject = JSON.parse(jqXHR.responseText);
                log("processPurchase::error:" + errorObject.Message);
            })
    });

    $("#btnForwarding").click(function () {
        var reservation = $("#reservationCodeForwarding").val();
        var lastName = $("#lastNameForwarding").val();
        var email = $("#emailForwarding").val();
        urlForwarding = "/consultation/forwardingTicket?ReservationCode=" + reservation + "&lastName=" + lastName + "&email=" + email;

        $.getJSON(urlForwarding, null, processForwarding)
            .fail(function (jqXHR, textStatus, data) {
                mixpanel.track("Error", { "Number": "2002", "Page": "Home", "url": "/consultation/forwardingTicket", "Details": "no se pudo reenviar ticket."});
                errorObject = JSON.parse(jqXHR.responseText);
                log("processForwarding::error:" + errorObject.Message);
            })
    });
    $("#btnRegister").click(function () {
        var email = $("#emailRegister").val();
        var emailConfirm = $("#emailRegisterConfirm").val();
        var pass = $("#passRegister").val();
        var passConfirm = $("#passRegisterConfirm").val();
        var name = $("#nameRegister").val();
        var lasName = $("#lastNameRegister").val();
        var document = $("#documentRegister").val();
        urlForwarding = "/base/singInProcess?email=" + email + "&emailConfirm=" + emailConfirm + "&pass=" + pass + "&passConfirm=" + passConfirm + "&name=" + name + "&lasName=" + lasName + "&document=" + document;
        if (email == emailConfirm) {
            if (pass == passConfirm) {
                if (passValidation(pass)) {
                    if (name != "" && lasName != "" && document != "") {
                        $.getJSON(urlForwarding, null, processRegister)
                            .fail(function (jqXHR, textStatus, data) {
                                mixpanel.track("Error", { "Number": "2003", "Page": "Home", "url": "/base/singInProcess", "Details": "no se pudo registrar pasajeros." });
                                errorObject = JSON.parse(jqXHR.responseText);
                                log("processRegister::error:" + errorObject.Message);
                            })
                    } else {
                        invokeModalError("Por favor complete todos los campos");
                    }
                }
            } else {
                invokeModalError("La contraseña y la contraseña de confirmación no coinciden")
            }
        } else {
            invokeModalError("El email y email de confirmación no coinciden")
        }
    });
    $("#logInHomeSend").click(function () {
        var email = $("#emailModal").val();
        var pass = $("#passModal").val();
        var checked = $("#emailRememberModal").val();
        var emailRemember = false;
        if (checked == "on") {
            emailRemember = true;
        }

        $.getJSON("/base/logInModal", { email: email, pass: pass, emailRemember: emailRemember }, processLogIn)
            .fail(function (jqXHR, textStatus, data) {
                mixpanel.track("Error", { "Number": "2004", "Page": "Home", "url": "/base/logInModal", "Details": "no se pudo loguear usuario correctamente." });
                errorObject = JSON.parse(jqXHR.responseText);
                log("processRegister::error:" + errorObject.Message);
            })
    });

    $("#modal5 .bot_buscar").click(function () {
        $("#modal5, .reveal-modal-bg").hide();
    });

    $(".olvido_cont").click(function () {
        $("#drop2").show();
    });

    $("#registerModalOpen").click(function () {
        $("#inscriptionDiv").remove();
    })

    $("#logInModalOpen").click(function () {
        $("#inscriptionDiv").remove();
        $(".link2").click();
    })

    $("#drop2 .bot-ir").click(function () {
        $("#drop2").hide();
        $("#drop3").show();
    });

    $("#drop3 .boton").click(function () {
        $("#drop3").hide();
    });

    $(".link1").click(function () {
        $(".box1").fadeIn();
        $(".link1").animate({
            width: "300px",
        }, 500);
        $(".link2").animate({
            width: "140px",
        }, 500);
        $(".link3").animate({
            width: "220px",
        }, 500);
    });

    $(document).click(function (e) {
        if (!$(e.target).hasClass("link1")
            && $(e.target).parents(".box1").length === 0) {
            $(".box1").fadeOut();
            $(".link1").css({
                width: "140px",
            });
        }
    });

    $(".link2").click(function () {
        $(".box2").fadeIn();
        $(".link2").animate({
            width: "300px",
        }, 500);
        $(".link1").animate({
            width: "140px",
        }, 500);
        $(".link3").animate({
            width: "220px",
        }, 500);
    });

    $(document).click(function (e) {
        if (!$(e.target).hasClass("link2")
            && $(e.target).parents(".box2").length === 0) {
            $(".box2").fadeOut();
            $(".link2").css({
                width: "140px",
            });
        }
    });

    $(".link3").click(function () {
        $(".box3").fadeIn();
        $(".link3").animate({
            width: "300px",
        }, 500);
        $(".link2,.link1").animate({
            width: "170px",
        }, 500);
    });

    $(document).click(function (e) {
        if (!$(e.target).hasClass("link3")
            && $(e.target).parents(".box3").length === 0) {
            $(".box3").fadeOut();
            $(".link3").css({
                width: "220px",
            });
        }
    });

    $(document).click(function (e) {
        if ($(e.target).parents(".box1,.box2,.box3").length === 0) {
            $(".link1,.link2,.link3").css({
                width: "220px",
            });
        }
    });

    $(document).click(function (e) {
        if ($(e.target).parents(".box3").length != 0) {
            $(".link1,.link2").css({
                width: "170px",
            });
        }
    });

    $(window).resize(function () {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {
            if (!resized) {
                resized = true;
                $(document).mouseover(function () {
                    resized = false;
                    // do something here
                    $(this).unbind("mouseover");
                })
            }
        }, 500);
    });

    

    log(logHead, "setted calendar options  intialized");

    $(".input_pasajeros").focus(function (event) {
        $(".pasajeros").fadeIn("slow");
        //$(".pasajeros").removeClass("pasajeros");
        //$(".pasajeros").addClass("pasajeros2");
    });

    $(document).click(function (e) {
        if (!$(e.target).hasClass("input_pasajeros")
        && $(e.target).parents(".pasajeros").length === 0) {
            $(".pasajeros").fadeOut();
        }
    });

    $('#radio5').change(function () {
        $('.fecha_regreso').prop('disabled', true);
    });

    $('#radio4').change(function () {
        $('.fecha_regreso').prop('disabled', false);
    });

    $("#inscriptionDiv").mouseenter(function () {
        $(this).stop(true, true).show();
    });
    $("#inscriptionDiv").mouseleave(function () {
        disappearingInscriptionBox();
    });
    //$("#inscriptionDiv").mouseenter(function () {
    //    $(this).stop(true, true).fadeIn();
    //});
    //$("#inscriptionDiv").mouseleave(function () {
    //    $(this).stop(true, true).fadeOut();
    //}) 

    $(".menos").mousedown(function () {
        $(this).css("border-color", "#415b78");
    }).mouseup(function () {
        $(this).css("border-color", "none");
    })
    $(".mas").mousedown(function () {
        $(this).css("border-color", "#415b78");
    }).mouseup(function () {
        $(this).css("border-color", "none");
    })

    $("#webCheckInEmail").change(function () {
        var canContinue = emailValidation($(this).val())
        if (canContinue) {
            return true;
        } else {
            $(this).val("")
            return false;
        }
    })

    $("#emailModal").change(function () {
        var canContinue = emailValidation($(this).val())
        if (canContinue) {
            return true;
        } else {
            $(this).val("")
            return false;
        }
    })
    $("#emailHome").change(function () {
        var canContinue = emailValidation($(this).val())
        if (canContinue) {
            return true;
        } else {
            $(this).val("")
            return false;
        }
    })
    $("#emailRegister").change(function () {
        var canContinue = emailValidation($(this).val())
        if (canContinue) {
            return true;
        } else {
            $(this).val("")
            return false;
        }
    })
    $("#emailRegisterConfirm").change(function () {
        var canContinue = emailValidation($(this).val())
        if (canContinue) {
            return true;
        } else {
            $(this).val("")
            return false;
        }
    })

    log(logHead, "foundation executing");
}



function btnRecuperar() {
    $(".bienvenido").hide();
    $(".recuperar").show();
}

function botEnviar() {
    $(".recuperar").hide();
    $(".bienvenido").show();
}

function processPurchase(data, textStatus, jqXHR) {
    $.unblockUI();
    log('okProcessPurchase');
    var rows = [];
    //purchaseInfo = data.data[0];

    $('#message').html('<tr><td>' + data.OK + '</tr>');
    if (data.data == 1)
        document.location.href = "../payment";
    if (data.data == 3)
        rows.push("<span>Su tiquete ya ha sido emitido.</span>");
    if (data.data == 5)
        rows.push("<span>Su reserva ha expirado.</span>");

    $("#divResponsePurchase").html(rows.join(""));

    $('#message').html('<div>Purchase obtained.</div>');
}
function bannerSearch() {
    $.getJSON("/Home/bannerSearch", {}, bannerUpdate)
}

function processForwarding(data, textStatus, jqXHR) {

    log('okProcessForwarding');
    var rows = [];

    $('#message').html('<tr><td>' + data.OK + '</tr>');
    if (data.data == 0)
        rows.push("<span>Error al enviar tiquete.</span>");
    if (data.data == 1)
        rows.push("<span>Su tiquete esta pendiente de emisión.</span>");
    if (data.data == 3)
        rows.push("<span>Su tiquete ha sido enviado al correo.</span>");
    if (data.data == 4)
        rows.push("<span>Su reserva no es válida.</span>");
    if (data.data == 5)
        rows.push("<span>Su reserva ha expirado.</span>");

    $("#divResponseForwarding").html(rows.join(""));

    $('#message').html('<div>Forwarding obtained.</div>');
}

function processRegister(data, textStatus, jqXHR) {

    log('okprocessRegister');
    var rows = [];

    $('#message').html('<tr><td>' + data.OK + '</tr>');
    if (data.data == "OK")
        rows.push("<span>Registro exitoso.</span>");

    $("#divResponseRegister").html(rows.join(""));

    $('#message').html('<div>Registration obtained.</div>');
}

function processLogIn(data, textStatus, jqXHR) {
    if (data.data == "OK") {
        location.href = "/Home/Index";
    } else {
        invokeModalError("Hubo un error al intentar ingresar, porfavor verifique que escribió bien sus credenciales");
        $("#emailModal").val("");
        $("#passModal").val("");
    }
}

function bannerSearch() {
    $.getJSON("/Home/bannerSearch", {}, bannerUpdate)
}


function bannerUpdate(data, textStatus, jqXHR) {
    var home = JSON.parse(data.home);
    var principal = JSON.parse(data.principal);
    var secundary = JSON.parse(data.secundary);
    var index = $(".banner").attr("name");
    var type = $(".banner").attr("class");
    $.each(principal, function (i, value) {
        $("#principal" + i).attr("src", value.url);
        $("#principal" + i).attr("alt", value.altText);
        $("#principal" + i).attr("title", value.title);
        if (value.urlSmall != null && value.urlSmall != "") {
            $("#principal" + i + "a").attr("href", value.urlSmall);
        } else {
            $("#principal" + i + "a").attr("href", "#");
        }
    })
    $.each(secundary, function (i, value) {
        $("#secundary" + i).attr("src", value.url);
        $("#secundary" + i).attr("alt", value.altText);
        $("#secundary" + i).attr("title", value.title);
        if (value.urlSmall != null && value.urlSmall != "") {
            $("#secundary" + i + "a").attr("href", value.urlSmall);
        } else {
            $("#secundary" + i + "a").attr("href", "#");
        }
    })
    var randomIndex = Math.floor(Math.random() * home.length);
    var bannerSelected = home[randomIndex];
    if (bannerSelected.urlSmall != null && bannerSelected.urlSmall != "") {
        $('<a href="' + bannerSelected.urlSmall + '"><img class=" class="fade-in" src="' + bannerSelected.url + '" alt="' + bannerSelected.altText + '" title="' + bannerSelected.title + '"></a>').appendTo('.banner-load');
    } else {
        $('<img class=" class="fade-in" src="' + bannerSelected.url + '" alt="' + bannerSelected.altText + '" title="' + bannerSelected.title + '">').appendTo('.banner-load');
    }
}

function toFlight(aviability) {
    var departureDateParsed = new Date(parseInt(aviability.DepartureDate.split('(')[1].split(')')[0]));
    var returnDateParsed = new Date(parseInt(aviability.ReturnDate.split('(')[1].split(')')[0]));
    var departureDate = departureDateParsed.getDate() + '-' + (departureDateParsed.getMonth() + 1) + '-' + departureDateParsed.getFullYear();
    var returnDate = returnDateParsed.getDate() + '-' + (returnDateParsed.getMonth() + 1) + '-' + returnDateParsed.getFullYear();
    urlFlightAvail = "/base/postAvail?OriginID=" + aviability.IdOrigin + "&destinationID=" + aviability.IdDestination + "&originIATA=" + aviability.IATAOrigin + "&destinationIATA=" + aviability.IATADestination + "&departureDate=" + departureDate + "&returnDate=" + returnDate + "&flightType=" + aviability.flightType + "&adult=" + aviability.Adult + "&child=" + aviability.Child + "&infant=" + aviability.Infant;
    $.get(urlFlightAvail, null, availRedirect)
    .fail(function (jqXHR, textStatus, errorThrown) {
        $.unblockUI();
        mixpanel.track("Error", { "Number": "2005", "Page": "Home", "url": "/base/postAvail", "Details": "no se pudo cargar vuelos." });
        errorObject = JSON.parse(jqXHR.responseText);
        log("submit::error:" + errorObject.Message);
    })
    .always(function () {
        //// AQUI HAY QUE PONER LIMPIEZA -- OPCIONAL
        //alert('getJSON request ended!');
    });
}

function lastestSearch() {
    $.blockUI();
    $.getJSON("/base/getLastestSearch", null, function (data, textStatus, jqXHR) {
        if (data.OK == "SUCCESS") {
            $.each(data.data.addInfo, function (i, value) {
                $("#lastestSearch").append("<option value='" + i + "'>" + data.data.lastestSearch[i] + "</option>");
                lastestSearches.push(value);
            })
            $("#lastestSearch option").each(function () {
                if ($(this).val() == "none") {
                    $(this).html(lastestSearches.length + " " + $(this).html().split(" ")[1] + " " + $(this).html().split(" ")[2]);
                }
            })
        }
        $.unblockUI();

    }).fail(function (jqXHR, textStatus, data) {
        $.unblockUI();
        mixpanel.track("Error", { "Number": "2006", "Page": "Home", "url": "/base/getLastestSearch", "Details": "no se pudo cargar últimas tres busquedas" });
        errorObject = JSON.parse(jqXHR.responseText);
        log("processPurchase::error:" + errorObject.Message);
    });
    $.unblockUI();
}