﻿var latitude = 0;
var longitude = 0;
getLonLat(false);

function log(logString) {
    if (recordLog) {
        console.log(logString + " @" + (new Date()).toString());
    }
}
function log(logHead, logString) {
    if (recordLog) {
        if (logHead)
            console.log(logHead + logString + ". @" + (new Date()).toString());
        else
            console.log(logString + ". @" + (new Date()).toString());
    }
}

/*
 * Main initialization point
 */
$(document).ready()
{
    var logHead = "document::ready:";

    log(logHead, "document ready");

    // header redirect to home on click
    $(".clickableLogo").click(function () { window.location.replace('/'); });

    log(logHead, "jquery intialized");


    // if has an error a modal is display with the message that is carried in ViewBag.otherErrorMessage 
    // with title ViewBag.otherError
    hasOtherError();
}

function hasOtherError() {
    $("input[type=hidden]").each(function () {
        if ("22030" == $(this).attr("id")) {
            invokeModalError("<h2>error :: " + $(this).attr("id") + "</h2><br /><p>" + $(this).attr("message") + "</p>");
            return false;
        }
        if ("sign" == $(this).attr("id")) {
            invokeModalError("<h2>error :: " + $(this).attr("id") + "</h2><br /><p>" + $(this).attr("message") + "</p>");
            return false;
        }
        if ("WrongDestination" == $(this).attr("id")) {
            invokeModalError($(this).attr("message"));
            return false;
        }
        if ("pageNotFoundCity" == $(this).attr("id")) {
            invokeModalError($(this).attr("message"));
            return false;
        }
        if ("checkInError" == $(this).attr("id")) {
            invokeModalError($(this).attr("message"));
            return false;
        }
    })
}
function getLonLat(isSearchEngine) {
    navigator.geolocation.getCurrentPosition(function (position) {
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;
        setTimeout(getCity(true), 3000);
        if (isSearchEngine) {
            setTimeout(getCity(false), 3000);
        }
    });
}
function getCity(isLayout) {
    var geocoder = new google.maps.Geocoder;
    var latlng = { lat: latitude, lng: longitude };
    geocoder.geocode({ 'location': latlng }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            if (results[1]) {
                cityName = results[1].formatted_address;
                if (cityName != "") {
                    if (isLayout) {
                        $("#citiesContact option").each(function () {
                            if (ascentMapped(cityName).indexOf(ascentMapped($(this).html()).split(" ")[0]) > -1) {
                                $("#citiesContact").val($(this).val());
                                $("#citiesPage").val($(this).val());
                                $("#cityFooter").attr('href', '/vuelos/tiquetes/vuelos-desde-'+(ascentMapped($(this).html()).split(" ")[0]).toLowerCase());

                                $("#citiesContact").change();
                                $("#citiesPage").change();

                            }
                        })
                    } else {
                        //SELECT SEARCH ENGINE
                        $("#origins option").each(function () {
                            if (ascentMapped(cityName).indexOf(ascentMapped($(this).html()).split(" ")[0]) > -1) {
                                $("#origins").val($(this).val());
                                $("#origins").change();
                            }
                        });
                        $("#originSelect option").each(function () {
                            if (ascentMapped(cityName).indexOf(ascentMapped($(this).html()).split(" ")[0]) > -1) {
                                $("#originSelect").val($(this).val());
                                $("#originSelect").change();
                            }
                        })
                    }
                } else {
                    invokeModalError('No se pudo obtener la ubicación');
                }
            } else {
                invokeModalError('No se pudo obtener la ubicación');
                return "";
            }
        } else {
            if (status == 'ZERO_RESULTS') {
                //invokeModalError('Para mejorar su experiencia através de la página de Easyfly le recomendamos permitirnos conocer su ubicación.');
            } else {
                invokeModalError('Geocoder falló debido a: ' + status);
            }
            return "";
        }
    });
}


function emailValidation(valor) {
    if (valor == "" || valor == null || typeof (valor) == "undefined") {
        return false;
    }
    var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
    if (filter.test(valor))
        return true;
    else {
        //alert("El correo electrónico no tiene el formato correcto.")
        invokeModalError("El correo electrónico no tiene el formato correcto.");
        $("#txtCorreo").val("");
        return false;
    }
}
function passValidation(value) {
    var filterUpper = /[A-Z]+/;
    var filterLower = /[a-z]+/;
    var filterDigits = /[1-9]+/;
    if (filterUpper.test(value) && filterLower.test(value) && filterDigits.test(value)) {
        var test = 1;
    }
    if (value.length >= 6) {
        if (filterUpper.test(value) && filterLower.test(value) && filterDigits.test(value))
            return true;
        else {
            //alert("El correo electrónico no tiene el formato correcto.")
            invokeModalError("La contraseña debe tener letras en minuscula y mayusculas, además debe contener al menos un número y ser " +
                "de al menos 6 caractéres"
                );
            return false;
        }
    } else {
        invokeModalError("La contraseña debe tener letras en minuscula y mayusculas, además debe contener al menos un número y ser " +
                "de al menos 6 caractéres"
                );
        return false;
    }
}

function ascentMapped(str) {
    var stringMapped = "";
    var invalidCharacter = "+-.,;:_{}'+][*¿'¡?=)(/&%$#!°|";
    jQuery.map((str).split(''), function (letter) {
        if (letter == 'á' || letter == 'Á') {
            if (letter === letter.toUpperCase())
                stringMapped = stringMapped + 'A';
            else
                stringMapped = stringMapped + 'a';
        }
        else if (letter == 'é' || letter == 'É') {
            if (letter === letter.toUpperCase())
                stringMapped = stringMapped + 'E';
            else
                stringMapped = stringMapped + 'e';
        }
        else if (letter == 'í' || letter == 'Í') {
            if (letter === letter.toUpperCase())
                stringMapped = stringMapped + 'I';
            else
                stringMapped = stringMapped + 'i';
        }
        else if (letter == 'ó' || letter == 'Ó') {
            if (letter === letter.toUpperCase())
                stringMapped = stringMapped + 'O';
            else
                stringMapped = stringMapped + 'o';
        }
        else if (letter == 'ú' || letter == 'Ú') {
            if (letter === letter.toUpperCase())
                stringMapped = stringMapped + 'U';
            else
                stringMapped = stringMapped + 'u';
        }
        else if (invalidCharacter.indexOf(letter)>=0) {
            stringMapped = stringMapped + '';
        }
        else {
            stringMapped = stringMapped + letter;
        }
    })

    return stringMapped;
}

function invokeModalError(message) {
    $('#modalToTest').click();
    $("#errorMessageGlobal").html(message);
}
