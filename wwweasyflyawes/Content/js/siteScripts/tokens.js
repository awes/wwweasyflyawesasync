﻿var MSG_DELETE_CONFIRMATION = "Delete tokenized payment method?";
var tokenId = 0;

function seeToken(tokenID, id) {
    var urlTokenInfo = "/tokenization/tokenizationinfo";
    tokenId = id;

    // SENDS DATA
    $.getJSON(urlTokenInfo, "tokenID=" + tokenID, okTokenInfoObtained)
        .fail(function (data, textStatus, jqXHR) {
            mixpanel.track("Error", { "Number": "6001", "Page": "Payment", "url": "/tokenization/tokenizationinfo", "Details": "no se pudo conseguir info de token" });
            errorObject = JSON.parse(data.responseText);
            console.log("error:" + errorObject.Message);
            $('#message').html('<tr><td>' + "Error:" + errorObject.Message + '</td></tr>');
        })
}

function deleteToken(tokenID, id) {
    if (confirm(MSG_DELETE_CONFIRMATION)) {
        var urldeleteToken = "/tokenization/deleteTokenization?id=" + id;

        // SENDS DATA
        $.getJSON(urldeleteToken, "creditCardTokenId=" + tokenID, okTokenAdded)
            .fail(function (data, textStatus, jqXHR) {
                mixpanel.track("Error", { "Number": "6002", "Page": "Payment", "url": "/tokenization/deleteTokenization", "Details": "no se pudo eliminar token" });
                errorObject = JSON.parse(data.responseText);
                console.log("error:" + errorObject.Message);
                $('#message').html('<tr><td>' + "Error:" + errorObject.Message + '</td></tr>');
            })
    }
}

function disableToken(tokenID, id) {
    var urlDisableToken = "/tokenization/disableTokenization?id=" + id;

    // SENDS DATA
    $.getJSON(urlDisableToken, "creditCardTokenId=" + tokenID, okTokenAdded)
        .fail(function (data, textStatus, jqXHR) {
            mixpanel.track("Error", { "Number": "6003", "Page": "Payment", "url": "/tokenization/disableTokenization", "Details": "no se pudo deshabilitar token" });
            errorObject = JSON.parse(data.responseText);
            console.log("error:" + errorObject.Message);
            $('#message').html('<tr><td>' + "Error:" + errorObject.Message + '</td></tr>');
        })
}

function enableToken(tokenID, id) {
    var urlEnableToken = "/tokenization/enableTokenization?id=" + id;

    // SENDS DATA
    $.getJSON(urlEnableToken, "creditCardTokenId=" + tokenID, okTokenAdded)
        .fail(function (data, textStatus, jqXHR) {
            mixpanel.track("Error", { "Number": "6004", "Page": "Payment", "url": "/tokenization/enableTokenization", "Details": "no se pudo habilitar token" });
            errorObject = JSON.parse(data.responseText);
            console.log("error:" + errorObject.Message);
            $('#message').html('<tr><td>' + "Error:" + errorObject.Message + '</td></tr>');
        })
}

function newToken() {
    //var postObject = {
    //    "payerId": $("#payerId").val(),
    //    "paymentMethod": $("#paymentMethod").val(),
    //    "name": $("#name").val(),
    //    "identificationNumber": $("#identificationNumber").val(),
    //    "number": $("#number").val(),
    //    "expirationDate": $("#expirationDate").val()
    //};
    var postObject = {
        "payerId": $("#payerId").val(),
        "paymentMethod": $("#paymentMethodToken").val(),
        "name": $("#nameOnCardToken").val(),
        "identificationNumber": $("#identificationNumberToken").val(),
        "number": $("#cardNumberToken").val(),
        "expirationDate": $("#expirationYearToken").val() + "/" + $("#expirationMonthToken").val()
    };

    var urlAddToken = "/tokenization/addTokenization";

    // SENDS DATA
    $.ajax({
        url: urlAddToken,
        data: { postObject: JSON.stringify(postObject) },
        traditional: true,
        success: okTokenAdded
    })
        .fail(function (data, textStatus, jqXHR) {
            mixpanel.track("Error", { "Number": "6005", "Page": "Payment", "url": "/tokenization/addTokenization", "Details": "no se pudo añadir token" });
            errorObject = JSON.parse(data.responseText);
            console.log("error:" + errorObject.Message);
            $('#message').html('<tr><td>' + "Error:" + errorObject.Message + '</td></tr>');
        })
}

function okTokenAdded(data, textStatus, jqXHR) {
    alert("Entra");
    log('okTokenAdded');
    var rows = [];

    $('#message').html('<tr><td>' + data.OK + '</tr>');

    $.each(data.data, function (key, item) {
        rowString = "<tr><td>%1$s</td><td>%2$s</td><td>%3$s</td><td>";
        if (item.isEnabled)
            rowString += "<a href=\"javascript:disableToken('%4$s',%1$s)\">Disable</a><br />";
        else
            rowString += "<a href=\"javascript:enableToken('%4$s',%1$s)\">Enable</a><br />";
        rowString += "<a href=\"javascript:seeToken('%4$s',%1$s)\">See token</a><br />";
        rowString += "<a href=\"javascript:historyToken('%4$s')\">See usage history</a><br /></td></tr>";

        rows.push(sprintf(rowString, item.ID, item.name, item.notes, item.token));
        $("#" + item.token).html('');
    });

    $("#tokenList").html(rows.join(""));

    log('okTokenListUpdated');

    var amountTokens = [];

    $(".tokenDetailclass").each(function (i) {
        amountTokens.push($(this).attr("idToken"));
    })

    tokenChanged(data.data, amountTokens);

    $('#message').html('<tr><td>Token Added.</tr>');
}

function tokenChanged(tokenBind, amountTokens) {
    $.each(tokenBind, function (key, item) {
        var htmlToAppend = '';
        var href = "panel1c" + item.ID;
        var tokenDetailID = item.token;
        var paymentMethod = item.paymentMethod.toLowerCase();
        if (paymentMethod == "amex") {
            paymentMethod = "american";
        }
        if (paymentMethod == "mastercard") {
            paymentMethod = "master";
        }
        var isRendered = $.inArray(item.ID+"", amountTokens);
        if (isRendered == -1) {
            htmlToAppend = htmlToAppend + '<dd class="accordion-navigation" id="TokenCards">' +
            '<div class="accordion_item" onclick="tokenSelect(' + "'" + item.ID + "'" + ');">' +
            '<input type="radio" name="radiog_dark" id="radioToken-' + item.ID + '" class="css-checkbox " />' +
            '<label for="radioToken-' + item.ID + '" class="css-label radGroup2">  </label>' +
            '<a href="#' + href + '" class="flecha_accordion" aria-expanded="false" onclick="seeToken(' + "'" + item.token + "'" + ', ' + item.ID + ');"></a>' +
            '<div class="' + paymentMethod + '"></div>' +
            '<div class="small-8 columns">' +
            '<span id="spnToken-' + item.ID + '" title="' + item.token + '" rel="' + item.paymentMethod + '" class="' + item.name + '">' + item.name + '-' + item.paymentMethod + '</span>' +
            '</div>' +
            '<div class="small-4 columns">' +
            '' + item.ID +
            '</div>' +
            '</div>' +
            '<div id="' + href + '" class="content">' +
            '<div id="' + tokenDetailID + '" class="tokenDetailclass" idToken="' + item.ID + '">' +
            '</div>' +
            '</div>' +
            '</dd>';
            $("#TokenCards").append(htmlToAppend);
        }
    })
}

function okTokenInfoObtained(data, textStatus, jqXHR) {

    log('okTokenInfoObtained');
    var rows = [];

    $('#message').html('<tr><td>' + data.OK + '</tr>');

    $.each(data.data, function (key, item) {
        //rows.push("<tr><td>Name</td><td>" + tokenInfo.name + '</td></tr>');
        //rows.push("<tr><td>number</td><td>" + tokenInfo.maskedNumber + '</td></tr>');
        //rows.push("<tr><td>paymentMethod</td><td>" + tokenInfo.paymentMethod + '</td></tr>');
        //rows.push("<tr><td colspan='2'><a href='javascript:deleteToken(\"" + tokenInfo.creditCardTokenId + "\", " + tokenId + ")'>Delete</a></td></tr>'");
        rows.push("<div class='large-3 small-12 columns'><span>Nombre en la Tarjeta de crédito</span>" + item.name + '</div>');
        rows.push("<div class='large-3 small-12 columns'><span>Tarjeta</span>" + item.maskedNumber + ' ' + item.paymentMethod + '</div>');
        rows.push("<div class='large-2 small-12 columns'><a class='bot_eliminar href='javascript:deleteToken(" + item.creditCardTokenId + ", " + tokenId + "')>Eliminar</a></div>");
        $("#" + item.creditCardTokenId).html(rows.join(""));
        rows = [];
    })

    log('okTokenInfoUpdated');

    $('#message').html('<div>Token obtained.</div>');
}