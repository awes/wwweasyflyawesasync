﻿var languages;
var flightType = 1; // default roundtrip
var months = {};
// contains a list of the lastest searches of the current user session with the enough info to redirect to flight page
var lastestSearches = []
var charged = 0;
var cityName = '';
var dateProvider = (function () {
    var SelectedReturnDate = new Date();
    var SelectedDepartureDate = new Date();

    function formatDate(stringDate) {
        var parts = stringDate.split("-");

        if (parts.length != 3)
            return null;

        try {
            return new Date(parseInt(parts[2]), parseInt(parts[1]), parseInt(parts[0]));
        } catch (e) {
            return null;
        }
    }
    return {
        getSelectedReturnDate: getSelectedReturnDate,
        getSelectedDepartureDate: getSelectedDepartureDate
    };
})();
$(document).ready(function () {
    if (charged == 1) {
        return false;
    }
    charged = 1;
    updateFlightType();
    $.datepicker.regional['cs'] = {
        closeText: 'Close Text',
        prevText: 'Prev Text',
        nextText: 'Next Text',
        currentText: 'Current Text',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto',
          'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['D','L', 'M', 'X', 'J', 'V', 'S'],
        dayNamesMin: ['D','L', 'M', 'X', 'J', 'V', 'S'],
        weekHeader: 'Week Header',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };

    $.datepicker.setDefaults($.datepicker.regional['cs']);
    $("#citiesContact").change(function(){
        var telphone=$("#citiesContact option:selected").attr("name");
        $("#telphone").val(telphone);
    })
    $("#citiesPage").change(function(){
        var telphone=$("#citiesPage option:selected").attr("name");
        $("#telphonePage").val(telphone);
    })
    //searchLanguages("key","keyDefault");
    $("#divLogIn").click(function () {
        var pass = $("#passwordHome").val();
        var email = $("#emailHome").val();
        logIn(email, pass);
    });

    window.onpopstate = urlBackButton;
    var url = $("#cannotUseBack").val();
    if (url == "/payment") {
        window.history.pushState({}, null, '');
    }
    isWrongDestination();
    accumulationInfo();
    //getSEO();
    //searchLogo();
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    bringMonthsOfYearResources();
    
    $("#lastestSearch").change(function () {

    })

    $("#form").submit(function () {
        $.blockUI({
            message: $('#domMessage'),
            css: { backgroundColor: 'transparent', color: '#fff', border: "none" }
        });

        if ($("#newItem").val() != "") {

            var OriginID = $("#origins").val();
            var destinationID = $("#destinations").val();
            var originIATA = $("#origins option:selected").html();
            if (originIATA != "Seleccione origen") {
                originIATA = originIATA.split("(")[1].split(")")[0];
            }           
            var destinationIATA = $("#destinations option:selected").html();
            if (destinationIATA != "Seleccione destino") {
                destinationIATA = destinationIATA.split("(")[1].split(")")[0];
            }
            var departureDate = $("#SelectedDepartureDate").val();
            var returnDate = $("#SelectedReturnDate").val();
            var adult = $("#adt").val();
            var child = $("#chd").val();
            var infant = $("#inf").val();
            var promotion = $("#promotionID").val();

            var urlFlightAvail = "/base/postAvail?OriginID=" + OriginID + "&destinationID=" + destinationID + "&originIATA=" + originIATA + "&destinationIATA=" + destinationIATA + "&departureDate=" + departureDate + "&returnDate=" + returnDate + "&flightType=" + flightType + "&adult=" + adult + "&child=" + child + "&infant=" + infant + "&promotion=" + promotion;
            if (originIATA != "Seleccione origen") {
            if (destinationIATA != "Seleccione destino") {
                if (departureDate != "") {
                    if ($("#rdoIda").is(':checked')) {
                        urlFlightAvail = "/base/postAvail?OriginID=" + OriginID + "&destinationID=" + destinationID + "&originIATA=" + originIATA + "&destinationIATA=" + destinationIATA + "&departureDate=" + departureDate + "&returnDate=" + returnDate + "&flightType=" + flightType + "&adult=" + adult + "&child=" + child + "&infant=" + infant + "&promotion=" + promotion;
                        $.get(urlFlightAvail, null, availRedirect)
                        .fail(function (jqXHR, textStatus, errorThrown) {
                            $.unblockUI();
                            mixpanel.track("Error", { "Number": "1001", "Page": "Layout", "url": "/base/postAvail", "Details": "no se pudo consultar los vuelos" });
                            errorObject = JSON.parse(jqXHR.responseText);
                            log("submit::error:" + errorObject.Message);
                        })
                        .always(function () {
                            //// AQUI HAY QUE PONER LIMPIEZA -- OPCIONAL
                            //alert('getJSON request ended!');
                        });
                    } else {
                        if (returnDate == "") {
                            invokeModalError("Debe seleccionar una fecha de regreso");
                            $.unblockUI();
                        } else {
                            $.get(urlFlightAvail, null, availRedirect)
                        .fail(function (jqXHR, textStatus, errorThrown) {
                            $.unblockUI();
                            mixpanel.track("Error", { "Number": "1001", "Page": "Layout", "url": "/base/postAvail", "Details": "no se pudo consultar los vuelos" });
                            errorObject = JSON.parse(jqXHR.responseText);
                            log("submit::error:" + errorObject.Message);
                        })
                        .always(function () {
                            //// AQUI HAY QUE PONER LIMPIEZA -- OPCIONAL
                            //alert('getJSON request ended!');
                        });
                        }
                    }
                } else {
                    invokeModalError("Debe seleccionar una fecha de salida");
                    $.unblockUI();
                }
            } else {
                invokeModalError("Debe seleccionar un destino");
                $.unblockUI();
            }
            } else {
                invokeModalError("Debe seleccionar un origen");
                $.unblockUI();
            }
            return false;
        }
    });
    if ($("#origins option:selected").html() != "Seleccione origen" && typeof($("#origins option:selected").html())!='undefined') {
        loadDestinationsData($("#origins").val());
    };
    $("#origins").change(function () {
        if ($("#origins").val() != 0) {
            loadDestinationsData($("#origins").val());
        } else {
            $("#destinations").html("");
            $("#destinations").append('<option value="0">Seleccione destino</option>');
        }
    });
    $("#rdoIda").change(updateFlightType);
    $("#rdoVuelta").change(updateFlightType);
    $(".fecha_regreso").datepicker({
        minDate: 0,
        altField: "#SelectedReturnDate",
        dateFormat: "dd-mm-yy",
        beforeShowDay: function (date) {
            var minDate = dateProvider.getSelectedDepartureDate();
            var maxDate = dateProvider.getSelectedReturnDate();

            log(logHead, "minDate:" + minDate == null ? 'null' : minDate + "-");
            log(logHead, "maxDate:" + maxDate == null ? 'null' : maxDate + "-");

            if (minDate & maxDate) {
                if (date >= minDate && date <= maxDate) {
                    return [true, 'ui-state-error', 'tooltipText'];
                }
            }
            return [true, '', ''];
        },
        onSelect: function (dateText, calFechaIda) {
            $(this).change();
        }
    });

    log(logHead, "fecha_regreso intialized");

    $(".fecha_ida").datepicker({
        altField: "#SelectedDepartureDate",
        minDate: 0,
        dateFormat: "dd-mm-yy",
        onSelect: function (dateText, calFechaIda) {
            $(".fecha_ida").datepicker("hide");
            $(".fecha_regreso").datepicker("setDate", dateText);
            $(".fecha_regreso").datepicker("option", "minDate", dateText);

            if (flightType == 1) {
                setTimeout(function () {
                    $('.fecha_regreso').datepicker("show")
                }, 50);
            }
            $(this).change();
            $("#fechaRegreso").change();
            
        },
        beforeShowDay: function (date) {
            var returnDate = $("#fechaRegresoHidden").val();
            if (!(typeof returnDate == 'undefined' || returnDate == "")) {
                returnDateArray = returnDate.split("-");
                dateToCampare = new Date(returnDateArray[2], returnDateArray[1] - 1, returnDateArray[0]);
                if (date.getDay() == dateToCampare.getDay() && date.getMonth() == dateToCampare.getMonth() && date.getYear() == dateToCampare.getYear()) {
                    $(this).addClass("ui-state-active");
                    return [true, '', ''];
                } else {
                    return [true, '', ''];
                }
            } else {
                return [true, '', ''];
            }
        }
    });

    log(logHead, "fecha_ida intialized");

    var numberOfMonths = $(".fecha_ida, .fecha_regreso").datepicker("option", "numberOfMonths");

    $(".fecha_ida, .fecha_regreso").datepicker("option", "numberOfMonths", [1, 2]);

    $(".fecha_ida, .fecha_regreso").on('focus', function () {
    var monthFirst = $("div.ui-datepicker-group-first").find("span.ui-datepicker-month").text();
    if (monthFirst in months) {
        $("div.ui-datepicker-group-first").find("span.ui-datepicker-month").text(months[monthFirst]);
    }
    var monthLast = $("div.ui-datepicker-group-last").find("span.ui-datepicker-month").text();
    if (monthLast in months) {
        $("div.ui-datepicker-group-last").find("span.ui-datepicker-month").text(months[monthLast]);
    }
    $(this).datepicker("show")
    var dim = $(this).offset();
    ////$("#ui-datepicker-div").offset({
    ////    top: dim.top - 15,
    ////    left: dim.left + 280,
    ////});
    $('#ui-datepicker-div').css({ top: (dim.top - 15) + 'px', left: (dim.left + 280) + 'px' });
    //event.stopPropagation();
    //event.preventDefault();
    });
    $(".cityDateDeparture, .cityDateArrival").on('focus', function () {
        //$(this).datepicker("hide")
        //$(this).datepicker("show")
        var dim = $(this).offset();
        var classCalled = $(this).attr('class');
        var classToChange = "";
        if (classCalled.indexOf("cityDateDeparture") > -1) {
            classToChange = "cityDateDeparture";
        } else {
            classToChange = "cityDateArrival";
        }
        
        if (!($('#ui-datepicker-div').hasClass("pg_ciudad"))) {
            $('#ui-datepicker-div').addClass("pg_ciudad");
        }
        var dephase = dim.top + 50;
        $('#ui-datepicker-div').css({ top: dephase + 'px', left: (dim.left) + 'px'});
    })

    $("#statePNR").click(function () {
        invokeModalError("<h2>Consulte aquí el estado de su reserva:</h2></br><input id='stateRes' type='text' placeholder='Cod. reserva'/><input id='stateButton' class='bot_naranja' type='button' value='buscar'></br><div id='stateResponse'></div>");
        $("#stateButton").click(function () {
            $.blockUI();
            var pnr = $("#stateRes").val();
            
            $.getJSON("/base/getPaymentState", {PNR:pnr} , paymentStateResponse)
            .fail(function (jqXHR, textStatus, data) {
                $.unblockUI();
                mixpanel.track("Error", { "Number": "1002", "Page": "Layout", "url": "/base/getPaymentState", "Details": "no se pudo consultar estado de reserva" });
                errorObject = JSON.parse(jqXHR.responseText);
                log("processPurchase::error:" + errorObject.Message);
            })
        })
    })

    $("#giveOpinion").click(function () {
        var html = "<h2>Denos su opinión</h2></br>" +
            "<table style='border:none;'>" +
            //"<div style='width:200px;height:20px'><p>Nombre: </p><input id='nameOpinion' type='text' /></div></br>" +
            //"<div style='width:200px;height:20px'><p>Teléfono: </p><input id='telephoneOpinion' type='text' /></div></br>" +
            //"<div style='width:200px;height:20px'><p>Email: </p><input id='emailOpinion' type='text' /></div></br>" +
            //"<div style='width:200px;height:20px'><p>Mensaje: </p><textarea id='messageOpinion' rows='4' cols='50'></textarea></div></br>" +
            //"<input id='opinionButton' class='bot_naranja' type='button' value='Enviar'></br>" +
            //"<div id='responseOpinion'></div>" +
            "<tr><td><p>Nombre: </p></td><td><input id='nameOpinion' type='text' /></td></tr>" +
            "<tr><td><p>Teléfono: </p></td><td><input id='telephoneOpinion' type='text' /></td></tr>" +
            "<tr><td><p>Email: </p></td><td><input id='emailOpinion' type='text' /></td></tr>" +
            "<tr><td><p>Mensaje: </p></td><td><textarea id='messageOpinion' rows='4' cols='50'></textarea></td></tr>" +
            "</table>" +
            "<input id='opinionButton' class='bot_naranja4' type='button' value='Enviar' style='margin-left:230px'></br>" +
            "<div id='responseOpinion'></div>";
        invokeModalError(html);
        $("#opinionButton").click(function () {
            var name = $("#nameOpinion").val();
            var telphone = $("#telephoneOpinion").val();
            var email = $("#emailOpinion").val();
            var message = $("#messageOpinion").val();
            $.blockUI();

            $.getJSON("/base/opinionSend", { name: name, telphone: telphone, email: email, message: message }, opinionSendResponse)
            .fail(function (jqXHR, textStatus, data) {
                $.unblockUI();
                mixpanel.track("Error", { "Number": "1003", "Page": "Layout", "url": "/base/opinionSend", "Details": "no se pudo enviar la opinión correctamente" });
                errorObject = JSON.parse(jqXHR.responseText);
                log("processPurchase::error:" + errorObject.Message);
            })
            
        })
        
    })

    $("#inscriptionLayout").click(function () {
        var email = $("#emailToRegister").val();
        var canRegister = emailValidation(email);
        if (canRegister) {
            $.blockUI();
            $.getJSON("/base/registerLayout", { email: email }, registerLayoutResponse)
                .fail(function (jqXHR, textStatus, data) {
                    $.unblockUI();
                    mixpanel.track("Error", { "Number": "1004", "Page": "Layout", "url": "/base/registerLayout", "Details": "no se pudo hacer inscripción correctamente" });
                    errorObject = JSON.parse(jqXHR.responseText);
                    log("processPurchase::error:" + errorObject.Message);
                })
        }
    })

    $(".input_pasajeros").focus(function (event) {
        $(".pasajeros").fadeIn("slow");
    });

    $(document).click(function (e) {
        if (!$(e.target).hasClass("input_pasajeros")
            && $(e.target).parents(".pasajeros").length === 0) {
            $(".pasajeros").fadeOut();
        }
    });
    $(".codigo_prom").focus(function (event) {
        $(".ico_codigo").css("color", "#E17125");
        $(".codigo_prom").attr("placeholder", "ESCRIBA EL CODIGO");
    });
    $('#radio5').change(function () {
        $('.fecha_regreso').prop('disabled', true);
        $('.returnDateRemove').show();
    });
    $('#radio4').change(function () {
        $('.fecha_regreso').prop('disabled', false);
        $('.returnDateRemove').hide();
    });

    //loadDestinationsData($("#origins").val())
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";

    //Array auxiliar and default if the months have not been charged yet or never charged (spain for default) 
    var monthES = new Array();
    monthES[0] = "Enero";
    monthES[1] = "Fabrero";
    monthES[2] = "Marzo";
    monthES[3] = "Abril";
    monthES[4] = "Mayo";
    monthES[5] = "Junio";
    monthES[6] = "Julio";
    monthES[7] = "Agosto";
    monthES[8] = "Septiembre";
    monthES[9] = "Octubre";
    monthES[10] = "Noviembre";
    monthES[11] = "Diciembre";

    $("#fechaSalida").change(function () {
        $("#fechaSalidaHidden").val($(this).val());
        var date = $(this).val();
        var daySelected = parseInt(date.split('-')[0], 10);
        var monthSelected = parseInt(date.split('-')[1], 10);
        if (monthSelected > 0 && monthSelected <= 12) {
            var countMonth = Object.keys(months).length;
            var canContinue = false;
            if (countMonth > 0) {
                canContinue = true;
            }
            if (month[monthSelected-1] in months) {
                $(this).val(months[month[monthSelected-1]] + " " + daySelected);
            }
            if (!canContinue) {
                $(this).val(monthES[monthSelected - 1] + " " + daySelected);
            }
        }
        //$("#fechaRegreso").datepicker("show");
    })
    $("#fechaRegreso").change(function () {
        $("#fechaRegresoHidden").val($(this).val());
        var date = $(this).val();
        var daySelected = parseInt(date.split('-')[0], 10);
        var monthSelected = parseInt(date.split('-')[1], 10);
        if (monthSelected > 0 && monthSelected <= 12) {
            var countMonth = Object.keys(months).length;
            var canContinue = false;
            if (countMonth > 0) {
                canContinue = true;
            }
            if (month[monthSelected-1] in months) {
                $(this).val(months[month[monthSelected-1]] + " " + daySelected);
            }
            if (!canContinue) {
                $(this).val(monthES[monthSelected - 1] + " " + daySelected);
            }
        }
    })
    
    
})


function getDestinationsFunc(ID) {
    loadDestinationsData(ID);
}


function getSEO() {
    //$.blockUI();
    $.getJSON("/base/getSEO", null, function (data, textStatus, jqXHR) {
        if (data.OK == "SUCCESS") {
            $('meta[name=description]').remove();
            $('meta[name=keywords]').remove();
            $('head').append('<meta name="description" content="' + data.data.descriptionSEO + '">');
            $('head').append('<meta name="keywords" content="' + data.data.keywords + '">');
        }
        $.unblockUI();
        
    }).fail(function (jqXHR, textStatus, data) {
        $.unblockUI();
        mixpanel.track("Error", { "Number": "1005", "Page": "Layout", "url": "/base/getSEO", "Details": "no se pudo traer SEO" });
        errorObject = JSON.parse(jqXHR.responseText);
        log("processPurchase::error:" + errorObject.Message);
    });
}
function registerLayoutResponse(data, textStatus, jqXHR) {
    if (data.OK == "SUCCESS") {
        invokeModalError(data.data);
    } else {
        if (data.data == "email") {
            invokeModalError("El correo con el que está intentando registrarse ya ha sido registrado");
        }
        if (data.data == "error") {
            invokeModalError("Ha ocurrido un error en el momento de inscribirse, por favor vuelva a intentarlo más tarde o comuniquese con nuestro call center al teléfono 4148095-94 o al correo reservas.call@easyfly.com.co");
        }
    }
    $.unblockUI();
}
function opinionSendResponse(data, textStatus, jqXHR) {
    $("#responseOpinion").html("<p>" + data.data + "</p>");
    $.unblockUI();
}
function paymentStateResponse(data, textStatus, jqXHR) {
    if (data.OK == "SUCCESS") {
        $("#stateResponse").html("<p style='float: left;width: 280px;padding-left: 0px;text-align: -webkit-left;'>Cod. reserva: " +
            data.data.pnr + "</p></br><p style='float: left;width: 280px;padding-left: 0px;text-align: -webkit-left;'>CUS: " +
            data.data.cus + "</p></br><p style='float: left;width: 280px;padding-left: 0px;text-align: -webkit-left;'>Valor: " +
            data.data.cop + " " +
            data.data.value + "</p></br><p style='float: left;width: 280px;padding-left: 0px;text-align: -webkit-left;'>Estado: " +
            data.data.state + "</p>");
    } else {
        $("#stateResponse").html("<p>la reserva " + data.data.pnr + " no pudo ser encontrada, porfavor comuniquese con nuestro call center al teléfono 4148095-94 o al correo reservas.call@easyfly.com.co</p>");
    }
    $.unblockUI();
}
function availRedirect() {
    setTimeout(document.location.href = '/flights', 100);
}

function formatDate(stringDate) {
    var parts = stringDate.split("-");

    if (parts.length != 3)
        return null;

    try {
        return new Date(parseInt(parts[2]), parseInt(parts[1]), parseInt(parts[0]));
    } catch (e) {
        return null;
    }
}

function getSelectedReturnDate() {
        return formatDate($("#SelectedReturnDate").val());
    }
function getSelectedDepartureDate() {
        return formatDate($("#SelectedDepartureDate").val());
}

function bringMonthsOfYearResources() {
    if (Object.keys(months).length == 0) {
        //$.blockUI();
        $.getJSON("/base/getMonthLanguage", null, function (data, textStatus, jqXHR) {
            var countMonth = Object.keys(months).length;
            if (countMonth == 0) {
                $.each(data.data, function (key, value) {
                    months[key] = value;
                });
            }
            $.unblockUI();
        }).fail(function (jqXHR, textStatus, data) {
            $.unblockUI();
            mixpanel.track("Error", { "Number": "1006", "Page": "Layout", "url": "/base/getMonthLanguage", "Details": "no se pudo lenguaje para calendario" });
            errorObject = JSON.parse(jqXHR.responseText);
            log("processPurchase::error:" + errorObject.Message);
        });
    }
}

// sets the variable according to the selected type (checkbox)
function updateFlightType() {
    if ($("#rdoIda").is(':checked')) {
        flightType = 0;
        if (!($(".fecha_regreso").hasClass("notDesapear"))) {
            //$(".fecha_regreso").hide(800);
            $('.returnDateRemove').hide(800);
        } else {
            $(".icon_regreso").fadeTo("slow", 0.0001, function () {
                // Animation complete.
            });
            $(".icon_regreso2").fadeTo("slow", 0.0001, function () {
                // Animation complete.
            });
            $(".notDesapear").fadeTo("slow", 0.0001, function () {
                // Animation complete.
            });
        }
    }
    else {
        flightType = 1;
        if (!($(".fecha_regreso").hasClass("notDesapear"))) {
            //$(".fecha_regreso").show(800);
            $('.returnDateRemove').show(800);
        } else {
            
            $(".icon_regreso").fadeTo("slow", 1, function () {
                // Animation complete.
            });
            $(".icon_regreso2").fadeTo("slow", 1, function () {
                // Animation complete.
            });
            $(".notDesapear").fadeTo("slow", 1, function () {
                // Animation complete.
            });
        }
    }
}

function flightsFind() {
    $("#form").submit();
}

function loadDestinationsData(originID) {
    //$.blockUI({
    //    message: $('#domMessage'),
    //    css: { backgroundColor: 'transparent', color: '#fff', border: "none" }
    //});

    urlDestinations = "/home/destinations?originID=" + originID;

    $.getJSON(urlDestinations, null, addedDestinations)
        .fail(function (jqXHR, textStatus, data) {
            $.unblockUI();
            mixpanel.track("Error", { "Number": "1007", "Page": "Layout", "url": "/home/destinations", "Details": "no se pudo traer destinos" });
            errorObject = JSON.parse(jqXHR.responseText);
            log("loadDestinationsData::error:" + errorObject.Message);
        })
}

function addedDestinations(data) {
    var items = [];

    destinations = JSON.parse(data.data);
    $("#destinations").html("");
    $("#destinations").append('<option value=\'\'>Seleccione destino</option>');
    $.each(destinations, function (key, destination) {
        //items.push("<option value='" + destination.destinationID + "'>" + destination.name + " (" + destination.IATA + ")" + "</option>");
        $("#destinations").append("<option value='" + destination.destinationID + "'>" + destination.name + " (" + destination.IATA + ")" + "</option>");
    });

    //var header = '<option value=\'\'>Seleccione destino</option>';

    //$('#destinations').html(header + items);

    $.unblockUI();
}

function increaseAdult() {
    var numAdt = $("#adt").val();
    var numChd = $("#chd").val();
    var numInf = $("#inf").val();
    if (numAdt < 6) {
        numAdt = parseInt(numAdt) + 1;
        $("#adt").attr("value", numAdt);
        $("#pax").attr("placeholder", numAdt + " Adultos, " + numChd + " Niños, " + numInf + " Infantes");
    }
}

function decreaseAdult() {
    var numAdt = $("#adt").val();
    var numChd = $("#chd").val();
    var numInf = $("#inf").val();
    if (numAdt > 1) {
        numAdt = parseInt(numAdt) - 1;
        $("#adt").attr("value", numAdt);
        $("#pax").attr("placeholder", numAdt + " Adultos, " + numChd + " Niños, " + numInf + " Infantes");
    }
}

function increaseChild() {
    var numAdt = $("#adt").val();
    var numChd = $("#chd").val();
    var numInf = $("#inf").val();
    if (numChd < 5) {
        numChd = parseInt(numChd) + 1;
        $("#chd").attr("value", numChd);
        $("#pax").attr("placeholder", numAdt + " Adultos, " + numChd + " Niños, " + numInf + " Infantes");
    }
}

function decreaseChild() {
    var numAdt = $("#adt").val();
    var numChd = $("#chd").val();
    var numInf = $("#inf").val();
    if (numChd > 0) {
        numChd = parseInt(numChd) - 1;
        $("#chd").attr("value", numChd);
        $("#pax").attr("placeholder", numAdt + " Adultos, " + numChd + " Niños, " + numInf + " Infantes");
    }
}

function increaseInfant() {
    var numAdt = $("#adt").val();
    var numChd = $("#chd").val();
    var numInf = $("#inf").val();
    if (numInf < 5) {
        numInf = parseInt(numInf) + 1;
        $("#inf").attr("value", numInf);
        $("#pax").attr("placeholder", numAdt + " Adultos, " + numChd + " Niños, " + numInf + " Infantes");
    }
}

function decreaseInfant() {
    var numAdt = $("#adt").val();
    var numChd = $("#chd").val();
    var numInf = $("#inf").val();
    if (numInf > 0) {
        numInf = parseInt(numInf) - 1;
        $("#inf").attr("value", numInf);
        $("#pax").attr("placeholder", numAdt + " Adultos, " + numChd + " Niños, " + numInf + " Infantes");
    }
}

function urlBackButton() {
    var url = $("#cannotUseBack").val();
    if (url == "/payment") {
        window.history.go(1);
        return true;
    } else {
        //window.history.back();
        return true;
    } 
}
function isWrongDestination() {
    var url = $("#cannotUseBack").val();
    if (url == "error") {
        window.history.back();
        $("#cannotUseBack").remove();
    }
}

function searchLogo() {
    urlLogIn = "/base/getLogo";
    //$.blockUI();
    $.getJSON(urlLogIn, null, logoUpdate)
            .fail(function (jqXHR, textStatus, data) {
                $.unblockUI();
                mixpanel.track("Error", { "Number": "1008", "Page": "Layout", "url": "/base/getLogo", "Details": "no se pudo cargar logo" });
                errorObject = JSON.parse(jqXHR.responseText);
                log("responseLogIn::error:" + errorObject.Message);
            })
}

function logoUpdate(data, textStatus, jqXHR) {
    $("#logoLayout").css("background", "url('" + data.data.urlImageLogo + "')")
    $("#logoLayout").css("background-repeat", "no-repeat")
    $("#logoLayout").attr("title", data.data.titleLogo)
    $.unblockUI();
}

function searchContactInfo(){
    $.getJSON("/base/contactInfo", null, changedContactInfo)
    //.fail(function (data, textStatus, jqXHR) {
    //    console.log("error:" + data.responseJSON.Message);
    //    $('#contentBody').html('<tr><td>' + "error:" + data.Message + '</tr>');
    //})
}

function changedContactInfo(data, textStatus, jqXHR){
    $.each(data.data,function(i,value){
        $("#citiesContact").append("<option name='"+value+"' value='"+i+"'>"+i+"</option>");
        $("#citiesPage").append("<option name='"+value+"' value='"+i+"'>"+i+"</option>");
        $("#citiesContactPage").append(i+"<br>");
        $("#telphonesContactPage").append(value + "<br>");
        $("#telephonesFooter").append("<span>• "+i+"   "+value+"</span>");
    })
    var telphone=$("#citiesContact option:selected").attr("name");
    $("#telphone").val(telphone);
    telphone=$("#citiesPage option:selected").attr("name");
    $("#telphonePage").val(telphone);
    setTimeout(getCity(true), 3000);
}

searchContactInfo();

function logIn(email, pass) {
    
    urlLogIn = "/base/logInLayout?email=" + email + "&pass=" + pass;
    $.getJSON(urlLogIn, null, responseLogIn)
            .fail(function (jqXHR, textStatus, data) {
                mixpanel.track("Error", { "Number": "1009", "Page": "Layout", "url": "/base/logInLayout", "Details": "no se pudo loguear usuario" });
                errorObject = JSON.parse(jqXHR.responseText);
                log("responseLogIn::error:" + errorObject.Message);
            })
}
function responseLogIn(data, textStatus, jqXHR) {
    
    log('okprocessLogInHome');

    $('#message').html('<tr><td>' + data.OK + '</tr>');
    if (data.data == "OK")
        if (data.url == "")
            document.location.href = "/";
        else
            document.location.href = data.url;
    else {
        invokeModalError("Hubo un error al intentar ingresar, porfavor verifique que escribió bien sus credenciales");
        $("#emailHome").val("");
        $("#passwordHome").val("");
    }

    $('#message').html('<div>Log in obtained.</div>');

}

function accumulationInfo() {
    urlLogIn = "/base/accumulationInfo";
    $.getJSON(urlLogIn, null, responseAccumulation)
            .fail(function (jqXHR, textStatus, data) {
                mixpanel.track("Error", { "Number": "1010", "Page": "Layout", "url": "/base/accumulationInfo", "Details": "no se pudo traer acomulado puntos" });
                errorObject = JSON.parse(jqXHR.responseText);
                log("responseAcumulation::error:" + errorObject.Message);
            })
}

function responseAccumulation(data, textStatus, jqXHR) {

    log('okresponseAcumulation');
    var rows = [];

    accumInfo = data.data[0];

    $('#message').html('<tr><td>' + data.OK + '</tr>');

    rows.push("<input type='text' id='easyPointsLayout' class='puntos_ef' value='" + data.data + "'></input>");

    $("#divAccumulation").html(rows.join(""));

    $("#easyPointsLayout").prop("readonly", true);

    $('#message').html('<div>Accumulation obtained.</div>');

}

function backToHome() {
    location.href = "/";
}

function goToOldPage() {
    $.getJSON("/base/goToOldPage", null, function (data, textStatus, jqXHR) {
        location.href = "http://www3.easyfly.com.co/";
        //location.href = "http://mdd.easyfly.com.co/";
    }).fail(function (jqXHR, textStatus, data) {
        location.href = "http://www3.easyfly.com.co/";
        //location.href = "http://mdd.easyfly.com.co/";
    });
}