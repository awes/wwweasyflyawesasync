﻿var passengers;
$("document").ready(function ($) {
    var auth = $("#isAuth");
    if (auth.html() == 'Auth') {
        loadPaxLoginData();
    }
    $(".agregar1").click(function () {
        $(".agregar_a").show()
        var heightDiv = $("#agregar_aID").height();
        var offSetDiv = $("#agregar_aID").offset().top;
        $("html, body").animate({ scrollTop: offSetDiv }, 1000);
    });
    $(".agregar2").click(function () {
        $(".agregar_a").show()
        var heightDiv = $("#agregar_aID").height();
        var offSetDiv = $("#agregar_aID").offset().top;
        $("html, body").animate({ scrollTop: offSetDiv }, 1000);
    });
    $(".agregar3").click(function () {
        $(".agregar_n").show()
        var heightDiv = $("#agregar_nID").height();
        var offSetDiv = $("#agregar_nID").offset().top;
        $("html, body").animate({ scrollTop: offSetDiv }, 1000);
    });
    $(".agregar4").click(function () {
        $(".agregar_i").show()
        var heightDiv = $("#agregar_iID").height();
        var offSetDiv = $("#agregar_iID").offset().top;
        $("html, body").animate({ scrollTop: offSetDiv }, 1000);
    });

    $("#loginPaxAP").change(function () {
        loadPaxInfoSelect($("#loginPaxAP").val(), "AP");
    });
    $("#loginPaxAS").change(function () {
        loadPaxInfoSelect($("#loginPaxAS").val(), "AS");
    });
    $("#loginPaxN").change(function () {
        loadPaxInfoSelect($("#loginPaxN").val(), "N");
    });
    $("#loginPaxI").change(function () {
        loadPaxInfoSelect($("#loginPaxI").val(), "I");
    });
    $(".cerrar1").click(function () {
        $(".agregar_a").hide()
        var offSetDiv = $("#AdultsID").offset().top;
        $("html, body").animate({ scrollTop: offSetDiv }, 1000);
    });
    $(".cerrar2").click(function () {
        $(".agregar_n").hide()
        var offSetDiv = $("#kidsID").offset().top;
        $("html, body").animate({ scrollTop: offSetDiv }, 1000);
    });
    $(".cerrar3").click(function () {
        $(".agregar_i").hide()
        var offSetDiv = $("#InfantsID").offset().top;
        $("html, body").animate({ scrollTop: offSetDiv }, 1000);
    });
    $("#txtCorreo").change(function () {
        var canContinue = emailValidation($(this).val())
        if (canContinue) {
            return true;
        } else {
            $(this).val("")
            return false;
        }
    })
})
function loadPaxInfoSelect(ID, typePax) {

    urlpaxSelect = "/pax/loadInfoPaxSelect?ID=" + ID;

    if (typePax == "AP") {
        $.getJSON(urlpaxSelect, null, addedInfoPaxSelectAP)
        .fail(function (jqXHR, textStatus, data) {
            mixpanel.track("Error", { "Number": "4001", "Page": "Pax", "url": "/pax/loadInfoPaxSelect", "Details": "no se pudo insertar Pasajero" });
            errorObject = JSON.parse(jqXHR.responseText);
            log("loadPaxInfoSelect::error:" + errorObject.Message);
        })
        $(".agregar_a").hide();
        var offSetDiv = $("#AdultsID").offset().top;
        $("html, body").animate({ scrollTop: offSetDiv }, 1000);
    }
    if (typePax == "AS") {
        $.getJSON(urlpaxSelect, null, addedInfoPaxSelectAS)
        .fail(function (jqXHR, textStatus, data) {
            mixpanel.track("Error", { "Number": "4001", "Page": "Pax", "url": "/pax/loadInfoPaxSelect", "Details": "no se pudo insertar Pasajero" });
            errorObject = JSON.parse(jqXHR.responseText);
            log("loadPaxInfoSelect::error:" + errorObject.Message);
        })
        $(".agregar_a").hide();
        var offSetDiv = $("#otherPassangersID").offset().top;
        $("html, body").animate({ scrollTop: offSetDiv }, 1000);
    }
    if (typePax == "N") {
        $.getJSON(urlpaxSelect, null, addedInfoPaxSelectN)
        .fail(function (jqXHR, textStatus, data) {
            mixpanel.track("Error", { "Number": "4001", "Page": "Pax", "url": "/pax/loadInfoPaxSelect", "Details": "no se pudo insertar Pasajero" });
            errorObject = JSON.parse(jqXHR.responseText);
            log("loadPaxInfoSelect::error:" + errorObject.Message);
        })
        $(".agregar_n").hide();
        var offSetDiv = $("#kidsID").offset().top;
        $("html, body").animate({ scrollTop: offSetDiv }, 1000);
    }
    if (typePax == "I") {
        $.getJSON(urlpaxSelect, null, addedInfoPaxSelectI)
        .fail(function (jqXHR, textStatus, data) {
            mixpanel.track("Error", { "Number": "4001", "Page": "Pax", "url": "/pax/loadInfoPaxSelect", "Details": "no se pudo insertar Pasajero" });
            errorObject = JSON.parse(jqXHR.responseText);
            log("loadPaxInfoSelect::error:" + errorObject.Message);
        })
        $(".agregar_i").hide();
        var offSetDiv = $("#InfantsID").offset().top;
        $("html, body").animate({ scrollTop: offSetDiv }, 1000);


    }
    $("#messaggeConfirmation").html();

}
function addedInfoPaxSelectAP(data) {
    passenger = JSON.parse(data.data);
    $('#txtNameAP').val(passenger.Name);
    $('#txtLastNameAP').val(passenger.LastName);
    $('#txtDocumentAP').val(passenger.DocumentNumber);
    $('#txtNameEdit').val(passenger.Name);
    $('#txtLastNameEdit').val(passenger.LastName);
    $('#txtDocumentEdit').val(passenger.DocumentNumber);
    $('#txtIdEdit').val(passenger.ID);
    $('#txtpaxTypeEdit').val(passenger.PaxType);
}
function addedInfoPaxSelectAS(data) {
    passenger = JSON.parse(data.data);
    $('#txtNameAS').val(passenger.Name);
    $('#txtLastNameAS').val(passenger.LastName);
    $('#txtDocumentAS').val(passenger.DocumentNumber);
}
function addedInfoPaxSelectN(data) {
    passenger = JSON.parse(data.data);
    $('#txtNameN').val(passenger.Name);
    $('#txtLastNameN').val(passenger.LastName);
    $('#txtDocumentN').val(passenger.DocumentNumber);
}
function addedInfoPaxSelectI(data) {
    passenger = JSON.parse(data.data);
    $('#txtNameI').val(passenger.Name);
    $('#txtLastNameI').val(passenger.LastName);
    $('#txtDocumentI').val(passenger.DocumentNumber);
}
function loadPaxLoginData() {
    //$.blockUI({
    //    message: $('#domMessage'),
    //    css: { backgroundColor: 'transparent', color: '#fff', border: "none" }
    //});

    urlDestinations = "/pax/getPaxLogin";

    $.getJSON(urlDestinations, null, addedPaxList)
        .fail(function (jqXHR, textStatus, data) {
            //$.unblockUI();
            mixpanel.track("Error", { "Number": "4002", "Page": "Pax", "url": "/pax/getPaxLogin", "Details": "no se pudo cargar Pasajeros" });
            errorObject = JSON.parse(jqXHR.responseText);
            log("loadDestinationsData::error:" + errorObject.Message);
        })
}
function addedPaxList(data) {
    var items = [];
    $("#messaggeConfirmation").html("pax was saved");
    passengers = JSON.parse(data.data);
    $.each(passengers, function (key, pax) {
        items.push("<option value='" + pax.ID + "'>" + pax.Name + " " + pax.LastName + "</option>");
    });

    var header = '<option value=\'\'>Seleccione</option>';

    $('#loginPaxAP').html(header + items);
    $('#loginPaxAS').html(header + items);
    $('#loginPaxN').html(header + items);
    $('#loginPaxI').html(header + items);
    $("#messaggeConfirmation").hide("slow");
    updateUI();
    //$.unblockUI();
}
function canContinue() {
    var canContinue = true;
    $("input[type='text']").each(function () {
        var value = $(this).val()
        if (value == "" && $(this).hasClass("single-line")) {
            canContinue = canContinue && false;
        }
    })
    if (canContinue) {
        $.blockUI();
        $("input[type='text']").each(function () {
            if ($(this).attr('id') != 'txtCorreo' && $(this).hasClass("single-line")) {
                $(this).val(ascentMapped($(this).val()).toUpperCase());
            }
        });
        $("#formPax").submit();
    } else {
        invokeModalError("Por favor llene todos los campos antes de continuar");
        return false;
    }
}
function addPaxLoginData(type) {
    //$.blockUI({
    //    message: $('#domMessage'),
    //    css: { backgroundColor: 'transparent', color: '#fff', border: "none" }
    //});
    var name = "";
    var lastName = "";
    var domument = "";
    var paxType = "";
    if (type == "A") {
        name = $("#namePaxLoginA").val();
        lastName = $("#lastNamePaxLoginA").val();
        domument = $("#documentPaxLoginA").val();
        paxType = "ADT";
        $(".agregar_a").hide();
        var offSetDiv = $("#AdultsID").offset().top;
        $("html, body").animate({ scrollTop: offSetDiv }, 1000);
    }
    else if (type == "N") {
        name = $("#namePaxLoginN").val();
        lastName = $("#lastNamePaxLoginN").val();
        domument = $("#documentPaxLoginN").val();
        paxType = "CNN";
        $(".agregar_n").hide();
        var offSetDiv = $("#kidsID").offset().top;
        $("html, body").animate({ scrollTop: offSetDiv }, 1000);
    }
    else if (type == "C") {
        name = $("#namePaxLoginC").val();
        lastName = $("#lastNamePaxLoginC").val();
        domument = $("#documentPaxLoginC").val();
        paxType = "INF";
        $(".agregar_i").hide();
        var offSetDiv = $("#InfantsID").offset().top;
        $("html, body").animate({ scrollTop: offSetDiv }, 1000);
    }
    urlLoginPax = "/pax/addPaxLogin?name=" + name + "&lastName=" + lastName + "&document=" + domument + "&paxType=" + paxType;

    $.getJSON(urlLoginPax, null, loadPaxLoginData)
        .fail(function (jqXHR, textStatus, data) {
            //$.unblockUI();
            mixpanel.track("Error", { "Number": "4003", "Page": "Pax", "url": "/pax/addPaxLogin", "Details": "no se pudo registrar Pasajero" });
            errorObject = JSON.parse(jqXHR.responseText);
            log("loadDestinationsData::error:" + errorObject.Message);
        })
    $("#namePaxLoginC").html();
    $("#lastNamePaxLoginC").html();
    $("#documentPaxLoginC").html();
    $("#namePaxLoginN").html();
    $("#lastNamePaxLoginN").html();
    $("#documentPaxLoginN").html();
    $("#namePaxLoginA").html();
    $("#lastNamePaxLoginA").html();
    $("#documentPaxLoginA").html();
}
function esTextoNombre(event) {

    cadena = parseInt(event);
    if (isNaN(cadena)) {
        return true;
    }
    else {
        $("#txtNombre").val("");
        return false;
    }
    return cadena;
}
function esTextoApellido(event) {

    cadena = parseInt(event);
    if (isNaN(cadena)) {
        return true;
    }
    else {
        $("#txtApellido").val("");
        return false;
    }
    return cadena;
}
function selectEditPax(entryID) {
    loadPaxInfoSelect(entryID, "AP")
}
function editPax() {
    var name = "";
    var lastName = "";
    var domument = "";
    var entryID;
    var paxType = "";
    name = $("#txtNameEdit").val();
    lastName = $("#txtLastNameEdit").val();
    domument = $("#txtDocumentEdit").val();
    entryID = $("#txtIdEdit").val();
    paxType = $("#txtPaxTypeEdit").val();

    urlLoginPax = "/pax/putPaxLogin?idPax=" + entryID + "&name=" + name + "&lastName=" + lastName + "&document=" + domument + "&paxType=" + paxType;

    $.getJSON(urlLoginPax, null, loadPaxLoginData)
        .fail(function (jqXHR, textStatus, data) {
            //$.unblockUI();
            mixpanel.track("Error", { "Number": "4004", "Page": "Pax", "url": "/pax/putPaxLogin", "Details": "no se pudo actualizar Pasajero" });
            errorObject = JSON.parse(jqXHR.responseText);
            log("editPax::error:" + errorObject.Message);
        })
}
function deletePax(entryID) {
    //if (!confirm('Delete entry ?'))
    //    return;

    urlLoadData = "/pax/deletePaxLogin?idPax=" + entryID;

    // GETS ALL CONFIGURATION DATA
    $.getJSON(urlLoadData, null, loadPaxLoginData)
        .fail(function (jqXHR, textStatus, data) {
            //$.unblockUI();
            mixpanel.track("Error", { "Number": "4005", "Page": "Pax", "url": "/pax/deletePaxLogin", "Details": "no se pudo eliminar Pasajero" });
            errorObject = JSON.parse(jqXHR.responseText);
            log("deletePax::error:" + errorObject.Message);
        })
}
function updateUI() {
    var rows = [];
    try {

        $.each(passengers, function (key, item) {
            rows.push(sprintf('<tr><td>%1$s</td><td>%2$s</td><td><a class="borrar" href="javascript:deletePax(%1$s)"></a><a class="editar3" href="javascript:selectEditPax(%1$s)"></a></td></tr>', item.ID, item.Name + " " + item.LastName));
        });

        $('#contentBodyPax').html(rows.join(""));

    } catch (e) {
        $('#Message').html('<tr><td>' + "error:" + e.message + '</tr>');
    }
    console.log("UI  updated " + (new Date()).toString());
}