﻿using AWESAirline.Controllers;
using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.messagingSupport;
using AWESMVCCommon.Filters;
using AWESMVCCommon.Security;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AWESCommon.Extensions;
using System.Configuration;
using AWESCommon.contentSupport;
using AWESCommon.marketingSupport;
using AWESCommon.authSupport;
using System.Text.RegularExpressions;

namespace wwweasyflyawes
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private string _encriptSalt;
        private string _value_name;
        private string _cookieName;

        public string encriptSalt
        {
            get
            {
                if (_encriptSalt == null)
                    _encriptSalt = configurationReader.read("auth.cookie.encriptSalt");

                return _encriptSalt;
            }
        }
        public string value_name
        {
            get
            {
                if (_value_name == null)
                    _value_name = configurationReader.read("auth.cookie.value.name");

                return _value_name;
            }
        }
        public string cookieName
        {
            get
            {
                if (_cookieName == null)
                    _cookieName = configurationReader.read("auth.cookie.name");

                return _cookieName;
            }
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_BeginRequest()
        {
            string clientIP = HttpContext.Current.Request.UserHostAddress;

            // CHECKS FOR COOKIE
            if (HttpContext.Current.Request.Cookies.AllKeys.Contains(cookieName))
            {
                // VALIDATES COOKIE STRUCTURE AND VALIDITY
                if (userCookie.instance.validateCookie(encriptSalt, cookieManager.readFromCookie(cookieName, value_name), clientIP))

                    // COOKIE IS VALID AND CURRENT
                    if ((userCookie.instance.isValidated) && (userCookie.instance.trackingCode.HasValue))
                    {
                        // OBTAINS USER INFO FROM DB
                        if (userAdministrator.instance.obtainUserInfo(userCookie.instance.trackingCode.Value))
                        {
                            // UPDATES COOKIE // RENEWS SESSION
                            userAdministrator.instance.writeUserCookie(userCookie.instance.trackingCode.Value.ToString());
                            userAdministrator.instance.registerUserSession(userAdministrator.instance.user.ID, userCookie.instance.trackingCode.Value.ToString());
                        }
                    }
            }

            //Hits per page
            string url = HttpContext.Current.Request.Url.AbsolutePath.ToLower();
            string urlToHit = url.Replace('/', '.');
            if (url == "/")
            {
                urlToHit = "Home";
            }
            string platformToHit = ConfigurationManager.AppSettings["platform"];
            if (HttpContext.Current.Request.UserLanguages != null)
            {
                contentHelper.postHits(platformToHit, urlToHit, HttpContext.Current.Request.UserLanguages[0].Split(';')[0]);
            }
            else {
                contentHelper.postHits(platformToHit, urlToHit, "es");
            }

            // Redirects with a specific status code (http://stackoverflow.com/questions/2111955/global-301-redirection-from-domain-to-www-domain)
            string currentUrl = HttpContext.Current.Request.Url.ToString().ToLower();//current cliente request url
            try
            {
                //SEO list 
                //Task<string> result = Task.Run(async () => await contentHelper.SEOList());

                string result = contentHelper.SEOListNotAsync();

                List<POCOSEOEntry> SEO = Newtonsoft.Json.JsonConvert.DeserializeObject<List<POCOSEOEntry>>(result);

                //PAIR VALUES STATUSCODE/DESCRIPTION THAT CONTAINS ALL THE STATUS CODE IN DB 
                //Task<Dictionary<int, string>> statusCodes = Task.Run(async () => await contentHelper.StatusCodeList());

                Dictionary<int, string> statusCodes = contentHelper.StatusCodeList();

                POCOSEOEntry SEOSelected = new POCOSEOEntry();

                // Verifying if the current url is in SEO List
                if (SEO != null)
                {
                    foreach (POCOSEOEntry item in SEO)
                        if (item.page.ToLower() == currentUrl.ToLower())
                        {
                            SEOSelected = item;
                            break;
                        }
                }

                //If current url exists in SEO list this request must be redirected to SEOSelected.newURL
                if (!string.IsNullOrEmpty(SEOSelected.newURL) && !string.IsNullOrEmpty(SEOSelected.page))
                {
                    if (currentUrl.ToLower() == SEOSelected.page.ToLower())
                    {
                        //Redirecting
                        Response.Clear();
                        Response.Status = SEOSelected.statusCode + " " + statusCodes[SEOSelected.statusCode];
                        Response.AddHeader("Location", SEOSelected.newURL);
                        Response.Redirect(SEOSelected.newURL);
                        Response.End();
                    }
                }

            }
            catch(AggregateException ex)
            { }
            // Rewrites
            //Absoluting url path 
            string currentAbsolutingPathUrl = ascentMapped(HttpContext.Current.Request.Url.AbsolutePath.ToString().ToLower());
            byte[] bytes = Encoding.Default.GetBytes(currentAbsolutingPathUrl);
            currentAbsolutingPathUrl = Encoding.ASCII.GetString(bytes);
            //get platform ID
            //Task<Dictionary<string, string>>  platforms = Task.Run(async () => await authHelper.getPlatformsAsync());
            Dictionary<string, string>  platforms = authHelper.getPlatforms();
            string platformID = platforms.FirstOrDefault(x => x.Value == ConfigurationManager.AppSettings["platform"]).Key;

            //Obtain list of rewrites
            //Task<List<POCORewriteEntry>> rewrites = Task.Run(async () => await marketingHelper.getRewritesAsync(platformID));
            List < POCORewriteEntry > rewrites = marketingHelper.getRewrites(platformID);

            foreach (POCORewriteEntry rewrite in rewrites)
            {
                try
                {
                    //Getting regular expression
                    string regex = rewrite.regexAbsolutePath;
                    //Bilding regular expression validator
                    Regex validator = new Regex(regex, RegexOptions.IgnoreCase);
                    string input = currentAbsolutingPathUrl;
                    if (validator.IsMatch(input))
                    {
                        string queryString= (validator.Match(input)).Groups[1].Value.ToString();
                        Context.RewritePath(rewrite.rewriteAbsolutePath+"?namePage="+queryString);
                    }
                }
                catch (Exception ex)
                {
                    // Nothing   
                }
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //if (Context.IsCustomErrorEnabled)
            ShowCustomErrorPage(Server.GetLastError());
        }

        protected string infoExcepcion(Exception ex)
        {
            //Get a StackTrace object for the exception
            StackTrace st = new StackTrace(ex, true);

            //Get the first stack frame
            StackFrame frame = st.GetFrame(0);

            //Get the file name
            string fileName = frame.GetFileName();

            //Get the method name
            string methodName = frame.GetMethod().Name;

            //Get the line number from the stack frame
            int line = frame.GetFileLineNumber();

            //Get the column number
            int col = frame.GetFileColumnNumber();

            return fileName + "-" + methodName + "-" + line + "-" + col + Environment.NewLine + "Excepcion:" + ex.Message;

        }

        private void ShowCustomErrorPage(Exception exception)
        {
            HttpException httpException = exception as HttpException;
            if (httpException == null)
                httpException = new HttpException(500, "Internal Server Error", exception);

            Response.Clear();
            RouteData routeData = new RouteData();

            if (exception.GetType() == typeof(NotAuthorizedException))
            {
                NotAuthorizedException ex = (NotAuthorizedException)exception;
                routeData.Values.Add("Exception", ex);
                httpException = new HttpException(403, "Access Denied", exception);
            }
            else if (exception.GetType() == typeof(NotTwoStepAuthorizedException))
            {
                NotTwoStepAuthorizedException ex = (NotTwoStepAuthorizedException)exception;
                routeData.Values.Add("Exception", ex);
                httpException = new HttpException(401, "Unathorized", exception);
            }
            else
                routeData.Values.Add("Exception", httpException);

            routeData.Values.Add("controller", "Error");
            routeData.Values.Add("fromAppErrorEvent", true);

            switch (httpException.GetHttpCode())
            {
                case 401:
                    routeData.Values.Add("action", "twoStep");
                    break;
                case 403:
                    routeData.Values.Add("action", "AccessDenied");
                    break;

                case 404:
                    routeData.Values.Add("action", "NotFound");
                    break;

                case 500:
                    routeData.Values.Add("action", "ServerError");
                    break;

                default:
                    routeData.Values.Add("action", "OtherHttpStatusCode");
                    routeData.Values.Add("httpStatusCode", httpException.GetHttpCode());
                    break;
            }

            Server.ClearError();

            try
            {
                // SEND EMAIL WITH ERROR INFO
                Task notification = Task.Factory.StartNew(() => sendExceptionNotification(httpException));
                bool sent = notification.Wait(1000);
            }
            catch (Exception)
            {
                // nothing
            }
            IController controller = new ErrorController();
            HttpContext.Current.Response.ContentType = "text/html";
            controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
        }

        private async Task<bool> sendExceptionNotification(HttpException exception)
        {
            try
            {
                bool notificationEnabled = false;
                bool.TryParse(configurationReader.read("errorNotificationEnabled"), out notificationEnabled);
                string exMessage = exception.StackTrace != null ? exception.detail() : exception.InnerException.detail();

                if (notificationEnabled)
                {
                    await messageHelper.messagingAsync(new messageEntry()
                    {
                        subject = "Unhandled exception",
                        body = "Datailed information :" + exMessage,
                        destinataires = configurationReader.read("errorNotificationEmails"),
                        Platform = ConfigurationManager.AppSettings["platform"],
                        FunctionalUnit = "Global.asax",
                        SecondaryFunctionalUnit = "Exception notification",
                        OperationMode = ConfigurationManager.AppSettings["operationMode"],
                        type = messageEntry.messageType.email
                    });
                }

                return notificationEnabled;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //Other functions
        private string ascentMapped(string stringToMapped)
        {
            string stringMapped = "";

            foreach (char letter in stringToMapped)
            {
                if (letter == 'á' || letter == 'Á')
                {
                    if (char.IsUpper(letter))
                        stringMapped = stringMapped + 'A';
                    else
                        stringMapped = stringMapped + 'a';
                }
                else if (letter == 'é' || letter == 'É')
                {
                    if (char.IsUpper(letter))
                        stringMapped = stringMapped + 'E';
                    else
                        stringMapped = stringMapped + 'e';
                }
                else if (letter == 'í' || letter == 'Í')
                {
                    if (char.IsUpper(letter))
                        stringMapped = stringMapped + 'I';
                    else
                        stringMapped = stringMapped + 'i';
                }
                else if (letter == 'ó' || letter == 'Ó')
                {
                    if (char.IsUpper(letter))
                        stringMapped = stringMapped + 'O';
                    else
                        stringMapped = stringMapped + 'o';
                }
                else if (letter == 'ú' || letter == 'Ú')
                {
                    if (char.IsUpper(letter))
                        stringMapped = stringMapped + 'U';
                    else
                        stringMapped = stringMapped + 'u';
                }
                else
                {
                    stringMapped = stringMapped + letter;
                }
            }

            return stringMapped;
        }
    }
}
