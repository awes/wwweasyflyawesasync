﻿using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using AWESMVCCommon.Security;
using AWESAirline.Models;
using AWESPaymentModel.BindingObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using AWESCommon.Extensions;
using wwweasyflyawes.Models;
using AWESAirline.Controllers;
using System.Data;

namespace wwweasyflyawes.Controllers
{
    public class SummaryController : baseController
    {
        public async Task<ActionResult> Index()
        {
            ViewBag.ETKT = new DataTable();
            ViewBag.Code = "";
            ViewBag.valueToPay = "";
            ViewBag.value = "";
            ViewBag.valueCO = "";
            ViewBag.valueYS = "";
            ViewBag.valueTA = "";
            ViewBag.valueTB = "";
            ViewBag.paxList = new List<Pax>();
            ViewBag.typeFlight = 0;
            ViewBag.origin = "";
            ViewBag.destination = "";
            ViewBag.reservationDate = DateTime.Now;
            ViewBag.departureFlight = "";
            ViewBag.returnFlight = "";
            ViewBag.emissionDate = DateTime.Now;
            ViewBag.departureClass = "";
            ViewBag.returnClass = "";
            ViewBag.departureTime = "";
            ViewBag.arrivalTime = "";
            ViewBag.departureTimeReturn = "";
            ViewBag.arrivalTimeReturn = "";
            ViewBag.paymentMethod = "";
            ViewBag.authorizationCode = "";
            ViewBag.order = "";
            //bindingTransactionResponseWrapper rspnse = new bindingTransactionResponseWrapper();
            try
            {
                DataTable ETKTInfoTable = default(DataTable);
                string payInfo = await paymentInfo();
                PaymentModel payObject = JsonConvert.DeserializeObject<PaymentModel>(payInfo);
                string returnString = await PNRInfo();
                PNR PNRTracking = JsonConvert.DeserializeObject<PNR>(returnString);
                ViewBag.isCreditOrDebit = false;
                if (payObject.PaymentMethod != "EFECTY" && payObject.PaymentMethod != "BALOTO" && payObject.PaymentMethod != "Referenciado")
                {
                    try
                    {
                        ViewBag.isCreditOrDebit = true;
                        string sendEtkt = await sendETKT();
                        string etktResponse = JsonConvert.DeserializeObject<string>(sendEtkt);
                        ETKTInfoTable = JsonConvert.DeserializeObject<DataTable>(etktResponse);
                        string sendEtktDeserialized = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(sendEtkt);
                        ETKTInfoTable = JsonConvert.DeserializeObject<DataTable>(sendEtktDeserialized);
                        ViewBag.ETKT = ETKTInfoTable;
                    }
                    catch {
                        ViewBag.ETKT = ETKTInfoTable;
                    }
                }
                else if (payObject.PaymentMethod == "EFECTY")
                    ViewBag.agreement = configurationReader.read("agreementEfecty", "MVCFuncional");
                else if (payObject.PaymentMethod == "BALOTO")
                    ViewBag.agreement = configurationReader.read("agreementBaloto", "MVCFuncional");
                else
                    ViewBag.agreement = "";
                ViewBag.Code = PNRTracking.ReservationCode;
                ViewBag.valueToPay = PNRTracking.KIU_TOTAL;
                ViewBag.value = PNRTracking.KIUPrice;
                ViewBag.valueCO = PNRTracking.KIU_CO;
                ViewBag.valueYS = PNRTracking.KIU_YS;
                ViewBag.valueTA = PNRTracking.KIU_TA;
                ViewBag.valueTB = PNRTracking.KIU_TB;
                string paxString = await paxInfoSync();
                List<Pax> paxList = JsonConvert.DeserializeObject<List<Pax>>(paxString);
                ViewBag.paxList = paxList;
                string returnStringSelect = await selectInfo();
                avail availTracking = JsonConvert.DeserializeObject<avail>(returnStringSelect);
                ViewBag.typeFlight = availTracking.flightType;
                ViewBag.origin = availTracking.IATAOrigin;
                ViewBag.destination = availTracking.IATADestination;
                string reservationInformation = await reservationInfo();
                Reservation reservationTracking = JsonConvert.DeserializeObject<Reservation>(reservationInformation);
                ViewBag.reservationDate = reservationTracking.Date;
                TimeSpan dateExpiration = Convert.ToDateTime(reservationTracking.DepartureDateTime) - reservationTracking.Date;
                if (userAdministrator.instance.user.isAuthenticated)
                {
                    if (dateExpiration.Days < 1)
                        ViewBag.expirationDate = reservationTracking.Date.AddHours(3);
                    else if (dateExpiration.Days > 1 && dateExpiration.Days < 3)
                        ViewBag.expirationDate = reservationTracking.Date.AddHours(6);
                    else if (dateExpiration.Days > 3)
                        ViewBag.expirationDate = reservationTracking.Date.AddHours(12);
                }
                else
                {
                    ViewBag.expirationDate = reservationTracking.Date.AddHours(1);
                }
                ViewBag.departureFlight = reservationTracking.DepartureFlight;
                ViewBag.returnFlight = reservationTracking.ReturnFlight;
                ViewBag.emissionDate = reservationTracking.PaymentDate;
                ViewBag.departureClass = reservationTracking.ClassDeparture;
                ViewBag.returnClass = reservationTracking.ClassReturn;
                ViewBag.departureTime = reservationTracking.DepartureDateTime;
                ViewBag.arrivalTime = reservationTracking.ArrivalDateTime;
                ViewBag.departureTimeReturn = reservationTracking.DepartureDateTimeReturn;
                ViewBag.arrivalTimeReturn = reservationTracking.ArrivalDateTimeReturn;
                ViewBag.paymentMethod = payObject.PaymentMethod;
                ViewBag.authorizationCode = payObject.AuthorizationCode;
                ViewBag.order = payObject.OrderID;
                
                return View();

            }
            catch
            {
                throw;
            }
        }
    }
}