﻿using AWESAirline.Controllers;
using AWESCommon.bannerSupport;
using AWESAirline.Models;
using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using AWESMVCCommon.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Utilidades.Models;
using AWESCommon.contentSupport;
using AWESCommon.logSupport;
using AWESCommon.authSupport;


namespace wwweasyflyawes.Controllers
{
    public class HomeController : baseController
    {
        private const string AUTH_PLATFORM = "AWESAuth";

        public async Task<ActionResult> Index()
        {
            if (HttpContext.Session.IsNewSession)
            {
                if (HttpContext.Request.Browser.IsMobileDevice)
                {
                    Response.Clear();
                    Response.Redirect("http://www3.easyfly.com.co/");
                    Response.End();
                }
            }
            List<catalogEntryPOCO> listCities = await getNameByIATAList();
            Dictionary<string, string> dictCities = new Dictionary<string, string>();

            foreach (catalogEntryPOCO item in listCities)
            {
                dictCities.Add(item.name, item.value);
            }

            cookieCities["dictCities"] = Newtonsoft.Json.JsonConvert.SerializeObject(dictCities);
            cookieCities.Expires = DateTime.Now.AddHours(1);
            Response.Cookies.Add(cookieCities);

            ViewBag.dictCities = dictCities;
            ViewBag.origin = "";
            ViewBag.TiposDocAd = "";
            ViewBag.TiposDocNi = "";
            ViewBag.TiposDocIn = "";
            //mdlCompra oModelo = null;
            //if (Session["mdlCompra"] != null)
            //    oModelo = (mdlCompra)Session["mdlCompra"];
            //else
            //    oModelo = new mdlCompra();
            //oModelo.Adultos = 1;
            //oModelo.Niños = 0;
            //oModelo.Infantes = 0;

            try
            {
                if (Request.UrlReferrer.ToString() == "http://www.easyfly.com.co/" || "http://www.easyfly.com.co/".Contains(Request.UrlReferrer.ToString()))
                //if (Request.UrlReferrer.ToString() == "http://mdd.easyfly.com.co/" || "http://mdd.easyfly.com.co/".Contains(Request.UrlReferrer.ToString()))
                {
                    contentHelper.postHits(ConfigurationManager.AppSettings["platform"], "TO-NEW-PAGE", "es");
                    cookieNewPage["onlyNewPage"] = "true";
                    cookieNewPage.Expires = DateTime.Now.AddDays(20);
                    cookieNewPage.Domain = ".easyfly.com.co";
                    Response.Cookies.Add(cookieNewPage);
                }
            }
            catch 
            {
                
            }

            if (Request["tstPost"] != null)
            {
                return Redirect("/Flights");
            }
            else
            {
                ErrorToShow();

                ViewBag.origin = await trackingFirstTime();

                var TiposDocAd = from s in (new Dictionary<string, string>() { { "CC", "ID" }, { "PAS", "PP" } })
                                 select new { Nombre = s.Key, Valor = s.Value };
                var TiposDocNi = from s in (new Dictionary<string, string>() { { "ID", "ID" }, { "PAS", "PP" } })
                                 select new { Nombre = s.Key, Valor = s.Value };
                var TiposDocIn = from s in (new Dictionary<string, string>() { { "RC", "ID" }, { "PAS", "PP" } })
                                 select new { Nombre = s.Key, Valor = s.Value };

                ViewBag.TiposDocAd = new SelectList(TiposDocAd, "Valor", "Nombre", null);
                ViewBag.TiposDocNi = new SelectList(TiposDocNi, "Valor", "Nombre", null);
                ViewBag.TiposDocIn = new SelectList(TiposDocIn, "Valor", "Nombre", null);
                //Session["mdlCompra"] = oModelo;

                //BANNERS
                string[] languages = new string[1];
                languages[0] = "es";
                if (Request.UserLanguages != null)
                {
                    languages = Request.UserLanguages;
                }
                List<byte[]> listOfBanners = bannerHelper.bannerByArray(platform, "", languages[0].Split(';')[0], "L", "H");
                List<POCOBannerEntry> principalBanner = bannerHelper.bannerList(platform, "", languages[0].Split(';')[0], "L", "H");
                List<byte[]> listOfPBanners = bannerHelper.bannerByArray(platform, "", languages[0].Split(';')[0], "L", "P");
                List<POCOBannerEntry> PBanners = bannerHelper.bannerList(platform, "", languages[0].Split(';')[0], "L", "P");
                List<byte[]> listOfSBanners = bannerHelper.bannerByArray(platform, "", languages[0].Split(';')[0], "L", "S");
                List<POCOBannerEntry> SBanners = bannerHelper.bannerList(platform, "", languages[0].Split(';')[0], "L", "S");

                string catalogID=configurationReader.read("BannerURL");

                List<catalogEntryPOCO> urlList = authHelper.getCatalogEntriesByCatalogID(catalogID);

                foreach (catalogEntryPOCO entry in urlList)
                {
                    List<POCOBannerEntry> bannerNames= (from i in principalBanner where i.name==entry.name select i).ToList();
                    if (bannerNames.Count > 0) {
                        int indexOf = principalBanner.IndexOf(bannerNames[0], 0);
                        POCOBannerEntry temp = new POCOBannerEntry();
                        temp = bannerNames[0];
                        temp.urlSmall = entry.value;
                        principalBanner[indexOf] = temp;
                    }
                    List<POCOBannerEntry> PbannerNames = (from i in PBanners where i.name == entry.name select i).ToList();
                    if (PbannerNames.Count > 0)
                    {
                        int indexOf = PBanners.IndexOf(PbannerNames[0], 0);
                        POCOBannerEntry temp = new POCOBannerEntry();
                        temp = PbannerNames[0];
                        temp.urlSmall = entry.value;
                        PBanners[indexOf] = temp;
                    }
                    List<POCOBannerEntry> SbannerNames = (from i in SBanners where i.name == entry.name select i).ToList();
                    if (SbannerNames.Count > 0)
                    {
                        int indexOf = SBanners.IndexOf(SbannerNames[0], 0);
                        POCOBannerEntry temp = new POCOBannerEntry();
                        temp = SbannerNames[0];
                        temp.urlSmall = entry.value;
                        SBanners[indexOf] = temp;
                    }
                }

                byte[] defaultArray= new byte[1];
                defaultArray[0] = 0;
                POCOBannerEntry defaultInfo = new POCOBannerEntry();
                List<byte[]> defaultListArray = new List<byte[]>();

                Random r = new Random();
                int rInt = r.Next(0, listOfBanners.Count-1);
                if (listOfBanners.Count > 0 && principalBanner.Count > 0)
                {
                    if (listOfBanners.Count - 1 >= rInt && principalBanner.Count - 1 >= rInt)
                    {
                        ViewBag.hBanner = listOfBanners[rInt];
                        ViewBag.hBannerInfo = principalBanner[rInt];
                    }
                    else
                    {
                        ViewBag.hBanner = listOfBanners[0];
                        ViewBag.hBannerInfo = principalBanner[0];
                    }
                }
                else {
                    ViewBag.hBanner = defaultArray;
                    ViewBag.hBannerInfo = defaultInfo;
                }

                if (listOfPBanners.Count > 0 && PBanners.Count > 0)
                {
                    ViewBag.pBanner = listOfPBanners;
                    ViewBag.pBannerInfo = PBanners;
                }
                else {
                    ViewBag.pBanner = defaultListArray;
                    ViewBag.pBannerInfo = defaultInfo;
                }

                if (listOfSBanners.Count > 0 && SBanners.Count > 0)
                {
                    ViewBag.sBanner = listOfSBanners;
                    ViewBag.sBannerInfo = SBanners;
                }
                else
                {
                    ViewBag.sBanner = defaultListArray;
                    ViewBag.sBannerInfo = defaultInfo;
                }


                return View("Index");
            }
        }

        //[authorizationConfirmedFilter()]
        public ActionResult Contact()
        {
            return View("Contact");
        }

        public async Task<JsonResult> destinations(int originID)
        {
            try
            {
                /************************************************************************************************************
                * INSERTS THE NEW ENTRY 
                *************************************************************************************************************/
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation detinations", "AWESReservations");

                string parametrizedUri = helper.uri.Replace("{originID}", originID.ToString());
                string body = "";
                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           parametrizedUri,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);

                var dummy = new[] { new { ID = 0, destinationID = 0, IATA = "" } };

                var entries = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(returnString, dummy);

                //CITIES CATALOG
                string catalogID = configurationReader.read("cities", "AWESAdminDashboard");
                helper = configurationReader.readHelper("REST.catalog entry list", configPlatform);
                parametrizedUri = helper.uri.Replace("{catalogID}", catalogID.ToString());
                string catalog = await callApi(helper, parametrizedUri);
                var dummyCatalog = new[] { new { ID = 0, name = "", value = "", catalogID = 0 } };
                var entriesCatalog = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(catalog, dummyCatalog);

                List<AirportsDummy> endList = new List<AirportsDummy>();

                foreach (var entry in entries)
                {
                    AirportsDummy airportDummy = new AirportsDummy();
                    airportDummy.ID = entry.ID;
                    airportDummy.destinationID = entry.destinationID;
                    airportDummy.IATA = entry.IATA;
                    airportDummy.name = (from i in entriesCatalog
                                         where i.name == entry.IATA
                                         select i.value).FirstOrDefault();
                    endList.Add(airportDummy);
                }

                endList = (from i in endList select i).OrderByDescending(i => i.name).Reverse().ToList();

                string serialized = JsonConvert.SerializeObject(endList);

                return Json(new { data = serialized }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> faqCharged() {
            return await getContentByType("FAQ");
        }
       
        public ActionResult bannerSearch() {
            string[] languages = new string[1];
            languages[0] = "es";
            if (Request.UserLanguages != null)
            {
                languages = Request.UserLanguages;
            }
            List<POCOBannerEntry> homeBanner = new List<POCOBannerEntry>();
            List<POCOBannerEntry> principalBanner = new List<POCOBannerEntry>();
            List<POCOBannerEntry> secundaryBanner = new List<POCOBannerEntry>();
            foreach (string language in languages)
            {
                principalBanner = bannerHelper.bannerList(platform, "", language.Split(';')[0], "L", "P");
                if (principalBanner.Count != 0)
                {
                    secundaryBanner = bannerHelper.bannerList(platform, "", language.Split(';')[0], "L", "S");
                    homeBanner = bannerHelper.bannerList(platform, "", language.Split(';')[0], "L", "H");
                    break;
                }
            }

            string catalogID=configurationReader.read("BannerURL");

            List<catalogEntryPOCO> urlList = authHelper.getCatalogEntriesByCatalogID(catalogID);

            foreach (catalogEntryPOCO entry in urlList)
            {
                if ((from i in homeBanner select i.name).Contains(entry.name)) { 
                    // if there is more than one immage with the same name
                    List<POCOBannerEntry> banners= (from i in homeBanner where i.name==entry.name select i).ToList();
                    foreach (POCOBannerEntry banner in banners)
                    {
                        int indexOf = homeBanner.IndexOf(banner, 0);
                        POCOBannerEntry temp = new POCOBannerEntry(banner);
                        // redirection banner url
                        temp.urlSmall = entry.value;
                        homeBanner[indexOf] = temp; 
                    }
                }
                if ((from i in principalBanner select i.name).Contains(entry.name))
                {
                    List<POCOBannerEntry> banners = (from i in principalBanner where i.name == entry.name select i).ToList();
                    foreach (POCOBannerEntry banner in banners)
                    {
                        int indexOf = principalBanner.IndexOf(banner, 0);
                        POCOBannerEntry temp = new POCOBannerEntry(banner);
                        // redirection banner url
                        temp.urlSmall = entry.value;
                        principalBanner[indexOf] = temp; 
                    }
                }
                if ((from i in secundaryBanner select i.name).Contains(entry.name))
                {
                    List<POCOBannerEntry> banners = (from i in secundaryBanner where i.name == entry.name select i).ToList();
                    foreach (POCOBannerEntry banner in banners)
                    {
                        int indexOf = secundaryBanner.IndexOf(banner, 0);
                        POCOBannerEntry temp = new POCOBannerEntry(banner);
                        // redirection banner url
                        temp.urlSmall = entry.value;
                        secundaryBanner[indexOf] = temp; 
                    }
                }
            }

            string homeURL = "";
            string principalUrl = "";
            string secundaryUrl = "";
            if (principalBanner.Count >= 3 && secundaryBanner.Count >= 2)
            {
                homeURL = Newtonsoft.Json.JsonConvert.SerializeObject(homeBanner);
                principalUrl = Newtonsoft.Json.JsonConvert.SerializeObject(principalBanner);
                secundaryUrl = Newtonsoft.Json.JsonConvert.SerializeObject(secundaryBanner);
            }
            else
            {
                while (principalBanner.Count < 3)
                    principalBanner.Add(new POCOBannerEntry(false));
                while (secundaryBanner.Count < 2)
                    secundaryBanner.Add(new POCOBannerEntry(false));

                homeURL = Newtonsoft.Json.JsonConvert.SerializeObject(homeBanner);
                principalUrl = Newtonsoft.Json.JsonConvert.SerializeObject(principalBanner);
                secundaryUrl = Newtonsoft.Json.JsonConvert.SerializeObject(secundaryBanner);
            }

            return Json(new { principal = principalUrl, secundary = secundaryUrl, home=homeURL }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Message(string Message, string url)
        {
            ViewBag.Message = Message;
            ViewBag.url = url;
            ViewBag.reservas = null;

            return View();
        }

        //Private Classes
        private class AirportsDummy {
            //ID = 0, destinationID = 0, IATA = "", name = "" 
            public int ID { get; set; }
            public int destinationID { get; set; }
            public string IATA { get; set; }
            public string name { get; set; }
        }
    }
}