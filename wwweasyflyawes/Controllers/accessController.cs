﻿using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Encryption;
using AWESMVCCommon.Filters;
using AWESMVCCommon.oAuth;
using AWESMVCCommon.Security;
using AWESCommon.Web;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth2;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AWESCommon.messagingSupport;
using AWESCommon.reservationSupport;
using AWESCommon.authSupport;

namespace AWESAirline.Controllers
{
    public class accessController : baseController
    {
        private string _encriptSalt;
        private string _cookieName;

        public string encriptSalt
        {
            get
            {
                if (_encriptSalt == null)
                    _encriptSalt = configurationReader.read("auth.cookie.encriptSalt");

                return _encriptSalt;
            }
        }

        public string cookieName
        {
            get
            {
                if (_cookieName == null)
                    _cookieName = configurationReader.read("auth.recovery.cookie.name");

                return _cookieName;
            }
        }

        // CUIDADDO. NO CAMBIAR A MENOS QUE HAYA PROTOCOLO DE MIGRACIÓN DE PASSWORDS
        private const string STR_PASSWORD_HASHING_SALT = "2CE5D046-FB7B-404E-BABC-4ADEF5CFEF77";
        private const string loginRedirection = "/";
        private string urlRedirect = "";

        [authorizationFilter(authorizationFilter.AUTHENTICATED_PERMISSION)]
        public ActionResult logOut(string userEmail = null, string userPassword = null)
        {
            userAdministrator.instance.clearUserCookie();
            if (string.IsNullOrEmpty(userEmail) && string.IsNullOrEmpty(userPassword))
            {
                return RedirectToAction("index", "home");
            }
            else
            {
                return Redirect("/access/logInHome?userEmail=" + userEmail + "&userPassword=" + userPassword);
            }
        }

        [RequireHttps]
        public ActionResult gmailAccess()
        {
            urlRedirect = Request.UrlReferrer.AbsolutePath.ToString().ToLower();
            cookieLoginUrlResponse["url"] = urlRedirect;
            cookieLoginUrlResponse.Expires = DateTime.Now.AddHours(0.5);
            Response.Cookies.Add(cookieLoginUrlResponse);

            // IF USER IS ALREADY LOGGED IN REDIRECTS TO HOME
            if (userAdministrator.instance.isUserAuthenticated)
                return Redirect(loginRedirection);

            // SI HACE LOGIN DESDE COOKIE
            if (new authorizationFilter(authorizationFilter.AUTHENTICATED_PERMISSION).validateFromCookie())
                return Redirect(loginRedirection);

            string clientID = CGmail.clientIDGmail;
            string clientSecret = CGmail.clientSecretGmail;

            WebServerClient consumer = new WebServerClient(CGmail.serverGmail, clientID, clientSecret);
            OutgoingWebResponse response = consumer.PrepareRequestUserAuthorization(CGmail.scopeGmail, new Uri(CGmail.redirectUriGmail));
            //https://github.com/DotNetOpenAuth/DotNetOpenAuth/issues/307
            //Install-Package DotNetOpenAuth.Mvc5
            return response.AsActionResultMvc5();
        }

        [RequireHttps]
        public ActionResult cbGmail()
        {
            urlRedirect = Request.Cookies["LoginUrlResponse"]["url"];
            Request.Cookies.Remove("LoginUrlResponse");
            // ASIGNA CALLBACLK PARA VALIDACION DE CERTIFICADOS SSL
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);

            CUser gmailUser = default(CUser);

            WebServerClient consumer = new WebServerClient(CGmail.serverGmail, CGmail.clientIDGmail, CGmail.clientSecretGmail);
            consumer.ClientCredentialApplicator = ClientCredentialApplicator.PostParameter(CGmail.clientSecretGmail);

            IAuthorizationState grantedAccess = consumer.ProcessUserAuthorization(Request);

            string accessToken = grantedAccess.AccessToken;

            if (accessToken != null)
            {
                try
                {
                    string trackingCode= "";
                    try 
	                {	        
		                trackingCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt);
	                }
	                catch 
	                {
                        return Redirect(loginRedirection);
	                }
                    System.Guid track = System.Guid.NewGuid();
                    if (string.IsNullOrEmpty(trackingCode))
                    {
                        trackingCode = Encrypt.encrypt(track.ToString(), encriptSalt);
                        cookieUser["tracking"] = trackingCode;
                    }
                    else
                    {
                        cookieUser["tracking"] = trackingCode;
                    }
                    var context = Request.RequestContext.HttpContext;

                    // OBTAINS USER INFORMATION FROM GMAIL
                    gmailUser = CGmail.getUSerInfoGmail(accessToken);

                    if (gmailUser == null)
                        return Redirect("/");

                    int userID = userAdministrator.instance.updatesUserInfo(gmailUser);

                    userAdministrator.instance.writeUserCookie(trackingCode.ToString());

                    userAdministrator.instance.registerUserSession(userID, trackingCode.ToString());

                    POCOTrackingEntry tracking = new POCOTrackingEntry();
                    Dictionary<string, string> platforms = authHelper.getPlatforms();

                    int platformID = 0;

                    int.TryParse(platforms.FirstOrDefault(m => m.Value == platform).Key, out platformID);

                    tracking.Platform = platformID;
                    tracking.User = userID;
                    tracking.TrackCode = trackingCode.ToString();

                    POCOTrackingEntry trackingTask = reservationHelper.putTracking(tracking, userAdministrator.instance.user.ID.ToString());
                    //POCOTrackingEntry trackingTask = reservationHelper.putTracking(tracking, userAdministrator.instance.user.ID.ToString());

                    tracking = trackingTask;

                    if (userAdministrator.instance.user.isAuthenticated && tracking.User == 0)
                    {
                        return Redirect(loginRedirection);
                    }

                    return loginActions(urlRedirect);
                }
                catch (Exception ex)
                {
                    // CONFIRM
                    return RedirectToAction("Message", "Home", new { url = "/Access/login", Message = "There has been an authentication problem. Please retry. (" + ex.Message + ")" });
                }
            }

            // CONFIRMA
            return RedirectToAction("Message", "Home", new { url = "/Acceso/login", Message = "Ha habido un problema en el proceso de autenticación. Por favor reintente." });
        }

        private ActionResult loginActions(string otherURL = null)
        {
            DateTime cleanDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            if (string.IsNullOrEmpty(otherURL))
                return Redirect(loginRedirection);
            else
                return Redirect(otherURL);
        }

        /// <summary>
        /// This version of the method logs in the user from a login page
        /// </summary>
        /// <param name="logIn"></param>
        /// <returns></returns>
        //[RequireHttps]
        public async Task<ActionResult> logIn(logInModel logIn)
        {
            return await logInProcess(logIn);
        }

        /// <summary>
        /// This version of the method logs in the user from a login page
        /// </summary>
        /// <param name="logIn"></param>
        /// <returns></returns>
        //[RequireHttps]
        public ActionResult logInHome(string userEmail = "", string userPassword = "")
        {
            // checks for valid information
            urlRedirect = Request.UrlReferrer.AbsolutePath.ToString();
            if (string.IsNullOrEmpty(userEmail) || string.IsNullOrEmpty(userPassword))
                return RedirectToAction("Message", "Home", new { url = "/", Message = "Please write both user email and password." });

            // wraps request into process object
            logInModel logIn = new logInModel()
            {
                email = userEmail,
                password = userPassword
            };

            logInProcess(logIn);

            if (userAdministrator.instance.isUserAuthenticated)
                return Json(new { OK = "Log in requested ", data = "OK", url=urlRedirect }, JsonRequestBehavior.AllowGet);
            //return Redirect(loginRedirection);
            else
                return Json(new { OK = "Log in requested ", data = "Fail" }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("logIn", logIn);
        }

        //[RequireHttps]
        public async Task<ActionResult> passwordRecoveryR(newPasswordModel newPassword, string val)
        {
            // IF USER IS ALREADY LOGGED IN REDIRECTS TO HOME
            if (userAdministrator.instance.isUserAuthenticated)
                return Redirect(loginRedirection);

            ViewBag.Title = "Password recovery";
            ViewBag.errors = new List<string>();

            List<string> lstError = new List<string>();

            // CHECKS FOR TEMPORARY COOKIE
            if (Request.Cookies.AllKeys.Contains(cookieName))
            {
                string cookieValue = cookieManager.readFromCookie(cookieName, "passwordRecovery");

                // DECRYPTS COOKIE
                string validationCode = Encrypt.decrypt(cookieValue, encriptSalt);
                string queryStringVal = Encrypt.decrypt(HttpUtility.HtmlDecode(val).Replace(' ', '+'), encriptSalt);

                if (queryStringVal != validationCode)
                    return RedirectToAction("Message", "Home", new { url = "/", Message = "The password rquest has timed out. Please try again." });


                if (newPassword == null)
                {
                    newPassword = new newPasswordModel();
                    newPassword.newPassword = Request.Form["newPassword"];
                    newPassword.newPasswordConfirmation = Request.Form["newPasswordConfirmation"];
                }

                if (!newPassword.isValid(out lstError))
                {
                    ViewBag.errors = lstError;
                    return View(new newPasswordModel());
                }
                else
                {
                    // OBTAINS VALIDATION CODE (IF IT IS CURRENT), CHECKS IP ADDRESS
                    List<string> validatedData = getPasswordRecoveryEntry(validationCode);

                    if (validatedData.Count() > 0)
                    {
                        // UPDATES PASSWORD
                        bool passwordChanged = await userAdministrator.instance.updatePasswordfromRecoveryAsync(newPassword.newPassword,
                                                                                                        newPassword.newPasswordConfirmation,
                                                                                                        validatedData[0],
                                                                                                        validatedData[1]);

                        if (passwordChanged)
                        {
                            // voids cookie
                            TimeSpan expiration = new TimeSpan(0, 0, 0);
                            cookieManager.updatesCookie(cookieName, "", "passwordRecovery", expiration);

                            return RedirectToAction("Message", "Home", new { url = "/access/login", Message = "Password has been changed." });
                        }
                    }

                    ViewBag.errores = new List<string>() { "Password not changed. Please try again." };
                    return View(new newPasswordModel());
                }
            }

            // DOES NOT HAVE THE COOKIE (DIFFERENT MACHINE, BROWSER OR COOKIE TIMED OUT)
            return RedirectToAction("Message", "Home", new { url = "/", Message = "The password rquest has timed out. Please try again." });
        }

        //[RequireHttps]
        public async Task<ActionResult> passwordRecovery(passwordRecoverModel recoveryInfo)
        {
            // IF USER IS ALREADY LOGGED IN REDIRECTS TO HOME
            if (userAdministrator.instance.isUserAuthenticated)
                return Redirect(loginRedirection);

            ViewBag.Title = "Password recovery";
            ViewBag.errors = new List<string>();

            List<string> lstError = new List<string>();

            if (recoveryInfo == null)
                return View();

            if (!recoveryInfo.isValid(out lstError))
                ViewBag.errors = lstError;
            else
            {
                try
                {
                    // GENERATES TEMPORARY VERIFICATION COOKIE
                    string validationCode = System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString();

                    // WRITES COOKIE
                    string cookieValue = Encrypt.encrypt(validationCode, encriptSalt);

                    // DEFINES COOKIE AND PROFILE EXPIRATION
                    TimeSpan expiration = new TimeSpan(0, 5, 0);
                    cookieManager.updatesCookie(cookieName, cookieValue, "passwordRecovery", expiration);

                    // SAVES REQUEST FOR FURTHER VERIFICATION
                    int newEntryCode = await registerPasswordRecoveryEntry(validationCode, recoveryInfo.email);

                    if (newEntryCode > 0)
                    {
                        // PREPARES MESSAGE
                        string destinataires = recoveryInfo.email;
                        messageEntry.messageType type = messageEntry.messageType.email;

                        // SENDS EMAIL
                        string subject = "Password recovery process";
                        string url = new Uri(Request.Url, Url.Content("~")) + "/access/passwordRecoveryR?ver=ksijd&val=" + HttpUtility.HtmlEncode(cookieValue);
                        string body = "please go to <a href='" + url + "'>" + url + "</a> and follow instructions to regenerate password";

                        // SENDS MESSAGE
                        await messageHelper.messagingAsync(new messageEntry(functionalUnit, callingMethod(),
                                                                           destinataires,
                                                                           subject, body,
                                                                           type));
                        
                    }

                    // REDIRECTS
                    //return RedirectToAction("Message", "Home", new { url = "/", Message = "If the email address you wrote exists, an email with additional instructions has been sent." });
                    return Json(new {OK = "SUCCESS", data = "If the email address you wrote exists, an email with additional instructions has been sent."},JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    // CONFIRM
                    //return RedirectToAction("Message", "Home", new { url = "/Access/passwordRecovery", Message = "There has been a password recovery problem." });
                    return Json(new { OK = "SUCCESS", data = "There has been a password recovery problem." }, JsonRequestBehavior.AllowGet);
                }
            }

            return View();
        }

        //[RequireHttps]
        public ActionResult signIn(string email, string emailConfirm, string pass, string passConfirm, string name, string lastName, string document)
        {
            ViewBag.Title = "New user ign in";
            ViewBag.error = new List<string>();
            signInModel signIn = new signInModel();
            CUser newUser = new CUser();
            List<string> lstError = new List<string>();

            signIn.email = email;
            signIn.emailConfirmation = emailConfirm;
            signIn.password = pass;
            signIn.passwordConfirmation = passConfirm;
            signIn.name = name;
            signIn.lastName = lastName;
            signIn.documentNumber = document;
            if (signIn == null)
                return View();

            if (!signIn.isValid(out lstError))
                ViewBag.errores = lstError;
            else
            {
                try
                {
                    newUser.email = signIn.email;
                    newUser.name = signIn.name;
                    newUser.lastName = signIn.lastName;
                    newUser.network = "local";
                    newUser.locale = "es-co";
                    newUser.token = "";
                    newUser.password = hash.HashString(signIn.password);

                    int userID = userAdministrator.instance.updatesUserInfo(newUser);

                    System.Guid trackingCode = System.Guid.NewGuid();

                    userAdministrator.instance.writeUserCookie(trackingCode.ToString());

                    userAdministrator.instance.registerUserSession(userID, trackingCode.ToString());

                    return Json(new { OK = "Registration requested ", data = "OK" }, JsonRequestBehavior.AllowGet);
                    //return loginActions();
                }
                catch (Exception ex)
                {
                    // CONFIRM
                    return RedirectToAction("Message", "Home", new { url = "/Access/login", Message = "There has been an authentication problem. Please retry. (" + ex.Message + ")" });
                }
            }
            return Json(new { OK = "Registration requested ", data = "OK" }, JsonRequestBehavior.AllowGet);
            //return View(signIn);
        }

        //[RequireHttps]
        //[authorizationFilter(authorizationFilter.AUTHENTICATED_PERMISSION)]
        public async Task<ActionResult> passwordChange(passwordChanegModel passwordChange = null)
        {
            ViewBag.Title = "Password change";
            ViewBag.errores = new List<string>();

            List<string> lstErrores = new List<string>();

            if (passwordChange == null)
                return View();

            // CAMBIA EL PASSWORD SI SE HA ESCRITO UNO NUEVO
            if (!passwordChange.isValid(out lstErrores))
                ViewBag.errores = lstErrores;
            else
            {
                try
                {
                    bool passwordChanged = await userAdministrator.instance.updateUserPasswordAsync(passwordChange.newPassword,
                                                                                                    passwordChange.newPasswordConfirmation,
                                                                                                    passwordChange.previousPassword);

                    if (passwordChanged)
                    {
                        userAdministrator.instance.updateUserFromTrackingCode();

                        return RedirectToAction("Message", "Home", new { url = "/", Message = "Password changed" });
                    }

                    ViewBag.errores = new List<string>() { "Password not changed. Please check information and try again." };
                }
                catch (Exception ex)
                {
                    // CONFIRM
                    return RedirectToAction("Message", "Home", new { url = "/Access/login", Message = "There has been a problem. Please retry. (" + ex.Message + ")" });
                }
            }

            return View();
        }

        private async Task<int> registerPasswordRecoveryEntry(string validationCode,
                                                              string email)
        {
            var dummyValue = new
            {
                email = email,
                validationCode = validationCode,
                clientIP = Request.UserHostAddress
            };

            POCOConfigHelper helper = configurationReader.readHelper("REST.new password Recovery entry", configPlatform);

            string returnString = await webPost.asyncAction(helper.method,
                                                            JsonConvert.SerializeObject(dummyValue),
                                                            helper.server,
                                                            helper.uri,
                                                            "application/json",
                                                            "application/json",
                                                            signatureName,
                                                            signatureValue);
            int newEntryID = 0;

            int.TryParse(returnString, out newEntryID);

            return newEntryID;
        }

        private List<string> getPasswordRecoveryEntry(string validationCode)
        {
            POCOConfigHelper helper = configurationReader.readHelper("REST.password Recovery list", configPlatform);

            string parametrizedURI = helper.uri.Replace("{id}", "") + "?validationCode=" + validationCode;

            string returnString = webPost.action(helper.method,
                                                            null,
                                                            helper.server,
                                                            parametrizedURI,
                                                            "application/json",
                                                            "application/json",
                                                            signatureName,
                                                            signatureValue);

            if (!string.IsNullOrEmpty(returnString))
            {
                var dummy = new
                {
                    ID = 0,
                    clientIP = "",
                    email = "",
                    validationCode = "",
                    minutesToExpiration = 0,
                    requestTime = DateTime.Now
                };

                var entry = JsonConvert.DeserializeAnonymousType(returnString, dummy);

                if ((entry != null) && (entry.clientIP == Request.UserHostAddress))
                    return new List<string>() { entry.validationCode, entry.email };
            }

            return new List<string>();
        }

        private async Task<ActionResult> logInProcess(logInModel logIn)
        {
            // IF USER IS ALREADY LOGGED IN REDIRECTS TO HOME
            if (userAdministrator.instance.isUserAuthenticated)
                return Redirect(loginRedirection);

            ViewBag.Title = "Log in";
            ViewBag.error = new List<string>();

            CUser newUser = new CUser();

            List<string> lstError = new List<string>();

            if (logIn == null)
                return View();

            if (!logIn.isValid(out lstError))
                ViewBag.errores = lstError;
            else
            {
                try
                {
                    string trackingCode = "";
                    try
                    {
                        trackingCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt);
                    }
                    catch
                    {
                        return Redirect(loginRedirection);
                    }
                    System.Guid track = System.Guid.NewGuid();
                    if (string.IsNullOrEmpty(trackingCode))
                    {
                        trackingCode = Encrypt.encrypt(track.ToString(), encriptSalt);
                        cookieUser["tracking"] = trackingCode;
                    }
                    else
                    {
                        cookieUser["tracking"] = trackingCode;
                    }
                    bool loggedInUser = await userAdministrator.instance.logInUserAsync(logIn.email, logIn.password);

                    if (loggedInUser)
                    {
                        userAdministrator.instance.writeUserCookie(trackingCode.ToString());

                        userAdministrator.instance.registerUserSession(userAdministrator.instance.user.ID, trackingCode.ToString());

                        POCOTrackingEntry tracking = new POCOTrackingEntry();
                        Dictionary<string, string> platforms = authHelper.getPlatforms();

                        int platformID = 0;

                        int.TryParse(platforms.FirstOrDefault(m => m.Value == platform).Key, out platformID);

                        tracking.Platform = platformID;
                        tracking.User = userAdministrator.instance.user.ID;
                        tracking.TrackCode = trackingCode.ToString();

                        POCOTrackingEntry trackingTask = reservationHelper.putTracking(tracking, userAdministrator.instance.user.ID.ToString());

                        tracking = trackingTask;

                        if (userAdministrator.instance.user.isAuthenticated && tracking.User == 0)
                        {
                            return Redirect(loginRedirection);
                        }

                        return loginActions(urlRedirect);
                    }

                    ViewBag.errores = new List<string>() { "Unsuccessfull attemp. Please check information and retry." };

                }
                catch (Exception ex)
                {
                    // CONFIRM
                    return RedirectToAction("Message", "Home", new { url = "/Access/login", Message = "There has been an authentication problem. Please retry. (" + ex.Message + ")" });
                }
            }

            return View();
        }
    }

    public class signInModel
    {
        private const string STR_ERROR_CONSTANT = "*";

        [Display(Name = "Password")]
        public string password { get; set; }
        [Display(Name = "Password confirmation")]
        public string passwordConfirmation { get; set; }
        [Display(Name = "Document number")]
        public string documentNumber { get; set; }
        [Display(Name = "Name")]
        public string name { get; set; }
        [Display(Name = "Last name")]
        public string lastName { get; set; }
        [Display(Name = "Email")]
        public string email { get; set; }
        [Display(Name = "Email confirmation")]
        public string emailConfirmation { get; set; }


        public string passwordError { get; set; }
        public string passwordCOnfirmationError { get; set; }
        public string documentNumberError { get; set; }
        public string nameError { get; set; }
        public string lastNameError { get; set; }
        public string emailError { get; set; }
        public string emailConfirmationError { get; set; }

        public bool isValid(out List<string> list)
        {
            bool blnValid = true;

            list = new List<string>();

            if (string.IsNullOrEmpty(password))
            {
                list.Add("empty password");
                passwordError = STR_ERROR_CONSTANT;
                blnValid = false;
            }

            if (password != passwordConfirmation)
            {
                list.Add("Password and confirmación mismatch");
                passwordError = STR_ERROR_CONSTANT;
                passwordCOnfirmationError = STR_ERROR_CONSTANT;
                blnValid = false;
            }

            if (string.IsNullOrEmpty(email))
            {
                list.Add("Empty email");
                emailError = STR_ERROR_CONSTANT;
                blnValid = false;
            }

            if (!string.IsNullOrEmpty(email) &&
                !Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                list.Add("Invalid email");
                emailError = STR_ERROR_CONSTANT;
                blnValid = false;
            }

            if (email != emailConfirmation)
            {
                list.Add("Email and confirmation mismatch");
                emailError = STR_ERROR_CONSTANT;
                emailConfirmationError = STR_ERROR_CONSTANT;
                blnValid = false;
            }

            return blnValid;
        }
    }
    public class passwordChanegModel
    {
        private const string ERROR_CONSTANT = "*";

        public string previousPassword { get; set; }
        public string newPassword { get; set; }
        public string newPasswordConfirmation { get; set; }

        public string previousPasswordError { get; set; }
        public string newPasswordError { get; set; }
        public string newPasswordConfirmationError { get; set; }

        public bool isValid(out List<string> list)
        {
            bool isValid = true;
            list = new List<string>();

            if (string.IsNullOrEmpty(previousPassword))
            {
                list.Add("Empty password");
                newPasswordError = ERROR_CONSTANT;
                isValid = false;
            }

            if (string.IsNullOrEmpty(newPassword))
            {
                list.Add("Empty new password");
                newPasswordError = ERROR_CONSTANT;
                isValid = false;
            }

            if (newPassword != newPasswordConfirmation)
            {
                list.Add("New password and confirmation mismatch");
                newPasswordError = ERROR_CONSTANT;
                newPasswordConfirmationError = ERROR_CONSTANT;
                isValid = false;
            }

            return isValid;
        }
    }
    public class logInModel
    {
        private const string ERROR_CONSTANT = "*";

        public string password { get; set; }
        public string email { get; set; }

        public string passwordError { get; set; }
        public string emailError { get; set; }

        public bool isValid(out List<string> list)
        {
            bool isValid = true;
            list = new List<string>();

            if (string.IsNullOrEmpty(password))
            {
                list.Add("Empty password");
                passwordError = ERROR_CONSTANT;
                isValid = false;
            }

            if (string.IsNullOrEmpty(email))
            {
                list.Add("Empty email");
                emailError = ERROR_CONSTANT;
                isValid = false;
            }

            return isValid;
        }
    }
    public class passwordRecoverModel
    {
        private const string ERROR_CONSTANT = "*";

        public string email { get; set; }

        public string emailError { get; set; }

        public bool isValid(out List<string> list)
        {
            bool isValid = true;
            list = new List<string>();

            if (string.IsNullOrEmpty(email))
            {
                list.Add("Empty email");
                emailError = ERROR_CONSTANT;
                isValid = false;
            }

            return isValid;
        }
    }
    public class newPasswordModel
    {
        private const string ERROR_CONSTANT = "*";

        public string newPassword { get; set; }
        public string newPasswordConfirmation { get; set; }

        public string newPasswordError { get; set; }
        public string newPasswordConfirmationError { get; set; }

        public bool isValid(out List<string> list)
        {
            bool isValid = true;
            list = new List<string>();

            if (string.IsNullOrEmpty(newPassword))
            {
                list.Add("Empty new password");
                newPasswordError = ERROR_CONSTANT;
                isValid = false;
            }

            if (newPassword != newPasswordConfirmation)
            {
                list.Add("New password and confirmation mismatch");
                newPasswordError = ERROR_CONSTANT;
                newPasswordConfirmationError = ERROR_CONSTANT;
                isValid = false;
            }

            return isValid;
        }
    }
}