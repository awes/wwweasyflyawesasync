﻿using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using AWESMVCCommon.Security;
using AWESAirline.Models;
using AWESPaymentModel.BindingObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using AWESCommon.Extensions;
using AWESMVCCommon.Filters;
using wwweasyflyawes.Models;

namespace AWESAirline.Controllers
{
    public partial class PaymentController : baseController
    {
        int userId = 20;

        /// <summary>
        /// Pagina en donde se envia el pago
        /// </summary>
        /// <returns></returns>
        [authorizationFilter(authorizationFilter.AUTHENTICATED_PERMISSION)]
        public async Task<ActionResult> Tokenization()
        {
            try
            {
                // GETS RESERVATION INFO
                // id interno de referencia
                string returnString = await PNRInfo();
                PNR PNRTracking = JsonConvert.DeserializeObject<PNR>(returnString);

                // id interno de referencia
                ViewBag.reference = PNRTracking.ReservationCode;

                // avlor total por pagar incluyendo todos los impuestos y cargos
                ViewBag.valueToPay = PNRTracking.KIU_TOTAL;


                // LISTA DE MEDIOS DE PAGO TOKENIZADOS
                @ViewBag.PaymentMethods = await getTokenList(userId);

                //****************************************************************************************************************
                // COOKIE
                //****************************************************************************************************************
                // GENERATES TEMPORARY VERIFICATION COOKIE
                // SAVE FOR FURTHER VALIDATION
                string validationCode = System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString();

                // WRITES COOKIE
                string cookieValue = Encrypt.encrypt(validationCode, encriptSalt);

                // DEFINES COOKIE AND PROFILE EXPIRATION
                TimeSpan expiration = new TimeSpan(0, 5, 0);
                cookieManager.updatesCookie(cookieName, cookieValue, "PAYMENT", expiration);

                //****************************************************************************************************************
                // DEVICE SESSION ID
                //****************************************************************************************************************
                ViewBag.deviceSessionId = (Encrypt.md5(HttpContext.Session.SessionID + DateTime.Now.microTime().ToString()) + "109009").ToUpper();

                return View();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Pago credito        
        /// </summary>
        /// <param name="postObject"></param>
        /// <returns></returns>
        [authorizationFilter(authorizationFilter.AUTHENTICATED_PERMISSION)]
        public async Task<ActionResult> tokenizationPayment(string postObject)
        {
            try
            {
                payment paymentObject = JsonConvert.DeserializeObject<payment>(postObject);
                double value = 0;
                double.TryParse(paymentObject.value, out value);

                // INCLUIR OTRAS VALIDACIONES
                if (value == 0)
                    return Json(new { Success = false, Message = "El valor por pagar no es válido" }, JsonRequestBehavior.AllowGet);

                //****************************************************************************************************************
                // VERIFIES COOKIE
                //****************************************************************************************************************
                string cookieValue = cookieManager.readFromCookie(cookieName, "PAYMENT");


                try
                {
                    // decrypts for validation
                    string decryptedCookieValue = Encrypt.decrypt(cookieValue, encriptSalt);
                }
                catch (Exception ex)
                {
                    Response.Clear();
                    Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                    return Json(new { Success = false, Error="COOKIE" ,Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }


                //****************************************************************************************************************
                // SENDS PAYMENT
                //****************************************************************************************************************
                POCOConfigHelper helper = configurationReader.readHelper("REST.Payment token", configPlatform);
                bindingTransactionWrapper transaction = new bindingTransactionWrapper()
                {
                    paymentCountry = "CO",
                    ipAddress = GetIPAddress(),
                    cookie = cookieValue,
                    userAgent = Request.UserAgent,
                    creditCardTokenId = paymentObject.token,
                    paymentMethod = paymentObject.paymentMethod,
                    deviceSessionId = paymentObject.deviceSessionId,
                    creditCard = new bindingCreditCard()
                    {
                        installements = paymentObject.installements
                    },
                    order = new bindingOrderWrapper()
                    {
                        accountId = configurationReader.read("PayU.merchantId", "AWESPaymentProvider"),
                        referenceCode = paymentObject.reference,
                        description = "PAGO DE LA RESERVA " + paymentObject.reference,
                        notifyUrl = "http://localhost:9793/payment/TokenizationResponse",
                        netValue = value,
                        buyer = new bindingBuyerWrapper()
                        {
                            merchantBuyerId = userId.ToString(),
                            fullName = paymentObject.fullName,
                            emailAddress = userAdministrator.instance.user.email,
                            contactPhone = "571342343",
                            dniNumber = "767554433",
                            shippingAddress = new bindingAddressWrapper()
                            {
                                street1 = "Direccion",
                                street2 = "",
                                city = "Ciudad",
                                state = "D.C.",
                                country = "CO",
                                postalCode = "00000",
                                phone = "571342343",
                            }
                        },
                        shippingAddress = new bindingAddressWrapper()
                        {
                            street1 = "Direccion",
                            street2 = "",
                            city = "Ciudad",
                            state = "D.C.",
                            country = "CO",
                            postalCode = "00000",
                            phone = "571342343",
                        }
                    },
                    payer = new bindingPayer()
                    {
                        fullName = paymentObject.fullName,
                        emailAddress = userAdministrator.instance.user.email,
                        contactPhone = "571342343",
                        dniNumber = "767554433",
                        billingAddress = new bindingAddressWrapper()
                        {
                            street1 = "Direccion",
                            street2 = "",
                            city = "Ciudad",
                            state = "D.C.",
                            country = "CO",
                            postalCode = "00000",
                            phone = "571342343",
                        },
                        merchantPayerId = userId.ToString()
                    }
                };

                // 
                var responseString = await webPost.asyncAction(helper.method,
                                                           JsonConvert.SerializeObject(transaction),
                                                           helper.server,
                                                           helper.uri,
                                                           "application/json",
                                                           "application/json",
                                                           paymentSignatureName,
                                                           paymentSignatureValue);

                //****************************************************************************************************************
                // DECODE RESPONSE
                //****************************************************************************************************************
                bindingTransactionResponseWrapper rspnse = JsonConvert.DeserializeObject<bindingTransactionResponseWrapper>(responseString);
                PaymentModel payment = new PaymentModel();
                payment.Franchise = paymentObject.paymentMethod;
                payment.PaymentDate = DateTime.Now;
                payment.PaymentMethod = paymentObject.paymentMethod;
                payment.PaymentState = rspnse.state;
                payment.TrackCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt);
                payment.AuthorizationCode = rspnse.authorizationCode;
                payment.OrderID = rspnse.orderId;
                payment.XmlCall = JsonConvert.SerializeObject(transaction);
                payment.XmlReply = responseString;
                string responsePayment = "";
                if (rspnse.state == "APPROVED" && rspnse.responseCode == "APPROVED")
                {
                    responsePayment = await paymentInsert(payment);
                    string responseEmission = await ETKTEmission(paymentObject.paymentMethod);
                }
                else
                    responsePayment = await paymentInsert(payment);

                return Json(new { OK = "payment requested ", data = responseString }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private async Task<List<tokenBind>> getTokenList(int userId)
        {
            // OBTAINS TOKEN LIST FOR THE USER
            POCOConfigHelper helper = configurationReader.readHelper("REST.User Token list", configPlatform);

            string parametrizedUri = helper.uri.Replace("{userID}", userId.ToString());

            string returnString = await webPost.asyncAction(helper.method,
                                                       null,
                                                       helper.server,
                                                       parametrizedUri,
                                                       "application/json",
                                                       "application/json",
                                                       signatureName,
                                                       signatureValue);

            return JsonConvert.DeserializeObject<List<tokenBind>>(returnString);
        }
    }
}