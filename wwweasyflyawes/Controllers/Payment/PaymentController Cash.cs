﻿using AWESAirline.Models;
using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using AWESMVCCommon.Security;
using AWESPaymentModel.BindingObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using wwweasyflyawes.Models;
using System.Web;
using AWESCommon.reservationSupport;

namespace AWESAirline.Controllers
{
    public partial class PaymentController : baseController
    {
        /// <summary>
        /// Pagina en donde se envia el pago
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> cash()
        {
            try
            {
                // GETS RESERVATION INFO
                // id interno de referencia
                string returnString = await PNRInfo();
                PNR PNRTracking = JsonConvert.DeserializeObject<PNR>(returnString);

                // id interno de referencia
                ViewBag.reference = PNRTracking.ReservationCode;

                // avlor total por pagar incluyendo todos los impuestos y cargos
                ViewBag.valueToPay = PNRTracking.KIU_TOTAL;

                //****************************************************************************************************************
                // COOKIE
                //****************************************************************************************************************
                // GENERATES TEMPORARY VERIFICATION COOKIE
                // SAVE FOR FURTHER VALIDATION
                string validationCode = System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString();

                // WRITES COOKIE
                string cookieValue = Encrypt.encrypt(validationCode, encriptSalt);

                // DEFINES COOKIE AND PROFILE EXPIRATION
                TimeSpan expiration = new TimeSpan(0, 5, 0);
                cookieManager.updatesCookie(cookieName, cookieValue, "PAYMENT", expiration);

                return View();

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// PAgo cash
        /// </summary>
        /// <param name="postObject"></param>
        /// <returns></returns>
        public async Task<JsonResult> cashPayment(string postObject)
        {
            try
            {
                payment paymentObject = JsonConvert.DeserializeObject<payment>(postObject);
                double value = 0;
                double.TryParse(paymentObject.value, out value);
                string paxString = await paxInfoSync();
                List<Pax> paxList = JsonConvert.DeserializeObject<List<Pax>>(paxString);

                // INCLUIR OTRAS VALIDACIONES
                if (value == 0)
                    return Json(new { Success = false, Message = "El valor por pagar no es válido" }, JsonRequestBehavior.AllowGet);

                //****************************************************************************************************************
                // VERIFIES COOKIE
                //****************************************************************************************************************
                string cookieValue = cookieManager.readFromCookie(cookieName, "PAYMENT");

                // decrypts for validation
                string decryptedCookieValue = Encrypt.decrypt(cookieValue, encriptSalt);

                // expiration date validation
                DateTime expirationDate = DateTime.UtcNow;

                if (!userAdministrator.instance.user.isAuthenticated)
                {
                    expirationDate=expirationDate.AddHours(1);
                }
                else {
                    int hoursToExpired = 0;
                    string returnString = await PNRInfo();
                    PNR PNRTracking = JsonConvert.DeserializeObject<PNR>(returnString);
                    string reservationInformation = await reservationInfo();
                    Reservation reservationTracking = JsonConvert.DeserializeObject<Reservation>(reservationInformation);

                    //COLOMBIAN CURRENT DATETIME
                    DateTime currentTime = DateTime.UtcNow.AddHours(-5);
                    DateTime PNRTime = DateTime.ParseExact(PNRTracking.PNRDate.Replace("T", " ").Split('.')[0], "yyyy-MM-dd HH:mm:ss",
                                               System.Globalization.CultureInfo.InvariantCulture);
                    DateTime DepartureTime = DateTime.ParseExact(reservationTracking.DepartureDateTime.Replace("T", " ").Split('.')[0], "yyyy-MM-dd HH:mm:ss",
                                               System.Globalization.CultureInfo.InvariantCulture);
                    PNRTime = PNRTime.AddHours(-5);
                    if (DepartureTime.Year == currentTime.Year && DepartureTime.Month == currentTime.Month && DepartureTime.Day == currentTime.Day)
                    {
                        hoursToExpired = 3;
                    }
                    else
                    {
                        if (DepartureTime > currentTime)
                        {
                            hoursToExpired = 6;
                        }
                    }
                    expirationDate=expirationDate.AddHours(hoursToExpired);
                }

                //****************************************************************************************************************
                // SENDS PAYMENT
                //****************************************************************************************************************
                POCOConfigHelper helper = configurationReader.readHelper("REST.Payment cash", configPlatform);
                bindingTransactionWrapper transaction = new bindingTransactionWrapper()
                {
                    paymentMethod = paymentObject.paymentMethod,
                    expirationDate = expirationDate.ToString("s"),
                    paymentCountry = "CO",
                    ipAddress = GetIPAddress(),
                    cookie = cookieValue,
                    userAgent = Request.UserAgent,
                    order = new bindingOrderWrapper()
                    {
                        accountId = configurationReader.read("PayU.merchantId", "AWESPaymentProvider"),
                        referenceCode = paymentObject.reference,
                        description = "PAGO DE LA RESERVA " + paymentObject.reference,
                        notifyUrl = configurationReader.read("urlResponse", "MVCFuncional") + "cashResponse",//"http://localhost:9793/payment/cashResponse",
                        netValue = value,
                        buyer = new bindingBuyerWrapper()
                        {
                            fullName = paxList[0].Name + " " + paxList[0].LastName,
                            emailAddress = paxList[0].Email,
                            contactPhone = paxList[0].MobileNumber,
                            dniNumber = paxList[0].DocumentNumber,
                        }
                    }
                };

                // 
                var responseString = await webPost.asyncAction(helper.method,
                                                           JsonConvert.SerializeObject(transaction),
                                                           helper.server,
                                                           helper.uri,
                                                           "application/json",
                                                           "application/json",
                                                           paymentSignatureName,
                                                           paymentSignatureValue);

                //****************************************************************************************************************
                // DECODE RESPONSE
                //****************************************************************************************************************
                bindingTransactionResponseWrapper rspnse = JsonConvert.DeserializeObject<bindingTransactionResponseWrapper>(responseString);
                PaymentModel payment = new PaymentModel();
                payment.Franchise = paymentObject.paymentMethod;
                payment.PaymentDate = DateTime.Now;
                payment.PaymentMethod = paymentObject.paymentMethod;
                payment.PaymentState = rspnse.state;
                payment.TrackCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt);
                payment.AuthorizationCode = rspnse.authorizationCode;
                payment.OrderID = rspnse.orderId;
                payment.XmlCall = JsonConvert.SerializeObject(transaction);
                payment.XmlReply = responseString;
                string responsePayment = await paymentInsert(payment);
                return Json(new { OK = "payment requested ", data = responseString, method = paymentObject.paymentMethod }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Página de respuesta de pago efectivo
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> cashResponse()
        {
            try
            {
                //****************************************************************************************************************
                // VALUES
                //****************************************************************************************************************
                debitPaymentResponse cashPayment = debitPaymentResponse.fromQueryString(Request.QueryString);
                PaymentModel payment = new PaymentModel();
                payment.Franchise = "CASH";
                payment.PaymentDate = DateTime.Now;
                payment.PaymentMethod = "CASH";
                payment.PaymentState = cashPayment.lapTransactionState;
                payment.TrackCode = "";//Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt);
                payment.AuthorizationCode = cashPayment.authorizationCode;
                payment.OrderID = cashPayment.reference_pol;
                payment.XmlCall = "";
                payment.XmlReply = JsonConvert.SerializeObject(Request.QueryString);

                //string responsePayment = "";
                //responsePayment = await reservationHelper.putPaymentAsync(payment.TrackCode, Newtonsoft.Json.JsonConvert.SerializeObject(payment));

                ViewBag.BadResponseCode = "";
                if (cashPayment.lapTransactionState == "APPROVED" && cashPayment.lapResponseCode == "APPROVED")
                {
                    string responseEmission = await ETKTEmission("CASH");
                }

                return Redirect("/");

            }
            catch (Exception ex)
            {
                return Redirect("/");
            }
        }
        
    }
}
