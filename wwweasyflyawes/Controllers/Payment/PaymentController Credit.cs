﻿using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using AWESMVCCommon.Security;
using AWESAirline.Models;
using AWESPaymentModel.BindingObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using AWESCommon.Extensions;
using wwweasyflyawes.Models;
using AWESCommon.reservationSupport;

namespace AWESAirline.Controllers
{
    public partial class PaymentController : baseController
    {
        /// <summary>
        /// Pagina en donde se envia el pago
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> credit()
        {
            try
            {
                // GETS RESERVATION INFO
                // id interno de referencia
                string returnString = await PNRInfo();
                PNR PNRTracking = JsonConvert.DeserializeObject<PNR>(returnString);

                // id interno de referencia
                ViewBag.reference = PNRTracking.ReservationCode;

                // avlor total por pagar incluyendo todos los impuestos y cargos
                ViewBag.valueToPay = PNRTracking.KIU_TOTAL;

                //****************************************************************************************************************
                // COOKIE
                //****************************************************************************************************************
                // GENERATES TEMPORARY VERIFICATION COOKIE
                // SAVE FOR FURTHER VALIDATION
                string validationCode = System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString();

                // WRITES COOKIE
                string cookieValue = Encrypt.encrypt(validationCode, encriptSalt);

                // DEFINES COOKIE AND PROFILE EXPIRATION
                TimeSpan expiration = new TimeSpan(0, 5, 0);
                cookieManager.updatesCookie(cookieName, cookieValue, "PAYMENT", expiration);

                //****************************************************************************************************************
                // DEVICE SESSION ID
                //****************************************************************************************************************
                ViewBag.deviceSessionId = (Encrypt.md5(HttpContext.Session.SessionID + DateTime.Now.microTime().ToString()) + "109009").ToUpper();

                return View();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Credit payment
        /// </summary>
        /// <param name="postObject"></param>
        /// <returns></returns>
        public async Task<ActionResult> creditCardPayment(string postObject)
        {
            try
            {
                payment paymentObject = JsonConvert.DeserializeObject<payment>(postObject);
                double value = 0;
                double.TryParse(paymentObject.value, out value);

                if (value == 0)
                    return Json(new { Success = false, Message = "El valor por pagar no es válido" }, JsonRequestBehavior.AllowGet);

                //****************************************************************************************************************
                // VERIFIES COOKIE
                //****************************************************************************************************************
                string cookieValue = cookieManager.readFromCookie(cookieName, "PAYMENT");

                try
                {
                    // decrypts for validation
                    string decryptedCookieValue = Encrypt.decrypt(cookieValue, encriptSalt);
                }
                catch (Exception ex)
                {
                    Response.Clear();
                    Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                    return Json(new { Success = false, Error = "COOKIE", Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }


                //****************************************************************************************************************
                // SENDS PAYMENT
                //****************************************************************************************************************
                POCOConfigHelper helper = configurationReader.readHelper("REST.Payment creditCard", configPlatform);
                bindingTransactionWrapper transaction = new bindingTransactionWrapper()
                {
                    paymentMethod = paymentObject.paymentMethod,
                    paymentCountry = "CO",
                    ipAddress = GetIPAddress(),
                    cookie = cookieValue,
                    userAgent = Request.UserAgent,
                    deviceSessionId = paymentObject.deviceSessionId,
                    order = new bindingOrderWrapper()
                    {
                        referenceCode = paymentObject.reference,
                        description = "PAGO DE LA RESERVA " + paymentObject.reference,
                        notifyUrl = configurationReader.read("urlResponseDefault", "MVCFuncional") + "summary",//"http://localhost:9793/payment/creditoResponse",
                        netValue = value,
                        buyer = new bindingBuyerWrapper()
                        {
                            fullName = paymentObject.fullName,
                            emailAddress = paymentObject.email,
                            contactPhone = paymentObject.phone,
                            dniNumber = paymentObject.documentNumber,
                            shippingAddress = new bindingAddressWrapper()
                            {
                                street1 = paymentObject.street,
                                city = paymentObject.city,
                                state = paymentObject.state,
                                country = paymentObject.coutryCode,
                                postalCode = paymentObject.postalCode,
                                phone = paymentObject.phone
                            }
                        },
                        shippingAddress = new bindingAddressWrapper()
                        {
                            street1 = paymentObject.street,
                            city = paymentObject.city,
                            state = paymentObject.state,
                            country = paymentObject.coutryCode,
                            postalCode = paymentObject.postalCode,
                            phone = paymentObject.phone
                        }
                    },
                    payer = new bindingPayer()
                    {
                        fullName = paymentObject.fullName,
                        emailAddress = paymentObject.email,
                        contactPhone = paymentObject.phone,
                        dniNumber = paymentObject.documentNumber,
                        billingAddress = new bindingAddressWrapper()
                        {
                            street1 = paymentObject.street,
                            city = paymentObject.city,
                            state = paymentObject.state,
                            country = paymentObject.coutryCode,
                            postalCode = paymentObject.postalCode,
                            phone = paymentObject.phone
                        }
                    },
                    creditCard = new bindingCreditCard()
                    {
                        number = paymentObject.cardNumber,
                        securityCode = paymentObject.securityCode,
                        expirationMonth = paymentObject.expirationMonth,
                        expirationYear = paymentObject.expirationYear,
                        installements = paymentObject.installements,
                        name = paymentObject.nameOnCard
                    }
                };

                // 
                var responseString = await webPost.asyncAction(helper.method,
                                                                   JsonConvert.SerializeObject(transaction),
                                                                   helper.server,
                                                                   helper.uri,
                                                                   "application/json",
                                                                   "application/json",
                                                                   paymentSignatureName,
                                                                   paymentSignatureValue);

                //****************************************************************************************************************
                // DECODE RESPONSE
                //****************************************************************************************************************
                bindingTransactionResponseWrapper rspnse = new bindingTransactionResponseWrapper();
                bool wasError = false;
                try
                {
                    rspnse = JsonConvert.DeserializeObject<bindingTransactionResponseWrapper>(responseString);
                }
                catch
                {
                    wasError = true;
                    PaymentModel payment = new PaymentModel();
                    payment.Franchise = paymentObject.paymentMethod;
                    payment.PaymentDate = DateTime.Now;
                    payment.PaymentMethod = paymentObject.paymentMethod;
                    payment.PaymentState = "PENDING";
                    payment.TrackCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt);
                    payment.AuthorizationCode = "";
                    payment.OrderID = "";
                    string responsePayment = "";
                    string returnPayment = paymentInfoNotAsync();

                    if (returnPayment == "null")
                    {
                        responsePayment = paymentInsertNotAsync(payment);
                    }
                    else
                    {
                        responsePayment = reservationHelper.putPayment(payment.TrackCode, Newtonsoft.Json.JsonConvert.SerializeObject(payment));
                    } 
                }
                if (!wasError)
                {
                    PaymentModel payment = new PaymentModel();
                    payment.Franchise = paymentObject.paymentMethod;
                    payment.PaymentDate = DateTime.Now;
                    payment.PaymentMethod = paymentObject.paymentMethod;
                    payment.PaymentState = rspnse.state;
                    payment.TrackCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt);
                    payment.AuthorizationCode = rspnse.authorizationCode;
                    payment.OrderID = rspnse.orderId;
                    transaction.creditCard = null;
                    transaction.payer.billingAddress = null;
                    transaction.order.buyer.shippingAddress = null;
                    transaction.order.shippingAddress = null;
                    transaction.payer.fullName = null;
                    transaction.payer.emailAddress = null;
                    transaction.payer.contactPhone = null;
                    transaction.payer.dniNumber = null;
                    payment.XmlCall = JsonConvert.SerializeObject(transaction);
                    payment.XmlReply = responseString;
                    string responsePayment = "";

                    string returnPayment = await paymentInfo();

                    if (returnPayment == "null")
                    {
                        responsePayment = await paymentInsert(payment);
                    }
                    else
                    {
                        responsePayment = reservationHelper.putPayment(payment.TrackCode, Newtonsoft.Json.JsonConvert.SerializeObject(payment));
                    } 
                }

                if (rspnse.state == "APPROVED" && rspnse.responseCode == "APPROVED")
                {
                    string responseEmission = await ETKTEmission(paymentObject.paymentMethod);
                }

                cookiePayResponse["State"] = rspnse.state;
                cookiePayResponse["CodeResponse"] = rspnse.responseCode;
                cookiePayResponse.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(cookiePayResponse);
                return Json(new { OK = "payment requested ", data = responseString }, JsonRequestBehavior.AllowGet);
                //return Redirect("/Summary");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("[PSE]"))
                {
                    return Json(new { OK = "badResponseCode", data = "PSENotAviable" }, JsonRequestBehavior.AllowGet);
                }
                if (ex.Message.Contains("[VISA]") || ex.Message.Contains("[MASTERCARD]") || ex.Message.Contains("[AMEX]") || ex.Message.Contains("[DINERS]"))
                {
                    return Json(new { OK = "badResponseCode", data = "TCNotAviable" }, JsonRequestBehavior.AllowGet);
                }
                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Página de respuesta de pago crédito
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> creditResponse(string strState, string strOrderID)
        {
            try
            {
                //****************************************************************************************************************
                // VERIFIES COOKIE
                //****************************************************************************************************************
                string cookieValue = cookieManager.readFromCookie(cookieName, "PAYMENT");

                // decrypts for validation
                string decryptedCookieValue = Encrypt.decrypt(cookieValue, encriptSalt);

                //****************************************************************************************************************
                // VALUES
                //****************************************************************************************************************
                //bindingTransactionResponseWrapper creditPayment = JsonConvert.DeserializeObject<bindingTransactionResponseWrapper>(strResponse);
                string returnString = await PNRInfo();
                PNR PNRTracking = JsonConvert.DeserializeObject<PNR>(returnString);
                ViewBag.Code = PNRTracking.ReservationCode;
                ViewBag.valueToPay = PNRTracking.KIUPrice;
                ViewBag.IP = PNRTracking.IP;
                ViewBag.state = strState;
                ViewBag.ID = strOrderID;
                ViewBag.date = DateTime.Now.AddHours(-5);
                return View();

            }
            catch (Exception ex)
            {
                return Redirect("/");
            }
        }
    }
}