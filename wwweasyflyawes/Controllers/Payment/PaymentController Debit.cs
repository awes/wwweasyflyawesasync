﻿using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using AWESMVCCommon.Security;
using AWESAirline.Models;
using AWESPaymentModel.BindingObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using wwweasyflyawes.Models;
using System.Configuration;
using AWESCommon.reservationSupport;

namespace AWESAirline.Controllers
{
    public partial class PaymentController : baseController
    {
        /// <summary>
        /// Pagina en donde se envia el pago
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> debit()
        {
            try
            {
                // GETS RESERVATION INFO
                // id interno de referencia
                string pnrInfo = await PNRInfo();
                PNR PNRTracking = JsonConvert.DeserializeObject<PNR>(pnrInfo);

                // id interno de referencia
                ViewBag.reference = PNRTracking.ReservationCode;

                // avlor total por pagar incluyendo todos los impuestos y cargos
                ViewBag.valueToPay = PNRTracking.KIU_TOTAL;


                // LISTA DE BANCOS
                ViewBag.listaBancos = new List<object>();

                try
                {
                    // LISTA DE BANCOS
                    POCOConfigHelper helper = configurationReader.readHelper("REST.Banks list", configPlatform);

                    string returnString = await webPost.asyncAction(helper.method,
                                                                    null,
                                                                    helper.server,
                                                                    helper.uri,
                                                                    "application/json",
                                                                    "application/json",
                                                                    paymentSignatureName,
                                                                    paymentSignatureValue);

                    // deserializa
                    ICollection<Bank> bankList = JsonConvert.DeserializeObject<ICollection<Bank>>(returnString);

                    // LISTA DE BANCOS
                    ViewBag.listaBancos = bankList;
                    if (bankList.Count > 0)
                    {
                        ViewBag.bankListHasValues = true;
                    }
                }
                catch 
                {
                    ViewBag.bankListHasValues = false;
                    ViewBag.listaBancos = "";
                }

                //****************************************************************************************************************
                // COOKIE
                //****************************************************************************************************************
                // GENERATES TEMPORARY VERIFICATION COOKIE
                // SAVE FOR FURTHER VALIDATION
                string validationCode = System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString();

                // WRITES COOKIE
                string cookieValue = Encrypt.encrypt(validationCode, encriptSalt);

                // DEFINES COOKIE AND PROFILE EXPIRATION
                TimeSpan expiration = new TimeSpan(0, 5, 0);
                cookieManager.updatesCookie(cookieName, cookieValue, "PAYMENT", expiration);

                return View();

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// PAgo debito
        /// </summary>
        /// <param name="postObject"></param>
        /// <returns></returns>
        public async Task<JsonResult> debitPayment(string postObject)
        {
            try
            {
                payment paymentObject = JsonConvert.DeserializeObject<payment>(postObject);
                double value = 0;
                double.TryParse(paymentObject.value, out value);

                // INCLUIR OTRAS VALIDACIONES
                if (value == 0)
                    return Json(new { Success = false, Message = "El valor por pagar no es válido" }, JsonRequestBehavior.AllowGet);

                //****************************************************************************************************************
                // VERIFIES COOKIE
                //****************************************************************************************************************
                string cookieValue = cookieManager.readFromCookie(cookieName, "PAYMENT");

                try
                {
                    // decrypts for validation
                    string decryptedCookieValue = Encrypt.decrypt(cookieValue, encriptSalt);
                }
                catch (Exception ex)
                {
                    Response.Clear();
                    Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                    return Json(new { Success = false, Error = "COOKIE", Message = ex.Message }, JsonRequestBehavior.AllowGet);
                }



                //****************************************************************************************************************
                // SENDS PAYMENT
                //****************************************************************************************************************
                //REST.reservation paymentGet
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation paymentGet", configPlatform);

                string parametrizedUri = helper.uri;
                parametrizedUri = parametrizedUri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt));

                parametrizedUri = parametrizedUri + "?email=" + paymentObject.email;

                string paymentVerification = webPost.action(helper.method,
                                                           null,
                                                           helper.server,
                                                           parametrizedUri,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);

                if (paymentVerification != "[]")
                {
                    return Json(new { OK = "badResponseCode", data = "emailVerification", pay=paymentVerification }, JsonRequestBehavior.AllowGet);
                }

                helper = configurationReader.readHelper("REST.Payment banktransfer", configPlatform);
                bindingTransactionWrapper transaction = new bindingTransactionWrapper()
                {

                    paymentMethod = "PSE",
                    paymentCountry = "CO",
                    ipAddress = GetIPAddress(),
                    cookie = cookieValue,
                    userAgent = Request.UserAgent,
                    order = new bindingOrderWrapper()
                    {
                        accountId = configurationReader.read("PayU.AccountID", "AWESPaymentProvider"),
                        referenceCode = paymentObject.reference,
                        description = "PAGO DE LA RESERVA " + paymentObject.reference,
                        language = "es",
                        //notifyUrl =  configurationReader.read("urlResponse", "MVCFuncional") + "debitResponse",//"http://awesairlineawestech.com/payment/debitResponse",
                        notifyUrl = null,
                        netValue = value,
                        buyer = new bindingBuyerWrapper()
                        {
                            fullName = paymentObject.fullName,
                            contactPhone = paymentObject.phone,                           
                            emailAddress = paymentObject.email,
                            dniNumber=paymentObject.documentNumber
                        }
                    },
                    payer = new bindingPayer()
                    {
                        fullName = paymentObject.fullName,
                        emailAddress = paymentObject.email,
                        contactPhone = paymentObject.phone,
                        dniNumber = paymentObject.documentNumber
                    },
                    pseParameters = new bindingPSEParameters()
                    {
                        ResponseURL = configurationReader.read("urlResponse", "MVCFuncional") + "debitResponse",//"http://awesairlineawestech.com/payment/debitResponse",
                        financialInstitutionCode = paymentObject.bank,
                        financialInstitutionName = paymentObject.bankName,
                        userType = paymentObject.personType,
                        IPAddress = GetIPAddress(),
                        userDocument = paymentObject.documentType,
                        userID = paymentObject.documentNumber
                    }
                };
                // 
                var responseString = webPost.action(helper.method,
                                                           JsonConvert.SerializeObject(transaction),
                                                           helper.server,
                                                           helper.uri,
                                                           "application/json",
                                                           "application/json",
                                                           paymentSignatureName,
                                                           paymentSignatureValue);

                //****************************************************************************************************************
                // DECODE RESPONSE
                //****************************************************************************************************************
                bool wasError = false;
                bindingTransactionResponseWrapper rspnse = new bindingTransactionResponseWrapper();
                try
                {
                    rspnse = JsonConvert.DeserializeObject<bindingTransactionResponseWrapper>(responseString);
                }
                catch
                {
                    wasError = true;
                    PaymentModel payment = new PaymentModel();
                    payment.Franchise = "PSE";
                    payment.PaymentDate = DateTime.Now;
                    payment.PaymentMethod = "PSE";
                    payment.PaymentState = "PENDING";
                    payment.TrackCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt);
                    payment.AuthorizationCode = "";
                    payment.OrderID = "";
                    payment.XmlCall = Newtonsoft.Json.JsonConvert.SerializeObject(paymentObject.email);
                    payment.XmlReply = responseString.ToString();

                    try
                    {
                        string responsePayment = "";
                        string returnPayment = paymentInfoNotAsync();

                        if (returnPayment == "null")
                        {
                            responsePayment = paymentInsertNotAsync(payment);
                        }
                        else
                        {
                            responsePayment = reservationHelper.putPayment(payment.TrackCode, Newtonsoft.Json.JsonConvert.SerializeObject(payment));
                        }
                    }
                    catch
                    {

                    }
                }

                if (!wasError)
                {
                    PaymentModel payment = new PaymentModel();
                    payment.Franchise = "PSE";
                    payment.PaymentDate = DateTime.Now;
                    payment.PaymentMethod = "PSE";
                    payment.PaymentState = "PENDING";
                    payment.TrackCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt);
                    payment.AuthorizationCode = "";
                    if (!string.IsNullOrEmpty(rspnse.orderId))
                    {
                        payment.OrderID = rspnse.orderId;
                    }
                    else
                    {
                        payment.OrderID = "";
                    }
                    payment.XmlCall = Newtonsoft.Json.JsonConvert.SerializeObject(paymentObject.email);
                    payment.XmlReply = responseString.ToString();
                    try
                    {
                        string responsePayment = "";
                        string returnPayment = await paymentInfo();

                        if (returnPayment == "null")
                        {
                            responsePayment = await paymentInsert(payment);
                        }
                        else
                        {
                            responsePayment = reservationHelper.putPayment(payment.TrackCode, Newtonsoft.Json.JsonConvert.SerializeObject(payment));
                        }
                    }
                    catch
                    {

                    }
                }
                return Json(new { OK = "payment requested ", data = responseString }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                if(ex.Message.Contains("[PSE]")){
                    return Json(new { OK = "badResponseCode", data = "PSENotAviable" }, JsonRequestBehavior.AllowGet);
                }
                if (ex.Message.Contains("[VISA]") || ex.Message.Contains("[MASTERCARD]") || ex.Message.Contains("[AMEX]") || ex.Message.Contains("[DINERS]"))
                {
                    return Json(new { OK = "badResponseCode", data = "TCNotAviable" }, JsonRequestBehavior.AllowGet);
                }
                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// PAgina de respuesta de pago
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> debitResponse()
        {
            try
            {
                //****************************************************************************************************************
                // VERIFIES COOKIE
                //****************************************************************************************************************
                //string cookieValue = cookieManager.readFromCookie(cookieName, "PAYMENT");

                //// decrypts for validation
                //string decryptedCookieValue = Encrypt.decrypt(cookieValue, encriptSalt);

                //****************************************************************************************************************
                // VALUES
                //****************************************************************************************************************
                debitPaymentResponse debitPayment = debitPaymentResponse.fromQueryString(Request.QueryString);
                PaymentModel payment = new PaymentModel();
                payment.Franchise = "PSE";
                payment.PaymentDate = DateTime.Now;
                payment.PaymentMethod = "PSE";
                payment.PaymentState = debitPayment.lapTransactionState;
                payment.TrackCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt);
                payment.AuthorizationCode = debitPayment.authorizationCode;
                payment.OrderID = debitPayment.reference_pol;
                payment.XmlCall = "";
                payment.XmlReply = JsonConvert.SerializeObject(Request.QueryString);

                string firstResponseCode = Request.QueryString["debit"];

                if (!string.IsNullOrEmpty(firstResponseCode)) {
                    debitPayment.lapResponseCode = firstResponseCode;
                }

                string responsePayment = "";
                responsePayment = reservationHelper.putPayment(payment.TrackCode, Newtonsoft.Json.JsonConvert.SerializeObject(payment));
                
                ViewBag.BadResponseCode = "";
                if (debitPayment.lapTransactionState == "APPROVED" && debitPayment.lapResponseCode == "APPROVED")
                {
                    string responseEmission = await ETKTEmission("PSE");
                }
                if (debitPayment.lapResponseCode == "EXCEEDED_AMOUNT") {
                    ViewBag.BadResponseCode = debitPayment.lapResponseCode;
                    ViewBag.MessageBadResponse = getStringResource("badCodeEXCEEDED_AMOUNT", "El monto de la transacción excede los límites establecidos, por favor comuníquese con nuestras líneas de atención al cliente al teléfono 4148095-94 o enviarnos sus inquietudes o sugerencias al correo reservas.call@easyfly.com.co", Request.Url.ToString(), Request.UserLanguages);
                    ViewBag.StatusBadResponse = getStringResource("badStatusDeclinedTransaction", "Transacción rechazada", Request.Url.ToString(), Request.UserLanguages);
                }
                if (debitPayment.lapResponseCode == "BANK_UNREACHABLE")
                {
                    return Redirect("/payment?badResponseCode=" + debitPayment.lapResponseCode);
                }
                if (debitPayment.lapResponseCode == "INTERNAL_PAYMENT_PROVIDER_ERROR") {
                    ViewBag.BadResponseCode = debitPayment.lapResponseCode;
                    ViewBag.MessageBadResponse = getStringResource("badCodeINTERNAL_PAYMENT_PROVIDER_ERROR", "No se pudo crear la transacción, por favor intente más tarde o comuníquese con nuestras líneas de atención al cliente al teléfono 4148095-94 o enviarnos sus inquietudes o sugerencias al correo reservas.call@easyfly.com.co", Request.Url.ToString(), Request.UserLanguages);
                    ViewBag.StatusBadResponse = getStringResource("badStatusDeclinedTransaction", "Transacción rechazada", Request.Url.ToString(), Request.UserLanguages);
                }
                cookiePayResponse["State"] = debitPayment.lapTransactionState;
                cookiePayResponse["CodeResponse"] = debitPayment.lapResponseCode;
                cookiePayResponse.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(cookiePayResponse);
                debitPayment.retryLink = "/payment";
                debitPayment.endTransactionLink = "/Summary";
                if (debitPayment.processingDate == null) {
                    debitPayment.processingDate = DateTime.UtcNow.AddHours(-5).ToString();
                }
                ViewBag.IP = GetIPAddress();

                return View(debitPayment);

                //return Redirect("/Summary");
                
            }
            catch (Exception ex)
            {
                return Redirect("/");
            }
        }
    }
}