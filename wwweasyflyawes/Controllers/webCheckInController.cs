﻿using AWESAirline.Controllers;
using AWESCommon.webCheckInSupport;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace wwweasyflyawes.Controllers
{
    public class webCheckInController : baseController
    {
        // GET: webCheckIn
        public async Task<ActionResult> Index(string codReservation, string lastName, string email, bool cancellation = false, string flightChecked=null)
        {
            if (string.IsNullOrEmpty(email))
                email = "diego.bernal@easyfly.com.co";
            List<POCOFlightInfoEntry> flightsInfo = new List<POCOFlightInfoEntry>();
            try
            {
                flightsInfo = await checkInHelper.getSeatMapAsync(codReservation, lastName, GetIPAddress());
            }
            catch
            {
                cookieHasError["error"] = "checkInError";
                cookieHasError.Expires = DateTime.Now.AddSeconds(30);
                Response.Cookies.Add(cookieHasError);

                return RedirectToAction("Index", "Home");
            }

            if (flightsInfo.Count == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            // dictionary to contain the availibility to do a checkIn per flight
            Dictionary<string, bool> canCheckIn = new Dictionary<string, bool>();
            foreach (POCOFlightInfoEntry flight in flightsInfo)
            {
                DateTime flightDate = DateTime.ParseExact(flight.departureDateTime, "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);

                bool paxesChecked = true;

                foreach (POCOPaxEntry pax in flight.passangers)
                {
                    if (string.IsNullOrEmpty(pax.Seat))
                    {
                        paxesChecked = paxesChecked && false;
                        break;
                    }
                    if (string.IsNullOrEmpty(pax.Seat.Trim()))
                    {
                        paxesChecked = paxesChecked && false;
                        break;
                    }
                }
                if (flightDate > DateTime.Now && DateTime.Now.AddHours(36) > flightDate && !paxesChecked)
                {
                    canCheckIn.Add(flight.flight, true);

                }
                else
                {
                    canCheckIn.Add(flight.flight, false);
                }
            }
            if (flightChecked != null) {
                List<string> flightsChecked = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(flightChecked);
                foreach (var flightToVerify in flightsChecked)
                {
                    if (canCheckIn.Keys.Contains(flightToVerify))
                    {
                        if (canCheckIn[flightToVerify])
                        {
                            canCheckIn[flightToVerify] = false;
                        }
                    }
                }
            }
            string flightSerialized = "";
            foreach (POCOFlightInfoEntry flight in flightsInfo)
            {
                if (canCheckIn[flight.flight]) {
                    flightSerialized = Newtonsoft.Json.JsonConvert.SerializeObject(flight).ToString();
                    break;
                }
            }
            ViewBag.flightSerialized = flightSerialized;
            ViewBag.checkInInfo = flightsInfo;
            ViewBag.canCheckIn = canCheckIn;
            ViewBag.codReservation = codReservation;
            ViewBag.LastName = lastName;
            ViewBag.email = email;

            string PNRSerialized = await checkInHelper.getPNRbyReservationCodeAsync(codReservation);
            var dummy = new { TrackCode = "" };
            var trackingCode = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(PNRSerialized, dummy);
            string etkt = await getETKT(trackingCode.TrackCode.ToString());
            DataTable ETKTInfoTable = default(DataTable);
            ETKTInfoTable = Newtonsoft.Json.JsonConvert.DeserializeObject<DataTable>(etkt);

            Dictionary<string, string> idETKT = new Dictionary<string, string>();

            foreach (DataRow row in ETKTInfoTable.Rows)
            {
                idETKT.Add(row["DocId"].ToString(), row["ETKT"].ToString()); 
            }

            bool moreCheckIn = false;
            foreach (KeyValuePair<string, bool> flight in canCheckIn)
            {
                moreCheckIn = flight.Value || moreCheckIn;
            }
            ViewBag.moreCheckIn = moreCheckIn;
            ViewBag.idETKT = idETKT;
            if (moreCheckIn)
                return View();
            else
                return RedirectToAction("confirmationCheckIn","webCheckIn",new {});
        }
        public async Task<ActionResult> checkIn(string pnr, string flightInfo, string lastName, string email, string alternativeEmail, string alternativeEmailConfirmation, string seat, string flightChecked)
        {

            flightInfo = flightInfo.Replace("&quot;", "" + '"' + "");
            POCOFlightInfoEntry flightDeserialized = Newtonsoft.Json.JsonConvert.DeserializeObject<POCOFlightInfoEntry>(flightInfo);
            Dictionary<string, string> paxSeat = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(seat);
            List<string> flightsChecked = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(flightChecked);

            List<POCOPaxEntry> paxes = new List<POCOPaxEntry>();

            foreach (POCOPaxEntry pax in flightDeserialized.passangers)
            {
                POCOPaxEntry paxToModify = new POCOPaxEntry();
                paxToModify = pax;
                paxToModify.Seat = paxSeat[paxToModify.DocumentNumber];
                paxes.Add(paxToModify);
            }

            flightDeserialized.passangers = paxes;
            // dictionary that contains <Seat,<ID,Confirmation>>
            Dictionary<string,Dictionary<string,bool>> checkInConfrimation = await checkInHelper.checkInAsync(pnr,flightDeserialized,lastName,email,GetIPAddress());
            
            //Sending boarding pass
            string PNRSerialized = await checkInHelper.getPNRbyReservationCodeAsync(pnr);
            var dummy = new { TrackCode = "" };
            var tracking = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(PNRSerialized, dummy);
            string trackingCode = tracking.TrackCode.ToString();

            foreach (KeyValuePair<string,Dictionary<string,bool>> seats in checkInConfrimation)
            {
                foreach (KeyValuePair<string,bool> boardings in seats.Value)
                {
                    if (boardings.Value) {
                        if (seats.Key != "0") {
                            if(alternativeEmail==alternativeEmailConfirmation && !string.IsNullOrEmpty(email)){
                                string response = await checkInHelper.boardingPassSendAsync(trackingCode,boardings.Key,seats.Key,lastName,email,alternativeEmail,flightDeserialized.flight);
                            }
                        }
                    }
                }
            }

            string listToSend = Newtonsoft.Json.JsonConvert.SerializeObject(flightsChecked);
            return RedirectToAction("Index", "webCheckIn", new { codReservation = pnr, lastName = lastName, email = email, cancellation = false, flightChecked=listToSend });
        }
        public async Task<ActionResult> confirmationCheckIn() {
            return RedirectToAction("Index", "Home", new { });
        }
    }
}