﻿using AWESAirline.Controllers;
using AWESAirline.Models;
using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.promotionSupport;
using AWESCommon.Web;
using AWESMVCCommon.Filters;
using AWESMVCCommon.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Utilidades.Models;
using wwweasyflyawes.Models;

namespace wwweasyflyawes.Controllers
{
    public class PaxController : baseController
    {
        POCOPromotionEntry promotionObject = new POCOPromotionEntry();
        private const string AUTH_PLATFORM = "AWESAuth";
        //
        public async Task<ActionResult> Index(mdlCompra oModelo)
        {
            ViewBag.origin = "";
            ViewBag.destination = "";
            ViewBag.departureTime = "";
            ViewBag.arrivalTime = "";
            ViewBag.departureTimeReturn = "";
            ViewBag.arrivalTimeReturn = "";
            ViewBag.price = "";
            ViewBag.tax = "";
            ViewBag.fee = "";
            ViewBag.typeFlight = "";
            string paxInserted = "";
            try
            {
                FormCollection Formulario = new FormCollection(HttpContext.Request.Params);

                if (Request.Cookies["UserSettings"] != null)
                {
                    Task<string> reservation;
                    string reservationResult = "";
                    string priceResult = "";
                    string returnString = await selectInfo();
                    avail availTracking = JsonConvert.DeserializeObject<avail>(returnString);
                    string reservationInformation = await reservationInfo();
                    Reservation reservationTracking = JsonConvert.DeserializeObject<Reservation>(reservationInformation);
                    oModelo.Adultos = availTracking.Adult;
                    oModelo.Niños = availTracking.Child;
                    oModelo.Infantes = availTracking.Infant;
                    string[] strPartesOrigin = availTracking.IATAOrigin.Split('(');
                    ViewBag.origin = strPartesOrigin[0];
                    string[] strPartesDestination = availTracking.IATADestination.Split('(');
                    ViewBag.destination = strPartesDestination[0];
                    ViewBag.departureTime = reservationTracking.DepartureDateTime;
                    ViewBag.arrivalTime = reservationTracking.ArrivalDateTime;
                    ViewBag.departureTimeReturn = reservationTracking.DepartureDateTimeReturn;
                    ViewBag.arrivalTimeReturn = reservationTracking.ArrivalDateTimeReturn;
                    ViewBag.price = reservationTracking.Price;
                    ViewBag.tax = reservationTracking.Tax;
                    ViewBag.fee = reservationTracking.Fee;
                    ViewBag.typeFlight = availTracking.flightType;
                    if (availTracking.DiscountCampaing != "")
                    {
                        try
                        {
                            promotionObject = promotionHelper.promotionByCode(platform, availTracking.DiscountCampaing.Substring(0, 2));
                        }
                        catch
                        {
                            promotionObject = null;
                        }
                        ViewBag.discountPercentage = promotionObject.discountPercentage;
                    }
                    else
                        ViewBag.discountPercentage = "";
                    oModelo.procesaForm(Formulario, "C");
                    oModelo.almacenarCookie = Formulario["almacenarCookie"] != "false";
                    string mobileNumber = oModelo._comprador.TelefonoCelular.Replace(",", "");
                    string email = oModelo._comprador.Email.Replace(",", "");
                    // VERIFICA SI SE HAN REGISTRADO LOS PAX
                    if (oModelo.isValidRegistroPax && oModelo.isValidComprador)
                    {
                        // GUARDA EL MODELO
                        try
                        {
                            foreach (mdlPax paxA in oModelo.paxAdultos)
                            {
                                paxInserted = paxRegister("ADT", "ID", paxA.NumeroDocumento, paxA.Nombre, paxA.Apellido, "0", mobileNumber, email);
                            }
                            foreach (mdlPax paxC in oModelo.paxNiños)
                            {
                                paxInserted = paxRegister("CNN", "ID", paxC.NumeroDocumento, paxC.Nombre, paxC.Apellido, "0", mobileNumber, email);
                            }
                            foreach (mdlPax paxI in oModelo.paxInfantes)
                            {
                                paxInserted = paxRegister("INF", "ID", paxI.NumeroDocumento, paxI.Nombre, paxI.Apellido, "0", mobileNumber, email);
                            }
                            DateTime departureTime = DateTime.ParseExact(reservationTracking.DepartureDateTime, "yyyy-MM-dd HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);
                            // COLOMBIAN CURRENT DATETIME 
                            DateTime currentTime= DateTime.UtcNow.AddHours(-5);
                            string timeLimitReservation = "";
                            if (userAdministrator.instance.user.isAuthenticated)
                            {
                                if (departureTime.Year == currentTime.Year && departureTime.Month == currentTime.Month && departureTime.Day == currentTime.Day)
                                {
                                    timeLimitReservation = "3";
                                }
                                else
                                {
                                    if (departureTime > currentTime)
                                    {
                                        timeLimitReservation = "6";
                                    }
                                    else
                                    {
                                        timeLimitReservation = "0";
                                    }
                                }
                            }
                            else
                                timeLimitReservation = "1";
                            //if (userAdministrator.instance.user.isAuthenticated)
                            //{
                                reservationResult = getReservation(availTracking.flightType.ToString(), reservationTracking.DiscountCampaing, timeLimitReservation);
                                List<string> reservationResultList = JsonConvert.DeserializeObject<List<string>>(reservationResult);
                                if (reservationResultList[0].ToString() == "Error")
                                {
                                    //throw new Exception("PaxController:getReservation:" + reservationResultList[1].ToString() + reservationResultList[2].ToString());
                                    if (reservationResultList[1].ToString() == "22030")
                                    {
                                        return Redirect("/flights?error=" + reservationResultList[1].ToString());
                                    }
                                }
                            //}
                            //else 
                            //{
                            //    priceResult = await getPrice(availTracking.flightType.ToString(), reservationTracking.DiscountCampaing);
                            //}
                            return Redirect("/Payment");
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("PaxController:Index:" + ex.Message, ex);
                        }
                    }
                    else
                    {
                        return View(oModelo);
                    }
                }
                else
                {
                    return Redirect("/");
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public string paxRegister(string paxType, string documentType, string documentNumber, string name, string lastName, string telephone, string mobile, string email)
        {
            POCOConfigHelper helper = configurationReader.readHelper("REST.reservation pax", "AWESAuth");
            string body = "{}";

            string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt));
            strParametrized = strParametrized.Replace("{paxType}", paxType);
            strParametrized = strParametrized.Replace("{documentType}", documentType);
            strParametrized = strParametrized.Replace("{documentNumber}", documentNumber);
            strParametrized = strParametrized.Replace("{name}", name);
            strParametrized = strParametrized.Replace("{lastName}", lastName);
            strParametrized = strParametrized.Replace("{telephone}", telephone);
            strParametrized = strParametrized.Replace("{mobile}", mobile);
            strParametrized = strParametrized.Replace("{email}", email);

            string returnString = webPost.action(helper.method,
                                                       body,
                                                       helper.server,
                                                       strParametrized,
                                                       "application/json",
                                                       "application/json",
                                                       ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                       ConfigurationManager.AppSettings["Consume.reservationSignature"]);
            string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);

            return serializeReturn;

            //var dummy = new[] { new { ID = 0, TrackCode = "", PaxType = "", Name = "", LastName = "", 
            //                          DocumentType = "", DocumentNumber = "", Rate = "", RateKIU = "", } };

            //var entries = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(serializeReturn, dummy);


            //return Json(new { data = JsonConvert.SerializeObject(entries) }, JsonRequestBehavior.AllowGet);
        }
        public async Task<string> paxRegisterAsync(string paxType, string documentType, string documentNumber, string name, string lastName, string telephone, string mobile, string email)
        {
            POCOConfigHelper helper = configurationReader.readHelper("REST.reservation pax", "AWESAuth");
            string body = "{}";

            string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt));
            strParametrized = strParametrized.Replace("{paxType}", paxType);
            strParametrized = strParametrized.Replace("{documentType}", documentType);
            strParametrized = strParametrized.Replace("{documentNumber}", documentNumber);
            strParametrized = strParametrized.Replace("{name}", name);
            strParametrized = strParametrized.Replace("{lastName}", lastName);
            strParametrized = strParametrized.Replace("{telephone}", telephone);
            strParametrized = strParametrized.Replace("{mobile}", mobile);
            strParametrized = strParametrized.Replace("{email}", email);

            string returnString = await webPost.asyncAction(helper.method,
                                                       body,
                                                       helper.server,
                                                       strParametrized,
                                                       "application/json",
                                                       "application/json",
                                                       ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                       ConfigurationManager.AppSettings["Consume.reservationSignature"]);
            string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);

            return serializeReturn;

            //var dummy = new[] { new { ID = 0, TrackCode = "", PaxType = "", Name = "", LastName = "", 
            //                          DocumentType = "", DocumentNumber = "", Rate = "", RateKIU = "", } };

            //var entries = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(serializeReturn, dummy);


            //return Json(new { data = JsonConvert.SerializeObject(entries) }, JsonRequestBehavior.AllowGet);
        }

        public string getPrice(string flightType, string discountCampaing)
        {
            POCOConfigHelper helper = configurationReader.readHelper("REST.Reservation Price", "AWESAuth");
            string body = null;
            string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt));
            strParametrized = strParametrized.Replace("{companyShortName}", "EF");
            strParametrized = strParametrized.Replace("{flightType}", flightType);
            strParametrized = strParametrized.Replace("{discountCampaing}", discountCampaing);
            strParametrized = strParametrized.Replace("{IP}", GetIPAddress());
            strParametrized = strParametrized.Replace("{terminalID}", "NET00EF000");
            strParametrized = strParametrized.Replace("{agentSine}", "NET00EFWW");
            strParametrized = strParametrized.Replace("{ISOCountry}", "CO");
            strParametrized = strParametrized.Replace("{ISOCurrency}", "COP");
            strParametrized = strParametrized.Replace("{requestorID}", "6");
            strParametrized = strParametrized.Replace("{bookingChannel}", "7");
            string returnString = webPost.action(helper.method,
                                                       body,
                                                       helper.server,
                                                       strParametrized,
                                                       "application/json",
                                                       "application/json",
                                                       ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                       ConfigurationManager.AppSettings["Consume.reservationSignature"]);
            string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
            return serializeReturn;
        }

        public string getReservation(string flightType, string discountCampaing, string timeLimitReservation = "6")
        {
            POCOConfigHelper helper = configurationReader.readHelper("REST.reservation reservation", "AWESAuth");
            string body = null;
            string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt));
            strParametrized = strParametrized.Replace("{timeLimitReservation}", timeLimitReservation);
            strParametrized = strParametrized.Replace("{companyShortName}", "EF");
            strParametrized = strParametrized.Replace("{flightType}", flightType);
            if (discountCampaing == "")
                strParametrized = strParametrized.Replace("{discountCampaing}", "UD");
            else
                strParametrized = strParametrized.Replace("{discountCampaing}", discountCampaing);
            strParametrized = strParametrized.Replace("{IP}", GetIPAddress());
            strParametrized = strParametrized.Replace("{terminalID}", "NET00EF000");
            strParametrized = strParametrized.Replace("{agentSine}", "NET00EFWW");
            strParametrized = strParametrized.Replace("{ISOCountry}", "CO");
            strParametrized = strParametrized.Replace("{ISOCurrency}", "COP");
            strParametrized = strParametrized.Replace("{requestorID}", "6");
            strParametrized = strParametrized.Replace("{bookingChannel}", "7");
            string returnString = webPost.action(helper.method,
                                                       body,
                                                       helper.server,
                                                       strParametrized,
                                                       "application/json",
                                                       "application/json",
                                                       ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                       ConfigurationManager.AppSettings["Consume.reservationSignature"]);
            string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
            return serializeReturn;
        }
        public async Task<JsonResult> getPaxLogin()
        {
            string returnPaxLogin = "";
            //if (userAdministrator.instance.user.isAuthenticated)
            //{
                returnPaxLogin = await paxLoginInfo(userAdministrator.instance.user.ID);
                
                //string serialized = JsonConvert.SerializeObject(returnPaxLogin);                
                //List<LoginPax> loginPaxList = JsonConvert.DeserializeObject<List<LoginPax>>(returnPaxLogin);
                //ViewBag.paxLogin = loginPaxList;
                
            //}
                return Json(new { data = returnPaxLogin }, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> deletePaxLogin(int idPax)
        {
            string returnPaxLogin = "";
            //if (userAdministrator.instance.user.isAuthenticated)
            //{
            returnPaxLogin = await deletePax(idPax);

            //string serialized = JsonConvert.SerializeObject(returnPaxLogin);                
            //List<LoginPax> loginPaxList = JsonConvert.DeserializeObject<List<LoginPax>>(returnPaxLogin);
            //ViewBag.paxLogin = loginPaxList;

            //}
            return Json(new { data = returnPaxLogin }, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> loadInfoPaxSelect(int Id)
        {
            string returnPaxSelect = "";
            returnPaxSelect = await paxLoginInfoSelect(Id);
            return Json(new { data = returnPaxSelect }, JsonRequestBehavior.AllowGet);
        }
        public string addPaxLogin(string name, string lastName, string document, string paxType)
        {
            POCOConfigHelper helper = configurationReader.readHelper("REST.reservation loginPaxPost", "AWESAuth");
            LoginPax valuePax = new LoginPax();
            valuePax.userID = userAdministrator.instance.user.ID;
            valuePax.Name = name;
            valuePax.LastName = lastName;
            valuePax.PaxType = paxType;
            valuePax.DocumentType = "ID";
            valuePax.DocumentNumber = document;
            valuePax.paxLoginType = 0;

            string body = JsonConvert.SerializeObject(valuePax);
            string strParametrized = helper.uri.Replace("{userId}", userAdministrator.instance.user.ID.ToString());
            string returnString = webPost.action(helper.method,
                                                       body,
                                                       helper.server,
                                                       strParametrized,
                                                       "application/json",
                                                       "application/json",
                                                       ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                       ConfigurationManager.AppSettings["Consume.reservationSignature"]);
            string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
            return serializeReturn;
        }
        public string putPaxLogin(int idPax, string name, string lastName, string document, string paxType)
        {
            POCOConfigHelper helper = configurationReader.readHelper("REST.reservation loginPaxPut", "AWESAuth");
            LoginPax valuePax = new LoginPax();
            valuePax.userID = userAdministrator.instance.user.ID;
            valuePax.Name = name;
            valuePax.LastName = lastName;
            valuePax.PaxType = paxType;
            valuePax.DocumentType = "ID";
            valuePax.DocumentNumber = document;
            valuePax.paxLoginType = 0;

            string body = JsonConvert.SerializeObject(valuePax);
            string strParametrized = helper.uri.Replace("{id}", idPax.ToString());
            string returnString = webPost.action(helper.method,
                                                       body,
                                                       helper.server,
                                                       strParametrized,
                                                       "application/json",
                                                       "application/json",
                                                       ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                       ConfigurationManager.AppSettings["Consume.reservationSignature"]);
            string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
            return serializeReturn;
        }
    }
}