﻿using AWESAirline.Controllers;
using AWESCommon.webCheckInSupport;
using AWESMVCCommon.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace wwweasyflyawes.Controllers
{
    public class ConsultationController : baseController
    {
        private const string AUTH_PLATFORM = "AWESAuth";

        //Retoma compra
        public async Task<JsonResult> purchaseReturn(string ReservationCode, string lastName)
        {
            string PNRSerialized = await checkInHelper.getPNRbyReservationCodeAsync(ReservationCode);
            var dummy = new { TrackCode = "" };
            var trackingCode = JsonConvert.DeserializeAnonymousType(PNRSerialized, dummy);
            string trackingCodeEncrypted = Encrypt.encrypt(trackingCode.TrackCode.ToString(), encriptSalt);
            cookieUser["tracking"] = trackingCodeEncrypted;
            cookieUser.Expires = DateTime.Now.AddHours(1);
            Response.Cookies.Add(cookieUser);
            string etkt = await getETKT(trackingCode.TrackCode.ToString());
            DataTable ETKTInfoTable = default(DataTable);
            ETKTInfoTable = JsonConvert.DeserializeObject<DataTable>(etkt);
            return Json(new { OK = "Info requested ", data = ETKTInfoTable.Rows[0]["TicketingStatus"].ToString() }, JsonRequestBehavior.AllowGet);
        }
        //Reenvío e-ticket
        public async Task<JsonResult> forwardingTicket(string ReservationCode, string lastName, string email)
        {
            DataTable ETKTInfoTable = new DataTable();
            string PNRSerialized = await checkInHelper.getPNRbyReservationCodeAsync(ReservationCode);
            if (PNRSerialized == "null")
                return Json(new { OK = "Info requested ", data = 4 }, JsonRequestBehavior.AllowGet);
            var dummy = new { TrackCode = "" };
            var trackingCode = JsonConvert.DeserializeAnonymousType(PNRSerialized, dummy);
            string trackingCodeEncrypted = Encrypt.encrypt(trackingCode.TrackCode.ToString(), encriptSalt);
            cookieUser["tracking"] = trackingCodeEncrypted;
            cookieUser.Expires = DateTime.Now.AddHours(1);
            Response.Cookies.Add(cookieUser);          
                try
                {
                    string sendEtkt = await sendETKT();
                    string etktResponse = JsonConvert.DeserializeObject<string>(sendEtkt);
                    ETKTInfoTable = JsonConvert.DeserializeObject<DataTable>(etktResponse);
                }
                catch
                {
                    return Json(new { OK = "Info requested ", data = 0 }, JsonRequestBehavior.AllowGet);
                }
            
            return Json(new { OK = "Info requested ", data = ETKTInfoTable.Rows[0]["TicketingStatus"].ToString() }, JsonRequestBehavior.AllowGet);
        }
    }
}