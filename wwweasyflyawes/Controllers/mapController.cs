﻿using AWESAirline.Controllers;
using AWESCommon.authSupport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace wwweasyflyawes.Controllers
{
    public class mapController : baseController
    {
        // GET: map
        public async Task<ActionResult> Index()
        {
            ViewBag.origin = await trackingFirstTime();
            List<catalogEntryPOCO> listCities = await getNameByIATAList();
            Dictionary<string, string> dictCities = new Dictionary<string, string>();

            foreach (catalogEntryPOCO item in listCities)
            {
                dictCities.Add(item.name, item.value);
            }

            ViewBag.dictCities = dictCities;
            return View();
        }
        public async Task<JsonResult> getContentByList(string listCitiesStr)
        {
            List<string> listCities = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(listCitiesStr);
            string typeName = "map";
            Dictionary<string, string> pageNameHtml =  await getContentByTypeDict(typeName);
            Dictionary<string, string> nameHtml = new Dictionary<string, string>();
            foreach (KeyValuePair<string, string> page in pageNameHtml)
            {
                string html = page.Value;
                string urlName = page.Key.ToLower();
                foreach (string city in listCities)
                {
                    if (ascentMapped(city).ToLower() == ascentMapped(urlName).ToLower())
                    {
                        string headerName = city;
                        nameHtml.Add(headerName, html);
                    }
                }
                break;
            }

            return Json(new { OK = "SUCCESS", data = nameHtml }, JsonRequestBehavior.AllowGet);
        }
    }
}