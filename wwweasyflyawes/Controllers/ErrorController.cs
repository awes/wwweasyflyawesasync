﻿using AWESCommon.Configuration;
using AWESCommon.messagingSupport;
using AWESMVCCommon.Filters;
using AWESMVCCommon.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AWESAirline.Controllers
{
    public class ErrorController : baseController
    {
        private string _encriptSalt;
        private string _cookieName;

        public string encriptSalt
        {
            get
            {
                if (_encriptSalt == null)
                    _encriptSalt = configurationReader.read("auth.cookie.encriptSalt");

                return _encriptSalt;
            }
        }

        public string cookieName
        {
            get
            {
                if (_cookieName == null)
                    _cookieName = configurationReader.read("auth.2step.cookie.name");

                return _cookieName;
            }
        }

        public ActionResult twoStep(NotTwoStepAuthorizedException exception, string email, string phone, string frhgtsoasdfnakei)
        {
            ViewBag.exception = exception;
            ViewBag.controller = Encrypt.encrypt(exception.controller, encriptSalt);
            ViewBag.action = Encrypt.encrypt(exception.action, encriptSalt);

            if (!userAdministrator.instance.user.isAuthenticated)
                return RedirectToAction("index", "home");

            // GENERATES TEMPORARY VERIFICATION COOKIE
            string validationCode = System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString();

            if (((email != null) && (email != "")) || ((phone != null) && (phone != "")))
            {
                // WRITES COOKIE
                string cookieValue = Encrypt.encrypt(validationCode, encriptSalt);

                // DEFINES COOKIE AND PROFILE EXPIRATION
                TimeSpan expiration = new TimeSpan(0, 5, 0);
                cookieManager.updatesCookie(cookieName, cookieValue, "twostep", expiration);

                // GENERATES VERIFICATION CODE
                string verificationCode = CRandomPassword.Generate(8);

                // READS CONTROLLER AND ACTION
                string[] route = frhgtsoasdfnakei.Split('?');
                string controller = Encrypt.decrypt(route[1], encriptSalt);
                string action = Encrypt.decrypt(route[0], encriptSalt);

                // WRITES PROFILE ENTRY
                var profile = new
                {
                    expiration = expiration,
                    validationCode = validationCode, // sent to cookie and to profile
                    verificationCode = verificationCode, // sent to user
                    action = action,
                    controller = controller,
                    clientIP = Request.UserHostAddress
                };

                // SERIALIZES AND ENCRYPTS
                userAdministrator.instance.updateProfileEntry(cookieName, Encrypt.encrypt(JsonConvert.SerializeObject(profile), encriptSalt));

                // PREPARES MESSAGE
                string destinataires = "";
                messageEntry.messageType type;

                if (email != "")
                {
                    type = messageEntry.messageType.email;
                    destinataires = userAdministrator.instance.user.email;
                }
                else
                {
                    type = messageEntry.messageType.sms;
                    destinataires = userAdministrator.instance.profile("mobilePhone").value;
                }

                string subject = "Verification code";
                string body = "The verification code is " + verificationCode;

                // SENDS MESSAGE
                messageHelper.messaging(new messageEntry(functionalUnit, callingMethod(),
                                                                   destinataires,
                                                                   subject, body,
                                                                   type));

                return RedirectToAction("twoStepVerification", "Error");
            }
            else
            {
                // SHOWS PAGE THAT CONFIRM THE USER'S INTENTION OF ACCESING THE PROTECTED RESOURCE
                ViewBag.validationCode = validationCode;
                return View();
            }
        }

        public ActionResult twoStepVerification(string sentCode)
        {
            if (!userAdministrator.instance.user.isAuthenticated)
                return RedirectToAction("index", "home");

            // CHECKS FOR TEMPORARY COOKIE
            if (Request.Cookies.AllKeys.Contains(cookieName))
            {
                // VALIDATES SENT CODE 
                if ((sentCode != null)
                    && (sentCode != ""))
                {
                    // READS COOKIE
                    string cookieValue = cookieManager.readFromCookie(cookieName, "twostep");

                    // DECRYPTS COOKIE
                    string validationCode = Encrypt.decrypt(cookieValue, encriptSalt);

                    // READS PROFILE VALUE FOR VALIDATION & VERIFICATION CODE
                    var dummy = new
                    {
                        expiration = new TimeSpan(),
                        validationCode = "", // sent to cookie and to profile
                        verificationCode = "", // sent to user
                        action = "",
                        controller = "",
                        clientIP = Request.UserHostAddress
                    };

                    // DECRYPTS, DESERIALIZES PROFILE ENTRY
                    var profileEntry = JsonConvert.DeserializeAnonymousType(Encrypt.decrypt(userAdministrator.instance.profile(cookieName).value, encriptSalt), dummy);

                    // VERIFIES VALUES COHERENCE
                    if ((validationCode == profileEntry.validationCode) &&
                        (sentCode == profileEntry.verificationCode))
                    {
                        // PERMISSION VALIDATION CODE
                        string newAuthorizationCode = System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString();

                        // CREATES AUTHORIZATION OBJECT
                        var permission = new
                        {
                            authorizationCode = newAuthorizationCode,
                            action = profileEntry.action,
                            controller = profileEntry.controller
                        };

                        // ISSUES NEW COOKIE FOR OPERATION
                        TimeSpan expiration = new TimeSpan(0, 5, 0);

                        string newCookieValue = Encrypt.encrypt(JsonConvert.SerializeObject(permission), encriptSalt);

                        cookieManager.updatesCookie(cookieName, newCookieValue, "twostep", expiration);
                        userAdministrator.instance.updateProfileEntry(cookieName, newCookieValue);

                        // REDIRECT TO PROTECTED RESOURCE
                        return RedirectToAction(profileEntry.action, profileEntry.controller);
                    }
                }
            }
            else
                // REDIRECT TO ERROR
                throw new Exception("No athentication request found. Please retry");

            return View();
        }

        [PreventDirectAccess]
        public ActionResult ServerError(HttpException exception)
        {
            ViewBag.exception = exception;
            return View();
        }

        public ActionResult AccessDenied(NotAuthorizedException exception)
        {
            ViewBag.exception = exception;
            return View();
        }

        public ActionResult NotFound(HttpException exception)
        {
            ViewBag.exception = exception;
            return View();
        }

        [PreventDirectAccess]
        public ActionResult OtherHttpStatusCode(HttpException exception)
        {
            ViewBag.exception = exception;
            return View("GenericHttpError", exception.GetHttpCode());
        }

        private class PreventDirectAccessAttribute : FilterAttribute, IAuthorizationFilter
        {
            public void OnAuthorization(AuthorizationContext filterContext)
            {
                object value = filterContext.RouteData.Values["fromAppErrorEvent"];
                if (!(value is bool && (bool)value))
                    filterContext.Result = new ViewResult { ViewName = "Error404" };
            }
        }

    }
}