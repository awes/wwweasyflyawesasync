﻿using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using AWESAirline.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AWESAirline.Controllers
{
    public class userTravelerController : baseController
    {
        // GET: listaPax
        public async Task<ActionResult> Index()
        {
            // 1 OBTIENE LA LISTA DE PASAJEROS DEL USUARIO

            //REST.User Travelers list
            POCOConfigHelper helper = configurationReader.readHelper("REST.User Travelers list", configPlatform);


            // /api/USER/{userID}/TRAVELER/
            // 20	jaime enrique	enrique	espinosa reyes	reyes	jespinosa@constructorasoftware.com		3212627294	5716967255	NULL	NULL	NULL	NULL	NULL
            // EL USER ID SALE DE   userAdministrator.instance.user.ID.ToString()
            string parametrizedUri = helper.uri.Replace("{userID}", "20");

            string returnString = await webPost.asyncAction(helper.method,
                                                            null,
                                                            helper.server,
                                                            parametrizedUri,
                                                            "application/json",
                                                            "application/json",
                                                            signatureName,
                                                            signatureValue);


            // deserializa
            ICollection<userTraveler> travelerList = JsonConvert.DeserializeObject<ICollection<userTraveler>>(returnString);

            if (travelerList == null)
                travelerList = new List<userTraveler>();

            return View(travelerList);
        }
        public async Task<JsonResult> addTraveler(string postObject)
        {
            try
            {
                //****************************************************************************************************************
                // ADDS TRAVELER
                //****************************************************************************************************************
                POCOConfigHelper helper = configurationReader.readHelper("REST.Add Traveler to list", configPlatform);

                // EL USER ID SALE DE   userAdministrator.instance.user.ID.ToString()
                string parametrizedUri = helper.uri.Replace("{userID}", "20");

                // travler id
                var travelerId = await webPost.asyncAction(helper.method,
                                                           postObject,
                                                           helper.server,
                                                           parametrizedUri,
                                                           "application/json",
                                                           "application/json",
                                                           signatureName,
                                                           signatureValue);

                //****************************************************************************************************************
                // ADDS TRAVELER
                //****************************************************************************************************************

                //RETURNS LIST
                helper = configurationReader.readHelper("REST.User Travelers list", configPlatform);
                parametrizedUri = helper.uri.Replace("{userID}", "20");

                string returnString = await webPost.asyncAction(helper.method,
                                                                null,
                                                                helper.server,
                                                                parametrizedUri,
                                                                "application/json",
                                                                "application/json",
                                                                signatureName,
                                                                signatureValue);

                // deserializa
                ICollection<userTraveler> travelerList = JsonConvert.DeserializeObject<ICollection<userTraveler>>(returnString);

                if (travelerList == null)
                    travelerList = new List<userTraveler>();


                return Json(new { OK = "traveler added ", data = travelerList }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> deleteTraveler(int travalerID)
        {
            try
            {
                //****************************************************************************************************************
                // DELETES TRAVELER
                //****************************************************************************************************************
                POCOConfigHelper helper = configurationReader.readHelper("REST.Delete Traveler from list", configPlatform);

                // EL USER ID SALE DE   userAdministrator.instance.user.ID.ToString()
                string parametrizedUri = helper.uri.Replace("{userID}", "20");
                parametrizedUri = parametrizedUri.Replace("{id}", travalerID.ToString());

                // travler id
                var strResponse = await webPost.asyncAction(helper.method,
                                                           null,
                                                           helper.server,
                                                           parametrizedUri,
                                                           "application/json",
                                                           "application/json",
                                                           signatureName,
                                                           signatureValue);

                //****************************************************************************************************************
                // DELETES TRAVELER
                //****************************************************************************************************************
                //RETURNS LIST
                helper = configurationReader.readHelper("REST.User Travelers list", configPlatform);
                parametrizedUri = helper.uri.Replace("{userID}", "20");

                string returnString = await webPost.asyncAction(helper.method,
                                                                null,
                                                                helper.server,
                                                                parametrizedUri,
                                                                "application/json",
                                                                "application/json",
                                                                signatureName,
                                                                signatureValue);

                // deserializa
                ICollection<userTraveler> travelerList = JsonConvert.DeserializeObject<ICollection<userTraveler>>(returnString);

                if (travelerList == null)
                    travelerList = new List<userTraveler>();


                return Json(new { OK = "traveler deleted ", data = travelerList }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}