﻿using AWESAirline.Controllers;
using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using AWESMVCCommon.Filters;
using AWESMVCCommon.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Utilidades.Models;

namespace wwweasyflyawes.Controllers
{
    public class TicketController : baseController
    {
        private const string AUTH_PLATFORM = "AWESAuth";
        public JsonResult ticket()
        {
            POCOConfigHelper helper = configurationReader.readHelper("REST.reservation etkt", "AWESAuth");
            string body = null;
            string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt));
            string returnString = webPost.action(helper.method,
                                                       body,
                                                       helper.server,
                                                       strParametrized,
                                                       "application/json",
                                                       "application/json",
                                                       ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                       ConfigurationManager.AppSettings["Consume.reservationSignature"]);
            string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
            return Json(new { data = JsonConvert.SerializeObject(serializeReturn) }, JsonRequestBehavior.AllowGet);
        }
    }
}