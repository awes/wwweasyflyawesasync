﻿using AWESAirline.Controllers;
using AWESCommon.authSupport;
using AWESCommon.bannerSupport;
using AWESCommon.Configuration;
using AWESCommon.contentSupport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace wwweasyflyawes.Controllers
{
    public class cityController : baseController
    {
        // GET: city
        public async Task<ActionResult> Index(string namePage = "vuelos-desde-bogota")
        {
            ViewBag.origin = await trackingFirstTime();
            List<catalogEntryPOCO> listCities = await getNameByIATAList();
            Dictionary<string, string> dictCities = new Dictionary<string, string>();

            foreach (catalogEntryPOCO item in listCities)
            {
                dictCities.Add(item.name, item.value);
            }

            ViewBag.dictCities = dictCities;
            string typeName = "cities";
            List<catalogEntryPOCO> cities = new List<catalogEntryPOCO>();
            string catalogID = configurationReader.read("cities", "AWESAdminDashboard");
            cities = authHelper.getCatalogEntriesByCatalogID(catalogID);
            cities = cities.OrderByDescending(i => i.name).Reverse().ToList();
            ViewBag.cities = cities;
            Dictionary<string, string> pageNameHtml = await getContentByTypeDict(typeName);

            string html = "";
            string IATACity = "";
            string headerName = "";
            bool isValidPage = false;
            foreach (KeyValuePair<string, string> page in pageNameHtml)
            {
                if (page.Key.ToLower() == namePage.ToLower())
                {
                    isValidPage = true;
                    html = page.Value;
                    if (namePage.ToLower().Split('-').Length == 3)
                    {
                        string urlName = namePage.ToLower().Split('-')[2];
                        foreach (catalogEntryPOCO city in cities)
                        {
                            if (ascentMapped(city.value).ToLower() == ascentMapped(urlName).ToLower())
                            {
                                headerName = city.value;
                                IATACity = city.name;
                            }
                        }
                    }
                    else
                    {
                        string urlName = namePage;
                        foreach (catalogEntryPOCO city in cities)
                        {
                            if (ascentMapped(city.value).ToLower() == ascentMapped(urlName).ToLower())
                            {
                                headerName = city.value;
                                IATACity = city.name;
                            }
                        }
                    }
                    break;
                }
            }
            if (!isValidPage)
            {
                return RedirectToAction("Index", "Home", new { error = "pageNotFoundCity" });
            }
            List<POCOBannerEntry> cityBanner = new List<POCOBannerEntry>();
            Dictionary<string, string> returnDict = new Dictionary<string, string>();
            string[] languages=new string[1];
            languages[0] = "es";
            if (Request.UserLanguages != null)
            {
                languages = Request.UserLanguages;
            }

            foreach (string language in languages)
            {
                try
                {
                    cityBanner = bannerHelper.bannerList(platform, "", language.Split(';')[0], "L", "C");
                    break;
                }
                catch
                {

                }
            }
            string urlImageCity = "";
            if (cityBanner.Count != 0)
                urlImageCity = (from i in cityBanner where i.name.ToLower() == namePage.ToLower() select i.url).FirstOrDefault();

            ViewBag.htmlCity = html;
            ViewBag.headerNameCity = headerName;
            ViewBag.IATACity = IATACity;
            ViewBag.imageCity = urlImageCity;

            return View();
        }
        public async Task<JsonResult> getContentCity()
        {
            string pageName = "cities";
            Dictionary<string, string> result = new Dictionary<string, string>();
            try
            {
                result = await getContentByTypeDict(pageName);
            }
            catch
            {
                return Json(new { Message = "FAILED", data = result }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Message = "SUCCESS", data = result }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getImages()
        {
            List<POCOBannerEntry> cityBanner = new List<POCOBannerEntry>();
            Dictionary<string, string> returnDict = new Dictionary<string, string>();
            string[] languages = new string[1];
            languages[0] = "es";
            if (Request.UserLanguages != null)
            {
                languages = Request.UserLanguages;
            }
            foreach (string language in languages)
            {
                try
                {
                    cityBanner = bannerHelper.bannerList(platform, "", language.Split(';')[0], "L", "C");
                }
                catch
                {
                    return Json(new { Message = "FAILED", data = "" }, JsonRequestBehavior.AllowGet);
                }
                if (cityBanner.Count > 0)
                {
                    break;
                }
            }
            return Json(new { Message = "SUCCESS", data = cityBanner }, JsonRequestBehavior.AllowGet);
        }
    }
}