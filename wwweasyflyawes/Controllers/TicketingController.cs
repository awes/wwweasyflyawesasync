﻿using AWESAirline.Controllers;
using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using AWESMVCCommon.Filters;
using AWESMVCCommon.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Utilidades.Models;

namespace wwweasyflyawes.Controllers
{
    public class TicketingController : baseController
    {
        private const string AUTH_PLATFORM = "AWESAuth";
        public JsonResult ticketing()
        {
             //COMENTADO POR JAIME ESPINOSA
             //ESTA FUNCIONALIDAD NO PUEDE ESTAR EXPUESTA VIA JSON A LOS USUARIOS DEL SITIO 
             //PODRIAN EMITIR SIN PASAR POR NINGUN CONTROL!


            POCOConfigHelper helper = configurationReader.readHelper("REST.reservation ticketing", "AWESAuth");
            string body = null;
            string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt));
            strParametrized = strParametrized.Replace("{strFranchise}", "MA");
            strParametrized = strParametrized.Replace("{terminalID}", "NET00EF000");
            strParametrized = strParametrized.Replace("{agentSine}", "NET00EFWW");
            strParametrized = strParametrized.Replace("{ISOCountry}", "CO");
            strParametrized = strParametrized.Replace("{ISOCurrency}", "COP");
            strParametrized = strParametrized.Replace("{requestorID}", "NET");
            strParametrized = strParametrized.Replace("{bookingChannel}", "7");
            strParametrized = strParametrized.Replace("{companyName}", "EF");

            string returnString = webPost.action(helper.method,
                                                       body,
                                                       helper.server,
                                                       strParametrized,
                                                       "application/json",
                                                       "application/json",
                                                       ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                       ConfigurationManager.AppSettings["Consume.reservationSignature"]);
            string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
            return Json(new { data = JsonConvert.SerializeObject(serializeReturn) }, JsonRequestBehavior.AllowGet);
        }
    }
}