﻿using AWESAirline.Controllers;
using AWESAirline.Models;
using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using AWESMVCCommon.Filters;
using AWESMVCCommon.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Utilidades.Models;

namespace wwweasyflyawes.Controllers
{
    public class ReservationController : baseController
    {
        private const string AUTH_PLATFORM = "AWESAuth";
        mdlCompra oModelo = new mdlCompra();
        List<DataTable> dtAvail = new List<DataTable>();
        //
        public async Task<ActionResult> reservation()
        {
            if (Request.Cookies["UserSettings"] != null)
            {
                string returnAvail = await selectInfo();
                avail availTracking = JsonConvert.DeserializeObject<avail>(returnAvail);
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation reservation", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt));
                strParametrized = strParametrized.Replace("{timeLimitReservation}", "6");
                strParametrized = strParametrized.Replace("{companyShortName}", "EF");
                strParametrized = strParametrized.Replace("{flightType}", availTracking.flightType.ToString());
                strParametrized = strParametrized.Replace("{discountCampaing}", "UD");
                strParametrized = strParametrized.Replace("{IP}", GetIPAddress());
                strParametrized = strParametrized.Replace("{terminalID}", "NET00EF000");
                strParametrized = strParametrized.Replace("{agentSine}", "NET00EFWW");
                strParametrized = strParametrized.Replace("{ISOCountry}", "CO");
                strParametrized = strParametrized.Replace("{ISOCurrency}", "COP");
                strParametrized = strParametrized.Replace("{requestorID}", "6");
                strParametrized = strParametrized.Replace("{bookingChannel}", "7");
                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                string serialize = JsonConvert.DeserializeObject<string>(serializeReturn);
                //var dummy = new[] { new { FlightNumber = "", DepartureDateTime = "",
                //                          ArrivalDateTime = "", Duration= "", Origin = "", Destination = "",
                //                          Class = "", ClassQuantity = 0, Rate = 0.0 } };

                //var entries = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(serializeReturn, dummy);


                return Json(new { data = JsonConvert.SerializeObject(serialize) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Redirect("/");
            }
        }

        public async Task<JsonResult> reservationPost(string strInfoSeleccion)
        {
            // STRIPS STRING AND SAVES COOKIE WITH SELECTION DATA 
            saveCookieWithSelection(strInfoSeleccion);

            string[] strPartes = strInfoSeleccion.Split('^');

            string selectString = await selectInfo();

            avail availTracking = JsonConvert.DeserializeObject<avail>(selectString);

            //if (Convert.ToInt32(strPartes[0]) == mdlCompra.INT_TIPO_VUELO_IDA_Y_REGRESO)
            //{
            //    if (strPartes[6] == "1")
            //    {
            //        var jsnTarifa = new
            //        {
            //            error = 2,
            //        };
            //        return Json(new { OK = "Entry updated", data = jsnTarifa }, JsonRequestBehavior.AllowGet);
            //    }
            //}

            // VERIFICA QUE EL MODELO SEA VALIDO PARA MOSTRAR DISCRIMINACION TARIFARIA
            // QUE TENGA SELECCION DE ITINERARIO

            if (Convert.ToInt32(strPartes[0]) == mdlCompra.INT_TIPO_VUELO_IDA_Y_REGRESO)
            {
                oModelo.TipoVuelo = mdlCompra.INT_TIPO_VUELO_IDA_Y_REGRESO;
                if (strPartes[6] == "1")
                {
                    oModelo.idVueloSeleccionadoIda = Response.Cookies["FlightSettingsDeparture"]["idVueloSeleccionadoIda"];
                    oModelo.ClaseIda = Response.Cookies["FlightSettingsDeparture"]["ClaseIda"];
                    oModelo.fechaSalida = Response.Cookies["FlightSettingsDeparture"]["fechaSalida"];
                    oModelo.fechaLlegada = Response.Cookies["FlightSettingsDeparture"]["fechaLlegada"];
                    if (Request.Cookies.AllKeys.Contains("FlightSettingsReturn"))
                    {
                        oModelo.fechaRegresoSalida = Request.Cookies["FlightSettingsReturn"]["fechaRegresoSalida"];
                        if (!string.IsNullOrEmpty(oModelo.fechaRegresoSalida) && !string.IsNullOrEmpty(oModelo.fechaLlegada))
                        {
                            DateTime fechaRegresoSalida = DateTime.ParseExact(oModelo.fechaRegresoSalida, "yyyy-MM-dd HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);
                            DateTime fechaLlegada = DateTime.ParseExact(oModelo.fechaLlegada, "yyyy-MM-dd HH:mm:ss",
                                               System.Globalization.CultureInfo.InvariantCulture);

                            if (fechaLlegada.AddHours(2) >= fechaRegresoSalida || fechaRegresoSalida < fechaLlegada)
                            {
                                return Json(new { error = 3, Mensaje = "La hora de salida del vuelo de ida no puede tener menos de dos horas de diferencia de la hora de partida del vuelo de regreso." }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    oModelo.TipoVuelo = availTracking.flightType;
                    oModelo.FechaSalida = Convert.ToDateTime(availTracking.DepartureDate);
                    oModelo.FechaRegreso = Convert.ToDateTime(availTracking.ReturnDate);
                    oModelo.Adultos = Convert.ToInt32(availTracking.Adult);
                    oModelo.Niños = Convert.ToInt32(availTracking.Child);
                    oModelo.Infantes = Convert.ToInt32(availTracking.Infant);
                    // ACTUALIZA LOS CALCULOS DE LA TARIFA SELECCIONADA
                    try
                    {
                        oModelo.calculaTarifa(strPartes[3], strPartes[9], strPartes[10], Request.Cookies["FlightSettingsReturn"]["priceR"],
                            Request.Cookies["FlightSettingsReturn"]["fuelR"], Request.Cookies["FlightSettingsReturn"]["taxR"], strPartes[6], strPartes[11], strPartes[12]);
                    }
                    catch
                    {
                        oModelo.calculaTarifa(strPartes[3], strPartes[9], strPartes[10], "",
                            "", "", strPartes[6], strPartes[11], strPartes[12]);
                    }
                    
                }
                else
                {
                    oModelo.idVueloSeleccionadoIda = Request.Cookies["FlightSettingsDeparture"]["idVueloSeleccionadoIda"];
                    oModelo.ClaseIda = Request.Cookies["FlightSettingsDeparture"]["ClaseIda"];
                    oModelo.idVueloSeleccionadoRegreso = Response.Cookies["FlightSettingsReturn"]["idVueloSeleccionadoRegreso"];
                    oModelo.ClaseRegreso = Response.Cookies["FlightSettingsReturn"]["ClaseRegreso"];
                    oModelo.fechaSalida = Request.Cookies["FlightSettingsDeparture"]["fechaSalida"];
                    oModelo.fechaLlegada = Request.Cookies["FlightSettingsDeparture"]["fechaLlegada"];
                    oModelo.fechaRegresoSalida = Response.Cookies["FlightSettingsReturn"]["fechaRegresoSalida"];
                    oModelo.fechaRegresoLlegada = Response.Cookies["FlightSettingsReturn"]["fechaRegresoLlegada"];
                    oModelo.TipoVuelo = availTracking.flightType;
                    oModelo.FechaSalida = Convert.ToDateTime(availTracking.DepartureDate);
                    oModelo.FechaRegreso = Convert.ToDateTime(availTracking.ReturnDate);
                    oModelo.Adultos = Convert.ToInt32(availTracking.Adult);
                    oModelo.Niños = Convert.ToInt32(availTracking.Child);
                    oModelo.Infantes = Convert.ToInt32(availTracking.Infant);

                    DateTime fechaRegresoSalida = DateTime.ParseExact(oModelo.fechaRegresoSalida, "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
                    DateTime fechaLlegada = DateTime.ParseExact(oModelo.fechaLlegada, "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);

                    if (((oModelo.FechaSalida.Value == oModelo.FechaRegreso.Value) && (fechaLlegada.AddHours(2)>=fechaRegresoSalida)) || fechaRegresoSalida<fechaLlegada)
                    {
                        return Json(new { error = 3, Mensaje = "La hora de salida del vuelo de ida no puede tener menos de dos horas de diferencia de la hora de partida del vuelo de regreso." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        // ACTUALIZA LOS CALCULOS DE LA TARIFA SELECCIONADA
                        oModelo.calculaTarifa(Request.Cookies["FlightSettingsDeparture"]["price"], Request.Cookies["FlightSettingsDeparture"]["fuel"], Request.Cookies["FlightSettingsDeparture"]["tax"], strPartes[3], strPartes[9], strPartes[10], strPartes[6], strPartes[11], strPartes[12]);
                    }
                }
            }
            else
            {
                oModelo.idVueloSeleccionadoIda = Request.Cookies["FlightSettingsDeparture"]["idVueloSeleccionadoIda"];
                oModelo.ClaseIda = Request.Cookies["FlightSettingsDeparture"]["ClaseIda"];
                oModelo.idVueloSeleccionadoRegreso = Request.Cookies["FlightSettingsReturn"]["idVueloSeleccionadoRegreso"];
                oModelo.ClaseRegreso = Request.Cookies["FlightSettingsReturn"]["ClaseRegreso"];
                oModelo.fechaSalida = Request.Cookies["FlightSettingsDeparture"]["fechaSalida"];
                oModelo.fechaLlegada = Request.Cookies["FlightSettingsDeparture"]["fechaLlegada"];
                oModelo.fechaRegresoSalida = Request.Cookies["FlightSettingsReturn"]["fechaRegresoSalida"];
                oModelo.fechaRegresoLlegada = Request.Cookies["FlightSettingsReturn"]["fechaRegresoLlegada"];
                oModelo.TipoVuelo = availTracking.flightType;
                oModelo.FechaSalida = Convert.ToDateTime(availTracking.DepartureDate);
                oModelo.FechaRegreso = Convert.ToDateTime(availTracking.ReturnDate);
                oModelo.Adultos = Convert.ToInt32(availTracking.Adult);
                oModelo.Niños = Convert.ToInt32(availTracking.Child);
                oModelo.Infantes = Convert.ToInt32(availTracking.Infant);
                oModelo.TipoVuelo = mdlCompra.INT_TIPO_VUELO_IDA;
                // ACTUALIZA LOS CALCULOS DE LA TARIFA SELECCIONADA
                oModelo.calculaTarifa(strPartes[3], strPartes[9], strPartes[10], "",
                            "", "", strPartes[6], strPartes[11], strPartes[12]);
            }
            cookiePrice["Price"] = (oModelo.TarifaTotal.mdblValor + oModelo.TarifaTotal.mdblCombustible).ToString();
            cookiePrice["Tax"] = (oModelo.TarifaTotal.mdblIVAValor + oModelo.TarifaTotal.mdblIVACombustible + oModelo.TarifaTotal.mdblIVACargoAdministrativo + oModelo.TarifaTotal.mdblTasa).ToString();
            cookiePrice["Fee"] = (oModelo.TarifaTotal.mdblCargoAdministrativo).ToString();
            cookiePrice.Expires = DateTime.Now.AddHours(1);
            Response.Cookies.Add(cookiePrice);
            if (Convert.ToInt32(strPartes[0]) == mdlCompra.INT_TIPO_VUELO_IDA_Y_REGRESO)
            {
                if (strPartes[6] == "1")
                {
                    var jsnTarifa = new
                    {
                        error = 0,
                        mdblValor = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblValor + oModelo.TarifaTotal.mdblCombustible),
                        mdblIVAValor = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblIVAValor),
                        mdblCombustible = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblCombustible),
                        mdblIVACombustible = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblIVACombustible),
                        mdblTasa = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblTasa),
                        mdblCargoAdministrativo = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblCargoAdministrativo),
                        mdblIVACargoAdministrativo = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblIVACargoAdministrativo),
                        mdblTasas = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblIVAValor + oModelo.TarifaTotal.mdblIVACombustible + oModelo.TarifaTotal.mdblIVACargoAdministrativo + oModelo.TarifaTotal.mdblTasa),
                        mdblTotal = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblTotal),
                        mdblValorDescontado = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblValorDescontado),
                        mdblTotalConDescuento = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblValorConDescuento),

                        mdblVueloIda = string.Format("{0:# ####}", oModelo.idVueloSeleccionadoIda),
                        mdblFechaIti = string.Format("{0:# ####}", oModelo.fechaSalida.ToString().Substring(0, 10)),
                        mdblHoraIti = string.Format("{0:# ####}", oModelo.fechaSalida.ToString().Substring(11, 5)),
                        mdblFechaItiLegada = string.Format("{0:# ####}", oModelo.fechaLlegada.ToString().Substring(0, 10)),
                        mdblHoraItiLlegada = string.Format("{0:# ####}", oModelo.fechaLlegada.ToString().Substring(11, 5)),
                    };
                    return Json(new { OK = "Entry updated", data = jsnTarifa }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var jsnTarifa = new
                    {
                        error = 0,
                        mdblValor = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblValor + oModelo.TarifaTotal.mdblCombustible),
                        mdblIVAValor = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblIVAValor),
                        mdblCombustible = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblCombustible),
                        mdblIVACombustible = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblIVACombustible),
                        mdblTasa = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblTasa),
                        mdblCargoAdministrativo = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblCargoAdministrativo),
                        mdblIVACargoAdministrativo = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblIVACargoAdministrativo),
                        mdblTasas = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblIVAValor + oModelo.TarifaTotal.mdblIVACombustible + oModelo.TarifaTotal.mdblIVACargoAdministrativo + oModelo.TarifaTotal.mdblTasa),
                        mdblTotal = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblTotal),
                        mdblValorDescontado = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblValorDescontado),
                        mdblTotalConDescuento = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblValorConDescuento),

                        mdblVueloIda = string.Format("{0:# ####}", oModelo.idVueloSeleccionadoIda),
                        mdblFechaIti = string.Format("{0:# ####}", oModelo.fechaSalida.ToString().Substring(0, 10)),
                        mdblHoraIti = string.Format("{0:# ####}", oModelo.fechaSalida.ToString().Substring(11, 5)),
                        mdblFechaItiLegada = string.Format("{0:# ####}", oModelo.fechaLlegada.ToString().Substring(0, 10)),
                        mdblHoraItiLlegada = string.Format("{0:# ####}", oModelo.fechaLlegada.ToString().Substring(11, 5)),
                        mdblVueloRegreso = string.Format("{0:# ####}", oModelo.idVueloSeleccionadoRegreso),
                        mdblFechaItiRegreso = string.Format("{0:# ####}", oModelo.fechaRegresoSalida.ToString().Substring(0, 10)),
                        mdblHoraItiRegreso = string.Format("{0:# ####}", oModelo.fechaRegresoSalida.ToString().Substring(11, 5)),
                        mdblFechaItiRegresoLlegada = string.Format("{0:# ####}", oModelo.fechaRegresoLlegada.ToString().Substring(0, 10)),
                        mdblHoraItiRegresoLlegada = string.Format("{0:# ####}", oModelo.fechaRegresoLlegada.ToString().Substring(11, 5))

                    };
                    return Json(new { OK = "Entry updated", data = jsnTarifa }, JsonRequestBehavior.AllowGet);
                }
                    
            }
            else
            {
                var jsnTarifa = new
                {
                    error = 0,
                    mdblValor = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblValor + oModelo.TarifaTotal.mdblCombustible),
                    mdblIVAValor = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblIVAValor),
                    mdblCombustible = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblCombustible),
                    mdblIVACombustible = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblIVACombustible),
                    mdblTasa = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblTasa),
                    mdblCargoAdministrativo = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblCargoAdministrativo),
                    mdblIVACargoAdministrativo = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblIVACargoAdministrativo),
                    mdblTasas = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblIVAValor + oModelo.TarifaTotal.mdblIVACombustible + oModelo.TarifaTotal.mdblIVACargoAdministrativo + oModelo.TarifaTotal.mdblTasa),
                    mdblTotal = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblTotal),
                    mdblValorDescontado = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblValorDescontado),
                    mdblTotalConDescuento = string.Format("{0:$ #,###}", oModelo.TarifaTotal.mdblValorConDescuento),

                    mdblVueloIda = string.Format("{0:# ####}", oModelo.idVueloSeleccionadoIda),
                    mdblFechaIti = string.Format("{0:# ####}", oModelo.fechaSalida.ToString().Substring(0, 10)),
                    mdblHoraIti = string.Format("{0:# ####}", oModelo.fechaSalida.ToString().Substring(11, 5)),
                    mdblFechaItiLegada = string.Format("{0:# ####}", oModelo.fechaLlegada.ToString().Substring(0, 10)),
                    mdblHoraItiLlegada = string.Format("{0:# ####}", oModelo.fechaLlegada.ToString().Substring(11, 5)),


                };
                return Json(new { OK = "Entry updated", data = jsnTarifa }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// STRIPS THE RECEIVED STRING AND SAVES THE COMPONENTS INTO THE SELECTION COOKIE
        /// </summary>
        /// <param name="strInfoSeleccion"></param>
        private void saveCookieWithSelection(string strInfoSeleccion)
        {
            //strInfoSeleccion = '@flightType^@flightSelect^@classSelectE^@priceE^@departureDate^@arrivalDate^@select^@i^E^priceE^taxE'
            // reservationSelect('1^902^B^140000^2015-05-20 12:00:00^2015-05-20 13:00:00^1^4^P^60000^12800');
            // reservationSelect('1^9021^B^140000^2015-05-28 19:00:00^2015-05-28 19:50:00^2^12^PR^60000^12800');

            string[] strPartes = strInfoSeleccion.Split('^');

            int flightType = 0;
            int.TryParse(strPartes[0], out flightType);

            string selectedFlight = strPartes[1];
            string selectedClass = strPartes[2];
            string selectedPrice = strPartes[3];
            string selectedDepartureDate = strPartes[4];
            string selectedArrivalDate = strPartes[5];
            string selectedFuel = strPartes[9];
            string selectedTax = strPartes[10];
            // 1 go / 2 return
            string selectWay = strPartes[6];
            // econo / pref I/R
            string selectedLevel = strPartes[7];

            /****************************************************************************************************************
            / SAVES COOKIE WITH THE SELECTION
            / ****************************************************************************************************************/
            if (flightType == mdlCompra.INT_TIPO_VUELO_IDA)
            {
                if (Request.Cookies["FlightSettingsReturn"] != null && Request.Cookies["FlightSettingsDeparture"] != null)
                {
                    Response.Cookies.Remove("FlightSettingsDeparture");
                    Response.Cookies.Remove("FlightSettingsReturn");
                }
                cookieFlightDeparture["idVueloSeleccionadoIda"] = selectedFlight;
                cookieFlightDeparture["ClaseIda"] = selectedClass;
                cookieFlightDeparture["fechaSalida"] = selectedDepartureDate;
                cookieFlightDeparture["fechaLlegada"] = selectedArrivalDate;
                cookieFlightReturn["idVueloSeleccionadoRegreso"] = "";
                cookieFlightReturn["ClaseRegreso"] = "";
                cookieFlightReturn["fechaRegresoSalida"] = "";
                cookieFlightReturn["fechaRegresoLlegada"] = "";
                cookieFlightDeparture["price"] = selectedPrice;
                cookieFlightDeparture["fuel"] = selectedFuel;
                cookieFlightDeparture["tax"] = selectedTax;
                cookieFlightReturn["priceR"] = "";
                cookieFlightReturn["fuelR"] = "";
                cookieFlightReturn["taxR"] = "";
                cookieFlightDeparture.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(cookieFlightDeparture);
                cookieFlightReturn.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(cookieFlightReturn);
            }
            else
            {
                if (selectWay == "1") // go
                {
                    if (Request.Cookies["FlightSettingsDeparture"] != null)
                    {
                        Response.Cookies.Remove("FlightSettingsDeparture");
                        //HttpCookie cookieFlightDeparture = new HttpCookie("FlightSettingsDeparture");
                        //cookieFlightDeparture.Expires = DateTime.Now.AddDays(-1d);
                        //Response.Cookies.Add(cookieFlightDeparture);
                    }
                    cookieFlightDeparture["idVueloSeleccionadoIda"] = selectedFlight;
                    cookieFlightDeparture["ClaseIda"] = selectedClass;
                    cookieFlightDeparture["fechaSalida"] = selectedDepartureDate;
                    cookieFlightDeparture["fechaLlegada"] = selectedArrivalDate;
                    cookieFlightDeparture["price"] = selectedPrice;
                    cookieFlightDeparture["fuel"] = selectedFuel;
                    cookieFlightDeparture["tax"] = selectedTax;
                    cookieFlightDeparture.Expires = DateTime.Now.AddHours(1);
                    Response.Cookies.Add(cookieFlightDeparture);
                }
                if (selectWay=="2")
                {
                    if (Request.Cookies["FlightSettingsReturn"] != null)
                    {
                        Response.Cookies.Remove("FlightSettingsReturn");
                    }
                    cookieFlightReturn["idVueloSeleccionadoRegreso"] = selectedFlight;
                    cookieFlightReturn["ClaseRegreso"] = selectedClass;
                    cookieFlightReturn["fechaRegresoSalida"] = selectedDepartureDate;
                    cookieFlightReturn["fechaRegresoLlegada"] = selectedArrivalDate;
                    cookieFlightReturn["priceR"] = selectedPrice;
                    cookieFlightReturn["fuelR"] = selectedFuel;
                    cookieFlightReturn["taxR"] = selectedTax;
                    cookieFlightReturn.Expires = DateTime.Now.AddHours(1);
                    Response.Cookies.Add(cookieFlightReturn);
                }
            }
        }
    }
}