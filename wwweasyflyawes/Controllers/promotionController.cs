﻿using AWESAirline.Controllers;
using AWESCommon.authSupport;
using AWESCommon.bannerSupport;
using AWESCommon.promotionSupport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace wwweasyflyawes.Controllers
{
    public class promotionController : baseController
    {
        // GET: promotion
        public async Task<ActionResult> Index(string namePage = "promo")
        {
            ViewBag.origin = await trackingFirstTime();
            List<catalogEntryPOCO> listCities = await getNameByIATAList();
            Dictionary<string, string> dictCities = new Dictionary<string, string>();

            foreach (catalogEntryPOCO item in listCities)
            {
                dictCities.Add(item.name,item.value);
            }

            ViewBag.dictCities = dictCities;
            List<POCOPromotionEntry> promotions = promotionHelper.promotionList(platform);
            POCOPromotionEntry promotion = new POCOPromotionEntry();
            List<string> userLang = new List<string>();
            userLang.Add("es");
            if (Request.UserLanguages != null)
            {
                userLang = new List<string>(Request.UserLanguages);
            }
            ViewBag.htmlPromotion = "";
            if (promotions.Select(x => x.name.ToLower()).Contains(namePage))
            {


                promotion = (from i in promotions where i.name.ToLower() == namePage.ToLower() && i.state == "Active" select i).FirstOrDefault();
                if (promotion == null)
                {
                    return RedirectToAction("Index", "Home", new { error = "pageNotFoundCity" });
                }
                ViewBag.NamePage = promotion.name;
                Dictionary<string, List<string>> promotionContent = promotionHelper.languageList(promotion.ID.ToString(), platform, "true");
                foreach (KeyValuePair<string, List<string>> content in promotionContent)
                {
                    foreach (string lang in userLang)
                    {
                        string langMasked = lang.Split(';')[0];
                        if ((content.Key == langMasked || content.Key.Contains(langMasked)) && content.Value[3] == promotion.ID.ToString())
                        {
                            ViewBag.htmlPromotion = content.Value[2];
                        }   
                    }
                }
            }
            else
            {
                ViewBag.htmlPromotion = "La página solicitada no existe";
            }

            List<POCOBannerEntry> promoBanner = new List<POCOBannerEntry>();
            Dictionary<string, string> returnDict = new Dictionary<string, string>();
            string[] languages = new string[1];
            languages[0] = "es";
            if (Request.UserLanguages != null)
            {
                languages = Request.UserLanguages;
            }
            foreach (string language in languages)
            {
                try
                {
                    promoBanner = bannerHelper.bannerList(platform, "", language.Split(';')[0], "L", "Pr");
                    break;
                }
                catch
                {

                }
            }
            string urlImagePromo = "";
            if (promoBanner.Count != 0)
                urlImagePromo = (from i in promoBanner where i.name.ToLower() == namePage.ToLower() select i.url).FirstOrDefault();

            ViewBag.imagePromo = urlImagePromo;

            return View();
        }
    }
}