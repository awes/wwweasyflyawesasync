﻿using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.reservationSupport;
using AWESCommon.Web;
using AWESMVCCommon.Filters;
using AWESMVCCommon.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AWESAirline.Controllers
{
    public class profileController : baseController
    {
        //[authorizationConfirmedFilter()]
        public async Task<ActionResult> Index()
        {
            // user profile
            ViewBag.User = userAdministrator.instance.user;

            ViewBag.mobilePhone = "not defined";
            ViewBag.landLine = "not defined";
            ViewBag.document = "not defined";
            ViewBag.address = "not defined";
            ViewBag.city = "not defined";
            ViewBag.country = "not defined";

            foreach (CProfileInfo profile in userAdministrator.instance.user.profiles)
            {
                if (profile.name == "mobilePhone")
                    // phone
                    ViewBag.mobilePhone = profile.value;

                if (profile.name == "landLine")
                    // landline
                    ViewBag.landLine = profile.value;

                if (profile.name == "document")
                    // landline
                    ViewBag.document = profile.value;

                if (profile.name == "address")
                    // landline
                    ViewBag.address = profile.value;

                if (profile.name == "city")
                    // landline
                    ViewBag.city = profile.value;

                if (profile.name == "country")
                    // landline
                    ViewBag.country = profile.value;

            }

            ViewBag.tokenList = await getTokenList();

            return View();
        }

        //[authorizationConfirmedFilter()]
        public async Task<JsonResult> updatePersonalProfile(List<string> updateList)
        {
            if(!userAdministrator.instance.user.isAuthenticated)
                return Json(new { OK = "ERROR", data = "user is not athenticaded" }, JsonRequestBehavior.AllowGet);
            int userID = userAdministrator.instance.user.ID;
            string name = "";
            string lastName = "";
            string email = "";
            bool userPropertyChanged = false;
            try
            {
                var dummyValue = new { name = "", value = "" };

                POCOConfigHelper helper = configurationReader.readHelper("REST.User update profile entry", configPlatform);

                foreach (string upd in updateList)
                {
                    bool isProfileEntry = true;

                    var update = JsonConvert.DeserializeAnonymousType(upd, dummyValue);

                    if (update.name == "name") { name = update.value; userPropertyChanged = true; isProfileEntry = false; }
                    if (update.name == "lastName") { lastName = update.value; userPropertyChanged = true; isProfileEntry = false; }
                    if (update.name == "email") { email = update.value; userPropertyChanged = true; isProfileEntry = false; }

                    if (isProfileEntry)
                    {
                        string parametrizedUri = helper.uri.Replace("{userID}", userID.ToString());
                        parametrizedUri = parametrizedUri.Replace("{id}", "0");
                        parametrizedUri += "?name=" + update.name;

                        string returnString = await webPost.asyncAction(helper.method,
                                                                   upd,
                                                                   helper.server,
                                                                   parametrizedUri,
                                                                   "application/json",
                                                                   "application/json",
                                                                   signatureName,
                                                                   signatureValue);
                    }
                }

                if (userPropertyChanged)
                {
                    var userInfo = new
                    {
                        ID = userID,
                        name = name,
                        lastName = lastName,
                        email = email
                    };

                    helper = configurationReader.readHelper("REST.update user", configPlatform);

                    string parametrizedUri = helper.uri.Replace("{ID}", userID.ToString());
                    parametrizedUri += "?property=true";

                    string returnString = await webPost.asyncAction(helper.method,
                                                              JsonConvert.SerializeObject(userInfo),
                                                               helper.server,
                                                               parametrizedUri,
                                                               "application/json",
                                                               "application/json",
                                                               signatureName,
                                                               signatureValue);
                }

                userAdministrator.instance.updateUserFromTrackingCode();

                return Json(new { OK = "Profile updated", data = data() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public async Task<JsonResult> updatePaxLoginInfo(int idPax, string name, string lastName, string document, string paxType, string DocumentType)
        { 
            if (!userAdministrator.instance.user.isAuthenticated)
                return Json(new { OK = "ERROR", data = idPax }, JsonRequestBehavior.AllowGet);
            int userID = userAdministrator.instance.user.ID;
            string returnString = "";
            try
            {
                returnString = await reservationHelper.putPaxLogin(idPax, name, lastName, document, paxType, userID, DocumentType);
            }
            catch (Exception ex)
            {
                return Json(new { OK = "ERROR", data = idPax }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { OK = "pax updated", data = idPax }, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getInfoByUserId()
        {
            if (!userAdministrator.instance.user.isAuthenticated)
                return Json(new { OK = "ERROR", data = "user is not athenticaded" }, JsonRequestBehavior.AllowGet);

            Dictionary<string, List<string>> dictionaryToReturn = new Dictionary<string, List<string>>();
            string ID = userAdministrator.instance.user.ID.ToString();
            POCOConfigHelper helper = configurationReader.readHelper("REST.reservation Accumulation info", "AWESAuth");

            string parametrizedUri = helper.uri;
            parametrizedUri = parametrizedUri.Replace("{userId}", ID);
            string returnString = await webPost.asyncAction(helper.method,
                                                                   null,
                                                                   helper.server,
                                                                   parametrizedUri,
                                                                   "application/json",
                                                                   "application/json",
                                                                   ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                                   ConfigurationManager.AppSettings["Consume.reservationSignature"]);

            List<accumulationPOCO> accumulationsPerUser = Newtonsoft.Json.JsonConvert.DeserializeObject<List<accumulationPOCO>>(returnString);

            foreach (accumulationPOCO accumulation in accumulationsPerUser)
            {
                try
                {
                    List<string> infoToShow = new List<string>();
                    helper = configurationReader.readHelper("REST.reservation paymentGet", "AWESAuth");

                    parametrizedUri = helper.uri;
                    parametrizedUri = parametrizedUri.Replace("{trackingCode}", accumulation.TrackCode.ToString());

                    returnString = await webPost.asyncAction(helper.method,
                                                                           null,
                                                                           helper.server,
                                                                           parametrizedUri,
                                                                           "application/json",
                                                                           "application/json",
                                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);

                    var dummyPayment = new { PaymentDate = new DateTime() };
                    DateTime paymentDate = (Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(returnString, dummyPayment)).PaymentDate;

                    helper = configurationReader.readHelper("REST.reservation PNRTracking", "AWESAuth");

                    parametrizedUri = helper.uri;
                    parametrizedUri = parametrizedUri.Replace("{trackingCode}", accumulation.TrackCode.ToString());

                    returnString = await webPost.asyncAction(helper.method,
                                                                           null,
                                                                           helper.server,
                                                                           parametrizedUri,
                                                                           "application/json",
                                                                           "application/json",
                                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);

                    var dummyPNR = new { ReservationCode = "" };
                    string PNR = (Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(returnString, dummyPNR)).ReservationCode;
                    helper = configurationReader.readHelper("REST.reservation availabilityTracking", "AWESAuth");

                    parametrizedUri = helper.uri;
                    parametrizedUri = parametrizedUri.Replace("{trackingCode}", accumulation.TrackCode.ToString());

                    returnString = await webPost.asyncAction(helper.method,
                                                                           null,
                                                                           helper.server,
                                                                           parametrizedUri,
                                                                           "application/json",
                                                                           "application/json",
                                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);

                    var dummyAvailabilities = new { IATAOrigin = "", IATADestination = "", flightType = 0 };
                    var originDestinations = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(returnString, dummyAvailabilities);
                    string origin = originDestinations.IATAOrigin;
                    string destination = originDestinations.IATADestination;
                    int flightType = originDestinations.flightType;
                    string concept = "";
                    if (flightType == 1)
                    {
                        concept = origin + "-" + destination + "-" + origin;
                    }
                    else
                    {
                        concept = origin + "-" + destination;
                    }
                    double money = accumulation.money;

                    string month = configurationReader.read("accumulationMonths", "MVCFuncional");
                    int monthNumber = 0;
                    int.TryParse(month, out monthNumber);

                    DateTime expirationPointsDate = accumulation.buyDate.AddMonths(monthNumber);

                    infoToShow.Add(paymentDate.ToShortDateString());
                    infoToShow.Add(PNR);
                    infoToShow.Add(concept);
                    infoToShow.Add(string.Format("{0:$ #,###}", money));
                    infoToShow.Add(expirationPointsDate.ToShortDateString());
                    if(!dictionaryToReturn.Keys.Contains(accumulation.TrackCode.ToString()))
                        dictionaryToReturn.Add(accumulation.TrackCode.ToString(), infoToShow);
                }
                catch (Exception)
                {
                    List<string> errorList = new List<string>();
                    errorList.Add("default");
                    errorList.Add("default");
                    errorList.Add("default");
                    errorList.Add("default");
                    errorList.Add("default");
                    dictionaryToReturn.Add(accumulation.TrackCode.ToString(), errorList);
                }
            }
            return Json(new { OK = "SUCCESS", data = dictionaryToReturn },JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> getPaxesByUserId() {
            if (!userAdministrator.instance.user.isAuthenticated)
                return Json(new { OK = "ERROR", data = "user is not athenticaded" }, JsonRequestBehavior.AllowGet);
            Dictionary<string, List<string>> dictionaryToReturn = new Dictionary<string, List<string>>();
            string ID = userAdministrator.instance.user.ID.ToString();
            POCOConfigHelper helper = configurationReader.readHelper("REST.reservation loginPaxInfo", "AWESAuth");
            string body = null;
            string strParametrized = helper.uri.Replace("{userId}", ID);
            string returnString = await webPost.asyncAction(helper.method,
                                                       body,
                                                       helper.server,
                                                       strParametrized,
                                                       "application/json",
                                                       "application/json",
                                                       ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                       ConfigurationManager.AppSettings["Consume.reservationSignature"]);
            string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
            var dummy = new[] { new {ID=0, PaxType = "", Name = "", LastName = "", DocumentType = "", DocumentNumber = "" } };
            var paxes = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(serializeReturn,dummy);

            paxes = (from i in paxes
                     orderby i.PaxType
                     select i
                       ).ToArray();

            foreach (var pax in paxes)
            {
                List<string> paxInfo=new List<string>();
                string type = pax.PaxType;
                string name = pax.Name + " " + pax.LastName;
                string document = pax.DocumentType+" "+pax.DocumentNumber;
                //To show
                paxInfo.Add(type);
                paxInfo.Add(name);
                paxInfo.Add(document);
                //to update
                paxInfo.Add(pax.Name);
                paxInfo.Add(pax.LastName);
                paxInfo.Add(pax.DocumentNumber);
                paxInfo.Add(pax.DocumentType);

                dictionaryToReturn.Add(pax.ID.ToString(),paxInfo);
            }

            dictionaryToReturn = dictionaryToReturn.OrderByDescending(m=>m.Value[0]).ToDictionary(x=>x.Key,x=>x.Value);

            return Json(new { OK="SUCCESS", data=dictionaryToReturn }, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> sendETKTToMail(string trackingCode) {
            if (!userAdministrator.instance.user.isAuthenticated)
                return Json(new { OK = "ERROR", data = "user is not athenticaded" }, JsonRequestBehavior.AllowGet);
            try
            {
                await sendETKT(trackingCode);                
            }
            catch (Exception ex)
            {
                return Json(new { OK = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { OK = "ETKT was sent", data = "" }, JsonRequestBehavior.AllowGet);
        }

        //PRIVATE FUNCTIONS
        private List<KeyValuePair<string, string>> data()
        {
            string mobilePhone = "not defined";
            string landLine = "not defined";
            string document = "not defined";
            string address = "not defined";
            string city = "not defined";
            string country = "not defined";


            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            foreach (CProfileInfo profile in userAdministrator.instance.user.profiles)
            {
                if (profile.name == "mobilePhone")
                    // phone
                    mobilePhone = profile.value;

                if (profile.name == "landLine")
                    // landline
                    landLine = profile.value;

                if (profile.name == "document")
                    // landline
                    document = profile.value;

                if (profile.name == "address")
                    // landline
                    address = profile.value;

                if (profile.name == "city")
                    // landline
                    city = profile.value;

                if (profile.name == "country")
                    // landline
                    country = profile.value;

            }

            list.Add(new KeyValuePair<string, string>("Name", userAdministrator.instance.user.name));
            list.Add(new KeyValuePair<string, string>("lastName", userAdministrator.instance.user.lastName));
            list.Add(new KeyValuePair<string, string>("email", userAdministrator.instance.user.email));
            list.Add(new KeyValuePair<string, string>("mobilePhone", mobilePhone));
            list.Add(new KeyValuePair<string, string>("landLine", landLine));
            list.Add(new KeyValuePair<string, string>("document", document));
            list.Add(new KeyValuePair<string, string>("address", address));
            list.Add(new KeyValuePair<string, string>("city", city));
            list.Add(new KeyValuePair<string, string>("country", country));


            return list;
        }

        //PRIVATE CLASSES
        private class accumulationPOCO
        {
            public int ID { get; set; }
            public int User { get; set; }
            public virtual Guid TrackCode { get; set; }
            public DateTime buyDate { get; set; }
            public double money { get; set; }
            public int points { get; set; }
            public int state { get; set; }

            public accumulationPOCO(int ID, int User, Guid TrackCode, DateTime buyDate, double money, int points, int state)
            {
                this.ID = ID;
                this.User = User;
                this.TrackCode = TrackCode;
                this.buyDate = buyDate;
                this.money = money;
                this.state = state;
            }
        }
    }
}