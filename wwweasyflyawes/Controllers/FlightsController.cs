﻿using AWESAirline.Controllers;
using AWESAirline.Models;
using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using AWESCommon.promotionSupport;
using AWESMVCCommon.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Utilidades.Models;
using AWESCommon.authSupport;

namespace wwweasyflyawes.Controllers
{
    public class FlightsController : baseController
    {
        private const string AUTH_PLATFORM = "AWESAuth";

        List<DataTable> dtAvail = new List<DataTable>();
        DataTable dtAvailDebug = new DataTable();
        DataTable dtAvailDebugReturn = new DataTable();
        avail availTracking = new avail();
        POCOPromotionEntry promotionObject = new POCOPromotionEntry();
        Boolean promotionValid = false;
        string promotionCode = "";
        string serializeReturn = "";        //
        public async Task<ActionResult> Index()
        {
            List<catalogEntryPOCO> listCities = await getNameByIATAList();
            Dictionary<string, string> dictCities = new Dictionary<string, string>();

            foreach (catalogEntryPOCO item in listCities)
            {
                dictCities.Add(item.name, item.value);
            }

            ViewBag.dictCities = dictCities;
            ViewBag.Discount = "";
            ViewBag.DiscountCode = "";
            ViewBag.PromotionValid = false;
            ViewBag.availDaysDeparture = "";
            ViewBag.availDaysReturn = "";
            ViewBag.departureDate = DateTime.Now;
            ViewBag.returnDate = DateTime.Now;
            try
            {
                if (Request.Cookies["UserSettings"] != null)
                {
                    ErrorToShow();
                    string returnString = await selectInfo();
                    availTracking = JsonConvert.DeserializeObject<avail>(returnString);
                    if (Request["tstPost"] != null)
                    {
                        if (Request.Cookies["FlightSettingsDeparture"] != null && Request.Cookies["FlightSettingsReturn"] != null)
                        {
                            try
                            {
                                string returnReservation = await reservationSelect("");
                                return Redirect("/Pax");
                            }
                            catch
                            {
                                return Redirect("/Pax");
                            }
                        }                      
                        else
                            return View();
                    }
                    else if (Request["tstPostDisc"] != null)
                    {
                        if (Request.Cookies["FlightSettingsDeparture"] != null && Request.Cookies["FlightSettingsReturn"] != null)
                        {
                            try
                            {
                                string returnReservation = await reservationSelect(availTracking.DiscountCampaing.Substring(0, 2));
                                return Redirect("/Pax");
                            }
                            catch
                            {
                                return Redirect("/Pax");
                            }
                        }

                        else
                            return View();
                    }
                    else
                    {
                        mdlCompra oModelo = new mdlCompra();
                        if (!string.IsNullOrEmpty(availTracking.DiscountCampaing))
                        {
                            promotionCode = availTracking.DiscountCampaing.Substring(0, 2);
                            try
                            {
                                promotionObject = promotionHelper.promotionByCode(platform, promotionCode);
                            }
                            catch
                            {
                                promotionObject = null;
                            }

                            if (promotionObject != null)
                            {
                                Dictionary<int, string> codes = new Dictionary<int, string>();
                                codes = promotionHelper.promotionCodeList(platform, promotionObject.ID.ToString());
                                //DATES COMPARISON
                                if (DateTime.Now >= Convert.ToDateTime(promotionObject.initDateSale) && DateTime.Now <= Convert.ToDateTime(promotionObject.endDateSale))
                                {
                                    if (availTracking.flightType == 0)
                                    {
                                        if (availTracking.DepartureDate >= Convert.ToDateTime(promotionObject.initDateFly) && availTracking.DepartureDate <= Convert.ToDateTime(promotionObject.endDateFly))
                                            promotionValid = true;
                                    }
                                    else
                                        if (availTracking.DepartureDate >= Convert.ToDateTime(promotionObject.initDateFly) && availTracking.DepartureDate <= Convert.ToDateTime(promotionObject.endDateFly) && availTracking.ReturnDate >= Convert.ToDateTime(promotionObject.initDateFly) && availTracking.ReturnDate <= Convert.ToDateTime(promotionObject.endDateFly))
                                            promotionValid = true;
                                }
                                if(promotionValid){
                                    //CODE VALIDATION
                                    if (!codes.Values.Contains(availTracking.DiscountCampaing))
                                    {
                                        promotionValid = false;
                                    }
                                }
                                if (promotionValid)
                                {
                                    promotionValid = false;
                                    //RUTES VALIDATION
                                    List<POCORoutesEntry> routes = promotionHelper.promotionRouteList(platform, promotionObject.ID);
                                    if (routes.Count == 0) {
                                        promotionValid = true;
                                    }
                                    foreach (POCORoutesEntry route in routes)
                                    {
                                        if ((route.origin == availTracking.IATAOrigin && route.destination == availTracking.IATADestination)
                                            || (route.origin == availTracking.IATADestination && route.destination == availTracking.IATAOrigin))
                                        {
                                            promotionValid = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (promotionValid)
                            {
                                ViewBag.Discount = promotionObject.discountPercentage;
                                ViewBag.DiscountCode = promotionCode;
                                ViewBag.PromotionValid = promotionValid;
                            }
                            else
                            {
                                ViewBag.Discount = 0;
                                ViewBag.PromotionValid = promotionValid;
                            }
                        }
                        else
                        {
                            ViewBag.Discount = 0;
                            ViewBag.PromotionValid = promotionValid;
                            ViewBag.DiscountCode = "";
                        }

                            dtAvail = availInfo(availTracking.IdOrigin,
                                availTracking.IdDestination,
                                availTracking.DepartureDate,
                                availTracking.ReturnDate,
                                availTracking.flightType,
                                availTracking.Adult,
                                availTracking.Child,
                                availTracking.Infant);

                        // INICIALIZA EN CEROS LA TARIFA
                        if (oModelo.TarifaTotal == null)
                        {
                            oModelo.TarifaTotal = new CTarifaAcumulada();
                            oModelo.TarifaTotal.intTotalPax = oModelo.Adultos + oModelo.Niños;
                        }
                        //INICIALIZA FECHAS
                        oModelo.fechaSalida = "";
                        oModelo.fechaLlegada = "";
                        oModelo.fechaRegresoSalida = "";
                        oModelo.fechaRegresoLlegada = "";
                        oModelo.IATAOrigen = availTracking.IATAOrigin;
                        oModelo.IATADestino = availTracking.IATADestination;
                        oModelo.TipoVuelo = availTracking.flightType;
                        DataTable dtDebugDeparture = new DataTable();
                        double classMinPrice = 0;
                        double classMaxPrice = 0;
                        bool flightExpire = false;
                        DataRow drMinPrice = dtAvail[0].Rows[0];
                        DataRow drMaxPrice = dtAvail[0].Rows[0];
                        DataRow drFinal = dtAvail[0].Rows[0];
                        string flight = "";
                        string description = "";
                        DataRow last = dtAvail[0].Rows[0];
                        try
                        {
                            if (dtAvail.Count > 1)
                            {
                                //if (dtAvail[0].Rows.Count >= 6)
                                //{
                                if (Convert.ToDateTime(dtAvail[0].Rows[0]["DepartureDateTime"]).Day == availTracking.DepartureDate.Day)
                                {
                                    dtDebugDeparture = dtAvail[0].Clone();
                                    classMinPrice = Convert.ToDouble(dtAvail[0].Rows[0]["Rate"]);
                                    classMaxPrice = Convert.ToDouble(dtAvail[0].Rows[0]["Rate"]);
                                    drMinPrice = dtAvail[0].Rows[0];
                                    drMaxPrice = dtAvail[0].Rows[0];
                                    drFinal = dtAvail[0].Rows[0];
                                    flight = dtAvail[0].Rows[0]["FlightNumber"].ToString();
                                    description = dtAvail[0].Rows[0]["Description"].ToString();
                                    last = dtAvail[0].Rows[dtAvail[0].Rows.Count - 1];
                                    foreach (DataRow drDebug in dtAvail[0].Rows)
                                    {
                                        TimeSpan hoursDiff = Convert.ToDateTime(drDebug["DepartureDateTime"]) - DateTime.Now.AddHours(-5);
                                        if (hoursDiff.Days > 0 || ( hoursDiff.Days == 0 && hoursDiff.Hours >= 2))
                                        {
                                            if (drDebug["Class"].ToString() != "X" && drDebug["Class"].ToString() != "I")
                                            {
                                                if (drDebug["Description"].ToString() == "E")
                                                {
                                                    if (flight == drDebug["FlightNumber"].ToString())
                                                    {
                                                        if (classMinPrice > Convert.ToDouble(drDebug["Rate"]) && Convert.ToInt32(drDebug["ClassQuantity"]) >= (availTracking.Adult + availTracking.Child))
                                                        {
                                                            classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                            drMinPrice = drDebug;
                                                        }
                                                        if (last == drDebug)
                                                        {
                                                            dtDebugDeparture.ImportRow(drMinPrice);
                                                            drMinPrice = drDebug;
                                                            classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                        }
                                                        if (description == "P")
                                                        {
                                                            dtDebugDeparture.ImportRow(drMaxPrice);
                                                            classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (description == "P")
                                                        {
                                                            if (flightExpire != true)
                                                                dtDebugDeparture.ImportRow(drMaxPrice);
                                                            classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                        }
                                                        if (flightExpire != true)
                                                            dtDebugDeparture.ImportRow(drMinPrice);
                                                        drMinPrice = drDebug;
                                                        classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                    }
                                                }
                                                else if (drDebug["Description"].ToString() == "P")
                                                {
                                                    if (flight == drDebug["FlightNumber"].ToString())
                                                    {
                                                        if (classMaxPrice > Convert.ToDouble(drDebug["Rate"]) && Convert.ToInt32(drDebug["ClassQuantity"]) >= (availTracking.Adult + availTracking.Child))
                                                        {
                                                            classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                            drMaxPrice = drDebug;
                                                        }
                                                        if (last == drDebug)
                                                        {
                                                            dtDebugDeparture.ImportRow(drMaxPrice);
                                                            drMaxPrice = drDebug;
                                                            classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                        }
                                                        if (description == "E")
                                                        {
                                                            dtDebugDeparture.ImportRow(drMinPrice);
                                                            classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (description == "E")
                                                        {
                                                            if (flightExpire != true)
                                                                dtDebugDeparture.ImportRow(drMinPrice);
                                                            classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                        }
                                                        if (flightExpire != true)
                                                            dtDebugDeparture.ImportRow(drMaxPrice);
                                                        drMaxPrice = drDebug;
                                                        classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                    }
                                                }

                                            }
                                            flightExpire = false;
                                        }
                                        else
                                            flightExpire = true;
                                        flight = drDebug["FlightNumber"].ToString();
                                        description = drDebug["Description"].ToString();
                                        //drFinal = drDebug;
                                    }
                                    //dtDebugDeparture.ImportRow(drFinal);
                                    dtAvailDebug = dtDebugDeparture;
                                    ViewBag.availDaysDeparture = dtAvail[1];
                                }
                                else
                                {
                                    ViewBag.availDaysDeparture = dtAvail[0];
                                    ViewBag.availDaysReturn = dtAvail[1];
                                }
                            }
                            if (dtAvail.Count == 1)
                                ViewBag.availDaysDeparture = dtAvail[0];
                            if (dtAvail.Count > 3)
                            {
                                DataTable dtDebugReturn = dtAvail[1].Clone();
                                classMinPrice = Convert.ToDouble(dtAvail[1].Rows[0]["Rate"]);
                                classMaxPrice = Convert.ToDouble(dtAvail[1].Rows[0]["Rate"]);
                                drMinPrice = dtAvail[1].Rows[0];
                                drMaxPrice = dtAvail[1].Rows[0];
                                drFinal = dtAvail[1].Rows[0];
                                flight = dtAvail[1].Rows[0]["FlightNumber"].ToString();
                                description = dtAvail[1].Rows[0]["Description"].ToString();
                                last = dtAvail[1].Rows[dtAvail[1].Rows.Count - 1];
                                flightExpire = false;
                                foreach (DataRow drDebug in dtAvail[1].Rows)
                                {
                                    TimeSpan hoursDiff = Convert.ToDateTime(drDebug["DepartureDateTime"]) - DateTime.Now.AddHours(-5);
                                    if (hoursDiff.Days > 0 || (hoursDiff.Days == 0 && hoursDiff.Hours >= 2))
                                    {
                                        if (drDebug["Class"].ToString() != "X" && drDebug["Class"].ToString() != "I")
                                        {
                                            if (drDebug["Description"].ToString() == "E")
                                            {
                                                if (flight == drDebug["FlightNumber"].ToString())
                                                {
                                                    if (classMinPrice > Convert.ToDouble(drDebug["Rate"]) && Convert.ToInt32(drDebug["ClassQuantity"]) >= (availTracking.Adult + availTracking.Child))
                                                    {
                                                        classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                        drMinPrice = drDebug;
                                                    }
                                                    if (last == drDebug)
                                                    {
                                                        dtDebugReturn.ImportRow(drMinPrice);
                                                        drMinPrice = drDebug;
                                                        classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                    }
                                                    if (description == "P")
                                                    {
                                                        dtDebugReturn.ImportRow(drMaxPrice);
                                                        classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                    }
                                                }
                                                else
                                                {
                                                    if (description == "P")
                                                    {
                                                        if (flightExpire != true)
                                                            dtDebugReturn.ImportRow(drMaxPrice);
                                                        classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                    }
                                                    if (flightExpire != true)
                                                        dtDebugReturn.ImportRow(drMinPrice);
                                                    drMinPrice = drDebug;
                                                    classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                }
                                            }
                                            else if (drDebug["Description"].ToString() == "P")
                                            {
                                                if (flight == drDebug["FlightNumber"].ToString())
                                                {
                                                    if (classMaxPrice > Convert.ToDouble(drDebug["Rate"]) && Convert.ToInt32(drDebug["ClassQuantity"]) >= (availTracking.Adult + availTracking.Child))
                                                    {
                                                        classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                        drMaxPrice = drDebug;
                                                    }
                                                    if (last == drDebug)
                                                    {
                                                        dtDebugReturn.ImportRow(drMaxPrice);
                                                        drMaxPrice = drDebug;
                                                        classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                    }
                                                    if (description == "E")
                                                    {
                                                        dtDebugReturn.ImportRow(drMinPrice);
                                                        classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                    }
                                                }
                                                else
                                                {
                                                    if (description == "E")
                                                    {
                                                        if (flightExpire != true)
                                                            dtDebugReturn.ImportRow(drMinPrice);
                                                        classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                    }

                                                    if (flightExpire != true)
                                                        dtDebugReturn.ImportRow(drMaxPrice);
                                                    drMaxPrice = drDebug;
                                                    classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                }
                                            }
                                        }
                                        flightExpire = false;
                                    }
                                    else
                                        flightExpire = true;

                                    flight = drDebug["FlightNumber"].ToString();
                                    description = drDebug["Description"].ToString();
                                    //drFinal = drDebug;
                                }
                                //dtDebugReturn.ImportRow(drFinal); 
                                dtAvailDebugReturn = dtDebugReturn;
                                ViewBag.availDaysDeparture = dtAvail[2];
                                ViewBag.availDaysReturn = dtAvail[3];
                            }
                            if (dtAvail.Count == 3)
                            {
                                if (Convert.ToDateTime(dtAvail[0].Rows[0]["DepartureDateTime"]).Day == availTracking.ReturnDate.Day)
                                {
                                    dtDebugDeparture = dtAvail[0].Clone();
                                    classMinPrice = Convert.ToDouble(dtAvail[0].Rows[0]["Rate"]);
                                    classMaxPrice = Convert.ToDouble(dtAvail[0].Rows[0]["Rate"]);
                                    drMinPrice = dtAvail[0].Rows[0];
                                    drMaxPrice = dtAvail[0].Rows[0];
                                    drFinal = dtAvail[0].Rows[0];
                                    flight = dtAvail[0].Rows[0]["FlightNumber"].ToString();
                                    description = dtAvail[0].Rows[0]["Description"].ToString();
                                    last = dtAvail[0].Rows[dtAvail[0].Rows.Count - 1];
                                    foreach (DataRow drDebug in dtAvail[0].Rows)
                                    {
                                        TimeSpan hoursDiff = Convert.ToDateTime(drDebug["DepartureDateTime"]) - DateTime.Now.AddHours(-5);
                                        if (hoursDiff.Days > 0 || (hoursDiff.Days == 0 && hoursDiff.Hours >= 2))
                                        {
                                            if (drDebug["Class"].ToString() != "X" && drDebug["Class"].ToString() != "I")
                                            {
                                                if (drDebug["Description"].ToString() == "E")
                                                {
                                                    if (flight == drDebug["FlightNumber"].ToString())
                                                    {
                                                        if (classMinPrice > Convert.ToDouble(drDebug["Rate"]) && Convert.ToInt32(drDebug["ClassQuantity"]) >= (availTracking.Adult + availTracking.Child))
                                                        {
                                                            classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                            drMinPrice = drDebug;
                                                        }
                                                        if (last == drDebug)
                                                        {
                                                            dtDebugDeparture.ImportRow(drMinPrice);
                                                            drMinPrice = drDebug;
                                                            classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                        }
                                                        if (description == "P")
                                                        {
                                                            dtDebugDeparture.ImportRow(drMaxPrice);
                                                            classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (description == "P")
                                                        {
                                                            if (flightExpire != true)
                                                                dtDebugDeparture.ImportRow(drMaxPrice);
                                                            classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                        }
                                                        if (flightExpire != true)
                                                            dtDebugDeparture.ImportRow(drMinPrice);
                                                        drMinPrice = drDebug;
                                                        classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                    }
                                                }
                                                else if (drDebug["Description"].ToString() == "P")
                                                {
                                                    if (flight == drDebug["FlightNumber"].ToString())
                                                    {
                                                        if (classMaxPrice > Convert.ToDouble(drDebug["Rate"]) && Convert.ToInt32(drDebug["ClassQuantity"]) >= (availTracking.Adult + availTracking.Child))
                                                        {
                                                            classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                            drMaxPrice = drDebug;
                                                        }
                                                        if (last == drDebug)
                                                        {
                                                            dtDebugDeparture.ImportRow(drMaxPrice);
                                                            drMaxPrice = drDebug;
                                                            classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                        }
                                                        if (description == "E")
                                                        {
                                                            dtDebugDeparture.ImportRow(drMinPrice);
                                                            classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (description == "E")
                                                        {
                                                            if (flightExpire != true)
                                                                dtDebugDeparture.ImportRow(drMinPrice);
                                                            classMinPrice = Convert.ToDouble(drDebug["Rate"]);
                                                        }
                                                        if (flightExpire != true)
                                                            dtDebugDeparture.ImportRow(drMaxPrice);
                                                        drMaxPrice = drDebug;
                                                        classMaxPrice = Convert.ToDouble(drDebug["Rate"]);
                                                    }
                                                }

                                            }
                                            flightExpire = false;
                                        }
                                        else
                                            flightExpire = true;
                                        flight = drDebug["FlightNumber"].ToString();
                                        description = drDebug["Description"].ToString();
                                        //drFinal = drDebug;
                                    }
                                    //dtDebugDeparture.ImportRow(drFinal);
                                    dtAvailDebugReturn = dtDebugDeparture;
                                    ViewBag.availDaysDeparture = dtAvail[1];
                                }
                                ViewBag.availDaysReturn = dtAvail[2];
                            }
                            //else if (dtAvail.Count == 2)
                            //{
                            //    ViewBag.availDaysDeparture = dtAvail[0];
                            //    ViewBag.availDaysReturn = dtAvail[1];
                            //}
                        }
                        catch
                        {
                            ViewBag.availDaysDeparture = dtAvail[0];
                            ViewBag.availDaysReturn = dtAvail[1];
                        }
                        //ViewBag.JsonText = dtAvail;
                        ViewBag.JsonText = dtAvailDebug;                     
                        //oModelo.dtAvail = dtAvail;
                        oModelo.Adultos = availTracking.Adult;
                        oModelo.Niños = availTracking.Child;
                        oModelo.Infantes = availTracking.Infant;
                        oModelo.dtAvail = dtAvailDebug;
                        oModelo.dtAvailReturn = dtAvailDebugReturn;
                        ViewBag.departureDate = availTracking.DepartureDate;
                        ViewBag.returnDate = availTracking.ReturnDate;

                        return View(oModelo);
                    }
                }
                else
                {
                    return Redirect("/home/index");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }



        public async Task<ActionResult> mueveFecha(bool blnSalida, int intDias)
        {
            string returnString = await selectInfo();
            availTracking = JsonConvert.DeserializeObject<avail>(returnString);
            if (blnSalida)
                try
                {
                    // MUEVE LA FECHA DE SALIDA 
                    availTracking.DepartureDate = availTracking.DepartureDate.AddDays(intDias);
                }
                catch (Exception)
                {
                    
                    throw;
                }
            else
                // MUEVE LA FECHA DE REGRESO
                availTracking.ReturnDate = availTracking.ReturnDate.AddDays(intDias);
            if (availTracking.DepartureDate > availTracking.ReturnDate)
            {
                TimeSpan diferenceDays = availTracking.DepartureDate - availTracking.ReturnDate;
                availTracking.ReturnDate = availTracking.ReturnDate.AddDays(diferenceDays.Days);
            }
            await postAvail(availTracking.IdOrigin, availTracking.IdDestination, availTracking.IATAOrigin, availTracking.IATADestination, availTracking.DepartureDate.ToString(), availTracking.ReturnDate.ToString(), availTracking.flightType, availTracking.Adult,
                availTracking.Child,
                availTracking.Infant, availTracking.DiscountCampaing);
            //dtAvail = availInfo(availTracking.IdOrigin,
            //    availTracking.IdDestination,
            //    availTracking.DepartureDate,
            //    availTracking.ReturnDate,
            //    availTracking.flightType,
            //    availTracking.Adult,
            //    availTracking.Child,
            //    availTracking.Infant);

            return Redirect("/flights/index");
        }

        public async Task<ActionResult> atrasIDA1() { return await mueveFecha(true, -1); }
        public async Task<ActionResult> atrasIDA2() { return await mueveFecha(true, -2); }
        public async Task<ActionResult> atrasIDA3() { return await mueveFecha(true, -3); }
        public async Task<ActionResult> atrasIDA4() { return await mueveFecha(true, -4); }
        public async Task<ActionResult> adelanteIDA1() { return await mueveFecha(true, 1); }
        public async Task<ActionResult> adelanteIDA2() { return await mueveFecha(true, 2); }
        public async Task<ActionResult> adelanteIDA3() { return await mueveFecha(true, 3); }
        public async Task<ActionResult> adelanteIDA4() { return await mueveFecha(true, 4); }
        public async Task<ActionResult> adelanteIDA5() { return await mueveFecha(true, 5); }
        public async Task<ActionResult> adelanteIDA6() { return await mueveFecha(true, 6); }
        public async Task<ActionResult> adelanteIDA7() { return await mueveFecha(true, 7); }
        public async Task<ActionResult> adelanteIDA8() { return await mueveFecha(true, 8); }

        public async Task<ActionResult> atrasREGRESO1() { return await mueveFecha(false, -1); }
        public async Task<ActionResult> atrasREGRESO2() { return await mueveFecha(false, -2); }
        public async Task<ActionResult> atrasREGRESO3() { return await mueveFecha(false, -3); }
        public async Task<ActionResult> atrasREGRESO4() { return await mueveFecha(false, -4); }
        public async Task<ActionResult> adelanteREGRESO1() { return await mueveFecha(false, 1); }
        public async Task<ActionResult> adelanteREGRESO2() { return await mueveFecha(false, 2); }
        public async Task<ActionResult> adelanteREGRESO3() { return await mueveFecha(false, 3); }
        public async Task<ActionResult> adelanteREGRESO4() { return await mueveFecha(false, 4); }
        public async Task<ActionResult> adelanteREGRESO5() { return await mueveFecha(false, 5); }
        public async Task<ActionResult> adelanteREGRESO6() { return await mueveFecha(false, 6); }
        public async Task<ActionResult> adelanteREGRESO7() { return await mueveFecha(false, 7); }
        public async Task<ActionResult> adelanteREGRESO8() { return await mueveFecha(false, 8); }
    }

}