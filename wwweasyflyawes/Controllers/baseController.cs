﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AWESCommon.Configuration;
using System.Threading.Tasks;
using AWESMVCCommon.Filters;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using System.Configuration;
using Newtonsoft.Json;
using System.Runtime.CompilerServices;
using System.Data;
using System.Net;
using AWESCommon.languageSupport;
using wwweasyflyawes.Controllers;
using Utilidades.Models;
using AWESAirline.Models;
using AWESCommon.contentSupport;
using AWESCommon.webCheckInSupport;
using AWESMVCCommon.Security;
using wwweasyflyawes.Models;
using AWESCommon.paySupport;
using AWESPaymentModel.BindingObjects;
using AWESCommon.bannerSupport;
using AWESCommon.reservationSupport;
using AWESCommon.messagingSupport;
using AWESCommon.promotionSupport;
using AWESCommon.logSupport;
using AWESCommon.authSupport;


namespace AWESAirline.Controllers
{
    public class baseController : Controller
    {
        public HttpCookie cookieSelect = new HttpCookie("SelectSettings");
        public HttpCookie cookieUser = new HttpCookie("UserSettings");
        public HttpCookie cookieFlightDeparture = new HttpCookie("FlightSettingsDeparture");
        public HttpCookie cookieFlightReturn = new HttpCookie("FlightSettingsReturn");
        public HttpCookie cookiePrice = new HttpCookie("PriceReservation");
        public HttpCookie cookiePayResponse = new HttpCookie("PayResponse");
        public HttpCookie cookieLoginUrlResponse = new HttpCookie("LoginUrlResponse");
        public HttpCookie cookieHasError = new HttpCookie("ErrorResponse");
        public HttpCookie cookieWasAuthenticated = new HttpCookie("WasAuthenticated");
        public HttpCookie cookieNewPage = new HttpCookie("NewPage");
        public HttpCookie cookieCities = new HttpCookie("Cities");

        private const string PAYMENT_PLATFORM = "AWESPaymentProvider";

        private string _callingMethod = "";
        private string _platform;
        private string _signatureName;
        private string _signatureValue;
        private string _paymentSignatureName;
        private string _paymentSignatureValue;
        private string _encriptSalt;


        public string platform 
        {
            get
            {
                if ((_platform == null) || (_platform == ""))
                    _platform = ConfigurationManager.AppSettings["platform"];

                return _platform;
            }
        }
        public string configPlatform
        {
            get
            {
                return "AWESAuth";
            }
        }
        public string signatureName
        {
            get
            {
                if ((_signatureName == null) || (_signatureName == ""))
                    _signatureName = ConfigurationManager.AppSettings["Consume.confSignatureName"];

                return _signatureName;
            }
        }
        public string signatureValue
        {
            get
            {
                if ((_signatureValue == null) || (_signatureValue == ""))
                    _signatureValue = ConfigurationManager.AppSettings["Consume.confSignature"];

                return _signatureValue;
            }
        }
        public string paymentSignatureName
        {
            get
            {
                if ((_paymentSignatureName == null) || (_paymentSignatureName == ""))
                    _paymentSignatureName = configurationReader.read("Consume.paymentSignatureName", PAYMENT_PLATFORM);

                return _paymentSignatureName;
            }
        }
        public string paymentSignatureValue
        {
            get
            {
                if ((_paymentSignatureValue == null) || (_paymentSignatureValue == ""))
                    _paymentSignatureValue = configurationReader.read("Consume.paymentSignature", PAYMENT_PLATFORM);

                return _paymentSignatureValue;
            }
        }
        public string encriptSalt
        {
            get
            {
                if (_encriptSalt == null)
                    _encriptSalt = configurationReader.read("auth.cookie.encriptSalt");

                return _encriptSalt;
            }
        }
        protected string functionalUnit
        {
            get
            {
                return this.GetType().Name;
            }
        }
        protected string callingMethod([CallerMemberName] string _method = "")
        {
            _callingMethod = _method;
            return _callingMethod;
        }
        protected string callingContext
        {
            get
            {
                return functionalUnit + "." + _callingMethod;
            }
        }
        public static Dictionary<string, Dictionary<string, string>> langDictionaries
        {
            get
            {
                return languageHelper.langDictionary;
            }
        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            // Control the flow of a buy process
            string url = requestContext.HttpContext.Request.Url.AbsolutePath.ToLower();
            List<POCOPNREntry> pnr= new List<POCOPNREntry>();
            ViewBag.url = "";
            try
            {
                if (requestContext.HttpContext.Request.Cookies.AllKeys.Contains("UserSettings") && url != "/")
                    pnr = reservationHelper.getPNRByTrackingCode(Encrypt.decrypt(requestContext.HttpContext.Request.Cookies["UserSettings"]["tracking"], encriptSalt));
            }
            catch
            {
                pnr = new List<POCOPNREntry>();
            }
            if (pnr.Count>0) {
                if (url.Contains("/flights") || url.Contains("/pax") || url.Contains("/payment"))
                {
                    if(!url.Contains("/payment")){
                        try
                        {
                            Request.Cookies.Remove("WasAuthenticated");
                        }
                        catch
                        {
                            //Nothing
                        }
                    }
                    ViewBag.url = "/payment";
                    if (url.Contains("/flights") || url.Contains("/pax")) {
                        ViewBag.url = "error";
                    }
                }
            }
            // ACCEPT LANGUAGE
            ViewBag.acceptLang = "es";
            if (requestContext.HttpContext.Request.UserLanguages!=null) {
                ViewBag.acceptLang = requestContext.HttpContext.Request.UserLanguages[0];
            }

            KeyValuePair<byte[], POCOBannerEntry> itemLogo = getLogoArray(requestContext.HttpContext.Request.UserLanguages);

            ViewBag.itemLogoArray = itemLogo.Key;
            ViewBag.itemLogoInfo = itemLogo.Value;

            base.Initialize(requestContext);
        }
        public baseController()
        {
            try
            {
                //SEO
                // generic title for all the site
                string catalogID = configurationReader.read("Titles");

                List<catalogEntryPOCO> urlList = authHelper.getCatalogEntriesByCatalogID(catalogID);

                string urlAbs = System.Web.HttpContext.Current.Request.RawUrl.ToString().ToLower();
                List<catalogEntryPOCO> urlSelected = (from i in urlList where i.name.ToString().ToLower() == urlAbs select i).ToList();

                if (urlSelected.Count != 0)
                {
                    ViewBag.title= urlSelected[0].value;
                }
                else
                {
                    ViewBag.title= configurationReader.read("SiteTitle");
                }

                string catalogIDCity = configurationReader.read("cities","AWESAdminDashboard");

                List<catalogEntryPOCO> nameList = authHelper.getCatalogEntriesByCatalogID(catalogIDCity);

                List<string> urlHrefSEO = new List<string>();
                List<string> innerText = new List<string>(); 

                foreach (catalogEntryPOCO name in nameList)
                {
                    if (ascentMapped(name.value).ToLower() != "cucuta" && 
                        ascentMapped(name.value).ToLower() != "yopal" &&
                        ascentMapped(name.value).ToLower() != "apartado" &&
                        ascentMapped(name.value).ToLower() != "pereira" &&
                        ascentMapped(name.value).ToLower() != "neiva" &&
                        (System.Web.HttpContext.Current.Request.RawUrl.Split('-').Length == 3 && System.Web.HttpContext.Current.Request.RawUrl.Split('-')[1]=="desde")
                        )
                    {
                        urlHrefSEO.Add("http://" + System.Web.HttpContext.Current.Request.Url.Host.ToString().ToLower() + System.Web.HttpContext.Current.Request.RawUrl.Split('-')[0] + "-" + System.Web.HttpContext.Current.Request.RawUrl.Split('-')[1] + "-" + name.value);
                        innerText.Add("Vuelos desde " + System.Web.HttpContext.Current.Request.RawUrl.Split('-')[2] + " a " + name.value +" - Tiquetes y pasajes baratos");
                    }
                }



                ViewBag.header = ViewBag.title;
                ViewBag.hrefSEO = urlHrefSEO;
                ViewBag.hrefInnerText = innerText;

                string [] SEO= getSEOMVC();
                if (SEO != null)
                {
                    if (SEO.Length == 2)
                    {
                        string descSEO = SEO[0];
                        string keySEO = SEO[1];
                        ViewBag.descSEO = descSEO;
                        ViewBag.keySEO = keySEO;
                    }
                    else {
                        ViewBag.descSEO = "";
                        ViewBag.keySEO = "";
                    }
                }
                else {
                    ViewBag.descSEO = "";
                    ViewBag.keySEO = "";
                }
                // if any page brings an error to show
                ViewBag.hasOtherError = false;
                ViewBag.otherError = "";
                ViewBag.otherErrorMessage = "";

                // script initialization variables
                ViewBag.jscriptLogEnabled = configurationReader.read("javascript.jscriptLogEnabled");
                // initialization language 
                //getDictionaryFromDataBase();
            }
            catch (Exception)
            {
                ViewBag.jscriptLogEnabled = false;
            }
        }

        // Reservation info methods
        /// <summary>
        /// TODO: DOCUMENTAR! QUE HACE ESTE METODO?
        /// </summary>
        /// <returns></returns>
        public async Task<Dictionary<int, string>> trackingFirstTime()
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation trackingOD", "AWESReservations");
                string body = null;
                string strParametrized = helper.uri.Replace("{platformID}", "201");
                strParametrized = strParametrized.Replace("{operationModeID}", "110");
                if (userAdministrator.instance.user.isAuthenticated)
                {
                    strParametrized = strParametrized.Replace("{userID}", userAdministrator.instance.user.ID.ToString());
                }
                else {
                    strParametrized = strParametrized.Replace("{userID}", "0");
                }

                string returnString = await webPost.asyncAction(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                var dummy = new[] { new { id = 0, IATA = "", name = "", location = "", tax = "", contact = "", catalogID = 0 } };

                List<string> strings = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(returnString);
                var entries = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(strings[1], dummy);

                Dictionary<int, string> airports = new Dictionary<int, string>();

                string trackingCode = Encrypt.encrypt(strings[0],encriptSalt);

                cookieUser["tracking"] = trackingCode;
                cookieUser.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(cookieUser);

                List<KeyValuePair<string, string>> IATAName = new List<KeyValuePair<string, string>>();

                foreach (var entry in entries)
                {
                    string name = await getNameByIATA(entry.IATA);
                    KeyValuePair<string, string> item = new KeyValuePair<string, string>(entry.IATA,name);
                    IATAName.Add(item);
                }

                try
                {
                    entries = (from i in entries select i).OrderByDescending(i => (from j in IATAName where j.Key == i.IATA select i.name).FirstOrDefault()).Reverse().ToArray();
                }
                catch 
                {
                    
                }

                foreach (var entry in entries)
                    airports.Add(entry.id, entry.IATA);

                return airports;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::trackingFirstTime:" + exceptionMessage, ex);
            }
        }
        /// <summary>
        /// TODO: DOCUMENTAR! QUE HACE ESTE METODO?
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, string> trackingFirstTimeNotAsync()
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation trackingOD", "AWESReservations");
                string body = null;
                string strParametrized = helper.uri.Replace("{platformID}", "201");
                strParametrized = strParametrized.Replace("{operationModeID}", "110");
                if (userAdministrator.instance.user.isAuthenticated)
                {
                    strParametrized = strParametrized.Replace("{userID}", userAdministrator.instance.user.ID.ToString());
                }
                else
                {
                    strParametrized = strParametrized.Replace("{userID}", "0");
                }

                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                var dummy = new[] { new { id = 0, IATA = "", name = "", location = "", tax = "", contact = "", catalogID = 0 } };

                List<string> strings = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(returnString);
                var entries = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(strings[1], dummy);

                Dictionary<int, string> airports = new Dictionary<int, string>();

                string trackingCode = Encrypt.encrypt(strings[0], encriptSalt);

                cookieUser["tracking"] = trackingCode;
                cookieUser.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(cookieUser);

                List<KeyValuePair<string, string>> IATAName = new List<KeyValuePair<string, string>>();

                foreach (var entry in entries)
                {
                    string name = getNameByIATANotAsync(entry.IATA);
                    KeyValuePair<string, string> item = new KeyValuePair<string, string>(entry.IATA, name);
                    IATAName.Add(item);
                }

                try
                {
                    entries = (from i in entries select i).OrderByDescending(i => (from j in IATAName where j.Key == i.IATA select i.name).FirstOrDefault()).Reverse().ToArray();
                }
                catch
                {

                }

                foreach (var entry in entries)
                    airports.Add(entry.id, entry.IATA);

                return airports;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::trackingFirstTime:" + exceptionMessage, ex);
            }
        }

        public async Task<JsonResult> getLogo() {
            List<POCOBannerEntry> logoBanner = new List<POCOBannerEntry>();
            Dictionary<string, string> returnDict = new Dictionary<string, string>();
            string[] languages = new string[1];
            languages[0] = "es";
            if (Request.UserLanguages != null)
            {
                languages = Request.UserLanguages;
            }
            foreach (string language in languages)
            {
                try
                {
                    logoBanner = await bannerHelper.bannerListAsync(platform, "", language.Split(';')[0], "L", "L");
                    break;
                }
                catch
                {

                }
            }
            string urlImageLogo = logoBanner[0].url;
            string altImageLogo = logoBanner[0].altText;
            string titleLogo = logoBanner[0].title;

            return Json(new { Message = "SUCCESS", data = new { urlImageLogo = urlImageLogo, altImageLogo = altImageLogo, titleLogo = titleLogo } }, JsonRequestBehavior.AllowGet);
        }

        public KeyValuePair<byte[],POCOBannerEntry> getLogoArray(string[] languagesIn)
        {
            List<byte[]> logoBanner = new List<byte[]>();
            List<POCOBannerEntry> returnDict = new List<POCOBannerEntry>();
            string[] languages = new string[1];
            languages[0] = "es";
            if (languagesIn != null)
            {
                languages = languagesIn;
            }
            foreach (string language in languages)
            {
                try
                {
                    logoBanner = bannerHelper.bannerByArray(platform, "", language.Split(';')[0], "L", "L");
                    returnDict = bannerHelper.bannerList(platform, "", language.Split(';')[0], "L", "L");
                    break;
                }
                catch
                {

                }
            }

            KeyValuePair<byte[], POCOBannerEntry> endObj = new KeyValuePair<byte[], POCOBannerEntry>(logoBanner[0], returnDict[0]);

            return endObj;
        }

        /// <summary>
        /// TODO: DOCUMENTAR! QUE HACE ESTE METODO?
        /// </summary>
        /// <returns></returns>
        public async Task<string> selectInfo()
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation availabilityTracking", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt));
                string returnString = await webPost.asyncAction(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);

                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::selectInfo:" + exceptionMessage, ex);
            }
        }

        /// <summary>
        /// TODO: DOCUMENTAR! QUE HACE ESTE METODO?
        /// </summary>
        /// <returns></returns>
        public string selectInfoNotAsync()
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation availabilityTracking", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt));
                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);

                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::selectInfo:" + exceptionMessage, ex);
            }


        }
        //public async Task<JsonResult> searchLanguages(string key, string defaultValue)
        //{
        //    Dictionary<string, string> keyValues = new Dictionary<string, string>();

        //    string clientUrl = Request.UrlReferrer.ToString();
        //    string[] test = clientUrl.Split('/');
        //    string clientCurrentUrl = Request.Url.ToString();
        //    string action = this.ControllerContext.RouteData.Values["action"].ToString();
        //    string controller = this.ControllerContext.RouteData.Values["controller"].ToString();
        //    //languageHelper.createRecurces(,);
        //    string keyValueSerialized = Newtonsoft.Json.JsonConvert.SerializeObject(keyValues);
        //    string data = Newtonsoft.Json.JsonConvert.SerializeObject(new { keyValue = keyValueSerialized, defaultValue = defaultValue });
        //    return Json(new { Message = "Language was charged", data = data }, JsonRequestBehavior.AllowGet);
        //}

        /// <summary>
        /// Obtains availability FROM KIU and mixes data with the airline's rate database
        /// </summary>
        /// <returns></returns>
        public List<DataTable> availInfo(int originID, int destinationID,
            DateTime departureDate, DateTime returnDate,
            int flightType, int adult, int child, int infant)
        {

            POCOConfigHelper helper = configurationReader.readHelper("REST.reservation availability", "AWESAuth");
            string body = null;
            string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt));
            strParametrized = strParametrized.Replace("{originID}", originID.ToString());
            strParametrized = strParametrized.Replace("{destinationID}", destinationID.ToString());
            strParametrized = strParametrized.Replace("{departureDate}", string.Format("{0:dd-MM-yyy}", departureDate));
            strParametrized = strParametrized.Replace("{returnDate}", string.Format("{0:dd-MM-yyy}", returnDate));
            strParametrized = strParametrized.Replace("{terminalID}", "NET00EF000");
            strParametrized = strParametrized.Replace("{agentSine}", "NET00EFWW");
            strParametrized = strParametrized.Replace("{flightType}", flightType.ToString());
            strParametrized = strParametrized.Replace("{adult}", adult.ToString());
            strParametrized = strParametrized.Replace("{child}", child.ToString());
            strParametrized = strParametrized.Replace("{infant}", infant.ToString());
            strParametrized = strParametrized.Replace("{IP}", GetIPAddress());
            string returnString = webPost.action(helper.method,
                                                       body,
                                                       helper.server,
                                                       strParametrized,
                                                       "application/json",
                                                       "application/json",
                                                       ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                       ConfigurationManager.AppSettings["Consume.reservationSignature"]);
            string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);

            return JsonConvert.DeserializeObject<List<DataTable>>(serializeReturn);
        }
        /// <summary>
        /// Insert avail select
        /// </summary>
        /// <returns></returns>
        public async Task<JsonResult> postAvail(int originID, int destinationID,
                                               string originIATA, string destinationIATA,
                                               string departureDate, string returnDate,
                                               int flightType, int adult, int child, int infant, string promotion)
        {
            // VERIFICA QUE HAYA INFORMACION DE SESSION DE MODELO
            avail avail = new avail();

            string serializeReturn = "";

            try
            {
                if (Request.Cookies["UserSettings"] != null)
                {
                    POCOConfigHelper helper = configurationReader.readHelper("REST.reservation availabilitypost", "AWESAuth");
                    avail.TrackCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt);
                    avail.IdOrigin = originID;
                    avail.IdDestination = destinationID;
                    avail.IATAOrigin = originIATA;
                    avail.IATADestination = destinationIATA;
                    avail.Date = DateTime.Now;
                    avail.DepartureDate = Convert.ToDateTime(departureDate);
                    avail.ReturnDate = Convert.ToDateTime(returnDate);
                    avail.Adult = adult;
                    avail.Child = child;
                    avail.Infant = infant;
                    avail.flightType = flightType;
                    avail.IP = GetIPAddress();
                    avail.DiscountCampaing = promotion;
                    string serializeAvail = JsonConvert.SerializeObject(avail);
                    string body = serializeAvail;

                    string returnString = await webPost.asyncAction(helper.method,
                                                               body,
                                                               helper.server,
                                                               helper.uri,
                                                               "application/json",
                                                               "application/json",
                                                               ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                               ConfigurationManager.AppSettings["Consume.reservationSignature"]);

                    serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                }
            }
            catch (Exception ex)
            {
                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(serializeReturn, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Insert avail select
        /// </summary>
        /// <returns></returns>
        public JsonResult postAvailNotAsync(int originID, int destinationID,
                                               string originIATA, string destinationIATA,
                                               string departureDate, string returnDate,
                                               int flightType, int adult, int child, int infant, string promotion)
        {
            // VERIFICA QUE HAYA INFORMACION DE SESSION DE MODELO
            avail avail = new avail();

            string serializeReturn = "";

            try
            {
                if (Request.Cookies["UserSettings"] != null)
                {
                    POCOConfigHelper helper = configurationReader.readHelper("REST.reservation availabilitypost", "AWESAuth");
                    avail.TrackCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt);
                    avail.IdOrigin = originID;
                    avail.IdDestination = destinationID;
                    avail.IATAOrigin = originIATA;
                    avail.IATADestination = destinationIATA;
                    avail.Date = DateTime.Now;
                    avail.DepartureDate = Convert.ToDateTime(departureDate);
                    avail.ReturnDate = Convert.ToDateTime(returnDate);
                    avail.Adult = adult;
                    avail.Child = child;
                    avail.Infant = infant;
                    avail.flightType = flightType;
                    avail.IP = GetIPAddress();
                    avail.DiscountCampaing = promotion;
                    string serializeAvail = JsonConvert.SerializeObject(avail);
                    string body = serializeAvail;

                    string returnString = webPost.action(helper.method,
                                                               body,
                                                               helper.server,
                                                               helper.uri,
                                                               "application/json",
                                                               "application/json",
                                                               ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                               ConfigurationManager.AppSettings["Consume.reservationSignature"]);

                    serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                }
            }
            catch (Exception ex)
            {
                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(serializeReturn, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// TODO: DOCUMENTAR !!! QUE HACE ESTE METODO?
        /// </summary>
        /// <returns></returns>
        public async Task<string> reservationInfo(string reservationValidation=null, string trackingCode=null)
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation reservationTracking", "AWESAuth");
                string body = null;
                string strParametrized = "";
                if (string.IsNullOrEmpty(trackingCode))
                {
                    strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt));
                }
                else
                {
                    strParametrized = helper.uri.Replace("{trackingCode}", trackingCode);
                }
                if (!string.IsNullOrEmpty(reservationValidation))
                {
                    strParametrized = strParametrized + "?resValidation=" + reservationValidation;
                }
                string returnString = await webPost.asyncAction(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);


                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::reservationInfo:" + exceptionMessage, ex);
            }
        }

        /// <summary>
        /// TODO: DOCUMENTAR !!! QUE HACE ESTE METODO?
        /// </summary>
        /// <returns></returns>
        public string reservationInfoNotAsync(string reservationValidation = null, string trackingCode = null)
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation reservationTracking", "AWESAuth");
                string body = null;
                string strParametrized = "";
                if (string.IsNullOrEmpty(trackingCode))
                {
                    strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt));
                }
                else
                {
                    strParametrized = helper.uri.Replace("{trackingCode}", trackingCode);
                }
                if (!string.IsNullOrEmpty(reservationValidation))
                {
                    strParametrized = strParametrized + "?resValidation=" + reservationValidation;
                }
                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);


                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::reservationInfo:" + exceptionMessage, ex);
            }
        }

        /// <summary>
        /// TODO: DOCUMENTAR !!! QUE HACE ESTE METODO?
        /// </summary>
        /// <returns></returns>
        public async Task<string> reservationSelect(string discount)
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation reservationPost", "AWESAuth");
                Reservation reservationObject = new Reservation();
                reservationObject.Date = DateTime.Now;
                reservationObject.DepartureFlight = Request.Cookies["FlightSettingsDeparture"]["idVueloSeleccionadoIda"];
                reservationObject.ClassDeparture = Request.Cookies["FlightSettingsDeparture"]["ClaseIda"];
                reservationObject.ReturnFlight = Request.Cookies["FlightSettingsReturn"]["idVueloSeleccionadoRegreso"];
                reservationObject.ClassReturn = Request.Cookies["FlightSettingsReturn"]["ClaseRegreso"];
                reservationObject.DepartureDateTime = Request.Cookies["FlightSettingsDeparture"]["fechaSalida"];
                reservationObject.ArrivalDateTime = Request.Cookies["FlightSettingsDeparture"]["fechaLlegada"];
                reservationObject.DepartureDateTimeReturn = Request.Cookies["FlightSettingsReturn"]["fechaRegresoSalida"];
                reservationObject.ArrivalDateTimeReturn = Request.Cookies["FlightSettingsReturn"]["fechaRegresoLlegada"];
                reservationObject.FlightType = mdlCompra.INT_TIPO_VUELO_IDA_Y_REGRESO;
                reservationObject.DiscountCampaing = discount;
                reservationObject.Price = Convert.ToDouble(Request.Cookies["PriceReservation"]["Price"]);
                reservationObject.Tax = Convert.ToDouble(Request.Cookies["PriceReservation"]["Tax"]);
                reservationObject.Fee = Convert.ToDouble(Request.Cookies["PriceReservation"]["Fee"]);

                string body = JsonConvert.SerializeObject(reservationObject);
                string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt));

                string returnString = await webPost.asyncAction(helper.method,
                                                   body,
                                                   helper.server,
                                                   strParametrized,
                                                   "application/json",
                                                   "application/json",
                                                   ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                   ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return serializeReturn;
                //return Json(new { OK = "Entry updated", data = serializeReturn }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::reservationInfo:" + exceptionMessage, ex);
            }
        }

        /// <summary>
        /// TODO: DOCUMENTAR !!! QUE HACE ESTE METODO?
        /// </summary>
        /// <returns></returns>
        public string reservationSelectNotAsync(string discount)
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation reservationPost", "AWESAuth");
                Reservation reservationObject = new Reservation();
                reservationObject.Date = DateTime.Now;
                reservationObject.DepartureFlight = Request.Cookies["FlightSettingsDeparture"]["idVueloSeleccionadoIda"];
                reservationObject.ClassDeparture = Request.Cookies["FlightSettingsDeparture"]["ClaseIda"];
                reservationObject.ReturnFlight = Request.Cookies["FlightSettingsReturn"]["idVueloSeleccionadoRegreso"];
                reservationObject.ClassReturn = Request.Cookies["FlightSettingsReturn"]["ClaseRegreso"];
                reservationObject.DepartureDateTime = Request.Cookies["FlightSettingsDeparture"]["fechaSalida"];
                reservationObject.ArrivalDateTime = Request.Cookies["FlightSettingsDeparture"]["fechaLlegada"];
                reservationObject.DepartureDateTimeReturn = Request.Cookies["FlightSettingsReturn"]["fechaRegresoSalida"];
                reservationObject.ArrivalDateTimeReturn = Request.Cookies["FlightSettingsReturn"]["fechaRegresoLlegada"];
                reservationObject.FlightType = mdlCompra.INT_TIPO_VUELO_IDA_Y_REGRESO;
                reservationObject.DiscountCampaing = discount;
                reservationObject.Price = Convert.ToDouble(Request.Cookies["PriceReservation"]["Price"]);
                reservationObject.Tax = Convert.ToDouble(Request.Cookies["PriceReservation"]["Tax"]);
                reservationObject.Fee = Convert.ToDouble(Request.Cookies["PriceReservation"]["Fee"]);

                string body = JsonConvert.SerializeObject(reservationObject);
                string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt));

                string returnString = webPost.action(helper.method,
                                                   body,
                                                   helper.server,
                                                   strParametrized,
                                                   "application/json",
                                                   "application/json",
                                                   ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                   ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return serializeReturn;
                //return Json(new { OK = "Entry updated", data = serializeReturn }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::reservationInfo:" + exceptionMessage, ex);
            }
        }

        /// <summary>
        /// TODO: DOCUMENTAR !!! QUE HACE ESTE METODO?
        /// </summary>
        /// <returns></returns>
        public async Task<string> PNRInfo()
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation PNRTracking", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt));
                string returnString = await webPost.asyncAction(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                //string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::PNRInfo:" + exceptionMessage, ex);
            }
        }

        /// <summary>
        /// TODO: DOCUMENTAR !!! QUE HACE ESTE METODO?
        /// </summary>
        /// <returns></returns>
        public string PNRInfoNotAsync()
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation PNRTracking", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt));
                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                //string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::PNRInfo:" + exceptionMessage, ex);
            }
        }

        /// <summary>
        /// Get pax information by tracking
        /// </summary>
        /// <returns></returns>
        public string paxInfoNotAsync()
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation paxInfo", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt));
                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return serializeReturn;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::PaxInfo:" + exceptionMessage, ex);
            }
        }
        /// <summary>
        /// Get pax information by tracking
        /// </summary>
        /// <returns></returns>
        public async Task<string> paxInfoSync()
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation paxInfo", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt));
                string returnString = await webPost.asyncAction(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return serializeReturn;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::PaxInfo:" + exceptionMessage, ex);
            }
        }
        /// <summary>
        /// Get pax login information passengers by userId
        /// </summary>
        /// <returns></returns>
        public async Task<string> paxLoginInfo(int userId)
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation loginPaxInfo", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{userId}", userId.ToString());
                string returnString = await webPost.asyncAction(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return serializeReturn;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::PaxInfo:" + exceptionMessage, ex);

            }
        }

        /// <summary>
        /// Get pax login information passengers by userId
        /// </summary>
        /// <returns></returns>
        public string paxLoginInfoNotAsync(int userId)
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation loginPaxInfo", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{userId}", userId.ToString());
                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return serializeReturn;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::PaxInfo:" + exceptionMessage, ex);

            }
        }

        /// <summary>
        /// Get pax login information passengers by userId
        /// </summary>
        /// <returns></returns>
        public async Task<string> deletePax(int idPax)
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation loginPaxDelete", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{id}", idPax.ToString());
                string returnString = await webPost.asyncAction(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return serializeReturn;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::PaxInfo:" + exceptionMessage, ex);

            }
        }

        /// <summary>
        /// Get pax login information passengers by userId
        /// </summary>
        /// <returns></returns>
        public string deletePaxNotAsync(int idPax)
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation loginPaxDelete", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{id}", idPax.ToString());
                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return serializeReturn;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::PaxInfo:" + exceptionMessage, ex);

            }
        }

        /// <summary>
        /// Get pax login information passengers by Id
        /// </summary>
        /// <returns></returns>
        public async Task<string> paxLoginInfoSelect(int Id)
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation loginPaxInfoSelect", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{id}", Id.ToString());
                string returnString = await webPost.asyncAction(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return serializeReturn;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::PaxInfo:" + exceptionMessage, ex);

            }
        }

        /// <summary>
        /// Get pax login information passengers by Id
        /// </summary>
        /// <returns></returns>
        public string paxLoginInfoSelectNotAsync(int Id)
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation loginPaxInfoSelect", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{id}", Id.ToString());
                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return serializeReturn;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::PaxInfo:" + exceptionMessage, ex);

            }
        }

        /// <summary>
        /// Electronic ticket emission
        /// </summary>
        /// <returns></returns>
        public async Task<string> ETKTEmission(string paymentMethod)
        {
            string strFranchise = "";
            int paymentType = 0;
            switch (paymentMethod)
            {
                case "AMEX":
                    strFranchise = "AX";
                    paymentType = 5;
                    break;
                case "DINERS":
                    strFranchise = "DC";
                    paymentType = 5;
                    break;
                case "EFECTY":
                    strFranchise = "MA";
                    paymentType = 1;
                    break;
                case "MASTERCARD":
                    strFranchise = "IK";
                    paymentType = 5;
                    break;
                case "PSE":
                    strFranchise = "MA";
                    paymentType = 6;
                    break;
                case "Verified_by_VISA":
                    strFranchise = "VI";
                    paymentType = 5;
                    break;
                case "VISA":
                    strFranchise = "VI";
                    paymentType = 5;
                    break;
                case "VISA_Debito":
                    strFranchise = "EL";
                    paymentType = 6;
                    break;
                default:
                    strFranchise = "VI";
                    paymentType = 5;
                    break;
            }
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation ticketing", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt));
                strParametrized = strParametrized.Replace("{intPaymentType}", paymentType.ToString());
                strParametrized = strParametrized.Replace("{strFranchise}", strFranchise);
                strParametrized = strParametrized.Replace("{terminalID}", "NET00EF000");
                strParametrized = strParametrized.Replace("{agentSine}", "NET00EFWW");
                strParametrized = strParametrized.Replace("{ISOCountry}", "CO");
                strParametrized = strParametrized.Replace("{ISOCurrency}", "COP");
                strParametrized = strParametrized.Replace("{requestorID}", "NET");
                strParametrized = strParametrized.Replace("{bookingChannel}", "7");
                strParametrized = strParametrized.Replace("{companyName}", "EF");

                string returnString = await webPost.asyncAction(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                if (userAdministrator.instance.user.isAuthenticated)
                {
                    accumulation value = new accumulation();
                    string returnPNR = PNRInfoNotAsync();
                    PNR PNRTracking = JsonConvert.DeserializeObject<PNR>(returnPNR);
                    value.User = userAdministrator.instance.user.ID;
                    value.money = (PNRTracking.KIUPrice * Convert.ToInt32(configurationReader.read("accumulationPercentage", "MVCFuncional"))) / 100;
                    value.points = Convert.ToInt32(PNRTracking.KIUPrice / Convert.ToInt32(configurationReader.read("accumulationPointsFactor", "MVCFuncional")));
                    value.TrackCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt);
                    value.state = 0;
                    await accumulationInsert(value);
                }
                return serializeReturn;
                //return Json(new { data = JsonConvert.SerializeObject(serializeReturn) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::ETKTEmission:" + exceptionMessage, ex);
            }
        }

        /// <summary>
        /// Electronic ticket emission
        /// </summary>
        /// <returns></returns>
        public string ETKTEmissionNotAsync(string paymentMethod)
        {
            string strFranchise = "";
            int paymentType = 0;
            switch (paymentMethod)
            {
                case "AMEX":
                    strFranchise = "AX";
                    paymentType = 5;
                    break;
                case "DINERS":
                    strFranchise = "DC";
                    paymentType = 5;
                    break;
                case "EFECTY":
                    strFranchise = "MA";
                    paymentType = 1;
                    break;
                case "MASTERCARD":
                    strFranchise = "IK";
                    paymentType = 5;
                    break;
                case "PSE":
                    strFranchise = "MA";
                    paymentType = 6;
                    break;
                case "Verified_by_VISA":
                    strFranchise = "VI";
                    paymentType = 5;
                    break;
                case "VISA":
                    strFranchise = "VI";
                    paymentType = 5;
                    break;
                case "VISA_Debito":
                    strFranchise = "EL";
                    paymentType = 6;
                    break;
                default:
                    strFranchise = "VI";
                    paymentType = 5;
                    break;
            }
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation ticketing", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt));
                strParametrized = strParametrized.Replace("{intPaymentType}", paymentType.ToString());
                strParametrized = strParametrized.Replace("{strFranchise}", strFranchise);
                strParametrized = strParametrized.Replace("{terminalID}", "NET00EF000");
                strParametrized = strParametrized.Replace("{agentSine}", "NET00EFWW");
                strParametrized = strParametrized.Replace("{ISOCountry}", "CO");
                strParametrized = strParametrized.Replace("{ISOCurrency}", "COP");
                strParametrized = strParametrized.Replace("{requestorID}", "NET");
                strParametrized = strParametrized.Replace("{bookingChannel}", "7");
                strParametrized = strParametrized.Replace("{companyName}", "EF");

                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                if (userAdministrator.instance.user.isAuthenticated)
                {
                    accumulation value = new accumulation();
                    string returnPNR = PNRInfoNotAsync();
                    PNR PNRTracking = JsonConvert.DeserializeObject<PNR>(returnPNR);
                    value.User = userAdministrator.instance.user.ID;
                    value.money = (PNRTracking.KIUPrice * Convert.ToInt32(configurationReader.read("accumulationPercentage", "MVCFuncional"))) / 100;
                    value.points = Convert.ToInt32(PNRTracking.KIUPrice / Convert.ToInt32(configurationReader.read("accumulationPointsFactor", "MVCFuncional")));
                    value.TrackCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt);
                    value.state = 0;
                    accumulationInsertNotAsync(value);
                }
                return serializeReturn;
                //return Json(new { data = JsonConvert.SerializeObject(serializeReturn) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::ETKTEmission:" + exceptionMessage, ex);
            }
        }

        /// <summary>
        /// Send E-ticket
        /// </summary>
        /// <returns></returns>
        public async Task<string> sendETKT(string trackingCode=null)
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation ETKTSend", "AWESAuth");
                string body = null;
                string strParametrized = "";
                if(string.IsNullOrEmpty(trackingCode))
                    strParametrized=helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt));
                else
                    strParametrized = helper.uri.Replace("{trackingCode}", trackingCode);
                string lang="";
                if (Request.UserLanguages != null)
                {
                    lang = Newtonsoft.Json.JsonConvert.SerializeObject(Request.UserLanguages);
                    strParametrized = strParametrized + "?lang=" + lang;
                }
                else {
                    lang = "es";
                    strParametrized = strParametrized + "?lang=" + lang;
                }
                string returnString = await webPost.asyncAction(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);

                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::sendETKT:" + exceptionMessage, ex);
            }


        }

        /// <summary>
        /// Send E-ticket
        /// </summary>
        /// <returns></returns>
        public string sendETKTNotAsync(string trackingCode = null)
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation ETKTSend", "AWESAuth");
                string body = null;
                string strParametrized = "";
                if (string.IsNullOrEmpty(trackingCode))
                    strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt));
                else
                    strParametrized = helper.uri.Replace("{trackingCode}", trackingCode);
                string lang = "";
                if (Request.UserLanguages != null)
                {
                    lang = Newtonsoft.Json.JsonConvert.SerializeObject(Request.UserLanguages);
                    strParametrized = strParametrized + "?lang=" + lang;
                }
                else
                {
                    lang = "es";
                    strParametrized = strParametrized + "?lang=" + lang;
                }
                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);

                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::sendETKT:" + exceptionMessage, ex);
            }


        }

        /// <summary>
        /// E-ticket get info
        /// </summary>
        /// <returns></returns>
        public async Task<string> getETKT(string trackingCode)
        {
            try
            {
                string etkt = await checkInHelper.getETKTAsync(trackingCode);
                string returnString = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(etkt);
                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::getETKT:" + exceptionMessage, ex);
            }


        }

        /// <summary>
        /// E-ticket get info
        /// </summary>
        /// <returns></returns>
        public string getETKTNotAsync(string trackingCode)
        {
            try
            {
                string etkt = checkInHelper.getETKT(trackingCode);
                string returnString = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(etkt);
                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::getETKT:" + exceptionMessage, ex);
            }


        }

        /// <summary>
        /// obtains Client IP address
        /// </summary>
        /// <returns></returns>
        public string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            //string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            //if (!string.IsNullOrEmpty(ipAddress))
            //{
            //    string[] addresses = ipAddress.Split(',');
            //    if (addresses.Length != 0)
            //    {
            //        return addresses[0];
            //    }
            //}
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        /// <summary>
        /// Get accumulation money information
        /// </summary>
        /// <returns></returns>
        public async Task<JsonResult> accumulationInfo()
        {
            double accumulationMoney = 0;
            if (userAdministrator.instance.user.isAuthenticated)
            {
                try
                {
                    POCOConfigHelper helper = configurationReader.readHelper("REST.reservation Accumulation info", "AWESAuth");
                    string body = null;
                    string strParametrized = helper.uri.Replace("{userId}", userAdministrator.instance.user.ID.ToString());
                    string returnString = await webPost.asyncAction(helper.method,
                                                               body,
                                                               helper.server,
                                                               strParametrized,
                                                               "application/json",
                                                               "application/json",
                                                               ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                               ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                    //string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                    List<accumulation> accumulationReturn = JsonConvert.DeserializeObject<List<accumulation>>(returnString);
                    foreach (accumulation drAccumulation in accumulationReturn)
                    {
                        if (drAccumulation.buyDate.AddYears(1) > DateTime.Now)
                        {
                            accumulationMoney = accumulationMoney + drAccumulation.money;
                        }
                    }
                    return Json(new { OK = "Log in requested ", data = accumulationMoney }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    string exceptionMessage = ex.Message;

                    if (ex.InnerException != null)
                        exceptionMessage += ":" + ex.InnerException.Message;

                    throw new Exception("www::baseController::accumulationInfo:" + exceptionMessage, ex);
                }
            }
            else
                return Json(new { OK = "Log in requested ", data = accumulationMoney }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get accumulation money information
        /// </summary>
        /// <returns></returns>
        public JsonResult accumulationInfoNotAsync()
        {
            double accumulationMoney = 0;
            if (userAdministrator.instance.user.isAuthenticated)
            {
                try
                {
                    POCOConfigHelper helper = configurationReader.readHelper("REST.reservation Accumulation info", "AWESAuth");
                    string body = null;
                    string strParametrized = helper.uri.Replace("{userId}", userAdministrator.instance.user.ID.ToString());
                    string returnString = webPost.action(helper.method,
                                                               body,
                                                               helper.server,
                                                               strParametrized,
                                                               "application/json",
                                                               "application/json",
                                                               ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                               ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                    //string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                    List<accumulation> accumulationReturn = JsonConvert.DeserializeObject<List<accumulation>>(returnString);
                    foreach (accumulation drAccumulation in accumulationReturn)
                    {
                        if (drAccumulation.buyDate.AddYears(1) > DateTime.Now)
                        {
                            accumulationMoney = accumulationMoney + drAccumulation.money;
                        }
                    }
                    return Json(new { OK = "Log in requested ", data = accumulationMoney }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    string exceptionMessage = ex.Message;

                    if (ex.InnerException != null)
                        exceptionMessage += ":" + ex.InnerException.Message;

                    throw new Exception("www::baseController::accumulationInfo:" + exceptionMessage, ex);
                }
            }
            else
                return Json(new { OK = "Log in requested ", data = accumulationMoney }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get accumulation money information String.Format
        /// </summary>
        /// <returns></returns>
        public async Task<JsonResult> accumulationInfoFormated()
        {
            double accumulationMoney = 0;
            string accumulationMoneyFormated = string.Format("{0:$ #,###}", accumulationMoney);
            if (userAdministrator.instance.user.isAuthenticated)
            {
                try
                {
                    POCOConfigHelper helper = configurationReader.readHelper("REST.reservation Accumulation info", "AWESAuth");
                    string body = null;
                    string strParametrized = helper.uri.Replace("{userId}", userAdministrator.instance.user.ID.ToString());
                    string returnString = await webPost.asyncAction(helper.method,
                                                               body,
                                                               helper.server,
                                                               strParametrized,
                                                               "application/json",
                                                               "application/json",
                                                               ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                               ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                    //string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                    List<accumulation> accumulationReturn = JsonConvert.DeserializeObject<List<accumulation>>(returnString);
                    foreach (accumulation drAccumulation in accumulationReturn)
                    {
                        if (drAccumulation.buyDate.AddYears(1) > DateTime.Now)
                        {
                            accumulationMoney = accumulationMoney + drAccumulation.money;
                        }
                    }
                    accumulationMoneyFormated=string.Format("{0:$ #,###}", accumulationMoney);
                    return Json(new { OK = "Log in requested ", data = accumulationMoneyFormated }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    string exceptionMessage = ex.Message;

                    if (ex.InnerException != null)
                        exceptionMessage += ":" + ex.InnerException.Message;

                    throw new Exception("www::baseController::accumulationInfo:" + exceptionMessage, ex);
                }
            }
            else
                return Json(new { OK = "Log in requested ", data = accumulationMoneyFormated }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get accumulation money information String.Format
        /// </summary>
        /// <returns></returns>
        public JsonResult accumulationInfoFormatedNotAsync()
        {
            double accumulationMoney = 0;
            string accumulationMoneyFormated = string.Format("{0:$ #,###}", accumulationMoney);
            if (userAdministrator.instance.user.isAuthenticated)
            {
                try
                {
                    POCOConfigHelper helper = configurationReader.readHelper("REST.reservation Accumulation info", "AWESAuth");
                    string body = null;
                    string strParametrized = helper.uri.Replace("{userId}", userAdministrator.instance.user.ID.ToString());
                    string returnString = webPost.action(helper.method,
                                                               body,
                                                               helper.server,
                                                               strParametrized,
                                                               "application/json",
                                                               "application/json",
                                                               ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                               ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                    //string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                    List<accumulation> accumulationReturn = JsonConvert.DeserializeObject<List<accumulation>>(returnString);
                    foreach (accumulation drAccumulation in accumulationReturn)
                    {
                        if (drAccumulation.buyDate.AddYears(1) > DateTime.Now)
                        {
                            accumulationMoney = accumulationMoney + drAccumulation.money;
                        }
                    }
                    accumulationMoneyFormated = string.Format("{0:$ #,###}", accumulationMoney);
                    return Json(new { OK = "Log in requested ", data = accumulationMoneyFormated }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    string exceptionMessage = ex.Message;

                    if (ex.InnerException != null)
                        exceptionMessage += ":" + ex.InnerException.Message;

                    throw new Exception("www::baseController::accumulationInfo:" + exceptionMessage, ex);
                }
            }
            else
                return Json(new { OK = "Log in requested ", data = accumulationMoneyFormated }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get accumulation money information
        /// </summary>
        /// <returns></returns>
        public async Task<double> accumulationValue()
        {
            double accumulationMoney = 0;
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation Accumulation info", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{userId}", userAdministrator.instance.user.ID.ToString());
                string returnString = await webPost.asyncAction(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                //string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                List<accumulation> accumulationReturn = JsonConvert.DeserializeObject<List<accumulation>>(returnString);
                foreach (accumulation drAccumulation in accumulationReturn)
                {
                    if (drAccumulation.buyDate.AddYears(1) > DateTime.Now)
                    {
                        accumulationMoney = accumulationMoney + drAccumulation.money;
                    }
                }
                return accumulationMoney;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::accumulationInfo:" + exceptionMessage, ex);
            }
        }

        /// <summary>
        /// Get accumulation money information
        /// </summary>
        /// <returns></returns>
        public double accumulationValueNotAsync()
        {
            double accumulationMoney = 0;
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation Accumulation info", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{userId}", userAdministrator.instance.user.ID.ToString());
                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                //string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                List<accumulation> accumulationReturn = JsonConvert.DeserializeObject<List<accumulation>>(returnString);
                foreach (accumulation drAccumulation in accumulationReturn)
                {
                    if (drAccumulation.buyDate.AddYears(1) > DateTime.Now)
                    {
                        accumulationMoney = accumulationMoney + drAccumulation.money;
                    }
                }
                return accumulationMoney;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::accumulationInfo:" + exceptionMessage, ex);
            }
        }

        /// <summary>
        /// Accumulation money insert
        /// </summary>
        /// <returns></returns>
        public async Task<string> accumulationInsert(accumulation value)
        {
            try
            {
                value.buyDate = DateTime.Now;
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation Accumulation post", "AWESAuth");
                string body = JsonConvert.SerializeObject(value);
                string returnString = await webPost.asyncAction(helper.method,
                                                           body,
                                                           helper.server,
                                                           helper.uri,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                //string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::accumulationInfo:" + exceptionMessage, ex);
            }
        }

        /// <summary>
        /// Accumulation money insert
        /// </summary>
        /// <returns></returns>
        public string accumulationInsertNotAsync(accumulation value)
        {
            try
            {
                value.buyDate = DateTime.Now;
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation Accumulation post", "AWESAuth");
                string body = JsonConvert.SerializeObject(value);
                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           helper.uri,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                //string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::accumulationInfo:" + exceptionMessage, ex);
            }
        }

        /// <summary>
        /// Payment insert
        /// </summary>
        /// <returns></returns>
        public async Task<string> paymentInsert(PaymentModel value)
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation paymentPost", "AWESAuth");
                string body = JsonConvert.SerializeObject(value);
                string returnString = await webPost.asyncAction(helper.method,
                                                           body,
                                                           helper.server,
                                                           helper.uri,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                //string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::paymentInsert:" + exceptionMessage, ex);
            }
        }
        /// <summary>
        /// Payment insert
        /// </summary>
        /// <returns></returns>
        public string paymentInsertNotAsync(PaymentModel value)
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation paymentPost", "AWESAuth");
                string body = JsonConvert.SerializeObject(value);
                string returnString =  webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           helper.uri,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                //string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::paymentInsert:" + exceptionMessage, ex);
            }
        }
        /// <summary>
        /// Get payment information
        /// </summary>
        /// <returns></returns>
        public async Task<string> paymentInfo()
        {
                try
                {
                    POCOConfigHelper helper = configurationReader.readHelper("REST.reservation paymentGet", "AWESAuth");
                    string body = null;
                    string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"],encriptSalt));
                    string returnString = await webPost.asyncAction(helper.method,
                                                               body,
                                                               helper.server,
                                                               strParametrized,
                                                               "application/json",
                                                               "application/json",
                                                               ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                               ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                    //string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                    return returnString;
                }
                catch (Exception ex)
                {
                    string exceptionMessage = ex.Message;

                    if (ex.InnerException != null)
                        exceptionMessage += ":" + ex.InnerException.Message;

                    throw new Exception("www::baseController::paymentInfo:" + exceptionMessage, ex);
                }
        }
        /// <summary>
        /// Get payment information not async
        /// </summary>
        /// <returns></returns>
        public string paymentInfoNotAsync()
        {
            try
            {
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation paymentGet", "AWESAuth");
                string body = null;
                string strParametrized = helper.uri.Replace("{trackingCode}", Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt));
                string returnString = webPost.action(helper.method,
                                                           body,
                                                           helper.server,
                                                           strParametrized,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                //string serializeReturn = JsonConvert.DeserializeObject<string>(returnString);
                return returnString;
            }
            catch (Exception ex)
            {
                string exceptionMessage = ex.Message;

                if (ex.InnerException != null)
                    exceptionMessage += ":" + ex.InnerException.Message;

                throw new Exception("www::baseController::paymentInfo:" + exceptionMessage, ex);
            }
        }
        /// <summary>
        /// Get the city's name by a code IATA
        /// </summary>
        /// <param name="IATA"></param>
        /// <returns></returns>
        public static async Task<string> getNameByIATA(string IATA) {
            //CITIES CATALOG
            string catalogID = configurationReader.read("cities", "AWESAdminDashboard");
            List<catalogEntryPOCO> catalog = await authHelper.getCatalogEntriesByCatalogIDAsync(catalogID);

            string name = (from i in catalog
                                     where i.name == IATA
                                     select i.value).FirstOrDefault();
            return name;
        }

        public static string getNameByIATANotAsync(string IATA)
        {
            //CITIES CATALOG
            string catalogID = configurationReader.read("cities", "AWESAdminDashboard");
            List<catalogEntryPOCO> catalog = authHelper.getCatalogEntriesByCatalogID(catalogID);

            string name = (from i in catalog
                           where i.name == IATA
                           select i.value).FirstOrDefault();
            return name;
        }

        public static async Task<List<catalogEntryPOCO>> getNameByIATAList()
        {
            //CITIES CATALOG
            string catalogID = configurationReader.read("cities", "AWESAdminDashboard");
            List<catalogEntryPOCO> catalog = await authHelper.getCatalogEntriesByCatalogIDAsync(catalogID);

            return catalog;
        }

        public static List<catalogEntryPOCO> getNameByIATAListNotAsync()
        {
            //CITIES CATALOG
            string catalogID = configurationReader.read("cities", "AWESAdminDashboard");
            List<catalogEntryPOCO> catalog = authHelper.getCatalogEntriesByCatalogID(catalogID);

            return catalog;
        }

        protected virtual async Task<List<tokenizationController.tokenBind>> getTokenList()
        {
            // OBTAINS TOKEN LIST FOR THE USER
            POCOConfigHelper helper = configurationReader.readHelper("REST.User Token list", configPlatform);

            string parametrizedUri = helper.uri.Replace("{userID}", userAdministrator.instance.user.ID.ToString());

            string returnString = await webPost.asyncAction(helper.method,
                                                       null,
                                                       helper.server,
                                                       parametrizedUri,
                                                       "application/json",
                                                       "application/json",
                                                       signatureName,
                                                       signatureValue);

            return JsonConvert.DeserializeObject<List<tokenizationController.tokenBind>>(returnString);
        }

        protected virtual List<tokenizationController.tokenBind> getTokenListNotAsync()
        {
            // OBTAINS TOKEN LIST FOR THE USER
            POCOConfigHelper helper = configurationReader.readHelper("REST.User Token list", configPlatform);

            string parametrizedUri = helper.uri.Replace("{userID}", userAdministrator.instance.user.ID.ToString());

            string returnString = webPost.action(helper.method,
                                                       null,
                                                       helper.server,
                                                       parametrizedUri,
                                                       "application/json",
                                                       "application/json",
                                                       signatureName,
                                                       signatureValue);

            return JsonConvert.DeserializeObject<List<tokenizationController.tokenBind>>(returnString);
        }

        public async Task<JsonResult> getLastestSearch() {
            if (!userAdministrator.instance.user.isAuthenticated)
            {
                return Json(new { OK = "FAILED", data = "" }, JsonRequestBehavior.AllowGet);
            }
            else {
                int userID = userAdministrator.instance.user.ID;
                string trackingCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt);

                List<POCOAviabilityEntry> aviabilities = await reservationHelper.getAviabilitiesListAsync(trackingCode,userID.ToString());

                if (aviabilities.Count == 0) {
                    return Json(new { OK = "FAILED", data = "" }, JsonRequestBehavior.AllowGet);
                }

                List<string> lastestSearch = new List<string>();
                foreach (POCOAviabilityEntry aviability in aviabilities)
                {
                    if (aviability.flightType == 1)
                    {
                        lastestSearch.Add(getNameByIATANotAsync(aviability.IATAOrigin) + " > " + getNameByIATANotAsync(aviability.IATADestination) + ": " + aviability.DepartureDate.ToShortDateString().Replace('/', '-') + " / " + aviability.ReturnDate.ToShortDateString().Replace('/', '-'));
                    }
                    else {
                        lastestSearch.Add(getNameByIATANotAsync(aviability.IATAOrigin) + " > " + getNameByIATANotAsync(aviability.IATADestination) + ": " + aviability.DepartureDate.ToShortDateString().Replace('/', '-'));
                    }

                }
                return Json(new { OK = "SUCCESS", data = new { lastestSearch = lastestSearch, addInfo = aviabilities }, }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getLastestSearchNotAsync()
        {
            if (!userAdministrator.instance.user.isAuthenticated)
            {
                return Json(new { OK = "FAILED", data = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                int userID = userAdministrator.instance.user.ID;
                string trackingCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt);

                List<POCOAviabilityEntry> aviabilities = reservationHelper.getAviabilitiesList(trackingCode, userID.ToString());

                if (aviabilities.Count == 0)
                {
                    return Json(new { OK = "FAILED", data = "" }, JsonRequestBehavior.AllowGet);
                }

                List<string> lastestSearch = new List<string>();
                foreach (POCOAviabilityEntry aviability in aviabilities)
                {
                    if (aviability.flightType == 1)
                    {
                        lastestSearch.Add(getNameByIATANotAsync(aviability.IATAOrigin) + " > " + getNameByIATANotAsync(aviability.IATADestination) + ": " + aviability.DepartureDate.ToShortDateString().Replace('/', '-') + " / " + aviability.ReturnDate.ToShortDateString().Replace('/', '-'));
                    }
                    else
                    {
                        lastestSearch.Add(getNameByIATANotAsync(aviability.IATAOrigin) + " > " + getNameByIATANotAsync(aviability.IATADestination) + ": " + aviability.DepartureDate.ToShortDateString().Replace('/', '-'));
                    }

                }
                return Json(new { OK = "SUCCESS", data = new { lastestSearch = lastestSearch, addInfo = aviabilities }, }, JsonRequestBehavior.AllowGet);
            }
        }

        //Layout methods       
        /// <summary>
        /// Brings the telephones to contact each airport
        /// </summary>
        /// <returns></returns>
        public async Task<JsonResult> contactInfo()
        {
            //<City,Telphone>
            Dictionary<string, string> contactInfo = new Dictionary<string, string>();

            //AIRPORTS
            POCOConfigHelper helper = configurationReader.readHelper("REST.airlineConfig airportsList", configPlatform);
            string parametrizedUri = helper.uri.Replace("{id}", "");

            string airports = await callApi(helper, parametrizedUri, "", configurationReader.read("Consume.confSignatureName", "AWESAirlineConfig"), configurationReader.read("Consume.confSignature", "AWESAirlineConfig"));

            var dummyAirports = new[] { new { ID = 0, IATA = "", name = "", location = "", tax = "", contact = "", catalogID = 0 } };

            var entriesAirports = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(airports, dummyAirports);

            //CONFIGURATIONS
            helper = configurationReader.readHelper("REST.platform configuration entry value list", configPlatform);

            parametrizedUri = helper.uri.Replace("{platformName}", "AWESAdminDashboard");

            string configuration = await callApi(helper, parametrizedUri);

            var dummyConfiguration = new[] { new { configurationEntryID = 0, configurationEntryName = "",
                                      operationModeID = 0, operationModeName="", 
                                      configurationEntryValueID=0, configurationEntryValue="" } };
            var entriesConfiguration = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(configuration, dummyConfiguration);

            string catalogID = "";

            foreach (var entry in entriesConfiguration)
            {
                if (entry.configurationEntryName == "cities")
                {
                    catalogID = entry.configurationEntryValue;
                    break;
                }
            }

            List<catalogEntryPOCO> entriesCatalog = await authHelper.getCatalogEntriesByCatalogIDAsync(catalogID);

            List<KeyValuePair<string, string>> IATAName = new List<KeyValuePair<string, string>>();

            foreach (var entry in entriesAirports)
            {
                string name = await getNameByIATA(entry.IATA);
                KeyValuePair<string, string> item = new KeyValuePair<string, string>(entry.IATA, name);
                IATAName.Add(item);
            }

            try
            {
                entriesAirports = (from i in entriesAirports select i).OrderByDescending(i => (from j in IATAName where j.Key == i.IATA select i.name).FirstOrDefault()).Reverse().ToArray();
            }
            catch
            {

            }

            foreach (var entry in entriesAirports)
            {
                string cityName = "";
                try
                {
                    cityName = (from i in entriesCatalog
                                where i.name == entry.IATA
                                select i.value).FirstOrDefault();
                    contactInfo.Add(cityName, entry.contact);
                }
                catch (Exception ex)
                {
                    string exceptionMessage = ex.Message;

                    if (ex.InnerException != null)
                        exceptionMessage += ":" + ex.InnerException.Message;

                    throw new Exception("www::baseController::contactInfo:" + exceptionMessage, ex);
                }
            }
            return Json(new { Message = "Cities were charged", data = contactInfo }, JsonRequestBehavior.AllowGet);
        }

        //Layout methods       
        /// <summary>
        /// Brings the telephones to contact each airport
        /// </summary>
        /// <returns></returns>
        public JsonResult contactInfoNotAsync()
        {
            //<City,Telphone>
            Dictionary<string, string> contactInfo = new Dictionary<string, string>();

            //AIRPORTS
            POCOConfigHelper helper = configurationReader.readHelper("REST.airlineConfig airportsList", configPlatform);
            string parametrizedUri = helper.uri.Replace("{id}", "");

            string airports = callApiNotAsync(helper, parametrizedUri, "", configurationReader.read("Consume.confSignatureName", "AWESAirlineConfig"), configurationReader.read("Consume.confSignature", "AWESAirlineConfig"));

            var dummyAirports = new[] { new { ID = 0, IATA = "", name = "", location = "", tax = "", contact = "", catalogID = 0 } };

            var entriesAirports = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(airports, dummyAirports);

            //CONFIGURATIONS
            helper = configurationReader.readHelper("REST.platform configuration entry value list", configPlatform);

            parametrizedUri = helper.uri.Replace("{platformName}", "AWESAdminDashboard");

            string configuration = callApiNotAsync(helper, parametrizedUri);

            var dummyConfiguration = new[] { new { configurationEntryID = 0, configurationEntryName = "",
                                      operationModeID = 0, operationModeName="", 
                                      configurationEntryValueID=0, configurationEntryValue="" } };
            var entriesConfiguration = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(configuration, dummyConfiguration);

            string catalogID = "";

            foreach (var entry in entriesConfiguration)
            {
                if (entry.configurationEntryName == "cities")
                {
                    catalogID = entry.configurationEntryValue;
                    break;
                }
            }

            List<catalogEntryPOCO> entriesCatalog = authHelper.getCatalogEntriesByCatalogID(catalogID);

            List<KeyValuePair<string, string>> IATAName = new List<KeyValuePair<string, string>>();

            foreach (var entry in entriesAirports)
            {
                string name = getNameByIATANotAsync(entry.IATA);
                KeyValuePair<string, string> item = new KeyValuePair<string, string>(entry.IATA, name);
                IATAName.Add(item);
            }

            try
            {
                entriesAirports = (from i in entriesAirports select i).OrderByDescending(i => (from j in IATAName where j.Key == i.IATA select i.name).FirstOrDefault()).Reverse().ToArray();
            }
            catch
            {

            }

            foreach (var entry in entriesAirports)
            {
                string cityName = "";
                try
                {
                    cityName = (from i in entriesCatalog
                                where i.name == entry.IATA
                                select i.value).FirstOrDefault();
                    contactInfo.Add(cityName, entry.contact);
                }
                catch (Exception ex)
                {
                    string exceptionMessage = ex.Message;

                    if (ex.InnerException != null)
                        exceptionMessage += ":" + ex.InnerException.Message;

                    throw new Exception("www::baseController::contactInfo:" + exceptionMessage, ex);
                }
            }
            return Json(new { Message = "Cities were charged", data = contactInfo }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Do the calls to the specific API to make it easier from www platform
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="parametrizedUri"></param>
        /// <param name="otherBase"></param>
        /// <returns></returns>
        public async Task<string> callApi(POCOConfigHelper helper, string parametrizedUri, string otherBase = "", string signatureNameDefault = "", string signatureValueDefault = "")
        {
            string returnString = "";
            try
            {
                WebClient client = new WebClient();
                client.Headers[HttpRequestHeader.Accept] = "application/json";
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                if (string.IsNullOrEmpty(signatureNameDefault) && string.IsNullOrEmpty(signatureValueDefault))
                {
                    if ((signatureName != "") && (signatureValue != ""))
                        client.Headers.Add(signatureName, signatureValue);
                }
                else
                {
                    client.Headers.Add(signatureNameDefault, signatureValueDefault);
                }
                string Verb = helper.method;
                Verb = Verb.ToUpper().Trim();
                string result = "";
                string Uri = parametrizedUri;
                string body = "";
                string baseAddress = "";
                if (otherBase == "")
                    baseAddress = helper.server;
                else
                    baseAddress = otherBase;

                client.Encoding = System.Text.Encoding.UTF8;
                if (Verb == "GET")
                    result = await client.DownloadStringTaskAsync(baseAddress + Uri);
                else
                    if (body == null)
                        result = await client.UploadStringTaskAsync(baseAddress + Uri, Verb, "");
                    else
                        result = await client.UploadStringTaskAsync(baseAddress + Uri, Verb, body);

                returnString = result;
            }
            catch (Exception)
            {

                throw;
            }

            return returnString;
        }

        /// <summary>
        /// Do the calls to the specific API to make it easier from www platform
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="parametrizedUri"></param>
        /// <param name="otherBase"></param>
        /// <returns></returns>
        public string callApiNotAsync(POCOConfigHelper helper, string parametrizedUri, string otherBase = "", string signatureNameDefault = "", string signatureValueDefault = "")
        {
            string returnString = "";
            try
            {
                WebClient client = new WebClient();
                client.Headers[HttpRequestHeader.Accept] = "application/json";
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                if (string.IsNullOrEmpty(signatureNameDefault) && string.IsNullOrEmpty(signatureValueDefault))
                {
                    if ((signatureName != "") && (signatureValue != ""))
                        client.Headers.Add(signatureName, signatureValue);
                }
                else
                {
                    client.Headers.Add(signatureNameDefault, signatureValueDefault);
                }
                string Verb = helper.method;
                Verb = Verb.ToUpper().Trim();
                string result = "";
                string Uri = parametrizedUri;
                string body = "";
                string baseAddress = "";
                if (otherBase == "")
                    baseAddress = helper.server;
                else
                    baseAddress = otherBase;

                client.Encoding = System.Text.Encoding.UTF8;
                if (Verb == "GET")
                    result = client.DownloadString(baseAddress + Uri);
                else
                    if (body == null)
                        result = client.UploadString(baseAddress + Uri, Verb, "");
                    else
                        result = client.UploadString(baseAddress + Uri, Verb, body);

                returnString = result;
            }
            catch (Exception)
            {

                throw;
            }

            return returnString;
        }

        /// <summary>
        /// Do the calls to the specific API to make it easier from www platform
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="parametrizedUri"></param>
        /// <param name="otherBase"></param>
        /// <returns></returns>
        public static async Task<string> callApiStatic(POCOConfigHelper helper, string parametrizedUri, string otherBase = "", string signatureNameDefault = "", string signatureValueDefault = "")
        {
            string returnString = "";
            try
            {
                WebClient client = new WebClient();
                client.Headers[HttpRequestHeader.Accept] = "application/json";
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                if (string.IsNullOrEmpty(signatureNameDefault) && string.IsNullOrEmpty(signatureValueDefault))
                {
                    client.Headers.Add(ConfigurationManager.AppSettings["Consume.confSignatureName"], ConfigurationManager.AppSettings["Consume.confSignature"]);
                }
                else
                {
                    client.Headers.Add(signatureNameDefault, signatureValueDefault);
                }
                string Verb = helper.method;
                Verb = Verb.ToUpper().Trim();
                string result = "";
                string Uri = parametrizedUri;
                string body = "";
                string baseAddress = "";
                if (otherBase == "")
                    baseAddress = helper.server;
                else
                    baseAddress = otherBase;

                client.Encoding = System.Text.Encoding.UTF8;

                if (Verb == "GET")
                    result = await client.DownloadStringTaskAsync(baseAddress + Uri);
                else
                    if (body == null)
                        result = await client.UploadStringTaskAsync(baseAddress + Uri, Verb, "");
                    else
                        result = await client.UploadStringTaskAsync(baseAddress + Uri, Verb, body);

                returnString = result;
            }
            catch
            {

            }

            return returnString;
        }

        /// <summary>
        /// Do the calls to the specific API to make it easier from www platform
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="parametrizedUri"></param>
        /// <param name="otherBase"></param>
        /// <returns></returns>
        public static string callApiStaticNotAsync(POCOConfigHelper helper, string parametrizedUri, string otherBase = "", string signatureNameDefault = "", string signatureValueDefault = "")
        {
            string returnString = "";
            try
            {
                WebClient client = new WebClient();
                client.Headers[HttpRequestHeader.Accept] = "application/json";
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                if (string.IsNullOrEmpty(signatureNameDefault) && string.IsNullOrEmpty(signatureValueDefault))
                {
                    client.Headers.Add(ConfigurationManager.AppSettings["Consume.confSignatureName"], ConfigurationManager.AppSettings["Consume.confSignature"]);
                }
                else
                {
                    client.Headers.Add(signatureNameDefault, signatureValueDefault);
                }
                string Verb = helper.method;
                Verb = Verb.ToUpper().Trim();
                string result = "";
                string Uri = parametrizedUri;
                string body = "";
                string baseAddress = "";
                if (otherBase == "")
                    baseAddress = helper.server;
                else
                    baseAddress = otherBase;

                client.Encoding = System.Text.Encoding.UTF8;

                if (Verb == "GET")
                    result = client.DownloadString(baseAddress + Uri);
                else
                    if (body == null)
                        result = client.UploadString(baseAddress + Uri, Verb, "");
                    else
                        result = client.UploadString(baseAddress + Uri, Verb, body);

                returnString = result;
            }
            catch
            {

            }

            return returnString;
        }

        /// <summary>
        /// returns a list with the system available document type for buyers and pax
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> isoDOcumentos()
        {

            Dictionary<string, string> retorno = new Dictionary<string, string>();
            retorno.Add("CC", "Cédula de ciudadanía");
            retorno.Add("CE", "Cédula de extranjería");
            retorno.Add("NIT", "En caso de ser una empresa");
            retorno.Add("TI", "Tarjeta de Identidad");
            retorno.Add("PP", "Pasaporte");
            retorno.Add("IDC", "Identificador único de cliente, para el caso de ID’s únicos de clientes/usuarios de servicios públicos");
            retorno.Add("CEL", "En caso de identificarse a través de la línea del móvil");
            retorno.Add("RC", "Registro civil de nacimiento");
            retorno.Add("DE", "Documento de identificación extranjero");

            return retorno;
        }

        /// <summary>
        /// Reads a querystring from current request. If is n ot available returns emty string
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string safeQueryStringValue(string name)
        {
            if (Request.Form.AllKeys.Contains(name))
                return Request.Form[name];
            else
                return "Not found";
        }
        /// <summary>
        /// gets the content asociated with 
        /// </summary>
        /// <returns></returns>
        public async Task<JsonResult> getContentByType(string type) {

            string[] languages = new string[1];
            languages[0] = "es";
            if (Request.UserLanguages != null)
            {
                languages = Request.UserLanguages;
            }
            string lang = "";
            Dictionary<string, string> html = new Dictionary<string, string>();
            foreach (var language in languages)
            {
                lang = language.Split(';')[0];
                html = await contentHelper.pageListHtmlAsync("-1", platform, lang, type);
                if (html.Count != 0)
                    break;
            }
            return Json(new { Message = "SUCCESS", data = Newtonsoft.Json.JsonConvert.SerializeObject(html) }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// gets the content asociated with 
        /// </summary>
        /// <returns></returns>
        public async Task<Dictionary<string,string>> getContentByTypeDict(string type)
        {

            string[] languages = new string[1];
            languages[0] = "es";
            if (Request.UserLanguages != null)
            {
                languages = Request.UserLanguages;
            }
            string lang = "";
            Dictionary<string, string> html = new Dictionary<string, string>();
            foreach (var language in languages)
            {
                lang = language.Split(';')[0];
                html = await contentHelper.pageListHtmlAsync("-1", platform, lang, type);
                if (html.Count != 0)
                    break;
            }
            return html;
        }

        public Dictionary<string, string> getContentByTypeDictNotAsync(string type)
        {

            string[] languages = new string[1];
            languages[0] = "es";
            if (Request.UserLanguages != null)
            {
                languages = Request.UserLanguages;
            }
            string lang = "";
            Dictionary<string, string> html = new Dictionary<string, string>();
            foreach (var language in languages)
            {
                lang = language.Split(';')[0];
                html = contentHelper.pageListHtml("-1", platform, lang, type);
                if (html.Count != 0)
                    break;
            }
            return html;
        }

        public async Task<JsonResult> getPaymentState(string PNR) {

            string PNRSerialized = await checkInHelper.getPNRbyReservationCodeAsync(PNR);
            var dummy = new { TrackCode = "" };
            var trackingCodeObject = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(PNRSerialized, dummy);
            string state = "";
            string OK = "SUCCESS";
            string CUS = "";
            string COP = "COP";
            double value = 0;
            string valueFormatted="";

            if (PNRSerialized != "null")
            {
                string trackingCode = trackingCodeObject.TrackCode.ToString();
                POCOConfigHelper helper = configurationReader.readHelper("REST.reservation paymentGet", configPlatform);

                string parametrizedUri = helper.uri;
                parametrizedUri = parametrizedUri.Replace("{trackingCode}", trackingCode);

                string paymentVerification = await webPost.asyncAction(helper.method,
                                                           null,
                                                           helper.server,
                                                           parametrizedUri,
                                                           "application/json",
                                                           "application/json",
                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);
                if (!string.IsNullOrEmpty(paymentVerification))
                {
                    POCOPaymentEntry paymentEntry = new POCOPaymentEntry();
                    paymentEntry = Newtonsoft.Json.JsonConvert.DeserializeObject<POCOPaymentEntry>(paymentVerification);
                    state = paymentEntry.PaymentState;
                    if (state == "PENDING")
                    {
                        state = "Pendiente";
                    }
                    else if (state == "APPROVED")
                    {
                        state = "Aprobada";
                    }
                    else
                    {
                        state = "Declinada";
                    }
                    string resSerialized = reservationInfoNotAsync(null, trackingCode);

                    POCOReservationEntry reservation = Newtonsoft.Json.JsonConvert.DeserializeObject<POCOReservationEntry>(resSerialized);
                    value = reservation.Tax + reservation.Price + reservation.Fee;
                    valueFormatted = string.Format("{0:$ #,###}", value);
                    var dummyXml = new { trazabilityCode = "" };
                    var deserializedXml = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(paymentEntry.XmlReply, dummyXml);
                    CUS = deserializedXml.trazabilityCode;
                }
                else
                {
                    OK = "FAILED";
                }
            }
            else {
                OK = "FAILED";
            }

            return Json(new { OK=OK, data= new {pnr=PNR,cus=CUS,cop=COP,value=valueFormatted,state=state} }, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> opinionSend(string name, string telphone, string email, string message) {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(telphone) || string.IsNullOrEmpty(email) || string.IsNullOrEmpty(message))
                return Json(new { OK = "FAILED", data = "Todos los campos son requeridos" },JsonRequestBehavior.AllowGet);

            try
            {
                string destinataires = configurationReader.read("opinionSendOpinion", "MVCFuncional");
                string subject = "Denos su opinión:: Comentarios easyFly";
                string body = "";

                body = body + "<div><p>Nombre: </p><p>" + name + "</p></div></br>";
                body = body + "<div><p>Telefono: </p><p>" + telphone + "</p></div></br>";
                body = body + "<div><p>Email: </p><p>" + email + "</p></div></br>";
                body = body + "<div><p>Mensaje: </p><p>" + message + "</p></div></br>";

                await messageHelper.messagingAsync(new messageEntry(functionalUnit, callingMethod(),
                                                                                   destinataires,
                                                                                   subject, body,
                                                                                   messageEntry.messageType.email));
            }
            catch 
            {
                return Json(new { OK = "FAILED", data = "Ha ocurrido un error, por favor intentelo más tarde o cumuniquese con nuestro call center al teléfono 4148095-94 o al correo reservas.call@easyfly.com.co" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { OK="SUCCESS", data="Enviado con éxito" }, JsonRequestBehavior.AllowGet);

        }

        public async Task<JsonResult> registerLayout(string email) {

            try
            {
                string registerID = configurationReader.read("registerLayoutID", "AWESMarketingProvider");
                int registerIDint = 0;
                int.TryParse(registerID, out registerIDint);
                List<POCORegisterDetailEntry> listRegisterDetails = await promotionHelper.promotionRegisterDetailListAsync(platform, registerID);

                bool canRegister = true;

                foreach (POCORegisterDetailEntry registerDetail in listRegisterDetails)
                {
                    if (registerDetail.email == email)
                    {
                        canRegister = false;
                        break;
                    }
                }

                if (canRegister)
                {
                    POCORegisterDetailEntry valueToSend = new POCORegisterDetailEntry();
                    valueToSend.ID = 0;
                    valueToSend.date = DateTime.UtcNow;
                    valueToSend.documentNumber = "";
                    valueToSend.email = email;
                    valueToSend.name = "";
                    valueToSend.platform = platform;
                    valueToSend.registerID = registerIDint;
                    valueToSend.telephone = "";
                    listRegisterDetails = await promotionHelper.promotionRegisterDetailCreateAsync(platform, registerID, valueToSend);

                    return Json(new { OK = "SUCCESS", data = "Registro exitoso" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { OK = "FAILED", data = "email" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch 
            {
                return Json(new { OK = "FAILED", data = "error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> getSEO() {
            List<POCOSEOEntry> SEO = new List<POCOSEOEntry>();
            try
            {
                string currentUrl = Request.UrlReferrer.AbsolutePath.ToString().ToLower();
                string stringToConvert = await contentHelper.SEOListAsync(platform, currentUrl);
                SEO = Newtonsoft.Json.JsonConvert.DeserializeObject<List<POCOSEOEntry>>(stringToConvert);
            }
            catch 
            {
                return Json(new {OK="FAILED" , data=""},JsonRequestBehavior.AllowGet);
            }
            if (SEO.Count > 0)
            {
                return Json(new { OK = "SUCCESS", data = new { descriptionSEO = SEO[0].descriptionSEO, keywords = SEO[0].keywords } }, JsonRequestBehavior.AllowGet);
            }
            else {
                return Json(new { OK="FAILED", data=""}, JsonRequestBehavior.AllowGet);
            }
        }

        public string[] getSEOMVC()
        {
            List<POCOSEOEntry> SEO = new List<POCOSEOEntry>();
            string [] endArray = new string [2];
            endArray[0] = "";
            endArray[1] = "";
            try
            {
                string currentUrl = System.Web.HttpContext.Current.Request.RawUrl.ToString().ToLower();
                string stringToConvert = contentHelper.SEOListMVC(platform, currentUrl);
                SEO = Newtonsoft.Json.JsonConvert.DeserializeObject<List<POCOSEOEntry>>(stringToConvert);
            }
            catch
            {
                return endArray;
            }
            if (SEO.Count > 0)
            {
                endArray[0] = SEO[0].descriptionSEO;
                endArray[1] = SEO[0].keywords;
                return endArray;
            }
            else
            {
                return endArray;
            }
        }

        //Login/out methods

        /// <summary>
        /// This version of the method logs in the user from a login page
        /// </summary>
        /// <param name="logIn"></param>
        /// <returns></returns>
        //[RequireHttps]
        public async Task<ActionResult> logInLayout(string email, string pass)
        {
            return Redirect("/access/logInHome?userEmail=" + email + "&userPassword=" + pass);
        }
        /// <summary>
        /// This version of the method logs in the user from a login page
        /// </summary>
        /// <param name="logIn"></param>
        /// <returns></returns>
        //[RequireHttps]
        public async Task<ActionResult> logInModal(string email, string pass, bool emailRemember = false)
        {
            if (userAdministrator.instance.user.isAuthenticated)
            {
                return Redirect("/access/logOut?userEmail=" + email + "&userPassword=" + pass);
            }
            
            return Redirect("/access/logInHome?userEmail=" + email + "&userPassword=" + pass);
        }

        /// <summary>
        /// This version of the method logs in the user from a login page
        /// </summary>
        /// <param name="logIn"></param>
        /// <returns></returns>
        //[RequireHttps]
        public async Task<ActionResult> singInProcess(string email, string emailConfirm, string pass, string passConfirm, string name, string lastName, string document)
        {
            signInModel SIValues = new signInModel();
            SIValues.email = email;
            SIValues.emailConfirmation = emailConfirm;
            SIValues.password = pass;
            SIValues.passwordConfirmation = passConfirm;
            SIValues.name = name;
            SIValues.lastName = lastName;
            SIValues.documentNumber = document;
            if (!(await isSingIn(email)))
                return Redirect("/access/signIn?email=" + email + "&emailConfirm=" + emailConfirm + "&pass=" + pass + "&passConfirm=" + passConfirm + "&name=" + name + "&lastName=" + lastName + "&document=" + document);
            else
                return RedirectToAction("/Home/Index?error=sign");
        }

        public async Task<bool> isSingIn(string emailConfirm)
        {
            POCOConfigHelper helper = configurationReader.readHelper("REST.user list", configPlatform);

            string returnString = await webPost.asyncAction(helper.method,
                                                       null,
                                                       helper.server,
                                                       helper.uri + "?email=" + emailConfirm.ToString(),
                                                       "application/json",
                                                       "application/json",
                                                       signatureName,
                                                       signatureValue);

            List<CSecurityInfo> entries = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CSecurityInfo>>(returnString);
            // ********************************************************************************************

            // ********************************************************************************************
            // UPDATES TOKEN
            // ********************************************************************************************
            if ((entries.Count() > 0) && (entries[0].user != null))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Language resources
        /// <summary>
        /// Gets the months by a specific language
        /// </summary>
        /// <returns></returns>
        public async Task<JsonResult> getMonthLanguage() {
            Dictionary<string, string> months = new Dictionary<string, string>();
            List<string> monthsNames = new List<string>() { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            foreach (string month in monthsNames)
            {
                try
                {
                    var dummy = new { data = "" };
                    string stringResource = Newtonsoft.Json.JsonConvert.SerializeObject((await getStringResourceJson(month, month)).Data);
                    var value = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(stringResource, dummy);
                    string monthName = value.data;
                    months.Add(month, monthName);
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return Json(new { data = months }, JsonRequestBehavior.AllowGet);
        } 
        public static string getStringResource(string key, string value, string url, string[] UserLanguages, bool saveInDB = true, bool isLayout = false)
        {
            //if (true)
            //{
            //    return "Hola";
            //}
            List<KeyValuePair<string, string>> stringResources = new List<KeyValuePair<string, string>>();
            if (UserLanguages == null) { 
                UserLanguages= new string[1];
                UserLanguages[0] = "es";
            }
            try
            {
                stringResources = languageHelper.getListStringResources(key, value, url, UserLanguages, saveInDB, isLayout);
            }
            catch (Exception ex)
            {
                
            }
            //Find the value by key
            foreach (KeyValuePair<string, string> keyValue in stringResources)
            {
                if (keyValue.Key == key)
                {
                    value = keyValue.Value;
                }
            }
            return value;
        }

        public async Task<JsonResult> getStringResourceJson(string key, string value, bool saveInDB = true, bool isLayout = false)
        {
            //if (true)
            //{
            //    return Json(new { data = "Hola" }, JsonRequestBehavior.AllowGet);
            //}
            List<KeyValuePair<string, string>> stringResources = new List<KeyValuePair<string,string>>();
            string[] UserLanguages = new string[1];
            if (Request.UserLanguages == null)
            {
                UserLanguages[0] = "es";
            }
            else {
                UserLanguages = Request.UserLanguages;
            }
            stringResources = await languageHelper.getListStringResourcesAsync(key, value, Request.Url.ToString(), UserLanguages, saveInDB, isLayout);
            //Find the value by key
            foreach (KeyValuePair<string, string> keyValue in stringResources)
            {
                if (keyValue.Key == key)
                {
                    value = keyValue.Value;
                }
            }
            return Json(new { data=value }, JsonRequestBehavior.AllowGet);
        }

        public void ErrorToShow() {
            string error = Request.QueryString["error"];
            string cookieError = "";
            try
            {
                cookieError = Request.Cookies["ErrorResponse"]["error"];
            }
            catch
            {
                
            }
            
            ViewBag.hasOtherError = false;

            if (!string.IsNullOrEmpty(error) || !string.IsNullOrEmpty(cookieError))
            {
                if (error == "22030") // COTIZATION ERROR 
                {
                    ViewBag.hasOtherError = true;
                    ViewBag.otherError = error;
                    ViewBag.otherErrorMessage = getStringResource("22030COTIZATIONERROR", "La tarifa que ha seleccionado ya no está disponible, por favor vuelva a intentarlo más tarde.", Request.Url.ToString(), Request.UserLanguages);
                }
                if (error == "sign")
                {
                    ViewBag.hasOtherError = true;
                    ViewBag.otherError = error;
                    ViewBag.otherErrorMessage = getStringResource("signErrorHome", "Éste usuario ya se encuentra registrado en nuestro sistema", Request.Url.ToString(), Request.UserLanguages);
                }
                if (error == "WrongDestination") {
                    ViewBag.hasOtherError = true;
                    ViewBag.otherError = error;
                    ViewBag.otherErrorMessage = getStringResource("WrongDestination", "Mientras haga un proceso de compra no debe devolverse en ningún momento", Request.Url.ToString(), Request.UserLanguages);
                }
                if (error == "pageNotFoundCity") {
                    ViewBag.hasOtherError = true;
                    ViewBag.otherError = error;
                    ViewBag.otherErrorMessage = getStringResource("pageNotFoundCity", "La página solicitada no existe", Request.Url.ToString(), Request.UserLanguages);
                }
                if (cookieError == "checkInError")
                {
                    ViewBag.hasOtherError = true;
                    ViewBag.otherError = cookieError;
                    ViewBag.otherErrorMessage = getStringResource("checkInError", "Hubo un error, puede ser debido a que el vuelo de ésta reserva ya despegó o que la hora de despegue no está a 36 horas de la fecha de hoy o menos, para mayor información por favor comuniquese con nuestro callCenter", Request.Url.ToString(), Request.UserLanguages);
                    cookieHasError["error"] = "none";
                    cookieHasError.Expires = DateTime.Now.AddMinutes(5);
                    Response.Cookies.Add(cookieHasError);
                }
            }
        }

        //Validation
        public JsonResult luhnValidation(string accountNumber)
        {
            bool canPay = payHelper.luhn(accountNumber);
            return Json(new { data = canPay }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Banco de Bogotá referenced payment
        /// </summary>
        /// <param name="postObject"></param>
        /// <returns></returns>
        public void referencedPayment(string value, string reference)
        {
            try
            {
                double valuePay = 0;
                double.TryParse(value, out valuePay);
                string paxString = paxInfoNotAsync();
                List<Pax> paxList = JsonConvert.DeserializeObject<List<Pax>>(paxString);

                //****************************************************************************************************************
                // SENDS PAYMENT
                //****************************************************************************************************************
                POCOConfigHelper helper = configurationReader.readHelper("REST.Payment referenced", configPlatform);
                bindingTransactionWrapper transaction = new bindingTransactionWrapper()
                {
                    expirationDate = DateTime.UtcNow.AddDays(1).ToString("s"),
                    order = new bindingOrderWrapper()
                    {
                        referenceCode = reference,
                        description = "PAGO DE LA RESERVA " + reference,
                        netValue = valuePay,
                        buyer = new bindingBuyerWrapper()
                        {
                            fullName = paxList[0].Name + " " + paxList[0].LastName,
                            emailAddress = paxList[0].Email,
                            contactPhone = paxList[0].Telephone,
                            dniNumber = paxList[0].DocumentNumber,
                        }
                    },
                    pseParameters = new bindingPSEParameters()
                    {
                        ResponseURL = configurationReader.read("POL.Pagos.PostURL", "AWESPaymentProvider"),
                    }
                };

                var responseString = webPost.action(helper.method,
                                                           JsonConvert.SerializeObject(transaction),
                                                           helper.server,
                                                           helper.uri,
                                                           "application/json",
                                                           "application/json",
                                                           paymentSignatureName,
                                                           paymentSignatureValue);

                //****************************************************************************************************************
                // DECODE RESPONSE
                //****************************************************************************************************************
                //bindingTransactionResponseWrapper rspnse = JsonConvert.DeserializeObject<bindingTransactionResponseWrapper>(responseString);
                string response = JsonConvert.DeserializeObject<string>(responseString);
                PaymentModel payment = new PaymentModel();
                payment.Franchise = "Referenciado";
                payment.PaymentDate = DateTime.Now;
                payment.PaymentMethod = "Referenciado";
                payment.PaymentState = "PENDING";
                payment.TrackCode = Encrypt.decrypt(Request.Cookies["UserSettings"]["tracking"], encriptSalt);
                payment.XmlCall = transaction.ToString();
                payment.XmlReply = responseString;
                string responsePayment = paymentInsert(payment).ToString();
                System.Web.HttpContext.Current.Response.Write(response.ToString());
                System.Web.HttpContext.Current.Response.End();
                //return Json(new { OK = "payment requested ", data = responseString }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                //return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //Other functions
        public string ascentMapped(string stringToMapped) {
            string stringMapped = "";

            foreach (char letter in stringToMapped)
            {
                if (letter == 'á' || letter == 'Á')
                {
                    if(char.IsUpper(letter))
                        stringMapped = stringMapped + 'A';
                    else
                        stringMapped = stringMapped + 'a';
                }
                else if (letter == 'é' || letter == 'É')
                {
                    if (char.IsUpper(letter))
                        stringMapped = stringMapped + 'E';
                    else
                        stringMapped = stringMapped + 'e';
                }
                else if (letter == 'í' || letter == 'Í')
                {
                    if (char.IsUpper(letter))
                        stringMapped = stringMapped + 'I';
                    else
                        stringMapped = stringMapped + 'i';
                }
                else if (letter == 'ó' || letter == 'Ó')
                {
                    if (char.IsUpper(letter))
                        stringMapped = stringMapped + 'O';
                    else
                        stringMapped = stringMapped + 'o';
                }
                else if (letter == 'ú' || letter == 'Ú')
                {
                    if (char.IsUpper(letter))
                        stringMapped = stringMapped + 'U';
                    else
                        stringMapped = stringMapped + 'u';
                }
                else {
                    stringMapped = stringMapped + letter;
                }
            }

            return stringMapped;
        }

        public async Task<string> goToOldPage() {
            await contentHelper.postHitsAsync(ConfigurationManager.AppSettings["platform"], "TO-OLD-PAGE", "es");
            cookieNewPage["onlyNewPage"] = "false";
            cookieNewPage.Expires = DateTime.Now.AddDays(20);
            cookieNewPage.Domain = ".easyfly.com.co";
            Response.Cookies.Add(cookieNewPage);
            return "";
        }
    }
}
