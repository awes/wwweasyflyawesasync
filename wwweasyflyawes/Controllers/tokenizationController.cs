﻿using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using AWESMVCCommon.Filters;
using AWESMVCCommon.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AWESAirline.Controllers
{
    public class tokenizationController : baseController
    {
        //[authorizationConfirmedFilter()]
        public async Task<ActionResult> Index()
        {
            try
            {
                ViewBag.entries = await getTokenList();

                return View();
            }
            catch (Exception ex)
            {
                throw new Exception("tokenizationController::index::" + ex.Message);
            }
        }

        //[authorizationConfirmedFilter()]
        public async Task<JsonResult> addTokenization(string postObject)
        {
            try
            {
                var tokenObject = new { };

                int userID = userAdministrator.instance.user.ID;

                //****************************************************************************************************************
                // ADDS PAYU TOKEN
                //****************************************************************************************************************
                POCOConfigHelper helper = configurationReader.readHelper("REST.Add user payment token", configPlatform);

                var dummy = new
                {
                    payerId = 0,
                    name = "",
                    identificationNumber = "",
                    paymentMethod = "",
                    number = "",
                    expirationDate = "2017/01"
                };

                var creditCard = JsonConvert.DeserializeAnonymousType(postObject, dummy);
                string parametrizedUri = helper.uri.Replace("{userid}", userAdministrator.instance.user.ID.ToString());

                string returnString = await webPost.asyncAction(helper.method,
                                                                JsonConvert.SerializeObject(creditCard),
                                                                helper.server,
                                                                parametrizedUri,
                                                                "application/json",
                                                                "application/json",
                                                                paymentSignatureName,
                                                                paymentSignatureValue);

                //****************************************************************************************************************
                // END ADDS PAYU TOKEN
                //****************************************************************************************************************

                addPaymentTokenResponse response = JsonConvert.DeserializeObject<addPaymentTokenResponse>(returnString);
                if (response.code == "SUCCESS")
                {
                    //****************************************************************************************************************
                    // ADDS LOCAL TOKEN INFO
                    //****************************************************************************************************************
                    helper = configurationReader.readHelper("REST.Add user token", configPlatform);

                    var newUserToken = new
                    {
                        name = response.creditCardToken.name,
                        token = response.creditCardToken.creditCardTokenId,
                        notes = response.creditCardToken.maskedNumber,
                        paymentMethod = response.creditCardToken.paymentMethod,
                        isEnabled = true
                    };

                    parametrizedUri = helper.uri.Replace("{userID}", userAdministrator.instance.user.ID.ToString());

                    returnString = await webPost.asyncAction(helper.method,
                                                             JsonConvert.SerializeObject(newUserToken),
                                                             helper.server,
                                                             parametrizedUri,
                                                             "application/json",
                                                             "application/json",
                                                             signatureName,
                                                             signatureValue);

                    // RELOADS PAYMENT TOKENS
                    var data = await getTokenList();

                    //****************************************************************************************************************
                    // END ADDS LOCAL TOKEN INFO
                    //****************************************************************************************************************
                    return Json(new { OK = "Token added", data = data }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { Success = false, Message = response.error }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {

                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //[authorizationConfirmedFilter()]
        public async Task<JsonResult> tokenizationinfo(string tokenID)
        {
            try
            {
                var tokenObject = new { };

                int userID = userAdministrator.instance.user.ID;

                POCOConfigHelper helper = configurationReader.readHelper("REST.User payment token by id", configPlatform);

                string parametrizedUri = helper.uri.Replace("{userid}", userAdministrator.instance.user.ID.ToString());
                parametrizedUri = parametrizedUri.Replace("{id}", tokenID.ToString());

                string returnString = await webPost.asyncAction(helper.method,
                                                                null,
                                                                helper.server,
                                                                parametrizedUri,
                                                                "application/json",
                                                                "application/json",
                                                                paymentSignatureName,
                                                                paymentSignatureValue);

                tokenizationinfoResponse responseInfo = JsonConvert.DeserializeObject<tokenizationinfoResponse>(returnString);

                if (responseInfo.code == "SUCCESS")
                {
                    return Json(new { OK = "token info", data = responseInfo.creditCardTokenList }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { Success = false, Message = responseInfo.error }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {

                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //[authorizationConfirmedFilter()]
        public async Task<JsonResult> disableTokenization(string creditCardTokenId, string id)
        {
            try
            {
                //****************************************************************************************************************
                // DISABLES LOCAL TOKEN INFO
                //****************************************************************************************************************
                POCOConfigHelper helper = configurationReader.readHelper("REST.Update user token", configPlatform);

                string parametrizedUri = helper.uri.Replace("{userID}", userAdministrator.instance.user.ID.ToString());
                parametrizedUri = parametrizedUri.Replace("{id}", id.ToString());

                var newState = new { ID = id, isEnabled = false };


                string returnString = await webPost.asyncAction(helper.method,
                                                         JsonConvert.SerializeObject(newState),
                                                         helper.server,
                                                         parametrizedUri,
                                                         "application/json",
                                                         "application/json",
                                                         signatureName,
                                                         signatureValue);

                // RELOADS PAYMENT TOKENS
                var data = await getTokenList();

                //****************************************************************************************************************
                // DISABLES LOCAL TOKEN INFO
                //****************************************************************************************************************
                return Json(new { OK = "token disabled", data = data }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //[authorizationConfirmedFilter()]
        public async Task<JsonResult> enableTokenization(string creditCardTokenId, string id)
        {
            try
            {
                //****************************************************************************************************************
                // ENABLES LOCAL TOKEN INFO
                //****************************************************************************************************************
                POCOConfigHelper helper = configurationReader.readHelper("REST.Update user token", configPlatform);

                string parametrizedUri = helper.uri.Replace("{userID}", userAdministrator.instance.user.ID.ToString());
                parametrizedUri = parametrizedUri.Replace("{id}", id.ToString());

                var newState = new { ID = id, isEnabled = true };

                string returnString = await webPost.asyncAction(helper.method,
                                                         JsonConvert.SerializeObject(newState),
                                                         helper.server,
                                                         parametrizedUri,
                                                         "application/json",
                                                         "application/json",
                                                         signatureName,
                                                         signatureValue);

                // RELOADS PAYMENT TOKENS
                var data = await getTokenList();

                //****************************************************************************************************************
                // ENABLES LOCAL TOKEN INFO
                //****************************************************************************************************************
                return Json(new { OK = "token disabled", data = data }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //[authorizationConfirmedFilter()]
        public async Task<JsonResult> deleteTokenization(string creditCardTokenId, string id)
        {
            try
            {
                var tokenObject = new { };

                int userID = userAdministrator.instance.user.ID;

                //****************************************************************************************************************
                // DELETES PAYU TOKEN
                //****************************************************************************************************************
                POCOConfigHelper helper = configurationReader.readHelper("REST.delete user payment token", configPlatform);

                var creditCard = new
                {
                    payerId = userID,
                    creditCardTokenId = creditCardTokenId
                };

                string parametrizedUri = helper.uri.Replace("{userid}", userAdministrator.instance.user.ID.ToString());
                parametrizedUri = parametrizedUri.Replace("{id}", creditCardTokenId.ToString());

                string returnString = await webPost.asyncAction(helper.method,
                                                                JsonConvert.SerializeObject(creditCard),
                                                                helper.server,
                                                                parametrizedUri,
                                                                "application/json",
                                                                "application/json",
                                                                paymentSignatureName,
                                                                paymentSignatureValue);

                //****************************************************************************************************************
                // END DELETES PAYU TOKEN
                //****************************************************************************************************************

                addPaymentTokenResponse response = JsonConvert.DeserializeObject<addPaymentTokenResponse>(returnString);
                if (response.code == "SUCCESS")
                {
                    //****************************************************************************************************************
                    // DELETES LOCAL TOKEN INFO
                    //****************************************************************************************************************
                    helper = configurationReader.readHelper("REST.delete user token", configPlatform);

                    parametrizedUri = helper.uri.Replace("{userID}", userAdministrator.instance.user.ID.ToString());
                    parametrizedUri = parametrizedUri.Replace("{id}", id.ToString());

                    returnString = await webPost.asyncAction(helper.method,
                                                             null,
                                                             helper.server,
                                                             parametrizedUri,
                                                             "application/json",
                                                             "application/json",
                                                             signatureName,
                                                             signatureValue);

                    // RELOADS PAYMENT TOKENS
                    var data = await getTokenList();

                    //****************************************************************************************************************
                    // DELETES LOCAL TOKEN INFO
                    //****************************************************************************************************************
                    return Json(new { OK = "token deleted", data = data }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { Success = false, Message = response.error }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {

                Response.Clear();
                Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //[authorizationConfirmedFilter()]
        protected override async Task<List<tokenBind>> getTokenList()
        {
            return await base.getTokenList();
        }

        public class tokenBind
        {
            public int ID { get; set; }
            public bool isEnabled { get; set; }
            public string name { get; set; }
            public string notes { get; set; }
            public string token { get; set; }
            public string paymentMethod { get; set; }

            public tokenBind(int ID, bool isEnabled, string name, string notes)
            {
                this.ID = ID;
                this.isEnabled = isEnabled;
                this.name = name;
                this.notes = notes;
            }
        }

        public class tokenPaymentMethod
        {
            public int ID { get; set; }
            public string description { get; set; }
            public string country { get; set; }

            public tokenPaymentMethod(int ID, string description, string country)
            {
                this.ID = ID;
                this.description = description;
                this.country = country;
            }
        }

        public class CreditCardToken
        {
            public string creditCardTokenId { get; set; }
            public string name { get; set; }
            public int payerId { get; set; }
            public string identificationNumber { get; set; }
            public string paymentMethod { get; set; }
            public object number { get; set; }
            public object expirationDate { get; set; }
            public object creationDate { get; set; }
            public string maskedNumber { get; set; }
            public object errorDescription { get; set; }
            public object startDate { get; set; }
            public object endDate { get; set; }
        }

        public class addPaymentTokenResponse
        {
            public CreditCardToken creditCardToken { get; set; }
            public string code { get; set; }
            public object error { get; set; }
            public object transactionResponse { get; set; }
        }

        public class CreditCardTokenList
        {
            public string creditCardTokenId { get; set; }
            public string name { get; set; }
            public int payerId { get; set; }
            public string identificationNumber { get; set; }
            public string paymentMethod { get; set; }
            public object number { get; set; }
            public object expirationDate { get; set; }
            public string creationDate { get; set; }
            public string maskedNumber { get; set; }
            public object errorDescription { get; set; }
            public object startDate { get; set; }
            public object endDate { get; set; }
        }

        public class tokenizationinfoResponse
        {
            public List<CreditCardTokenList> creditCardTokenList { get; set; }
            public string code { get; set; }
            public object error { get; set; }
            public object transactionResponse { get; set; }
        }
    }
}