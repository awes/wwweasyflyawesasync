﻿using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using AWESMVCCommon.Security;
using AWESAirline.Models;
using AWESPaymentModel.BindingObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using AWESCommon.Extensions;
using wwweasyflyawes.Models;
using AWESMVCCommon.Filters;
using System.Configuration;

namespace AWESAirline.Controllers
{
    public partial class PaymentController : baseController
    {
        private string _encriptSalt;
        private string _cookieName;

        public string encriptSalt
        {
            get
            {
                if (_encriptSalt == null)
                    _encriptSalt = configurationReader.read("auth.cookie.encriptSalt");

                return _encriptSalt;
            }
        }

        public string cookieName
        {
            get
            {
                if (_cookieName == null)
                    _cookieName = configurationReader.read("auth.2step.cookie.name");

                return _cookieName;
            }
        }

        // GET: Payment
        public async Task<ActionResult> Index()
        {
            int intInf = 0;
            double dblFeeInf = 0;
            double dblIvaFeeInf = 0;
            ViewBag.Code = "";
            ViewBag.reference = "";
            ViewBag.origin = "";
            ViewBag.destination = "";
            ViewBag.departureTime = "";
            ViewBag.arrivalTime = "";
            ViewBag.departureTimeReturn = "";
            ViewBag.arrivalTimeReturn = "";
            ViewBag.paxList = new List<Pax>();
            ViewBag.accumulation = 0;
            ViewBag.valueToPay = "";
            ViewBag.value = "";
            ViewBag.valueCO = "";
            ViewBag.valueYS = "";
            ViewBag.valueTA = "";
            ViewBag.valueTB = "";
            ViewBag.typeFlight = "";
            ViewBag.entries = "";
            ViewBag.deviceSessionId = "";
            ViewBag.MessageBadResponse = "";
            ViewBag.BadResponseCode = "";
            ViewBag.isOtherResponseCode = "";
            try
            {
                // GETS RESERVATION INFO
                // id interno de referencia
                string returnString = await PNRInfo();
                PNR PNRTracking = JsonConvert.DeserializeObject<PNR>(returnString);
                ViewBag.Code = PNRTracking.ReservationCode;
                // id interno de referencia
                ViewBag.reference = PNRTracking.ReservationCode;
                string paxString = await paxInfoSync();
                List<Pax> paxList = JsonConvert.DeserializeObject<List<Pax>>(paxString);
                string returnStringSelect = await selectInfo();
                avail availTracking = JsonConvert.DeserializeObject<avail>(returnStringSelect);
                string reservationInformation = await reservationInfo();
                Reservation reservationTracking = JsonConvert.DeserializeObject<Reservation>(reservationInformation);
                ViewBag.origin = availTracking.IATAOrigin;
                ViewBag.destination = availTracking.IATADestination;
                ViewBag.departureTime = reservationTracking.DepartureDateTime;
                ViewBag.arrivalTime = reservationTracking.ArrivalDateTime;
                ViewBag.departureTimeReturn = reservationTracking.DepartureDateTimeReturn;
                ViewBag.arrivalTimeReturn = reservationTracking.ArrivalDateTimeReturn;
                //LISTA DE PASAJEROS
                ViewBag.paxList = paxList;
                // avlor total por pagar incluyendo todos los impuestos y cargos

                ViewBag.accumulation = accumulationValue();
                ViewBag.valueToPay = PNRTracking.KIU_TOTAL;              
                ViewBag.valueCO = PNRTracking.KIU_CO;
                ViewBag.valueYS = PNRTracking.KIU_YS;
                foreach (Pax px in paxList)
                {
                    if (px.PaxType == "INF")
                    { intInf = intInf + 1; }
                }
                if (intInf > 0)
                {
                    dblFeeInf = reservationTracking.Fee * intInf;
                    dblIvaFeeInf = reservationTracking.Fee * 0.16 * intInf;
                }
                ViewBag.valueTA = PNRTracking.KIU_TA - dblFeeInf;
                ViewBag.valueTB = PNRTracking.KIU_TB - dblIvaFeeInf;
                ViewBag.value = PNRTracking.KIUPrice - dblFeeInf - dblIvaFeeInf;
                ViewBag.typeFlight = availTracking.flightType;

                string codReservationCookie="";
                bool wasAuthenticated=userAdministrator.instance.user.isAuthenticated;
                try 
	            {
                    //codReservationCookie = Request.Cookies["WasAuthenticated"]["pnr"];
                    wasAuthenticated = Convert.ToBoolean(Request.Cookies["WasAuthenticated"]["value"]);
	            }
	            catch
	            {
                    //cookieWasAuthenticated["pnr"] = reservationTracking.TrackCode.ToString();
                    cookieWasAuthenticated["value"] = wasAuthenticated.ToString();
                    cookieWasAuthenticated.Expires = DateTime.Now.AddHours(1);
                    Response.Cookies.Add(cookieWasAuthenticated);
	            }

                int hoursToExpired = 0;
                //COLOMBIAN CURRENT DATETIME
                DateTime currentTime = DateTime.UtcNow.AddHours(-5);
                DateTime PNRTime = DateTime.ParseExact(PNRTracking.PNRDate.Replace("T"," ").Split('.')[0], "yyyy-MM-dd HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);
                DateTime DepartureTime = DateTime.ParseExact(reservationTracking.DepartureDateTime.Replace("T", " ").Split('.')[0], "yyyy-MM-dd HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);
                PNRTime = PNRTime.AddHours(-5);
                if (DepartureTime.Year == currentTime.Year && DepartureTime.Month == currentTime.Month && DepartureTime.Day == currentTime.Day)
                {
                    hoursToExpired = 3;
                }
                else
                {
                    if (DepartureTime > currentTime)
                    {
                        hoursToExpired = 6;
                    }
                }

                if (!wasAuthenticated)
                {
                    hoursToExpired = 1;
                }

                DateTime expirationTime = PNRTime.AddHours(hoursToExpired);
                TimeSpan elapsedTime = expirationTime - currentTime;
                int hoursLeftToPay = (int)elapsedTime.TotalHours;
                int minutesLeftToPay = (int)elapsedTime.Minutes;

                string message = getStringResource("hoursLeftToPay", "Tiene un plazo para el pago de su vuelo de {0} horas y {1} minutos.", Request.Url.ToString(), Request.UserLanguages);

                message = String.Format(message, hoursLeftToPay.ToString(),minutesLeftToPay.ToString());

                if (hoursLeftToPay == 1)
                    message = message.Replace("horas", "hora");
                if (minutesLeftToPay == 1)
                    message = message.Replace("minutos", "minuto");

                ViewBag.hoursLeftToPay = message;

                if (Request["tstPost"] != null)
                {
                    referencedPayment(PNRTracking.KIU_TOTAL.ToString(), PNRTracking.ReservationCode);
                }
                //USER LOGIN
                if (userAdministrator.instance.user.isAuthenticated)
                {
                    ViewBag.entries = await getTokenList();
                }
                
                    // BANK LIST
                    ViewBag.listaBancos = new List<object>();
                

                // LISTA DE BANCOS
                    try
                    {
                        POCOConfigHelper helper = configurationReader.readHelper("REST.Banks list", configPlatform);

                        string returnStringBank = webPost.action(helper.method,
                                                                        null,
                                                                        helper.server,
                                                                        helper.uri,
                                                                        "application/json",
                                                                        "application/json",
                                                                        paymentSignatureName,
                                                                        paymentSignatureValue);

                        // deserializa
                        ICollection<Bank> bankList = JsonConvert.DeserializeObject<ICollection<Bank>>(returnStringBank);

                        // LISTA DE BANCOS
                        ViewBag.listaBancos = bankList;
                        if (bankList.Count > 0) {
                            ViewBag.bankListHasValues = true;
                        }
                    }
                    catch
                    {
                        ViewBag.bankListHasValues = false;
                        ViewBag.listaBancos = "";
                    }
                //****************************************************************************************************************
                // COOKIE
                //****************************************************************************************************************
                // GENERATES TEMPORARY VERIFICATION COOKIE
                // SAVE FOR FURTHER VALIDATION
                string validationCode = System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString();

                // WRITES COOKIE
                string cookieValue = Encrypt.encrypt(validationCode, encriptSalt);

                // DEFINES COOKIE AND PROFILE EXPIRATION
                TimeSpan expiration = new TimeSpan(0, 5, 0);
                cookieManager.updatesCookie(cookieName, cookieValue, "PAYMENT", expiration);

                //****************************************************************************************************************
                // DEVICE SESSION ID
                //****************************************************************************************************************
                ViewBag.deviceSessionId = (Encrypt.md5(HttpContext.Session.SessionID + DateTime.Now.microTime().ToString()) + "109009").ToUpper();

                string badResponseCode = Request.QueryString["badResponseCode"];
                string trackingCode = Request.QueryString["TrackCode"];
                string xml = Request.QueryString["xml"];
                ViewBag.isOtherResponseCode = false;
                if (!string.IsNullOrEmpty(badResponseCode)) {
                    if (badResponseCode == "emailVerification")
                    {
                        ViewBag.BadResponseCode = badResponseCode;
                        ViewBag.isOtherResponseCode = true;

                        var dummyXml = new { trazabilityCode="" };
                        var deserializedXml = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(xml, dummyXml);

                        string CUS = deserializedXml.trazabilityCode;

                        POCOConfigHelper helper = configurationReader.readHelper("REST.reservation PNRTracking", "AWESAuth");

                        string parametrizedUri = helper.uri;
                        parametrizedUri = parametrizedUri.Replace("{trackingCode}", trackingCode);

                        returnString = webPost.action(helper.method,
                                                                           null,
                                                                           helper.server,
                                                                           parametrizedUri,
                                                                           "application/json",
                                                                           "application/json",
                                                                           ConfigurationManager.AppSettings["Consume.reservationSignatureName"],
                                                                           ConfigurationManager.AppSettings["Consume.reservationSignature"]);

                        var dummyPNR = new { ReservationCode = "" };
                        string PNR = (Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(returnString, dummyPNR)).ReservationCode;

                        string MessageToShow=getStringResource("emailTransactionPending", "Ya hay una transacción Pendiente a su nombre, porfavor vualva a intentar el pago con otros datos.", Request.Url.ToString(), Request.UserLanguages);

                        MessageToShow= string.Format(MessageToShow,PNR,CUS);

                        ViewBag.MessageBadResponse = MessageToShow;
                    }
                    else if (badResponseCode == "PSENotAviable")
                    {
                        ViewBag.BadResponseCode = badResponseCode;
                        ViewBag.isOtherResponseCode = true;
                        ViewBag.MessageBadResponse = getStringResource("PSENotAviable", "Lo sentimos, el método de pago PSE no se encuentra disponible en éste momento, por favor vuelva a intentar más tarde", Request.Url.ToString(), Request.UserLanguages);
                    }
                    else if (badResponseCode=="TCNotAviable")
                    {
                        ViewBag.BadResponseCode = badResponseCode;
                        ViewBag.isOtherResponseCode = true;
                        ViewBag.MessageBadResponse = getStringResource("TCNotAviable", "Lo sentimos, éste método de pago no se encuentra disponible en éste momento, por favor vuelva a intentar más tarde", Request.Url.ToString(), Request.UserLanguages);
                    }
                    else
                    {
                        ViewBag.BadResponseCode = badResponseCode;
                        ViewBag.isOtherResponseCode = true;
                        ViewBag.MessageBadResponse = getStringResource("badCodeBANK_UNREACHABLE", "La entidad financiera no puede ser contactada para iniciar la transacción, por favor seleccione otra o intente más tarde.", Request.Url.ToString(), Request.UserLanguages);
                    }
                }

                return View();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [authorizationConfirmedFilter()]
        private async Task<List<tokenBind>> getTokenList()
        {
            // OBTAINS TOKEN LIST FOR THE USER
            POCOConfigHelper helper = configurationReader.readHelper("REST.User Token list", configPlatform);

            string parametrizedUri = helper.uri.Replace("{userID}", userAdministrator.instance.user.ID.ToString());

            string returnString = await webPost.asyncAction(helper.method,
                                                       null,
                                                       helper.server,
                                                       parametrizedUri,
                                                       "application/json",
                                                       "application/json",
                                                       signatureName,
                                                       signatureValue);

            return JsonConvert.DeserializeObject<List<tokenBind>>(returnString);
        }
    }
}