﻿using AWESAirline.Controllers;
using AWESCommon.contentSupport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace wwweasyflyawes.Controllers
{
    public class termOfUseController : baseController
    {
        // GET: termOfUse
        public ActionResult Index()
        {
            return View();
        }
        public async Task<JsonResult> getTerms() {
            string data = "";
            string pageName = "TermsOfUse";
            try
            {
                var dummy = new { Message = "", data = new Dictionary<string,string>() };
                Dictionary<string, string> result = await getContentByTypeDict(pageName);
                data = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                
                //data = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(dataResult, dummy).data[pageName];
            }
            catch (Exception ex)
            {
                return Json(new {OK="SUCCESS",data="ERROR" + ex.Message},JsonRequestBehavior.AllowGet);    
            }
            return Json(new {OK="SUCCESS",data=data},JsonRequestBehavior.AllowGet);
        }
    }
}