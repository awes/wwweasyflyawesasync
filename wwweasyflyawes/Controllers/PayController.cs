﻿using AWESCommon.Configuration;
using AWESCommon.ConfigurationSupport;
using AWESCommon.Web;
using AWESMVCCommon.Security;
using AWESAirline.Models;
using AWESPaymentModel.BindingObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using AWESCommon.Extensions;
using AWESAirline.Controllers;

namespace AWESAirline.Controllers
{
    public partial class PayController : baseController
    {
        public async Task<ActionResult> Index()
        {
            try
            {
                // GETS RESERVATION INFO
                // id interno de referencia
                string returnString = await PNRInfo();
                PNR PNRTracking = JsonConvert.DeserializeObject<PNR>(returnString);
                ViewBag.Code = PNRTracking.ReservationCode;
                // id interno de referencia
                ViewBag.reference = PNRTracking.ReservationCode;

                // avlor total por pagar incluyendo todos los impuestos y cargos
                ViewBag.valueToPay = PNRTracking.KIU_TOTAL;

                int hoursToExpired = 0;
                //COLOMBIAN CURRENT DATETIME
                DateTime currentTime = DateTime.UtcNow.AddHours(-5);
                DateTime PNRTime = DateTime.ParseExact(PNRTracking.PNRDate, "yyyy-MM-dd HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);

                if (PNRTime.Year == currentTime.Year && PNRTime.Month == currentTime.Month && PNRTime.Day == currentTime.Day)
                {
                    hoursToExpired = 3;
                }
                else {
                    if (PNRTime > currentTime) {
                        hoursToExpired = 6;
                    }
                }
                
                DateTime expirationTime = PNRTime.AddHours(hoursToExpired);
                TimeSpan elapsedTime = expirationTime - currentTime;
                int hoursLeftToPay = (int) elapsedTime.TotalHours;

                string message = getStringResource("hoursLeftToPay", "Tiene un plazo para el pago de su vuelo de {0} horas.", Request.Url.ToString(), Request.UserLanguages);

                message = String.Format(message, hoursLeftToPay.ToString());

                if (hoursLeftToPay == 1)
                    message = message.Replace("horas", "hora");

                ViewBag.hoursLeftToPay = message;
                //****************************************************************************************************************
                // COOKIE
                //****************************************************************************************************************
                // GENERATES TEMPORARY VERIFICATION COOKIE
                // SAVE FOR FURTHER VALIDATION
                string validationCode = System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString() + System.Guid.NewGuid().ToString();

                //// WRITES COOKIE
                //string cookieValue = Encrypt.encrypt(validationCode, encriptSalt);

                //// DEFINES COOKIE AND PROFILE EXPIRATION
                //TimeSpan expiration = new TimeSpan(0, 5, 0);
                //cookieManager.updatesCookie(cookieName, cookieValue, "PAYMENT", expiration);

                //****************************************************************************************************************
                // DEVICE SESSION ID
                //****************************************************************************************************************
                ViewBag.deviceSessionId = (Encrypt.md5(HttpContext.Session.SessionID + DateTime.Now.microTime().ToString()) + "109009").ToUpper();

                return View();
            }
            catch (Exception ex)
            {
                throw;
            }
            
        }
    }
}