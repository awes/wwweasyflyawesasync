﻿using System.Web;
using System.Web.Optimization;

namespace wwweasyflyawes
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Content/js/jQuery/jquery-1.9.1.js",
                "~/Content/js/jQuery/jquery1.11.4-ui.min.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/jquery/all").Include(
                "~/Content/js/jQuery/jquery.blockUI.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/siteScripts").Include(
                "~/Content/js/siteScripts/flights.js",
                "~/Content/js/siteScripts/home.js",
                "~/Content/js/siteScripts/tokens.js",
                "~/Content/js/siteScripts/pax.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/siteScripts/all").Include(
                "~/Content/js/siteScripts/layout.js",
                "~/Content/js/siteScripts/functions.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/vendor").Include(
                "~/Content/js/vendor/foundation.min.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/vendor/all").Include(
                "~/Content/js/vendor/sprintf.min.js",
                "~/Content/js/vendor/modernizr.js",
                "~/Content/js/vendor/enscroll-0.js"
                ));
            bundles.Add(new StyleBundle("~/bundles/themes").Include(
                "~/Content/css/calendar.css",
                "~/Content/css/font-awesome.css",
                "~/Content/css/foundation.css",
                "~/Content/css/foundation.min.css",
                "~/Content/css/tinycarousel.css"
                ));
            bundles.Add(new StyleBundle("~/bundles/themes/site").Include(
                "~/Content/css/styles.css"
                ));
        }
    }
}
