﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESAirline.Models
{
    public class PNR
    {
        public int ID { get; set; }
        public string TrackCode { get; set; }
        public string IP { get; set; }
        public string ReservationCode { get; set; }
        public string PNRDate { get; set; }
        public string XmlCall { get; set; }
        public string XmlReply { get; set; }
        public double KIUPrice { get; set; }
        public double KIU_CO { get; set; }
        public double KIU_YS { get; set; }
        public double KIU_TA { get; set; }
        public double KIU_TB { get; set; }

        public double KIU_TOTAL
        {
            get
            {
                return KIUPrice + KIU_CO + KIU_YS + KIU_TA + KIU_TB;
            }
        }
    }
}
