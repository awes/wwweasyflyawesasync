﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESAirline.Models
{

    public class userTraveler
    {
        public int ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string document { get; set; }

        public virtual int userID { get; set; }

        public string type { get; set; }

    }
}
