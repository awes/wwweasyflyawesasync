﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESAirline.Models
{
    public class PaymentMethod
    {
        public int ID { get; set; }
        public string description { get; set; }
        public string country { get; set; }
    }
}
