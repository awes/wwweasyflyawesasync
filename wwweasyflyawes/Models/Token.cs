﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESAirline.Models
{
    public class tokenBind
    {
        public int ID { get; set; }
        public bool isEnabled { get; set; }
        public string name { get; set; }
        public string notes { get; set; }
        public string token { get; set; }

        public string paymentMethod { get; set; }

        public tokenBind(int ID, bool isEnabled, string name, string notes)
        {
            this.ID = ID;
            this.isEnabled = isEnabled;
            this.name = name;
            this.notes = notes;
        }
    }
}
