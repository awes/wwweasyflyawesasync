﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wwweasyflyawes.Models
{
    public class accumulation
    {
        public int ID { get; set; }
        public int User { get; set; }
        public string TrackCode { get; set; }
        public DateTime buyDate { get; set; }
        public double money { get; set; }
        public int points { get; set; }
        public int state { get; set; }
    }
}