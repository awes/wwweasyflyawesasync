﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESAirline.Models
{
    public class Reservation
    {
        public int ID { get; set; }
        public virtual Guid TrackCode { get; set; }
        public DateTime Date { get; set; }
        public string DepartureFlight { get; set; }
        public string ClassDeparture { get; set; }
        public string ReturnFlight { get; set; }
        public string ClassReturn { get; set; }
        public string DepartureDateTime { get; set; }
        public string ArrivalDateTime { get; set; }
        public string DepartureDateTimeReturn { get; set; }
        public string ArrivalDateTimeReturn { get; set; }
        public int FlightType { get; set; }
        public string PaymentMethod { get; set; }
        public int PaymentType { get; set; }
        public string Franchise { get; set; }
        public string DiscountCampaing { get; set; }
        public int PaymentState { get; set; }
        public DateTime PaymentDate { get; set; }
        public string AuthorizationCode { get; set; }
        public double Price { get; set; }
        public double Tax { get; set; }
        public double Fee { get; set; }

    }
}
