﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wwweasyflyawes.Models
{
    public class PaymentModel
    {
        public int ID { get; set; }
        public string TrackCode { get; set; }
        public DateTime PaymentDate { get; set; }
        public string PaymentMethod { get; set; }
        public string Franchise { get; set; }
        public string PaymentState { get; set; }
        public string AuthorizationCode { get; set; }
        public string OrderID { get; set; }
        public string XmlCall { get; set; }
        public string XmlReply { get; set; }
    }
}