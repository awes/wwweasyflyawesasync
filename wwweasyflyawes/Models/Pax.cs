﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace wwweasyflyawes.Models
{
    public class Pax
    {
        public int ID { get; set; }
        public virtual Guid TrackCode { get; set; }
        public string PaxType { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string DocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public string Rate { get; set; }
        public string RateKIU { get; set; }
        public string MobileNumber { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
    }
}