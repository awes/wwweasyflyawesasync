﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESAirline.Models
{
    public class debitPaymentResponse
    {
        public static debitPaymentResponse fromQueryString(NameValueCollection QueryString)
        {
            debitPaymentResponse newInstance = new debitPaymentResponse();

            newInstance.merchantId = QueryString["merchantId"]; //500238
            newInstance.merchant_name = QueryString["merchant_name"]; //Test%2bPayU%2bTest
            newInstance.merchant_address = QueryString["merchant_address"]; //Av%2b123%2bCalle%2b12
            newInstance.telephone = QueryString["telephone"]; //7512354
            newInstance.merchant_url = QueryString["merchant_url"]; //http%253A%252F%252Fpruebaslapv.xtrweb.com
            newInstance.transactionState = QueryString["transactionState"]; //4
            newInstance.lapTransactionState = QueryString["lapTransactionState"]; //APPROVED
            newInstance.message = QueryString["message"]; //Aprobada
            newInstance.referenceCode = QueryString["referenceCode"]; //ABCFRf20150429
            newInstance.reference_pol = QueryString["reference_pol"]; //6947485
            newInstance.transactionId = QueryString["transactionId"]; //fea605fa-fc4c-4a1d-aaea-4d67f88a097b
            newInstance.description = QueryString["description"]; //PAGO%2bDE%2bLA%2bRESERVA%2bABCFRf20150429
            newInstance.trazabilityCode = QueryString["trazabilityCode"]; //6378962
            newInstance.cus = QueryString["cus"]; //6378962
            newInstance.orderLanguage = QueryString["orderLanguage"]; //es
            newInstance.extra1 = QueryString["extra1"]; //
            newInstance.extra2 = QueryString["extra2"]; //
            newInstance.extra3 = QueryString["extra3"]; //
            newInstance.polTransactionState = QueryString["polTransactionState"]; //4
            newInstance.signature = QueryString["signature"]; //57ce89cc45ce3902a8a02d78127b3a8e
            newInstance.polResponseCode = QueryString["polResponseCode"]; //1
            newInstance.lapResponseCode = QueryString["lapResponseCode"]; //APPROVED
            newInstance.risk = QueryString["risk"]; //.00
            newInstance.polPaymentMethod = QueryString["polPaymentMethod"]; //254
            newInstance.lapPaymentMethod = QueryString["lapPaymentMethod"]; //PSE
            newInstance.polPaymentMethodType = QueryString["polPaymentMethodType"]; //4
            newInstance.lapPaymentMethodType = QueryString["lapPaymentMethodType"]; //PSE
            newInstance.installmentsNumber = QueryString["installmentsNumber"]; //1
            newInstance.TX_VALUE = QueryString["TX_VALUE"]; //234567.00
            newInstance.TX_TAX = QueryString["TX_TAX"]; //32354.00
            newInstance.currency = QueryString["currency"]; //COP
            newInstance.lng = QueryString["lng"]; //es
            newInstance.pseCycle = QueryString["pseCycle"]; //
            newInstance.buyerEmail = QueryString["buyerEmail"]; //email%2540dominio.com.co
            newInstance.pseBank = QueryString["pseBank"]; //
            newInstance.pseReference1 = QueryString["pseReference1"]; //%3a%3a1
            newInstance.pseReference2 = QueryString["pseReference2"]; //CC
            newInstance.pseReference3 = QueryString["pseReference3"]; //87654321
            newInstance.authorizationCode = QueryString["authorizationCode"]; //SUCCESS
            newInstance.TX_ADMINISTRATIVE_FEE = QueryString["TX_ADMINISTRATIVE_FEE"]; //.00
            newInstance.TX_TAX_ADMINISTRATIVE_FEE = QueryString["TX_TAX_ADMINISTRATIVE_FEE"]; //.00
            newInstance.TX_TAX_ADMINISTRATIVE_FEE_RETURN_BASE = QueryString["TX_TAX_ADMINISTRATIVE_FEE_RETURN_BASE"]; //.00
            newInstance.processingDate = QueryString["processingDate"]; //2015-05-03
            return newInstance;
        }

        public string retryLink { get; set; }

        public string endTransactionLink { get; set; }

        public string merchantId { get; set; } //500238
        public string merchant_name { get; set; } //Test%2bPayU%2bTest
        public string merchant_address { get; set; } //Av%2b123%2bCalle%2b12
        public string telephone { get; set; } //7512354
        public string merchant_url { get; set; } //http%253A%252F%252Fpruebaslapv.xtrweb.com
        public string transactionState { get; set; } //4
        public string lapTransactionState { get; set; } //APPROVED

        public string message { get; set; } //Aprobada
        public string referenceCode { get; set; } //ABCFRf20150429
        public string reference_pol { get; set; } //6947485
        public string transactionId { get; set; } //fea605fa-fc4c-4a1d-aaea-4d67f88a097b
        public string description { get; set; } //PAGO%2bDE%2bLA%2bRESERVA%2bABCFRf20150429
        public string trazabilityCode { get; set; } //6378962
        public string cus { get; set; } //6378962
        public string orderLanguage { get; set; } //es
        public string extra1 { get; set; } //
        public string extra2 { get; set; } //
        public string extra3 { get; set; } //
        public string polTransactionState { get; set; } //4
        public string signature { get; set; } //57ce89cc45ce3902a8a02d78127b3a8e
        public string polResponseCode { get; set; } //1
        public string lapResponseCode { get; set; } //APPROVED
        public string lapResponseCodeAsString
        {
            get
            {
                switch (lapResponseCode)
                {

                    case "APPROVED":
                        return "Transacción Aprobada";
                    case "PENDING_TRANSACTION_CONFIRMATION":
                        return "Transacción Pendiente, por favor revisar si el débito fue realizado en el banco";
                    case "PAYMENT_NETWORK_REJECTED":
                        return "Transacción Rechazada";
                    case "ENTITY_DECLINED":
                        return "Transacción Fallida";
                    default:
                        return "Transacción Pendiente, por favor revisar si el débito fue realizado en el banco";
                }
            } //APPROVED
        }
        public string risk { get; set; } //.00
        public string polPaymentMethod { get; set; } //254
        public string lapPaymentMethod { get; set; } //PSE
        public string polPaymentMethodType { get; set; } //4
        public string lapPaymentMethodType { get; set; } //PSE
        public string installmentsNumber { get; set; } //1
        public string TX_VALUE { get; set; } //234567.00
        public string TX_TAX { get; set; } //32354.00
        public string currency { get; set; } //COP
        public string lng { get; set; } //es
        public string pseCycle { get; set; } //
        public string buyerEmail { get; set; } //email%2540dominio.com.co
        public string pseBank { get; set; } //
        public string pseReference1 { get; set; } //%3a%3a1
        public string pseReference2 { get; set; } //CC
        public string pseReference3 { get; set; } //87654321
        public string authorizationCode { get; set; } //SUCCESS
        public string TX_ADMINISTRATIVE_FEE { get; set; } //.00
        public string TX_TAX_ADMINISTRATIVE_FEE { get; set; } //.00
        public string TX_TAX_ADMINISTRATIVE_FEE_RETURN_BASE { get; set; } //.00
        public string processingDate { get; set; } //2015-05-03

    }
}
