﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESAirline.Models
{
    public class avail
    {
        public int ID { get; set; }
        public string TrackCode { get; set; }
        public int IdOrigin { get; set; }
        public int IdDestination { get; set; }
        public string IATAOrigin { get; set; }
        public string IATADestination { get; set; }
        public DateTime Date { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public int Adult { get; set; }
        public int Child { get; set; }
        public int Infant { get; set; }
        public int flightType { get; set; }
        public string IP { get; set; }
        public string DiscountCampaing { get; set; }
        public string XmlCall { get; set; }
        public string XmlReply { get; set; }
    }
}
