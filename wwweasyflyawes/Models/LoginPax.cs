﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace wwweasyflyawes.Models
{
    public class LoginPax
    {
        public int ID { get; set; }
        public int userID { get; set; }
        public string PaxType { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string DocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public int paxLoginType { get; set; }
    }
}