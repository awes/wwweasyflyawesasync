﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESAirline.Models
{
    public class payment
    {
        public string bank { get; set; }
        public string bankName { get; set; }
        public string personType { get; set; }
        public string documentType { get; set; }
        public string documentNumber { get; set; }
        public string fullName { get; set; }
        public string reference { get; set; }
        public string value { get; set; }
        public string phone { get; set; }
        public string email { get; set; }

        // cARD SELECTION -- OPTIONAL FOR DEBIT PAYMENT
        public string paymentMethod { get; set; }

        public string deviceSessionId { get; set; }


        // PAYER ADDRESS
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string coutryCode { get; set; }
        public string postalCode { get; set; }

        // CREDIT CARD PAYMENT
        public string cardNumber { get; set; }
        public string securityCode { get; set; }
        public int expirationMonth { get; set; }
        public int expirationYear { get; set; }
        public int installements { get; set; }
        public string nameOnCard { get; set; }

        public string token { get; set; }

        //CASH PAYMENT
        public string cashMethod { get; set; }
    }
}
